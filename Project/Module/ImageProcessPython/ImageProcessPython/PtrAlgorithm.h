#pragma once

#include "afxwin.h"
#include <fstream>

#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementStruct.h"

using namespace std;

#ifdef _DEBUG
#define _DEBUG_WAS_DEFINED 1
#undef _DEBUG
#endif

#include <Python.h>
#define NPY_NO_DEPRECATED_API NPY_1_7_API_VERSION
#include <numpy\arrayobject.h>
#include <numpy\npy_3kcompat.h>

#ifdef _DEBUG_WAS_DEFINED
#define _DEBUG
#endif

#define CONFIG_FILE_FULLPATH					_T("C:\\EUVPTR\\Config\\ESOL_Config.ini")					// Config. 파일 경로
#define CONFIG_PATH								_T("C:\\EUVPTR\\Config")									// Config. 디렉토리 경로

class CPtrAlgorithm
{
public:
	CPtrAlgorithm();
	~CPtrAlgorithm();

protected:
	PyObject* m_pyModule_PythonAlgorithm;
	PyObject* m_pyFunc_ReferenceBsTableSearch;

	PyObject* m_pyModule_interpolation_try;
	PyObject* m_pyFunc_ReconstructImage;

public:
	BOOL PythonInitailize();
	BOOL PythonClose();
	BOOL RunPythonModule(string fileName, PyObject*& pyModule);
	BOOL RunPythonFunction(string functionName, PyObject*& pyModule, PyObject*& pyFunc);
	BOOL RunPythonFunctionCheck(PyObject*& pyFunc);
	BOOL _PyUnicode_FromString(const char* input, PyObject*& output);
	BOOL _PyImport_Import(PyObject*& input, PyObject*& output);
	BOOL _PyObject_GetAttrString(PyObject*& object, const char* functionName, PyObject*& output);

public:
	BOOL PyArray_Parameter_DoubleArray(PyObject*& args, unsigned int index, int nd, npy_intp* dims, NPY_TYPES type, double* value);
	double* PyArray_Parameter_DoubleArray_Ret(PyObject*& args, unsigned int index);

private:	
	//측정 Average
	double m_D1avg, m_D2avg, m_D3avg;

	double m_D1std, m_D2std, m_D3std;

	double m_numOfshot;

	//Without pellice, BG 제거후
	double m_D1wo, m_D2wo, m_D3wo;

	double m_D1wo_nor, m_D2wo_nor;

	//With pellice, BG 제거후
	double m_D1p, m_D2p, m_D3p;

	double m_Transmittance;
	double m_Reflectance;

	double m_D1p_nor;
	double m_D2p_nor;

	//Back Ground 측정, BG 제거후
	double m_D1bg, m_D2bg, m_D3bg;

	double m_D2toD1GainRatio; // D2/D1
	double m_D3toD1GainRatio; // D3/D1

	//BS 관련
	int m_NumBsData =0;
	double *m_BsWaveLengthTable = NULL; // wave length nm	
	double *m_BsReflectanceTable = NULL; //reflectance 0~1.0
	double *m_BsTransmittanceTable = NULL; // transmittance  0~1.0
	
	double m_WaveLengthBs;
	double m_TransmittnaceBs;
	double m_ReflectanceBs;

	double m_D1bs, m_D2bs, m_D3bs;

	CString m_FileNameBs;
	CString m_NameBs;

public:
	int LoadBsData();
	int GetBsFileName();
	int ReadBsFile(CString bs_Name);
	void CalculateTransmittanceAndReflectance();
	int Statistics(double* data, unsigned int n, double* mean, double* std);
	void FindWaveLengthAndReflectanceOfBs(double *waveLengthTable, double *transmittanceTable, double *reflectanceTable, int NumOfData, double waveLength_min, double waveLength_max, double _transmittnaceTarget, double*_waveLengthResult, double* _reflectanceResult);
	int CalculateBsCalibration(double *waveLengthTable, double *reflectanceTable, double *transmittanceTable, int NumOfData, double d1_bs, double d2_bs, double d3_bs, double d1_wo, double d2_wo, double d3_wo);
	int GetBsData(double* WaveLength, double* Transmittance, double* Reflectance, double * D2toD1GainRatio, double* D3toD1GainRatio);
	int MeasureBackGround(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
	int MeasureBs(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
	int MeasureWithoutPellicle(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
	int MeasurePellicle(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
	int MeasurePellicleSum(unsigned int* count, double* d1, double* d2, double* d3);
	int GetPellicleData(double* Transmittance, double* Reflectance);
	int MPacketStatistics(unsigned int count, double* pData, double* pData_avg, double* pData_std);
	int MPacketStatistics_Seq(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
	int ReconstructImage(double* Image_raw, double* x_raw, double* y_raw, double* Image_regrid, double* x_regrid, double* y_regrid, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV, int tempExtraPixel);
};

