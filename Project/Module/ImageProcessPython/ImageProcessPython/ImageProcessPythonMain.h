#pragma once

#include "PtrAlgorithm.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementStruct.h"

#ifndef __CALCULATOR__
#define __CALCULATOR__

#ifdef __cplusplus
extern "C"
{
#endif
	
#ifdef _IMAGEPROCESSPYTHON
#define __CAL_DLL __declspec(dllexport)
#else
#define __CAL_DLL __declspec(dllimport)
#endif

namespace PythonAlgorithm
{
__CAL_DLL __inline unsigned int MeasureBackGround(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
__CAL_DLL __inline unsigned int MeasureWithoutPellicle(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
__CAL_DLL __inline unsigned int MeasureBs(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
__CAL_DLL __inline unsigned int MeasurePellicle(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
__CAL_DLL __inline unsigned int GetBsData(double* WaveLength, double* Transmittance, double* Reflectance, double* D2toD1GainRatio, double* D3toD1GainRatio);
__CAL_DLL __inline unsigned int GetPellicleData(double* Transmittance, double* Reflectance);
__CAL_DLL __inline unsigned int LoadBsData();
__CAL_DLL __inline unsigned int MPacketStatistics(unsigned int count, double* pData, double* pData_avg, double* pData_std);
__CAL_DLL __inline unsigned int MPacketStatistics_Seq(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet);
__CAL_DLL __inline unsigned int MeasurePellicleSum(unsigned int* count, double* d1, double* d2, double* d3);
__CAL_DLL __inline unsigned int ReconstructImage(double* Image_raw, double* x_raw, double* y_raw, double* Image_regrid, double* x_regrid, double* y_regrid, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV, int tempExtraPixel);
}

#ifdef __cplusplus
}
#endif

#endif
