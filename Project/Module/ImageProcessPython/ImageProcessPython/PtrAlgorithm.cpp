#include "stdafx.h"
#include "PtrAlgorithm.h"
#include <vector>

void TraceRelease(LPCTSTR lpszFormat, ...)
{
	CString sResult;

	va_list vlMarker = NULL;
	va_start(vlMarker, lpszFormat);

	sResult.FormatV(lpszFormat, vlMarker);

	va_end(vlMarker);
	OutputDebugString(sResult);
}

template <class Function>
__int64 time_call(Function&& f)
{
	__int64 begin = GetTickCount();
	f();
	return GetTickCount() - begin;
}

CPtrAlgorithm::CPtrAlgorithm()
{	
	PythonInitailize();
}

CPtrAlgorithm::~CPtrAlgorithm()
{
	m_NumBsData = 0;

	if (m_BsWaveLengthTable		!= NULL) { delete[] m_BsWaveLengthTable; }
	if (m_BsReflectanceTable	!= NULL) { delete[] m_BsReflectanceTable; }
	if (m_BsTransmittanceTable	!= NULL) { delete[] m_BsTransmittanceTable; }

	PythonClose();
}

BOOL CPtrAlgorithm::PythonInitailize()
{
	Py_Initialize();

	// Load Python Module
	RunPythonModule("PythonAlgorithm", m_pyModule_PythonAlgorithm);

	// Load Python Module Function
	RunPythonFunction("ReferenceBsTableSearch", m_pyModule_PythonAlgorithm, m_pyFunc_ReferenceBsTableSearch);
	RunPythonFunction("reconstruct_image", m_pyModule_PythonAlgorithm, m_pyFunc_ReconstructImage);

//	// Load Python Module
//	RunPythonModule("interpolation_try", m_pyModule_interpolation_try);
//
//	// Load Python Module Function
//	RunPythonFunction("reconstruct_image", m_pyModule_interpolation_try, m_pyFunc_ReconstructImage);

	import_array();

	return TRUE;
}

BOOL CPtrAlgorithm::PythonClose()
{
	if (m_pyFunc_ReferenceBsTableSearch != nullptr)
	{
		Py_XDECREF(m_pyFunc_ReferenceBsTableSearch);
		m_pyFunc_ReferenceBsTableSearch = nullptr;
	}

	if (m_pyModule_PythonAlgorithm != nullptr)
	{
		Py_DECREF(m_pyModule_PythonAlgorithm);
		m_pyModule_PythonAlgorithm = nullptr;
	}

	if (m_pyFunc_ReconstructImage != nullptr)
	{
		Py_XDECREF(m_pyFunc_ReconstructImage);
		m_pyFunc_ReconstructImage = nullptr;
	}

	if (m_pyModule_interpolation_try != nullptr)
	{
		Py_DECREF(m_pyModule_interpolation_try);
		m_pyModule_interpolation_try = nullptr;
	}

	if (Py_IsInitialized()) { Py_Finalize(); }

	return TRUE;
}

BOOL CPtrAlgorithm::RunPythonModule(string fileName, PyObject*& pyModule)
{
	PyObject* pName;

	if (!_PyUnicode_FromString(fileName.c_str(), pName))
	{
		printf("No File Name");
		return FALSE;
	}
	if (!_PyImport_Import(pName, pyModule))
	{
		printf("No Module");
		Py_DECREF(pyModule);
		pyModule = NULL;
		return FALSE;
	}

	Py_DECREF(pName);
	pName = NULL;

	return TRUE;
}

BOOL CPtrAlgorithm::RunPythonFunction(string functionName, PyObject*& pyModule, PyObject*& pyFunc)
{
	if (!_PyObject_GetAttrString(pyModule, functionName.c_str(), pyFunc))
	{
		Py_XDECREF(pyFunc);
		pyFunc = NULL;
		Py_DECREF(pyModule);
		pyModule = NULL;
		return FALSE;
	}

	return TRUE;
}

BOOL CPtrAlgorithm::RunPythonFunctionCheck(PyObject*& pyFunc)
{
	if (pyFunc && PyCallable_Check(pyFunc)) { return TRUE; }

	return FALSE;
}

BOOL CPtrAlgorithm::_PyUnicode_FromString(const char* input, PyObject*& output)
{
	output = PyUnicode_FromString(input);
	return output != NULL;
}

BOOL CPtrAlgorithm::_PyImport_Import(PyObject*& input, PyObject*& output)
{
	output = PyImport_Import(input);
	return output != NULL;
}

BOOL CPtrAlgorithm::_PyObject_GetAttrString(PyObject*& object, const char* functionName, PyObject*& output)
{
	output = PyObject_GetAttrString(object, functionName);
	return output != NULL;
}

BOOL CPtrAlgorithm::PyArray_Parameter_DoubleArray(PyObject*& args, unsigned int index, int nd, npy_intp* dims, NPY_TYPES type, double* value)
{
	PyArrayObject* object = reinterpret_cast<PyArrayObject*>(PyArray_SimpleNewFromData(nd, dims, type, reinterpret_cast<void*>(value)));

	PyTuple_SetItem(args, index, reinterpret_cast<PyObject*>(object));

	return TRUE;
}

double* CPtrAlgorithm::PyArray_Parameter_DoubleArray_Ret(PyObject*& args, unsigned int index)
{
	return reinterpret_cast<double*>(PyArray_DATA(reinterpret_cast<PyArrayObject*>(PyTuple_GetItem(args, index))));
}

int CPtrAlgorithm::Statistics(double* data, unsigned int n, double* mean, double* std)
{
	double sum = 0;
	for (unsigned int i = 0; i < n; i++)
	{
		sum = sum + data[i];
	}
	*mean = sum / (double)n;

	double var_sum = 0;
	for (unsigned int i = 0; i < n; i++)
	{
		var_sum = var_sum + (data[i] - *mean) * (data[i] - *mean);
	}

	double var = var_sum / (double)n;
	*std = sqrt(var);

	return 0;
}


int CPtrAlgorithm::MeasureBackGround(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	int ret = 0;

	Statistics(d1, count, &m_D1avg, &m_D1std);
	Statistics(d2, count, &m_D2avg, &m_D2std);
	Statistics(d3, count, &m_D3avg, &m_D3std);

	m_D1bg = m_D1avg;
	m_D2bg = m_D2avg;
	m_D3bg = m_D3avg;

	sRet->dD1IntensityMeasure = m_D1avg;
	sRet->dD2IntensityMeasure = m_D2avg;
	sRet->dD3IntensityMeasure = m_D3avg;

	sRet->dD1StandardDeviation = m_D1std;
	sRet->dD2StandardDeviation = m_D2std;
	sRet->dD3StandardDeviation = m_D3std;

	return ret;
}

int CPtrAlgorithm::MeasureBs(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	int ret = 0;

	Statistics(d1, count, &m_D1avg, &m_D1std);
	Statistics(d2, count, &m_D2avg, &m_D2std);
	Statistics(d3, count, &m_D3avg, &m_D3std);

	m_D1bs = m_D1avg - m_D1bg;
	m_D2bs = m_D2avg - m_D2bg;
	m_D3bs = m_D3avg - m_D3bg;

	CalculateBsCalibration(m_BsWaveLengthTable, m_BsReflectanceTable, m_BsTransmittanceTable, m_NumBsData,
		m_D1bs, m_D2bs, m_D3bs, m_D1wo, m_D2wo, m_D3wo);

	sRet->dD1IntensityMeasure = m_D1avg;
	sRet->dD2IntensityMeasure = m_D2avg;
	sRet->dD3IntensityMeasure = m_D3avg;

	sRet->dD1StandardDeviation = m_D1std;
	sRet->dD2StandardDeviation = m_D2std;
	sRet->dD3StandardDeviation = m_D3std;

	sRet->dD1BackgroundNoise = m_D1bg;
	sRet->dD2BackgroundNoise = m_D2bg;
	sRet->dD3BackgroundNoise = m_D3bg;

	sRet->dD1Intensity = m_D1bs;
	sRet->dD2Intensity = m_D2bs;
	sRet->dD3Intensity = m_D3bs;

	return ret;
}

int CPtrAlgorithm::MeasureWithoutPellicle(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	int ret = 0;

	Statistics(d1, count, &m_D1avg, &m_D1std);
	Statistics(d2, count, &m_D2avg, &m_D2std);
	Statistics(d3, count, &m_D3avg, &m_D3std);

	m_D1wo = m_D1avg - m_D1bg;
	m_D2wo = m_D2avg - m_D2bg;
	m_D3wo = m_D3avg - m_D3bg;

	sRet->dD1IntensityMeasure = m_D1avg;
	sRet->dD2IntensityMeasure = m_D2avg;
	sRet->dD3IntensityMeasure = m_D3avg;

	sRet->dD1StandardDeviation = m_D1std;
	sRet->dD2StandardDeviation = m_D2std;
	sRet->dD3StandardDeviation = m_D3std;

	sRet->dD1BackgroundNoise = m_D1bg;
	sRet->dD2BackgroundNoise = m_D2bg;
	sRet->dD3BackgroundNoise = m_D3bg;

	sRet->dD1Intensity = m_D1wo;
	sRet->dD2Intensity = m_D2wo;
	sRet->dD3Intensity = m_D3wo;

	return ret;
}

int CPtrAlgorithm::MeasurePellicle(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	int ret = 0;

	Statistics(d1, count, &m_D1avg, &m_D1std);
	Statistics(d2, count, &m_D2avg, &m_D2std);
	Statistics(d3, count, &m_D3avg, &m_D3std);

	m_D1p = m_D1avg - m_D1bg;
	m_D2p = m_D2avg - m_D2bg;
	m_D3p = m_D3avg - m_D3bg;

	CalculateTransmittanceAndReflectance();

	if (sRet != nullptr)
	{
		sRet->dD1IntensityMeasure = m_D1avg;
		sRet->dD2IntensityMeasure = m_D2avg;
		sRet->dD3IntensityMeasure = m_D3avg;

		sRet->dD1StandardDeviation = m_D1std;
		sRet->dD2StandardDeviation = m_D2std;
		sRet->dD3StandardDeviation = m_D3std;

		sRet->dD1BackgroundNoise = m_D1bg;
		sRet->dD2BackgroundNoise = m_D2bg;
		sRet->dD3BackgroundNoise = m_D3bg;

		sRet->dD1Intensity = m_D1p;
		sRet->dD2Intensity = m_D2p;
		sRet->dD3Intensity = m_D3p;

		sRet->dWaveLength = m_WaveLengthBs;
	}

	return ret;
}

int CPtrAlgorithm::MeasurePellicleSum(unsigned int* count, double* d1, double* d2, double* d3)
{
	int ret = 0;

	m_D1avg = *d1 / (double)*count;
	m_D2avg = *d2 / (double)*count;
	m_D3avg = *d3 / (double)*count;

	m_D1p = m_D1avg - m_D1bg;
	m_D2p = m_D2avg - m_D2bg;
	m_D3p = m_D3avg - m_D3bg;

	CalculateTransmittanceAndReflectance();

	return ret;
}

void CPtrAlgorithm::CalculateTransmittanceAndReflectance()
{
	double Tp, Rp;

	Tp = m_D1p / m_D3p * m_D3wo / m_D1wo;
	Rp = 1 / m_D2toD1GainRatio * (m_D2p / m_D1p * Tp - m_D2wo / m_D1wo * Tp * Tp);

	if (isnan(Tp))  { Tp = 0;				}
	else			{ m_Transmittance = Tp; }

	if (isnan(Rp))  { Rp = 0;				}
	else			{ m_Reflectance = Rp;	}
}

int CPtrAlgorithm::GetBsFileName()
{
	int ret = 0;

	CString strAppName, strKeyName;
	char chGetString[128];

	strAppName.Format(_T("BS_CALIBRATION"));

	strKeyName.Format(_T("DataFile"));
	GetPrivateProfileString(strAppName, strKeyName, _T(""), chGetString, 128, CONFIG_FILE_FULLPATH);

	m_NameBs.Format("%s", chGetString);

	return ret;
}

int CPtrAlgorithm::ReadBsFile(CString bs_Name)
{
	int ret = 0;

	CString FileName;

	FileName.Format("%s\\%s.txt", CONFIG_PATH, bs_Name);

	FILE* pFile = fopen(FileName, "r");

	int lineCount = 0;

	vector<double> vec_lamda;
	vector<double> vec_reflectance;
	vector<double> vec_transmittance;

	if (pFile == NULL)
	{
		ret = 1;
		goto END;
	}

	char lamda_char[20];
	char nm_char[20];
	char trasmittance_char[20];
	char reflectance_char[20];
	char space[20];

	double lamda = 0;
	double transmittance = 0;
	double reflectance = 0;

	if (fscanf(pFile, "%s %s %s %s", lamda_char, nm_char, trasmittance_char, reflectance_char) != 4)
	{
		ret = 2;
		goto END;
	};

	while (fscanf(pFile, "%lf %lf %lf", &lamda, &reflectance, &transmittance) == 3)
	{
		lineCount++;
		vec_lamda.push_back(lamda);
		vec_reflectance.push_back(reflectance);
		vec_transmittance.push_back(transmittance);
	}

	m_NumBsData = lineCount;

	if (m_NumBsData > 0)
	{
		if (m_BsWaveLengthTable != NULL) { delete[] m_BsWaveLengthTable; }
		if (m_BsReflectanceTable != NULL) { delete[] m_BsReflectanceTable; }
		if (m_BsTransmittanceTable != NULL) { delete[] m_BsTransmittanceTable; }

		m_BsWaveLengthTable = new double[m_NumBsData];
		m_BsReflectanceTable = new double[m_NumBsData];
		m_BsTransmittanceTable = new double[m_NumBsData];

		std::copy(vec_lamda.begin(), vec_lamda.end(), m_BsWaveLengthTable);
		std::copy(vec_reflectance.begin(), vec_reflectance.end(), m_BsReflectanceTable);
		std::copy(vec_transmittance.begin(), vec_transmittance.end(), m_BsTransmittanceTable);
	}

END:
	if (pFile != NULL)
	{
		fclose(pFile);
	}

	return ret;
}

void CPtrAlgorithm::FindWaveLengthAndReflectanceOfBs(double *waveLengthTable, double *transmittanceTable, double *reflectanceTable, int NumOfData, double waveLength_min, double waveLength_max, double _transmittnaceTarget, double*_waveLengthResult, double* _reflectanceResult)
{
	if (RunPythonFunctionCheck(m_pyFunc_ReferenceBsTableSearch))
	{
		try
		{
			PyObject* pArgs = PyTuple_New(6);

			npy_intp input_dims[1];

			input_dims[0] = NumOfData;
			PyArray_Parameter_DoubleArray(pArgs, 0, 1, input_dims, NPY_DOUBLE, waveLengthTable);
			PyArray_Parameter_DoubleArray(pArgs, 1, 1, input_dims, NPY_DOUBLE, transmittanceTable);
			PyArray_Parameter_DoubleArray(pArgs, 2, 1, input_dims, NPY_DOUBLE, reflectanceTable);

			input_dims[0] = 1;
			PyArray_Parameter_DoubleArray(pArgs, 3, 1, input_dims, NPY_DOUBLE, &waveLength_min);
			PyArray_Parameter_DoubleArray(pArgs, 4, 1, input_dims, NPY_DOUBLE, &waveLength_max);
			PyArray_Parameter_DoubleArray(pArgs, 5, 1, input_dims, NPY_DOUBLE, &_transmittnaceTarget);

			PyObject* pArgsRet = PyObject_CallObject(m_pyFunc_ReferenceBsTableSearch, pArgs);

			if (m_pyFunc_ReferenceBsTableSearch != NULL && pArgs != NULL && pArgsRet != NULL)
			{
				double* output0 = PyArray_Parameter_DoubleArray_Ret(pArgsRet, 0);
				double* output1 = PyArray_Parameter_DoubleArray_Ret(pArgsRet, 1);

				*_waveLengthResult = output0[0];
				*_reflectanceResult = output1[0];
			}
			else
			{
				*_waveLengthResult = -13.0;
				*_reflectanceResult = -0.5;
			}

			Py_DECREF(pArgs);
			Py_DECREF(pArgsRet);
		}
		catch (...)
		{
			throw;
		}
	}
}

int CPtrAlgorithm::ReconstructImage(double* Image_raw, double* x_raw, double* y_raw, double* Image_regrid, double* x_regrid, double* y_regrid, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV, int tempExtraPixel)
{
	int ret = 0;

	if (RunPythonFunctionCheck(m_pyFunc_ReconstructImage))
	{
		try
		{
			double x_regrid_step = (FOV / reconstructed_size_x);
			double y_regrid_step = (FOV / reconstructed_size_y);

			for (int n = 0; n < reconstructed_size_y; n++)
			{
				for (int m = 0; m < reconstructed_size_x; m++)
				{
					x_regrid[(n * reconstructed_size_x) + m] = m * x_regrid_step + x_start;
					y_regrid[(n * reconstructed_size_x) + m] = n * y_regrid_step + y_start;
				}
			}

			npy_intp dims_input[1]{ raw_size_x * raw_size_y * num_scans + tempExtraPixel };
			npy_intp dims_output[1]{ reconstructed_size_x * reconstructed_size_y };
			npy_intp dims_scalar[1]{ 1 };

			double* num_scans_array;
			double* python_output;

			num_scans_array = new double[1];
			num_scans_array[0] = double(num_scans);

			PyObject* pArgs = PyTuple_New(6);

			PyArray_Parameter_DoubleArray(pArgs, 0, 1, dims_input	, NPY_DOUBLE, x_raw);
			PyArray_Parameter_DoubleArray(pArgs, 1, 1, dims_input	, NPY_DOUBLE, y_raw);
			PyArray_Parameter_DoubleArray(pArgs, 2, 1, dims_input	, NPY_DOUBLE, Image_raw);
			PyArray_Parameter_DoubleArray(pArgs, 3, 1, dims_output	, NPY_DOUBLE, x_regrid);
			PyArray_Parameter_DoubleArray(pArgs, 4, 1, dims_output	, NPY_DOUBLE, y_regrid);
			PyArray_Parameter_DoubleArray(pArgs, 5, 1, dims_scalar	, NPY_DOUBLE, num_scans_array);

			PyObject* pArgsRet = PyObject_CallObject(m_pyFunc_ReconstructImage, pArgs);

			if (m_pyFunc_ReconstructImage != NULL && pArgs != NULL && pArgsRet != NULL)
			{
				PyArrayObject* interpolated_image = reinterpret_cast<PyArrayObject*>(pArgsRet);

				if (interpolated_image != NULL)
				{
					if (PyErr_Occurred())
						PyErr_Print();

					python_output = reinterpret_cast<double*>(PyArray_DATA(interpolated_image));
					memcpy(Image_regrid, python_output, sizeof(double) * reconstructed_size_x * reconstructed_size_y);
				}
			}

			delete[] num_scans_array;

			if (pArgs != NULL) { Py_DECREF(pArgs); }
			if (pArgsRet != NULL) { Py_DECREF(pArgsRet); }
		}
		catch (...)
		{
			throw;
		}
	}
}

int CPtrAlgorithm::CalculateBsCalibration(double *waveLengthTable, double *reflectanceTable, double *transmittanceTable, int NumOfData, double d1_bs, double d2_bs, double d3_bs, double d1_wo, double d2_wo, double d3_wo)
{
	int ret = 0;
	//MeasureBackGround(), MeasureWithoutPellicle(), MeasureRbs() 실행되야함		
	//double Trbs, Rrbs, Lamda_nm, D2toD1GainRatio;

	double waveLength_min = 13.0;
	double waveLength_max = 14.0;

	double T = 0, R = 0, Lamda = 0;

	T = d1_bs / d3_bs * d3_wo / d1_wo;

	FindWaveLengthAndReflectanceOfBs(waveLengthTable, transmittanceTable, reflectanceTable, NumOfData, waveLength_min, waveLength_max, T, &Lamda, &R);

	//Updata Result
	m_D2toD1GainRatio = (d2_bs / d1_bs * T - d2_wo / d1_wo * T*T) / R;
	m_D3toD1GainRatio = d3_wo / d1_wo;

	m_WaveLengthBs = Lamda;
	m_TransmittnaceBs = T;
	m_ReflectanceBs = R;

	return ret;
}

int CPtrAlgorithm::LoadBsData()
{
	int ret = 0;

	GetBsFileName();
	ReadBsFile(m_NameBs);

	return ret;
}

int CPtrAlgorithm::GetBsData(double* WaveLength, double* Transmittance, double* Reflectance, double * D2toD1GainRatio, double* D3toD1GainRatio)
{
	int ret = 0;

	*WaveLength = m_WaveLengthBs;
	*Reflectance = m_ReflectanceBs;
	*Transmittance = m_TransmittnaceBs;

	*D2toD1GainRatio = m_D2toD1GainRatio;
	*D3toD1GainRatio = m_D3toD1GainRatio;


	return ret;
}

int CPtrAlgorithm::GetPellicleData(double* Transmittance, double* Reflectance)
{
	int ret = 0;

	*Transmittance = m_Transmittance;
	*Reflectance = m_Reflectance;

	return ret;
}

int CPtrAlgorithm::MPacketStatistics(unsigned int count, double* pData, double* pData_avg, double* pData_std)
{
	int ret = 0;
	Statistics(pData, count, pData_avg, pData_std);
	return ret;
}

int CPtrAlgorithm::MPacketStatistics_Seq(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	int ret = 0;

	Statistics(d1, count, &m_D1avg, &m_D1std);
	Statistics(d2, count, &m_D2avg, &m_D2std);
	Statistics(d3, count, &m_D3avg, &m_D3std);

	sRet->dD1IntensityMeasure = m_D1avg;
	sRet->dD2IntensityMeasure = m_D2avg;
	sRet->dD3IntensityMeasure = m_D3avg;

	sRet->dD1StandardDeviation = m_D1std;
	sRet->dD2StandardDeviation = m_D2std;
	sRet->dD3StandardDeviation = m_D3std;

	return ret;
}