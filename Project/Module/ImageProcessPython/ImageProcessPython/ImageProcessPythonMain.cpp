// SixthRxProjectMain.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ImageProcessPythonMain.h"

static CPtrAlgorithm g_cPtrAlgorithm;

namespace PythonAlgorithm
{

__CAL_DLL __inline unsigned int MeasureBackGround(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	return g_cPtrAlgorithm.MeasureBackGround(count, d1, d2, d3, sRet);
} 

__CAL_DLL __inline unsigned int MeasureWithoutPellicle(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	return g_cPtrAlgorithm.MeasureWithoutPellicle(count, d1, d2, d3, sRet);
} 

__CAL_DLL __inline unsigned int MeasureBs(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	return g_cPtrAlgorithm.MeasureBs(count, d1, d2, d3, sRet);
}

__CAL_DLL __inline unsigned int MeasurePellicle(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	return g_cPtrAlgorithm.MeasurePellicle(count, d1, d2, d3, sRet);
}

__CAL_DLL __inline unsigned int GetBsData(double* WaveLength, double* Transmittance, double* Reflectance, double* D2toD1GainRatio, double* D3toD1GainRatio)
{
	return g_cPtrAlgorithm.GetBsData(WaveLength, Transmittance, Reflectance, D2toD1GainRatio, D3toD1GainRatio);
}

__CAL_DLL __inline unsigned int GetPellicleData(double* Transmittance, double* Reflectance)
{
	return g_cPtrAlgorithm.GetPellicleData(Transmittance, Reflectance);
}

__CAL_DLL __inline unsigned int LoadBsData()
{
	return g_cPtrAlgorithm.LoadBsData();
}

__CAL_DLL __inline unsigned int MPacketStatistics(unsigned int count, double* pData, double* pData_avg, double* pData_std)
{
	return g_cPtrAlgorithm.MPacketStatistics(count, pData, pData_avg, pData_std);
}

__CAL_DLL __inline unsigned int MPacketStatistics_Seq(unsigned int count, double* d1, double* d2, double* d3, MeasureDataReturn* sRet)
{
	return g_cPtrAlgorithm.MPacketStatistics_Seq(count, d1, d2, d3, sRet);
}

__CAL_DLL __inline unsigned int MeasurePellicleSum(unsigned int* count, double* d1, double* d2, double* d3)
{
	return g_cPtrAlgorithm.MeasurePellicleSum(count, d1, d2, d3);
}

__CAL_DLL __inline unsigned int ReconstructImage(double* Image_raw, double* x_raw, double* y_raw, double* Image_regrid, double* x_regrid, double* y_regrid, int raw_size_x, int raw_size_y, int reconstructed_size_x, int reconstructed_size_y, int num_scans, double x_start, double y_start, double FOV, int tempExtraPixel)
{
	return g_cPtrAlgorithm.ReconstructImage(Image_raw, x_raw, y_raw, Image_regrid, x_regrid, y_regrid, raw_size_x, raw_size_y, reconstructed_size_x, reconstructed_size_y, num_scans, x_start, y_start, FOV, tempExtraPixel);
}

}