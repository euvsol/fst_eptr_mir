#include "stdafx.h"
#include "TcpClientSocket.h"
#include "TcpListenSocket.h"


CTcpClientSocket::CTcpClientSocket(void* pFramework, void* pParents, void* pListen, unsigned int nChannel, CString strIP, unsigned int nPort)
{
	m_pFramework				= pFramework;
	m_pParents					= pParents;
	m_pListen					= pListen;
	m_nChannel					= nChannel;

	m_nChannel					= nChannel;
	m_strIP						= strIP;
	m_nPort						= nPort;

	m_hSocket					= NULL;
	m_pServerProtocol			= nullptr;
	m_pServerSendData			= nullptr;
	m_byPacketBuffer			= nullptr;
	m_byPacketBufferRing		= nullptr;
	m_nPacketBufferSize			= 0;
	m_nBufferIndex				= 0;
	m_nRingBufferIndex			= 0;
	m_nRingBufferReceiveIndex	= 0;

	m_pServerProtocol = new CTcpServerProtocol(m_pFramework, m_pParents);
	m_pServerSendData = new CTcpServerSendData(m_pFramework, m_pParents, m_pListen);
}

CTcpClientSocket::~CTcpClientSocket()
{
	if (m_byPacketBufferRing != nullptr) { for (int i = 0; i < RING_PACKET_SIZE; i++) { delete[] m_byPacketBufferRing[i]; m_byPacketBufferRing[i] = nullptr; } delete[] m_byPacketBufferRing; m_byPacketBufferRing = nullptr; }
	if (m_byPacketBuffer != nullptr) { delete[] m_byPacketBuffer; m_byPacketBuffer = nullptr; }
	if (m_pServerProtocol != nullptr) { delete m_pServerProtocol; m_pServerProtocol = nullptr; }
	if (m_pServerSendData != nullptr) { delete m_pServerSendData; m_pServerSendData = nullptr; }
}

BOOL CTcpClientSocket::SetListenSocket(CAsyncSocket* pSocket)
{
	m_pListenSocket = pSocket;
	return TRUE;
}

void CTcpClientSocket::OnReceive(int nErrorCode)
{
	CTcpListenSocket* pListenSocket = (CTcpListenSocket*)m_pListenSocket;

	unsigned int nLength = Receive(m_byPacketBuffer, m_nPacketBufferSize);

	if (nLength > 0)
	{
		if (m_nBufferIndex == RING_PACKET_SIZE) { m_nBufferIndex = 0; }
		memcpy(m_byPacketBufferRing[m_nBufferIndex], m_byPacketBuffer, nLength);
		m_nBufferIndex++;
	}

	CSocket::OnReceive(nErrorCode);
}

void CTcpClientSocket::OnClose(int nErrorCode)
{
	CTcpListenSocket* pListenSocket = (CTcpListenSocket*)m_pListenSocket;

	pListenSocket->CloseClientSocket(this);

	CSocket::OnClose(nErrorCode);
}

BOOL CTcpClientSocket::BufferSizeChange(unsigned int nBufferSize)
{
	if (m_nPacketBufferSize != nBufferSize)
	{
		if (m_byPacketBuffer != nullptr) { delete[] m_byPacketBuffer; m_byPacketBuffer = nullptr; }
		m_byPacketBuffer = new BYTE[nBufferSize];
		if (m_byPacketBufferRing != nullptr) { for (int i = 0; i < RING_PACKET_SIZE; i++) { delete[] m_byPacketBufferRing[i]; m_byPacketBufferRing[i] = nullptr; } delete[] m_byPacketBufferRing; m_byPacketBufferRing = nullptr; }
		m_byPacketBufferRing = new BYTE *[RING_PACKET_SIZE];
		for (int i = 0; i < RING_PACKET_SIZE; i++) { m_byPacketBufferRing[i] = new BYTE[nBufferSize]; } m_nPacketBufferSize = nBufferSize;
	}

	return TRUE;
}