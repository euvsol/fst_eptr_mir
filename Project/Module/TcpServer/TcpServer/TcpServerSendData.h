#pragma once

#include "PTR_SocketProtocolStructData.h"
#include "PTR_SocketProtocolStructByte.h"

class CTcpServerSendData
{

public:
	CTcpServerSendData(void* pFramework, void* pParents, void* pListen);
	virtual ~CTcpServerSendData();

public:
	void*			m_pFramework;
	void*			m_pParents;
	void*			m_pListen;

	BYTE*			m_byPacketBuffer;
	unsigned int	m_nPacketBufferSize;

public:
	// Send Scan Imagef Packet
	void SendRunImagePacket(unsigned int nChanel, PACKET_M_IMAGE_DATA* pPacketData);
	void SendMImagePacket(unsigned int nChanel, PACKET_M_IMAGE_DATA* pPacketData);
	void SendAImagePacket(unsigned int nChanel, PACKET_A_IMAGE_DATA* pPacketData);

	// Send TCP Connect Ping Packet
	void SendSocketDataPacket(unsigned int nChanel, PACKET_SOCKET_PING_DATA* pPacketData);

	BOOL BufferSizeChange(unsigned int nBufferSize);

	CString ConvertToHex(CString data);

	void ConvertToHexDebugging(BYTE* pByte, int nLength);
};


