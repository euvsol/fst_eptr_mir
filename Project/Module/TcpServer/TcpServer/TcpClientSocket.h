#pragma once

#include <afxsock.h>
#include <vector>
#include <queue>

#include "TcpServerSendData.h"
#include "TcpServerProtocol.h"

#include "PTR_SocketProtocolDefine.h"
#include "PTR_UserMessage.h"

using namespace std;

class CTcpClientSocket : public CSocket
{

public:
	CTcpClientSocket(void* pFramework, void* pParents, void* pListen, unsigned int nChannel, CString strIP, unsigned int nPort);
	virtual ~CTcpClientSocket();

public:
	void*					m_pFramework;
	void*					m_pParents;
	void*					m_pListen;

	unsigned int			m_nChannel;
	CString					m_strIP;
	unsigned int			m_nPort;

	SOCKET					m_hSocket;
	CAsyncSocket*			m_pListenSocket;
	CTcpServerProtocol*		m_pServerProtocol;
	CTcpServerSendData*		m_pServerSendData;

	BYTE*					m_byPacketBuffer;
	BYTE**					m_byPacketBufferRing;
	unsigned int			m_nPacketBufferSize;

	unsigned int			m_nBufferIndex;
	unsigned int			m_nRingBufferIndex;
	unsigned int			m_nRingBufferReceiveIndex;

public:
	virtual void OnReceive(int nErrorCode);
	virtual void OnClose(int nErrorCode);

public:
	BOOL SetListenSocket(CAsyncSocket* pSocket);
	BOOL BufferSizeChange(unsigned int nBufferSize);
};


