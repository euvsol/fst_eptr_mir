#pragma once

#include "TcpListenSocket.h"

class CObjectDll
{
	
public:
	CObjectDll();
	~CObjectDll();

public:
	void*				m_pFramework;
	void*				m_pParents;

	CTcpListenSocket*	m_pListenSocket[SERVER_NETWORK_COUNT];

public:
//	(FUNCTION_RET)		(FUNCTION_NAME)							(Type* A, Type* B));
	BOOL				FrameworkInstance						(void* pFramework, void* pParents);
	BOOL				CreateServer							(unsigned int nServer, CString strIP, unsigned int nPort);
	CTcpClientSocket*	GetClientSocket							(unsigned int nServer, unsigned int nChannel);
	BOOL				InitSocketSetting						(CTcpListenSocket* pListenSocket);
	BOOL				BufferSizeChange						(unsigned int nServer, unsigned int nChannel, unsigned int nBufferSize);
	BOOL				SendRunImagePacket						(unsigned int nServer, unsigned int nChannel, PACKET_M_IMAGE_DATA* pData);
	BOOL				SendMImagePacket						(unsigned int nServer, unsigned int nChannel, PACKET_M_IMAGE_DATA* pData);
	BOOL				SendAImagePacket						(unsigned int nServer, unsigned int nChannel, PACKET_A_IMAGE_DATA* pData);
	BOOL				SendSocketDataPacket					(unsigned int nServer, unsigned int nChannel, PACKET_SOCKET_PING_DATA* pData);
	BOOL				GetClientStatus							(unsigned int nServer, unsigned int nChannel);
};

extern class CObjectDll ObjectDll;