#include "stdafx.h"
#include "TcpServerProtocol.h"
#include "ObjectDll.h"


CTcpServerProtocol::CTcpServerProtocol(void* pFramework, void* pParents)
{
	m_pFramework	= pFramework;
	m_pParents		= pParents;
}

CTcpServerProtocol::~CTcpServerProtocol()
{
}

BOOL CTcpServerProtocol::RunImageDataToRunImageByte(PACKET_M_IMAGE_DATA* pPacketData, PACKET_M_IMAGE_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));	

	int nVectorSize = pPacketData->QueueData.size();

	for (int i=0; i< nVectorSize; i++)
	{
		PACKET_M_IMAGE_BYTE_CONTENT* pByteContent = new PACKET_M_IMAGE_BYTE_CONTENT;
		
		PACKET_M_IMAGE_DATA_CONTENT* pDataContent = pPacketData->QueueData.front();

		memcpy(&pByteContent->PacketNumber		, &pDataContent->PacketNumber	, sizeof(unsigned int));
		memcpy(&pByteContent->EM1024X			, &pDataContent->EM1024X		, sizeof(double));
		memcpy(&pByteContent->EM1024Y			, &pDataContent->EM1024Y		, sizeof(double));
		memcpy(&pByteContent->Detector_1		, &pDataContent->Detector_1		, sizeof(double));	
		memcpy(&pByteContent->Detector_2		, &pDataContent->Detector_2		, sizeof(double));	
		memcpy(&pByteContent->Detector_3		, &pDataContent->Detector_3		, sizeof(double));	
		memcpy(&pByteContent->Detector_4		, &pDataContent->Detector_4		, sizeof(double));	
		memcpy(&pByteContent->Zr_IO				, &pDataContent->Zr_IO			, sizeof(double));	
		memcpy(&pByteContent->BS_IO				, &pDataContent->BS_IO			, sizeof(double));

		pPacketByte->QueueByte.push(pByteContent);

		delete pDataContent;
		pDataContent = nullptr;

		pPacketData->QueueData.pop();
	}

	return TRUE;
}

BOOL CTcpServerProtocol::MImageDataToMImageByte(PACKET_M_IMAGE_DATA* pPacketData, PACKET_M_IMAGE_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));	

	int nVectorSize = pPacketData->QueueData.size();

	for (int i=0; i< nVectorSize; i++)
	{
		PACKET_M_IMAGE_BYTE_CONTENT* pByteContent = new PACKET_M_IMAGE_BYTE_CONTENT;
		
		PACKET_M_IMAGE_DATA_CONTENT* pDataContent = pPacketData->QueueData.front();

		memcpy(&pByteContent->PacketNumber		, &pDataContent->PacketNumber	, sizeof(unsigned int));
		memcpy(&pByteContent->EM1024X			, &pDataContent->EM1024X		, sizeof(double));
		memcpy(&pByteContent->EM1024Y			, &pDataContent->EM1024Y		, sizeof(double));
		memcpy(&pByteContent->Detector_1		, &pDataContent->Detector_1		, sizeof(double));	
		memcpy(&pByteContent->Detector_2		, &pDataContent->Detector_2		, sizeof(double));	
		memcpy(&pByteContent->Detector_3		, &pDataContent->Detector_3		, sizeof(double));	
		memcpy(&pByteContent->Detector_4		, &pDataContent->Detector_4		, sizeof(double));	
		memcpy(&pByteContent->Zr_IO				, &pDataContent->Zr_IO			, sizeof(double));	
		memcpy(&pByteContent->BS_IO				, &pDataContent->BS_IO			, sizeof(double));

		pPacketByte->QueueByte.push(pByteContent);

		delete pDataContent;
		pDataContent = nullptr;

		pPacketData->QueueData.pop();
	}

	return TRUE;
}

BOOL CTcpServerProtocol::AImageDataToAImageByte(PACKET_A_IMAGE_DATA* pPacketData, PACKET_A_IMAGE_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));	

	int nVectorSize = pPacketData->QueueData.size();

	for (int i=0; i< nVectorSize; i++)
	{
		PACKET_A_IMAGE_BYTE_CONTENT* pByteContent = new PACKET_A_IMAGE_BYTE_CONTENT;
		
		PACKET_A_IMAGE_DATA_CONTENT* pDataContent = pPacketData->QueueData.front();

		memcpy(&pByteContent->PacketNumber		, &pDataContent->PacketNumber	, sizeof(unsigned int));
		memcpy(&pByteContent->EM1024X			, &pDataContent->EM1024X		, sizeof(double));
		memcpy(&pByteContent->EM1024Y			, &pDataContent->EM1024Y		, sizeof(double));
		memcpy(&pByteContent->Detector_1		, &pDataContent->Detector_1		, sizeof(double));	
		memcpy(&pByteContent->Detector_2		, &pDataContent->Detector_2		, sizeof(double));	
		memcpy(&pByteContent->Detector_3		, &pDataContent->Detector_3		, sizeof(double));	
		memcpy(&pByteContent->Detector_4		, &pDataContent->Detector_4		, sizeof(double));	
		memcpy(&pByteContent->Zr_IO				, &pDataContent->Zr_IO			, sizeof(double));	
		memcpy(&pByteContent->BS_IO				, &pDataContent->BS_IO			, sizeof(double));
		memcpy(&pByteContent->DetectorSub_1		, &pDataContent->DetectorSub_1	, sizeof(double));
		memcpy(&pByteContent->DetectorSub_2		, &pDataContent->DetectorSub_2	, sizeof(double));
		memcpy(&pByteContent->DetectorSub_3		, &pDataContent->DetectorSub_3	, sizeof(double));
		memcpy(&pByteContent->DetectorSub_4		, &pDataContent->DetectorSub_4	, sizeof(double));

		pPacketByte->QueueByte.push(pByteContent);

		delete pDataContent;
		pDataContent = nullptr;

		pPacketData->QueueData.pop();
	}

	return TRUE;
}

BOOL CTcpServerProtocol::ByteToRunStopData(BYTE* pByte, PACKET_RUN_STOP_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Data	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	return TRUE;
}

BOOL CTcpServerProtocol::ByteToRecipeData(BYTE* pByte, PACKET_RECIPE_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx				, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len				, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd				, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->ImageFOV_X			, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->ImageFOV_Y			, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->ScanGrid_X			, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->ScanGrid_Y			, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->InterpolationCount	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->AverageCount		, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->SetCount			, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->ScanDirection		, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->PacketCount		, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	return TRUE;
}

unsigned int CTcpServerProtocol::RunImageByteToByte(PACKET_M_IMAGE_BYTE* pPacketByte, BYTE* pByte)
{
	int nIndex = 0;
	
	memcpy(&pByte[nIndex], &pPacketByte->Stx, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Len, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->Cmd, sizeof(BYTE));			nIndex += sizeof(BYTE);

	int nHeaderSize		= PROTOCOL_HEADER_SIZE;
	int nContentSize	= sizeof(PACKET_M_IMAGE_BYTE_CONTENT);
	int nVectorSize		= pPacketByte->QueueByte.size();
	int nSendSize		= nHeaderSize + (nVectorSize * nContentSize);

	for (int i=0; i< nVectorSize; i++)
	{
		PACKET_M_IMAGE_BYTE_CONTENT* pByteContent = pPacketByte->QueueByte.front();

		memcpy(&pByte[nIndex], &pByteContent->PacketNumber	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
		memcpy(&pByte[nIndex], &pByteContent->EM1024X		, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->EM1024Y		, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_1	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_2	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_3	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_4	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Zr_IO			, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->BS_IO			, sizeof(double));			nIndex += sizeof(double);

		delete pByteContent;
		pByteContent = nullptr;

		pPacketByte->QueueByte.pop();
	}
	
	return nIndex;
}

unsigned int CTcpServerProtocol::MImageByteToByte(PACKET_M_IMAGE_BYTE* pPacketByte, BYTE* pByte)
{
	int nIndex = 0;
	
	memcpy(&pByte[nIndex], &pPacketByte->Stx, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Len, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->Cmd, sizeof(BYTE));			nIndex += sizeof(BYTE);

	int nHeaderSize		= PROTOCOL_HEADER_SIZE;
	int nContentSize	= sizeof(PACKET_M_IMAGE_BYTE_CONTENT);
	int nVectorSize		= pPacketByte->QueueByte.size();
	int nSendSize		= nHeaderSize + (nVectorSize * nContentSize);

	for (int i=0; i< nVectorSize; i++)
	{
		PACKET_M_IMAGE_BYTE_CONTENT* pByteContent = pPacketByte->QueueByte.front();

		memcpy(&pByte[nIndex], &pByteContent->PacketNumber	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
		memcpy(&pByte[nIndex], &pByteContent->EM1024X		, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->EM1024Y		, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_1	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_2	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_3	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_4	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Zr_IO			, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->BS_IO			, sizeof(double));			nIndex += sizeof(double);

		delete pByteContent;
		pByteContent = nullptr;

		pPacketByte->QueueByte.pop();
	}
	
	return nIndex;
}

unsigned int CTcpServerProtocol::AImageByteToByte(PACKET_A_IMAGE_BYTE* pPacketByte, BYTE* pByte)
{
	int nIndex = 0;
	
	memcpy(&pByte[nIndex], &pPacketByte->Stx, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Len, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->Cmd, sizeof(BYTE));			nIndex += sizeof(BYTE);

	int nHeaderSize		= PROTOCOL_HEADER_SIZE;
	int nContentSize	= sizeof(PACKET_A_IMAGE_BYTE_CONTENT);
	int nVectorSize		= pPacketByte->QueueByte.size();
	int nSendSize		= nHeaderSize + (nVectorSize * nContentSize);

	for (int i=0; i< nVectorSize; i++)
	{
		PACKET_A_IMAGE_BYTE_CONTENT* pByteContent = pPacketByte->QueueByte.front();

		memcpy(&pByte[nIndex], &pByteContent->PacketNumber	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
		memcpy(&pByte[nIndex], &pByteContent->EM1024X		, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->EM1024Y		, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_1	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_2	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_3	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Detector_4	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->Zr_IO			, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->BS_IO			, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->DetectorSub_1	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->DetectorSub_2	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->DetectorSub_4	, sizeof(double));			nIndex += sizeof(double);
		memcpy(&pByte[nIndex], &pByteContent->DetectorSub_4	, sizeof(double));			nIndex += sizeof(double);

		delete pByteContent;
		pByteContent = nullptr;

		pPacketByte->QueueByte.pop();
	}
	
	return nIndex;
}

unsigned int CTcpServerProtocol::SocketDataToByte(PACKET_SOCKET_PING_BYTE* pPacketData, BYTE* pByte)
{
	int nIndex = 0;

	memcpy(&pByte[nIndex], &pPacketData->Stx	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketData->Len	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketData->Cmd	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketData->Data	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	int nSendSize = nIndex;

	return nSendSize;
}

BOOL CTcpServerProtocol::ByteToAPacketRunData(BYTE* pByte, PACKET_A_PACKET_RUN_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Data	, &pByte[nIndex], sizeof(unsigned char));	nIndex += sizeof(unsigned char);

	return TRUE;
}

BOOL CTcpServerProtocol::ByteToAverageCountData(BYTE* pByte, PACKET_AVERAGE_COUNT_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Data	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	return TRUE;
}

BOOL CTcpServerProtocol::ByteToEm1024ResetData(BYTE* pByte, PACKET_EM1024_RESET_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Data	, &pByte[nIndex], sizeof(unsigned char));	nIndex += sizeof(unsigned char);

	return TRUE;
}

BOOL CTcpServerProtocol::ByteToMPacketRunData(BYTE* pByte, PACKET_M_PACKET_RUN_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Data	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	return TRUE;
}

BOOL CTcpServerProtocol::SocketDataToSocketByte(PACKET_SOCKET_PING_DATA* pPacketData, PACKET_SOCKET_PING_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));
	memcpy(&pPacketByte->Data	, &pPacketData->Data	, sizeof(unsigned int));

	return TRUE;
}

BOOL CTcpServerProtocol::ByteToSocketData(BYTE* pByte, PACKET_SOCKET_PING_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Data	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	return TRUE;
}