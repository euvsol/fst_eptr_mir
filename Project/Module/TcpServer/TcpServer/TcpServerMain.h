#pragma once

#include <vector>

using namespace std;

/*

DLL(Dynamic-Link Library)
- DLL은 마이크로소프트 윈도에서 구현된 동적 라이브러리이다. 
  내부에는 다른 프로그램이 불러서 쓸 수 있는 다양한 함수들을 가지고 있는데, 
  확장DLL인 경우는 클래스를 가지고 있기도 한다.
  사용하는 방법에는 두가지가 있다.

묵시적 링킹(Implicit linking)
- 실행 파일 자체에 어떤 DLL의 어떤 함수를 사용하겠다는 정보를 포함시키고 
  운영체제가 프로그램 실행 시 해당 함수들을 초기화한 후 그것을 이용하는 방법

명시적 링킹(Explicit linking)
- 프로그램이 실행 중일 때 API를 이용하여 DLL 파일이 있는지 검사하고 
  동적으로 원하는 함수만 불러와서 쓰는 방법

*/

// ********* 명시적 링킹 ********* //

#ifdef _TCP_SERVER
	#define EXT_CLASS_TCP_SERVER __declspec(dllexport)
#else
	#define EXT_CLASS_TCP_SERVER __declspec(dllimport)
#endif

// ret  : 리턴 값
// name : 함수 이름
// def  : 괄호 "()"가 들어간 파라미터 정의
#define DEF_EXTERNAL(ret, name, def) private: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_INTERNAL(ret, name, def) public: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_CHECK(function) if	(!function) { AfxMessageBox	(_T("CTcpServer Failed to get function pointer")); }

class EXT_CLASS_TCP_SERVER CTcpServer
{

private:
//				(FUNCTION_RET) (FUNCTION_NAME)						  (Type* A, Type* B));
	DEF_EXTERNAL(BOOL		 , FrameworkInstance					, (void* pFramework, void* pParents));
	DEF_EXTERNAL(BOOL		 , CreateServer							, (unsigned int nServer, CString strIP, unsigned int nPort));
	DEF_EXTERNAL(BOOL		 , BufferSizeChange						, (unsigned int nServer, unsigned int nChannel, unsigned int nBufferSize));
	DEF_EXTERNAL(BOOL		 , SendRunImagePacket					, (unsigned int nServer, unsigned int nChannel, PACKET_M_IMAGE_DATA* pData));
	DEF_EXTERNAL(BOOL		 , SendMImagePacket						, (unsigned int nServer, unsigned int nChannel, PACKET_M_IMAGE_DATA* pData));
	DEF_EXTERNAL(BOOL		 , SendAImagePacket						, (unsigned int nServer, unsigned int nChannel, PACKET_A_IMAGE_DATA* pData));
	DEF_EXTERNAL(BOOL		 , SendSocketDataPacket					, (unsigned int nServer, unsigned int nChannel, PACKET_SOCKET_PING_DATA* pData));
	DEF_EXTERNAL(BOOL		 , GetClientStatus						, (unsigned int nServer, unsigned int nChannel));

public:

	//생성자는 꼭 private나 protected로
	CTcpServer(HINSTANCE hInstance)
	{
		if (hInstance)
		{
//		   (FUNCTION_NAME)				  (FUNCTION_TYPE)					GetProcAddress(hInstance, FUNCTION_NAME)					;
			FrameworkInstance			= (fp_FrameworkInstance)			GetProcAddress(hInstance, "FrameworkInstance")				;
			CreateServer				= (fp_CreateServer)					GetProcAddress(hInstance, "CreateServer")					;
			BufferSizeChange			= (fp_BufferSizeChange)				GetProcAddress(hInstance, "BufferSizeChange")				;
			SendRunImagePacket			= (fp_SendRunImagePacket)			GetProcAddress(hInstance, "SendRunImagePacket")				;
			SendMImagePacket			= (fp_SendMImagePacket)				GetProcAddress(hInstance, "SendMImagePacket")				;
			SendAImagePacket			= (fp_SendAImagePacket)				GetProcAddress(hInstance, "SendAImagePacket")				;
			SendSocketDataPacket		= (fp_SendSocketDataPacket)			GetProcAddress(hInstance, "SendSocketDataPacket")			;
			GetClientStatus				= (fp_GetClientStatus)				GetProcAddress(hInstance, "GetClientStatus")				;

			DEF_CHECK(FrameworkInstance);
			DEF_CHECK(CreateServer);
			DEF_CHECK(BufferSizeChange);
			DEF_CHECK(SendRunImagePacket);
			DEF_CHECK(SendMImagePacket);
			DEF_CHECK(SendAImagePacket);
			DEF_CHECK(SendSocketDataPacket);
			DEF_CHECK(GetClientStatus);
		}
	}
	~CTcpServer() {}
};