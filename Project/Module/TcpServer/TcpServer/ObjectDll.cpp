#include "StdAfx.h"
#include "ObjectDll.h"

class CObjectDll ObjectDll;

CObjectDll::CObjectDll()
{
	m_pFramework		= nullptr;
	m_pParents			= nullptr;

	for (int i = 0; i < SERVER_NETWORK_COUNT; i++) { m_pListenSocket[i] = nullptr; }
}

CObjectDll::~CObjectDll()
{
	for (int i = 0; i < SERVER_NETWORK_COUNT; i++)
	{
		if (m_pListenSocket[i] != nullptr)
		{
			m_pListenSocket[i]->ShutDown(SD_BOTH);
			m_pListenSocket[i]->Close();
			delete m_pListenSocket[i];
			m_pListenSocket[i] = nullptr;
		}
	}
}

BOOL CObjectDll::FrameworkInstance(void* pFramework, void* pParents)
{
	m_pFramework	= pFramework;
	m_pParents		= pParents;
	return TRUE;
}

BOOL CObjectDll::CreateServer(unsigned int nServer, CString strIP, unsigned int nPort)
{
	if (!AfxSocketInit())
	{
		AfxMessageBox(_T("Windows 소켓을 초기화하지 못했습니다."));
		return FALSE;
	}

	if (m_pListenSocket[nServer] == nullptr)
	{
		m_pListenSocket[nServer] = new CTcpListenSocket(m_pFramework, this, strIP, nPort);

		struct linger optval;
		optval.l_onoff = 1;
		optval.l_linger = 0;
		int	optlen = sizeof(optval);

		::setsockopt(m_pListenSocket[nServer]->m_hSocket, SOL_SOCKET, SO_LINGER, (CHAR*)&optval, sizeof(optval));

		if (m_pListenSocket[nServer]->Create(nPort, SOCK_STREAM, _ttoi(strIP)))
		{
			InitSocketSetting(m_pListenSocket[nServer]);

			if (m_pListenSocket[nServer]->Listen() == FALSE)
			{
				m_pListenSocket[nServer]->ShutDown(SD_BOTH);
				m_pListenSocket[nServer]->Close();
				delete m_pListenSocket[nServer];
				m_pListenSocket[nServer] = nullptr;
			}
			else { return TRUE; }
		}
		else { return FALSE; }
	}

	return TRUE;
}

BOOL CObjectDll::InitSocketSetting(CTcpListenSocket* pListenSocket)
{
	int snd_buf = 0;
	int rcv_buf = 0;

	int state;
	socklen_t  len;

	/*입출력 버퍼 크기 확인*/
	len = sizeof(rcv_buf);
	state = getsockopt(pListenSocket->m_hSocket, SOL_SOCKET, SO_SNDBUF, (char*)&rcv_buf, &len); if (state) { TRACE(_T("SOCKET_ERROR\n")); }

	len = sizeof(snd_buf);
	state = getsockopt(pListenSocket->m_hSocket, SOL_SOCKET, SO_RCVBUF, (char*)&snd_buf, &len); if (state) { TRACE(_T("SOCKET_ERROR\n")); }

	TRACE(_T("데이터 입력받기 위한 소켓의 버퍼 크기 : %d(수신버퍼) \n"), rcv_buf);
	TRACE(_T("데이터 출력받기 위한 소켓의 버퍼 크기 : %d(송신버퍼) \n"), snd_buf);

	snd_buf = 65536;
	rcv_buf = 65536;

	/*입출력 버퍼 크기 설정*/
	state = setsockopt(pListenSocket->m_hSocket, SOL_SOCKET, SO_RCVBUF, (char*)&rcv_buf, sizeof(rcv_buf)); if (state) { TRACE(_T("SOCKET_ERROR\n")); }
	state = setsockopt(pListenSocket->m_hSocket, SOL_SOCKET, SO_SNDBUF, (char*)&snd_buf, sizeof(snd_buf)); if (state) { TRACE(_T("SOCKET_ERROR\n")); }

	/*입출력 버퍼 크기 확인*/
	len = sizeof(rcv_buf);
	state = getsockopt(pListenSocket->m_hSocket, SOL_SOCKET, SO_SNDBUF, (char*)&rcv_buf, &len); if (state) { TRACE(_T("SOCKET_ERROR\n")); }

	len = sizeof(snd_buf);
	state = getsockopt(pListenSocket->m_hSocket, SOL_SOCKET, SO_RCVBUF, (char*)&snd_buf, &len); if (state) { TRACE(_T("SOCKET_ERROR\n")); }

	TRACE(_T("데이터 입력받기 위한 소켓의 버퍼 크기 : %d(수신버퍼) \n"), rcv_buf);
	TRACE(_T("데이터 출력받기 위한 소켓의 버퍼 크기 : %d(송신버퍼) \n"), snd_buf);

	return TRUE;
}

CTcpClientSocket* CObjectDll::GetClientSocket(unsigned int nServer, unsigned int nChannel)
{
	return m_pListenSocket[nServer]->GetClientSocket(nChannel);
}

BOOL CObjectDll::BufferSizeChange(unsigned int nServer, unsigned int nChannel, unsigned int nBufferSize)
{
	return m_pListenSocket[nServer]->BufferSizeChange(nChannel, nBufferSize);
}

BOOL CObjectDll::SendRunImagePacket(unsigned int nServer, unsigned int nChannel, PACKET_M_IMAGE_DATA* pData)
{
	return m_pListenSocket[nServer]->SendRunImagePacket(nChannel, pData);
}

BOOL CObjectDll::SendMImagePacket(unsigned int nServer, unsigned int nChannel, PACKET_M_IMAGE_DATA* pData)
{
	return m_pListenSocket[nServer]->SendMImagePacket(nChannel, pData);
}

BOOL CObjectDll::SendAImagePacket(unsigned int nServer, unsigned int nChannel, PACKET_A_IMAGE_DATA* pData)
{
	return m_pListenSocket[nServer]->SendAImagePacket(nChannel, pData);
}

BOOL CObjectDll::SendSocketDataPacket(unsigned int nServer, unsigned int nChannel, PACKET_SOCKET_PING_DATA* pData)
{
	return m_pListenSocket[nServer]->SendSocketDataPacket(nChannel, pData);
}

BOOL CObjectDll::GetClientStatus(unsigned int nServer, unsigned int nChannel)
{
	return m_pListenSocket[nServer]->GetClientStatus(nChannel);
}
