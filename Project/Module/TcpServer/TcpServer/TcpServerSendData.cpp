#include "stdafx.h"
#include "TcpServerSendData.h"
#include "TcpListenSocket.h"
#include "ObjectDll.h"


CTcpServerSendData::CTcpServerSendData(void* pFramework, void* pParents, void* pListen)
{
	m_pFramework		= pFramework;
	m_pParents			= pParents;
	m_pListen			= pListen;

	m_byPacketBuffer	= nullptr;
	m_nPacketBufferSize = 0;
}

CTcpServerSendData::~CTcpServerSendData()
{
}

void CTcpServerSendData::SendRunImagePacket(unsigned int nChanel, PACKET_M_IMAGE_DATA* pPacketData)
{
	CTcpListenSocket*	pListen		= (CTcpListenSocket*)m_pListen;
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)pListen->m_stSocketThread[nChanel].m_pClient;
	CTcpServerProtocol* pProtocol	= pClient->m_pServerProtocol;

	PACKET_M_IMAGE_BYTE PacketByte;

	pProtocol->RunImageDataToRunImageByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize = pProtocol->RunImageByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength = pClient->Send(m_byPacketBuffer, nSendSize);

	//ConvertToHexDebugging(m_byPacketBuffer, nLength);
}

void CTcpServerSendData::SendMImagePacket(unsigned int nChanel, PACKET_M_IMAGE_DATA* pPacketData)
{
	CTcpListenSocket*	pListen		= (CTcpListenSocket*)m_pListen;
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)pListen->m_stSocketThread[nChanel].m_pClient;
	CTcpServerProtocol* pProtocol	= pClient->m_pServerProtocol;

	PACKET_M_IMAGE_BYTE PacketByte;

	pProtocol->MImageDataToMImageByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);
	
	unsigned int nSendSize	= pProtocol->MImageByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	//ConvertToHexDebugging(m_byPacketBuffer, nLength);
}

void CTcpServerSendData::SendAImagePacket(unsigned int nChanel, PACKET_A_IMAGE_DATA* pPacketData)
{
	CTcpListenSocket*	pListen		= (CTcpListenSocket*)m_pListen;
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)pListen->m_stSocketThread[nChanel].m_pClient;
	CTcpServerProtocol* pProtocol	= pClient->m_pServerProtocol;

	PACKET_A_IMAGE_BYTE PacketByte;

	pProtocol->AImageDataToAImageByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize	= pProtocol->AImageByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	//ConvertToHexDebugging(m_byPacketBuffer, nLength);
}

void CTcpServerSendData::SendSocketDataPacket(unsigned int nChanel, PACKET_SOCKET_PING_DATA* pPacketData)
{
	CTcpListenSocket*	pListen		= (CTcpListenSocket*)m_pListen;
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)pListen->m_stSocketThread[nChanel].m_pClient;
	CTcpServerProtocol* pProtocol	= pClient->m_pServerProtocol;

	PACKET_SOCKET_PING_BYTE PacketByte;

	pProtocol->SocketDataToSocketByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize	= pProtocol->SocketDataToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	//ConvertToHexDebugging(m_byPacketBuffer, nLength);
}

BOOL CTcpServerSendData::BufferSizeChange(unsigned int nBufferSize)
{
	if (m_nPacketBufferSize != nBufferSize)
	{
		if (m_byPacketBuffer != nullptr)
		{
			delete[] m_byPacketBuffer;
			m_byPacketBuffer = nullptr;
		}

		m_byPacketBuffer = new BYTE[nBufferSize];

		m_nPacketBufferSize = nBufferSize;
	}

	return TRUE;
}

CString CTcpServerSendData::ConvertToHex(CString data)
{
	CString returnvalue;
	for (int x = 0; x < data.GetLength(); x++)
	{
		CString temporary;
		int value = (int)(data[x]);
		temporary.Format(_T("%02X "), value);
		returnvalue += temporary;
	}
	return returnvalue;
}

void CTcpServerSendData::ConvertToHexDebugging(BYTE* pByte, int nLength)
{
	CString strResult;
	CString strTokData;

	strResult.Format(_T("[Server][Send][%d Byte] : "), nLength);

	for (int i = 0; i < nLength; i++)
	{
		strTokData.Format(_T("%c"), pByte[i]);
		strTokData = ConvertToHex(strTokData);
		strResult += strTokData;
	}

	TRACE(_T("* %s\n"), strResult);
}