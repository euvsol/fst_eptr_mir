#include "stdafx.h"
#include "TcpSocketThread.h"


IMPLEMENT_DYNCREATE(CTcpSocketThread, CWinThread)

CTcpSocketThread::CTcpSocketThread()
{
	m_pFramework	= nullptr;
	m_pParents		= nullptr;
	m_pListen		= nullptr;

	m_hSocket		= NULL;
	m_pClient		= nullptr;
}

CTcpSocketThread::~CTcpSocketThread()
{
}

BOOL CTcpSocketThread::InitInstance()
{
	if (!AfxSocketInit()) { AfxMessageBox(_T("Windows 소켓을 초기화하지 못했습니다.")); return FALSE; }
	m_pClient->Attach(m_hSocket);
	return TRUE;
}

int CTcpSocketThread::ExitInstance()
{
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CTcpSocketThread, CWinThread)
END_MESSAGE_MAP()