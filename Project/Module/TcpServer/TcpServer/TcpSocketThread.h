#pragma once

#include <afxsock.h>
#include "TcpClientSocket.h"
#include "TcpServerSendData.h"
#include "TcpServerProtocol.h"

class CTcpSocketThread : public CWinThread
{
	DECLARE_DYNCREATE(CTcpSocketThread)

protected:
	CTcpSocketThread();
	virtual ~CTcpSocketThread();

public:
	void*					m_pFramework;
	void*					m_pParents;
	void*					m_pListen;

	SOCKET					m_hSocket;
	CTcpClientSocket*		m_pClient;

protected:
	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
};


