#pragma once

#include "PTR_SocketProtocolStruct.h"

class CTcpListenSocket : public CAsyncSocket
{

public:
	CTcpListenSocket(void* pFramework, void* pParents, CString strIP, unsigned int nPort);
	virtual ~CTcpListenSocket();

public:
	void*				m_pFramework;
	void*				m_pParents;

	CString				m_strIP;
	unsigned int		m_nPort;

	SOCKET_THREAD		m_stSocketThread[CLIENT_NETWORK_COUNT];

public:
	virtual void OnAccept(int nErrorCode);

public:
	void CloseClientSocket(CTcpClientSocket* pClient);
	BOOL BufferSizeChange(int nChannel, unsigned int nBufferSize);
	BOOL SendRunImagePacket(int nChannel, PACKET_M_IMAGE_DATA* pData);
	BOOL SendMImagePacket(int nChannel, PACKET_M_IMAGE_DATA* pData);
	BOOL SendAImagePacket(int nChannel, PACKET_A_IMAGE_DATA* pData);
	BOOL SendSocketDataPacket(int nChannel, PACKET_SOCKET_PING_DATA* pData);
	BOOL GetClientStatus(int nChannel);
	CTcpClientSocket* GetClientSocket(int nChannel);
};


