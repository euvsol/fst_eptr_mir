// SixthRxProjectMain.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ObjectDll.h"
#include "TcpServerMain.h"

extern "C" __declspec(dllexport) BOOL FrameworkInstance(void* pFramework, void* pParents)
{
	return ObjectDll.FrameworkInstance(pFramework, pParents);
}

extern "C" __declspec(dllexport) BOOL CreateServer(unsigned int nServer, CString strIP, unsigned int nPort)
{
	return ObjectDll.CreateServer(nServer, strIP, nPort);
}

extern "C" __declspec(dllexport) BOOL BufferSizeChange(unsigned int nServer, unsigned int nChannel, unsigned int nBufferSize)
{
	return ObjectDll.BufferSizeChange(nServer, nChannel, nBufferSize);
}

extern "C" __declspec(dllexport) BOOL SendRunImagePacket(unsigned int nServer, unsigned int nChannel, PACKET_M_IMAGE_DATA * pData)
{
	return ObjectDll.SendRunImagePacket(nServer, nChannel, pData);
}

extern "C" __declspec(dllexport) BOOL SendMImagePacket(unsigned int nServer, unsigned int nChannel, PACKET_M_IMAGE_DATA* pData)
{
	return ObjectDll.SendMImagePacket(nServer, nChannel, pData);
}

extern "C" __declspec(dllexport) BOOL SendAImagePacket(unsigned int nServer, unsigned int nChannel, PACKET_A_IMAGE_DATA * pData)
{
	return ObjectDll.SendAImagePacket(nServer, nChannel, pData);
}

extern "C" __declspec(dllexport) BOOL SendSocketDataPacket(unsigned int nServer, unsigned int nChannel, PACKET_SOCKET_PING_DATA* pData)
{
	return ObjectDll.SendSocketDataPacket(nServer, nChannel, pData);
}

extern "C" __declspec(dllexport) BOOL GetClientStatus(unsigned int nServer, unsigned int nChannel)
{
	return ObjectDll.GetClientStatus(nServer, nChannel);
}