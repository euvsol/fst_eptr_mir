#include "stdafx.h"
#include "TcpListenSocket.h"
#include "ObjectDll.h"

CTcpListenSocket::CTcpListenSocket(void* pFramework, void* pParents, CString strIP, unsigned int nPort)
{
	m_pFramework		= pFramework;
	m_pParents			= pParents;

	m_strIP				= strIP;
	m_nPort				= nPort;
	
	for (int i = 0; i < CLIENT_NETWORK_COUNT; i++)
	{
		m_stSocketThread[i].m_pClient		= nullptr;
		m_stSocketThread[i].m_pSocketThread = nullptr;
		m_stSocketThread[i].m_nPort			= 0;
		m_stSocketThread[i].m_strIP			= _T("");
		m_stSocketThread[i].m_bConnect		= FALSE;
	}
}

CTcpListenSocket::~CTcpListenSocket()
{
	for (int i = 0; i < CLIENT_NETWORK_COUNT; i++)
	{
		if (m_stSocketThread[i].m_pClient != nullptr)
		{
			m_stSocketThread[i].m_pClient->m_hSocket = m_stSocketThread[i].m_pSocketThread->m_hSocket;
			m_stSocketThread[i].m_pClient->Attach(m_stSocketThread[i].m_pClient->m_hSocket);
			m_stSocketThread[i].m_pClient->ShutDown(SD_BOTH);
			m_stSocketThread[i].m_pClient->Close();
			delete m_stSocketThread[i].m_pClient;
			m_stSocketThread[i].m_pClient = nullptr;
			m_stSocketThread[i].m_bConnect = FALSE;
		}

		if (m_stSocketThread[i].m_pSocketThread != nullptr)
		{
			m_stSocketThread[i].m_pSocketThread->PostThreadMessage(WM_QUIT, 0, 0);
			m_stSocketThread[i].m_pSocketThread = nullptr;
			Sleep(100);
		}
	}
}

void CTcpListenSocket::OnAccept(int nErrorCode)
{
	for (int i = 0; i < CLIENT_NETWORK_COUNT; i++)
	{
		if (m_stSocketThread[i].m_pClient == nullptr)
		{
			m_stSocketThread[i].m_pClient = new CTcpClientSocket(m_pFramework, m_pParents, this, i, _T(""), 0);

			if (Accept(*m_stSocketThread[i].m_pClient))
			{
				m_stSocketThread[i].m_pClient->SetListenSocket(this);

				m_stSocketThread[i].m_pClient->GetPeerName(m_stSocketThread[i].m_pClient->m_strIP, m_stSocketThread[i].m_pClient->m_nPort);

				CObjectDll* pParent = (CObjectDll*)m_pParents;
				CWnd*		pWnd	= (CWnd*)pParent->m_pParents;
				WPARAM		wParam	= 0;
				LPARAM		lParam	= (LPARAM)m_stSocketThread[i].m_pClient;

				if (SendMessage(pWnd->m_hWnd, WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_ACCEPT, wParam, lParam))
				{
					m_stSocketThread[i].m_bConnect = TRUE;
					m_stSocketThread[i].m_pSocketThread = (CTcpSocketThread*)AfxBeginThread(RUNTIME_CLASS(CTcpSocketThread), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
					m_stSocketThread[i].m_pSocketThread->m_pClient = m_stSocketThread[i].m_pClient;
					m_stSocketThread[i].m_pSocketThread->m_hSocket = m_stSocketThread[i].m_pClient->Detach();
					m_stSocketThread[i].m_pSocketThread->m_pFramework = m_pFramework;
					m_stSocketThread[i].m_pSocketThread->m_pParents = m_pParents;
					m_stSocketThread[i].m_pSocketThread->m_pListen = this;
					m_stSocketThread[i].m_pSocketThread->ResumeThread();

					BufferSizeChange(i, SOCKET_BUFFER_SIZE);
					break;
				}
				else
				{
					delete m_stSocketThread[i].m_pClient;
					m_stSocketThread[i].m_pClient = nullptr;
				}
			}
		}
	}

	CAsyncSocket::OnAccept(nErrorCode);
}

void CTcpListenSocket::CloseClientSocket(CTcpClientSocket* pClient)
{
	for (int i = 0; i < CLIENT_NETWORK_COUNT; i++)
	{
		if (m_stSocketThread[i].m_pClient != nullptr)
		{
			if (pClient->m_nChannel == m_stSocketThread[i].m_pClient->m_nChannel)
			{
				CObjectDll* pParent = (CObjectDll*)m_pParents;
				CWnd*		pWnd	= (CWnd*)pParent->m_pParents;
				WPARAM		wParam	= 0;
				LPARAM		lParam	= (LPARAM)m_stSocketThread[i].m_pClient;

				if (SendMessage(pWnd->m_hWnd, WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_CLOSE, wParam, lParam))
				{
					//m_stSocketThread[i].m_pClient->m_hSocket = m_stSocketThread[nChannel].m_pSocketThread->m_hSocket;
					//m_stSocketThread[i].m_pClient->Attach(m_stSocketThread[nChannel].m_pClient->m_hSocket);
					m_stSocketThread[i].m_pClient->ShutDown(SD_BOTH);
					m_stSocketThread[i].m_pClient->Close();
					delete m_stSocketThread[i].m_pClient;
					m_stSocketThread[i].m_pClient = nullptr;
					m_stSocketThread[i].m_bConnect = FALSE;

					m_stSocketThread[i].m_pSocketThread->PostThreadMessage(WM_QUIT, 0, 0);
					m_stSocketThread[i].m_pSocketThread = nullptr;
					Sleep(100);
				}
				break;
			}
		}
	}
}

CTcpClientSocket* CTcpListenSocket::GetClientSocket(int nChannel)
{
	return m_stSocketThread[nChannel].m_pSocketThread->m_pClient;
}

BOOL CTcpListenSocket::BufferSizeChange(int nChannel, unsigned int nBufferSize)
{
	m_stSocketThread[nChannel].m_pClient->BufferSizeChange(nBufferSize);
	m_stSocketThread[nChannel].m_pClient->m_pServerSendData->BufferSizeChange(nBufferSize);
	return TRUE;
}

BOOL CTcpListenSocket::SendRunImagePacket(int nChannel, PACKET_M_IMAGE_DATA* pData)
{
	m_stSocketThread[nChannel].m_pClient->m_pServerSendData->SendRunImagePacket(nChannel, pData);
	return TRUE;
}

BOOL CTcpListenSocket::SendMImagePacket(int nChannel, PACKET_M_IMAGE_DATA* pData)
{
	m_stSocketThread[nChannel].m_pClient->m_pServerSendData->SendMImagePacket(nChannel, pData);
	return TRUE;
}

BOOL CTcpListenSocket::SendAImagePacket(int nChannel, PACKET_A_IMAGE_DATA* pData)
{
	m_stSocketThread[nChannel].m_pClient->m_pServerSendData->SendAImagePacket(nChannel, pData);
	return TRUE;
}

BOOL CTcpListenSocket::SendSocketDataPacket(int nChannel, PACKET_SOCKET_PING_DATA* pData)
{
	m_stSocketThread[nChannel].m_pClient->m_pServerSendData->SendSocketDataPacket(nChannel, pData);
	return TRUE;
}

BOOL CTcpListenSocket::GetClientStatus(int nChannel)
{
	return m_stSocketThread[nChannel].m_bConnect;
}
