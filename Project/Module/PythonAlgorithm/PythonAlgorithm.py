import numpy as np
import scipy.io
from scipy.optimize import curve_fit
from scipy import interpolate
from scipy.interpolate import griddata
#import matplotlib.pyplot as plt

## Python API Test##
def LoopBackTest(pos, intensity, dir,slope,refAbs, refMl ):

    outSize = np.array(len(pos),dtype =int)

    outPos = pos
    outIntensity = intensity
    outDir = dir
    outSlope = slope
    outRefAbs = refAbs
    outRefMl = refMl

    output =(outPos, outIntensity, outDir, outSlope, outRefAbs, outRefMl, outSize)

    return output



## AlignFunXIncrease ##
def AlignFunXIncrease(x, offset, refAbs, refMl):           
    #w = 0.5/1000.
    #d = 0.5/1000.
    w = 0.75/1000.
    d = 0.75/1000.
    N = len(x)       
    refLeft = refAbs
    refRight = refMl
    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
        
    ratioRight = 0.5
    ratioLeft = 1-ratioRight
        
    for i in range(N):
        if xprim[i] < -(w + d / 2):
            y[i] = refLeft
        elif xprim[i] >= -(w + d / 2) and xprim[i] < -d / 2:
            #y[i] = (refRight - refLeft) / w / 2 * (xprim[i] + d / 2) + (refLeft + refRight)/2
            y[i] = (ratioRight*refRight + (ratioLeft-1)*refLeft) / w * (xprim[i] + d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= -d / 2 and xprim[i] < d / 2:
            y[i] = ratioRight*refRight + ratioLeft*refLeft
        elif xprim[i] >= d / 2 and xprim[i] < (w + d / 2):
            y[i] = ((1-ratioRight)*refRight - ratioLeft*refLeft) / w  * (xprim[i] - d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= (w + d / 2):
            y[i] = refRight
        else:
            y[i] = 0
    return y


## AlignFunXDecrease ##
def AlignFunXDecrease(x, offset, refAbs, refMl):        
    #w = 0.5/1000.
    #d = 0.5/1000.
    w = 0.75/1000.
    d = 0.75/1000.
    N = len(x)    
    refLeft = refMl
    refRight = refAbs
    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
               
    ratioLeft = 0.5
    ratioRight = 1-ratioLeft

    for i in range(N):
        if xprim[i] < -(w + d / 2):
            y[i] = refLeft
        elif xprim[i] >= -(w + d / 2) and xprim[i] < -d / 2:
            #y[i] = (refRight - refLeft) / w /2 * (xprim[i] + d / 2) + (refLeft + refRight) / 2 
            y[i] = (ratioRight*refRight + (ratioLeft-1)*refLeft) / w * (xprim[i] + d / 2) + (ratioLeft*refLeft + ratioRight*refRight)  
        elif xprim[i] >= -d / 2 and xprim[i] < d / 2:
            #y[i] = (refRight + refLeft) /2
            y[i] = (ratioRight*refRight + ratioLeft*refLeft)
        elif xprim[i] >= d / 2 and xprim[i] < (w + d / 2):
            #y[i] = (refRight - refLeft) / w /2* (xprim[i] - d / 2) + (refLeft + refRight) /2
            y[i] = ((1-ratioRight)*refRight - ratioLeft*refLeft) / w * (xprim[i] - d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= (w + d / 2):
            y[i] = refRight
        else:
            y[i] = 0

    return y

## AlignFunYIncrease ##
def AlignFunYIncrease(x, offset, refAbs, refMl):    
    l = 30./1000.
    N = len(x)    
    refBottom = refAbs
    refTop = refMl

    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
    
    for i in range(N):
        if xprim[i] < -l / 2:
            y[i] = refBottom
        elif xprim[i] >= -l / 2 and xprim[i] < l / 2:
            y[i] = (refTop - refBottom) / l * (xprim[i] + l / 2) + refBottom   
        elif xprim[i] >= l / 2:
            y[i] = refTop
        else:
            y[i] = 0        
    
    return y

## AlignFunYDecrease ##
def AlignFunYDecrease(x, offset, refAbs, refMl):    
    l = 30./1000.
    N = len(x)    
    refBottom = refMl
    refTop = refAbs

    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
    
    for i in range(N):
        if xprim[i] < -l / 2:
            y[i] = refBottom
        elif xprim[i] >= -l / 2 and xprim[i] < l / 2:
            y[i] = (refTop - refBottom) / l * (xprim[i] + l / 2) + refBottom   
        elif xprim[i] >= l / 2:
            y[i] = refTop
        else:
            y[i] = 0

    return y


## FineAlignCurveFit ##
def FineAlignCurveFit(pos, intensity, dir, slope, refAbs, refMl):
    #  �수 �력� �� np.array 
    init = [(pos[0] + pos[-1]) / 2, refAbs[0], refMl[0]]

    if dir == 0 and slope == 1:        
        popt, pcov = curve_fit(AlignFunXIncrease, pos, intensity, p0 =init)
    if dir == 0 and slope == -1:                
        popt, pcov = curve_fit(AlignFunXDecrease, pos, intensity, p0 =init)
    if dir == 1 and slope == 1:        
        popt, pcov = curve_fit(AlignFunYIncrease, pos, intensity, p0 =init)
    if dir == 1 and slope == -1:        
        popt, pcov = curve_fit(AlignFunYDecrease, pos, intensity, p0 =init)

    targetIntensity = np.zeros((1,), dtype=np.float64)
    targetPos = np.zeros((1,), dtype=np.float64)
    targetPos[0] = popt[0]

    #�시
    if dir == 0 and slope == 1:
        intensity_fit = AlignFunXIncrease(pos,*popt)       
        targetIntensity[0]  = AlignFunXIncrease(targetPos,*popt);        
    if dir == 0 and slope == -1:
        intensity_fit = AlignFunXDecrease(pos,*popt)
        targetIntensity[0]  = AlignFunXDecrease(targetPos,*popt);        
    if dir == 1 and slope == 1:
        intensity_fit = AlignFunYIncrease(pos,*popt)
        targetIntensity[0]  = AlignFunYIncrease(targetPos,*popt);        
    if dir == 1 and slope == -1:
        intensity_fit = AlignFunYDecrease(pos,*popt)
        targetIntensity[0]  = AlignFunYDecrease(targetPos,*popt);        
        
    # �시    
    outPos = pos
    outIntensity = intensity
    outDir = dir
    outSlope = slope
    outRefAbs = refAbs
    outRefMl = refMl
    outSize = np.array(len(pos),dtype =int)
    

    
    output = (outPos, outIntensity, outDir, outSlope, outRefAbs, outRefMl, outSize, popt, intensity_fit, targetIntensity)

    #output = (popt, )
    
    return output

def Homography_solve(xin, yin, xout, yout):

    pin = np.vstack([xin, yin])
    pout = np.vstack([xout, yout])
    # pin, pout �이�같고, m=2, n>4 �어�함 
    n= pin.shape[1]
    x = pout[0, :]; y = pout[1,:]; X = pin[0,:]; Y = pin[1,:]
    rows0 = np.zeros((3, n))
    rowsXY = -1 * np.vstack([X, Y, np.ones((1,n))])
    hx = np.vstack([rowsXY, rows0, x*X,x*Y,x])
    hy = np.vstack([rows0, rowsXY,y*X, y*Y,y])
    h = np.hstack( [hx, hy])

    U, s, V = np.linalg.svd(h, full_matrices = True)    

    h = np.array(U[:,8])
    #h = U[:,8].reshape(3,3)

    return (h,xin,yin,xout,yout)
    



## FFT ##
# x: data n: span, T: data sampling interval
def EPhaseFFT(x, n, t):
    N = n[0]
    T = t[0]
    frequency = np.fft.rfftfreq(N, d=T)
    amplitude = np.fft.rfft(x,n=N,norm=None) #complex
    #magnitude = np.abs(amplitude)/n*2
    magnitude = np.abs(amplitude)
    phase = np.angle(amplitude) #(-pi, pi]
    #phase = np.angle(amplitude)*180/np.pi
    n_array = np.array([magnitude.shape[0]], dtype = np.int32)

    return (magnitude, phase, frequency, n_array)
    




## AlignFunXIncrease ##
def AlignFunXIncrease2(x, offset, refAbs, refMl, ratio):           
    #w = 0.5/1000.
    #d = 0.5/1000.
    w = 0.75/1000.
    d = 0.75/1000.
    N = len(x)       
    refLeft = refAbs
    refRight = refMl
    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
        
    #ratioRight = 0.5
    ratioRight = ratio
    ratioLeft = 1-ratioRight
        
    for i in range(N):
        if xprim[i] < -(w + d / 2):
            y[i] = refLeft
        elif xprim[i] >= -(w + d / 2) and xprim[i] < -d / 2:
            #y[i] = (refRight - refLeft) / w / 2 * (xprim[i] + d / 2) + (refLeft + refRight)/2
            y[i] = (ratioRight*refRight + (ratioLeft-1)*refLeft) / w * (xprim[i] + d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= -d / 2 and xprim[i] < d / 2:
            y[i] = ratioRight*refRight + ratioLeft*refLeft
        elif xprim[i] >= d / 2 and xprim[i] < (w + d / 2):
            y[i] = ((1-ratioRight)*refRight - ratioLeft*refLeft) / w  * (xprim[i] - d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= (w + d / 2):
            y[i] = refRight
        else:
            y[i] = 0
    return y


## AlignFunXDecrease ##
def AlignFunXDecrease2(x, offset, refAbs, refMl, ratio):        
    #w = 0.5/1000.
    #d = 0.5/1000.
    w = 0.75/1000.
    d = 0.75/1000.
    N = len(x)    
    refLeft = refMl
    refRight = refAbs
    xprim = x - offset
    y = np.zeros((N,), dtype=np.float64)
               
    #ratioLeft = 0.5
    ratioLeft = ratio
    ratioRight = 1-ratioLeft

    for i in range(N):
        if xprim[i] < -(w + d / 2):
            y[i] = refLeft
        elif xprim[i] >= -(w + d / 2) and xprim[i] < -d / 2:
            #y[i] = (refRight - refLeft) / w /2 * (xprim[i] + d / 2) + (refLeft + refRight) / 2 
            y[i] = (ratioRight*refRight + (ratioLeft-1)*refLeft) / w * (xprim[i] + d / 2) + (ratioLeft*refLeft + ratioRight*refRight)  
        elif xprim[i] >= -d / 2 and xprim[i] < d / 2:
            #y[i] = (refRight + refLeft) /2
            y[i] = (ratioRight*refRight + ratioLeft*refLeft)
        elif xprim[i] >= d / 2 and xprim[i] < (w + d / 2):
            #y[i] = (refRight - refLeft) / w /2* (xprim[i] - d / 2) + (refLeft + refRight) /2
            y[i] = ((1-ratioRight)*refRight - ratioLeft*refLeft) / w * (xprim[i] - d / 2) + (ratioLeft*refLeft + ratioRight*refRight)
        elif xprim[i] >= (w + d / 2):
            y[i] = refRight
        else:
            y[i] = 0

    return y


## FineAlignCurveFit ##
def FineAlignCurveFit2(pos, intensity, dir, slope, refAbs, refMl):
    #  �수 �력� �� np.array 
    init = [(pos[0] + pos[-1]) / 2, refAbs[0], refMl[0], 0.5]

    if dir == 0 and slope == 1:        
        popt, pcov = curve_fit(AlignFunXIncrease2, pos, intensity, p0 =init)
    if dir == 0 and slope == -1:                
        popt, pcov = curve_fit(AlignFunXDecrease2, pos, intensity, p0 =init)
    if dir == 1 and slope == 1:        
        popt, pcov = curve_fit(AlignFunYIncrease2, pos, intensity, p0 =init)
    if dir == 1 and slope == -1:        
        popt, pcov = curve_fit(AlignFunYDecrease2, pos, intensity, p0 =init)

    targetIntensity = np.zeros((1,), dtype=np.float64)
    targetPos = np.zeros((1,), dtype=np.float64)
    targetPos[0] = popt[0]

    #�시
    if dir == 0 and slope == 1:
        intensity_fit = AlignFunXIncrease2(pos,*popt)       
        targetIntensity[0]  = AlignFunXIncrease2(targetPos,*popt);        
    if dir == 0 and slope == -1:
        intensity_fit = AlignFunXDecrease2(pos,*popt)
        targetIntensity[0]  = AlignFunXDecrease2(targetPos,*popt);        
    if dir == 1 and slope == 1:
        intensity_fit = AlignFunYIncrease(pos,*popt)
        targetIntensity[0]  = AlignFunYIncrease2(targetPos,*popt);        
    if dir == 1 and slope == -1:
        intensity_fit = AlignFunYDecrease(pos,*popt)
        targetIntensity[0]  = AlignFunYDecrease2(targetPos,*popt);        
        
    # �시    
    outPos = pos
    outIntensity = intensity
    outDir = dir
    outSlope = slope
    outRefAbs = refAbs
    outRefMl = refMl
    outSize = np.array(len(pos),dtype =int)
    

    
    output = (outPos, outIntensity, outDir, outSlope, outRefAbs, outRefMl, outSize, popt, intensity_fit, targetIntensity)

    #output = (popt, )
    
    return output


def ReferenceBsTableSearchOld(lamda, tranmittance_RBS, reflectance_RBS, lamda_min, lamda_max, target_transmittance):

    #outSize = np.array(len(pos),dtype =int)
    #outIntensity2 = intensity2
    #outDir = dir
    #outDir = 8.7

    _lamda = lamda
    _tranmittance_RBS = tranmittance_RBS
    _reflectance_RBS=reflectance_RBS
    _lamda_min = lamda_min
    _lamda_max = lamda_max
    _target_transmittance = target_transmittance

    
    WaveLength = np.array([1.1])
    Reflectance = np.array([1.5])
    
    
    output =(WaveLength, Reflectance)

    return output


#20220223
def ReferenceBsTableSearch_old(lamda, tranmittance_RBS, reflectance_RBS, lamda_min, lamda_max, target_transmittance):

    boolArr = (lamda >= lamda_min) & (lamda <= lamda_max)

    lamda_new = lamda[boolArr]
    tranmittance_new = tranmittance_RBS[boolArr]
    reflectance_new = reflectance_RBS[boolArr]


    #Fitting T
    coeff_T  = np.polyfit(lamda_new, tranmittance_new, 3) #ax^3+bx^2+cx+d
    ffit_T = np.poly1d(coeff_T)


    coeff_root = coeff_T.copy()
    coeff_root[3] = coeff_root[3]-target_transmittance

    roots = np.roots(coeff_root)
    roots_real = roots.real[abs(roots.imag)<1e-5]
    target_lamda = roots_real[(roots_real >= lamda_min) & (roots_real <= lamda_max) ]


    #Fitting R
    coeff_R  = np.polyfit(lamda_new, reflectance_new, 3) #ax^3+bx^2+cx+d
    ffit_R = np.poly1d(coeff_R)

    target_reflectance = ffit_R(target_lamda)

    output = (np.array([target_lamda[0]]), np.array([target_reflectance[0]]))

    return output


def FuncPoly2(x, a, b, c):
    return a * x*x +b * x + c

def FuncPoly3(x, a, b, c, d):
    return  a * x*x*x +b * x * x + c * x + d


def ReferenceBsTableSearch(lamda, tranmittance_RBS, reflectance_RBS, lamda_min, lamda_max, target_transmittance):

    boolArr = (lamda >= lamda_min) & (lamda <= lamda_max)

    lamda_new = lamda[boolArr]
    tranmittance_new = tranmittance_RBS[boolArr]
    reflectance_new = reflectance_RBS[boolArr]

    t_min = np.min(tranmittance_new);
    t_max = np.max(tranmittance_new);

    lamda_min = np.min(lamda_new);
    lamda_max = np.max(lamda_new);

    if target_transmittance >= t_min and target_transmittance <=t_max:        
        #Fitting T
        f_t = interpolate.interp1d( tranmittance_new, lamda_new, kind='quadratic') 
        target_lamda = f_t(target_transmittance)
    else:
        target_lamda = -1.0

    #Fitting R
    if target_lamda >= lamda_min and target_lamda <=lamda_max:
        f_r = interpolate.interp1d( lamda_new, reflectance_new, kind='quadratic') 
        target_reflectance = f_r(target_lamda)
    else:
        target_reflectance = -2.0
    
    #target_lamda = -1.0
    #target_reflectance = -2.0
        
    output = (np.array([target_lamda]), np.array([target_reflectance]))
            
    return output

def reconstruct_image(X,Y,experimental_image,X_original,Y_original,num_scans):

    subarray_length = int(len(X)/int(num_scans))
    n = 0
    reconstructed_image = np.zeros(len(X_original))
    while n < int(num_scans):
        points = np.array( (X[n*subarray_length:(n+1)*subarray_length].flatten(), Y[n*subarray_length:(n+1)*subarray_length].flatten()) ).T
        values = experimental_image[n*subarray_length:(n+1)*subarray_length].flatten()
        reconstructed_image += griddata(points,values,(X_original,Y_original))/float(num_scans)
        n+=1

    return reconstructed_image
