# -*- coding: utf-8 -*-
"""
Created on Sun Aug  4 10:34:31 2019

@author: user
"""
import numpy as np
import scipy.io
from scipy import interpolate
from scipy.ndimage import gaussian_filter

def interp1(x,y,xi):
    f_interp = interpolate.interp1d(x,y)
    yi = f_interp(xi)
    return yi
    
def reconstruct_image(X,Y,experimental_image,X_original,Y_original,num_scans):
    subarray_length = int(len(X)/int(num_scans))
    n = 0
    reconstructed_image = np.zeros(len(X_original))
    while n < int(num_scans):
        points = np.array( (X[n*subarray_length:(n+1)*subarray_length].flatten(), Y[n*subarray_length:(n+1)*subarray_length].flatten()) ).T
        reconstructed_image += scipy.interpolate.griddata(points,experimental_image[n*subarray_length:(n+1)*subarray_length].flatten(),(X_original,Y_original))/float(num_scans)        
        n+=1
    return reconstructed_image

def filter_image(X,my_sigma):
    col_mean = np.nanmean(X, axis=0)
    inds_nan = np.where(np.isnan(col_mean))
    np.put(col_mean, inds_nan, 0.0)
    inds = np.where(np.isnan(X))
    X[inds] = np.take(col_mean, inds[1])
    output_image = gaussian_filter(X,sigma=my_sigma)
    return output_image

def reconstruct_image_cubic(X,Y,experimental_image,X_original,Y_original,num_scans):
    subarray_length = int(len(X)/int(num_scans))
    n = 0
    reconstructed_image = np.zeros(len(X_original))
    while n < int(num_scans):
        points = np.array( (X[n*subarray_length:(n+1)*subarray_length].flatten(), Y[n*subarray_length:(n+1)*subarray_length].flatten()) ).T
        reconstructed_image += scipy.interpolate.griddata(points,experimental_image[n*subarray_length:(n+1)*subarray_length].flatten(),(X_original,Y_original), method='cubic')/float(num_scans)        
        n+=1
    return reconstructed_image


    
def reconstruct_image_remove_nan(X,Y,experimental_image,X_original,Y_original):     
    length = int(len(X))  
    reconstructed_image = np.zeros(len(X_original))
    
    points = np.array( (X[0:length].flatten(), Y[0:length].flatten())).T
    reconstructed_image = scipy.interpolate.griddata(points,experimental_image[0:length].flatten(),(X_original,Y_original))

    if np.any(np.isnan(reconstructed_image)):
        nans = numpy.isnan(reconstructed_image)        
        reconstructed_image[nans] = scipy.interpolate.griddata(points,experimental_image[0:length].flatten(),(X_original[nans],Y_original[nans]), method='nearest')
                           
    return reconstructed_image