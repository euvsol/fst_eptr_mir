﻿#pragma once
#include "resource.h"

#include <locale.h>
#include <queue>
#include <mutex>

using namespace std;

#define LOG_ADD_MAX_LIMIT	1000
#define LOG_BUFFER_LIMIT	1000000

// LogTabDlg 대화 상자

class CLogTabDlg : public CDialogEx
{
	DECLARE_DYNAMIC(CLogTabDlg)

public:
	CLogTabDlg(CWnd* pFramework, CWnd* pParents, int nPosX, int nPosY, int nDialogWidth, int nDialogHeight);   // 표준 생성자입니다.
	virtual ~CLogTabDlg();

	// 대화 상자 데이터입니다.
	enum { IDD = IDD_TAB_DIALOG };

public:
	CWnd*			m_pFramework;
	CWnd*			m_pParents;

	HBRUSH m_brushsPaint;

	int	m_nPosX;
	int	m_nPosY;
	int	m_nDialogWidth;
	int	m_nDialogHeight;

	CString m_strTabName;
	CString m_strFolderPath;
	CString m_strProgramName;

	CListBox m_ctlLogViewer;

	unsigned int	m_nBufferIndex;
	CString			m_strLogMessage;
	CString			m_strFileMessage;

	CString			m_strTabBuffer[LOG_BUFFER_LIMIT];
	unsigned int	m_nBufferCount;

	BOOL			m_bUpdateFlag;

	BOOL GetUpdateFlag() { return m_bUpdateFlag;  }
	void SetUpdateFlag(BOOL bFlag) { m_bUpdateFlag = bFlag; }

	unsigned int GetBufferIndex() { return m_nBufferIndex % LOG_BUFFER_LIMIT; }
	void SetBufferIndex() { m_nBufferIndex++; }

	unsigned int GetBufferCount() { return m_nBufferCount % LOG_BUFFER_LIMIT; }
	void SetBufferCount() { m_nBufferCount++; }

	CWinThread*				m_pThread;
	HANDLE					m_hEvent;
	BOOL					m_bThreadStatus;
	CRITICAL_SECTION		m_pThreadCritical;
	recursive_mutex			m_Mutex_B;
	BOOL ReCreateThread();
	BOOL DeleteThread();
	BOOL InitDynamicVariable();
	BOOL Run(int nSequence, BOOL bMessage);
	static UINT Thread(LPVOID lpParam);
	BOOL StartThread();
	BOOL StopThread();
	void SetThreadStatus(BOOL bStatus) { m_bThreadStatus = bStatus; }
	BOOL GetThreadStatus()			   { return m_bThreadStatus;	}

public:
	BOOL SetWindowPosDlg(int cx, int cy);
	BOOL SetTabName(CString strTabName);
	BOOL SetProgramName(CString strProgramName);
	BOOL SetLogFolderPath(CString strPath);
	BOOL AddLog(CString* strMessage);
	BOOL CreateFile(CFile* cfile, CString strPath, int nFIleMode);
	BOOL WriteFile(CFile* cfile, CString strText);
	BOOL CloseFile(CFile* cfile);

public:
	void m_fnListBoxAddMessage(CString* strMessage);
	CString m_fnGetCurrentTimeFunction();
	CString m_fnGetCurruntDateFunction();
	void m_fnCreateHorizontalScroll();
	void m_fnFileAddMessage(CString* strMessage);
	CString m_fGetFileAndCheck();

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};
