#pragma once

#include "resource.h"
#include "stdafx.h"

#include "LogTabDlg.h"

#include <vector>
#include <queue>
#include <mutex>

using namespace std;

#define LOG_ADD_MAX_LIMIT	1000
#define LOG_BUFFER_LIMIT	1000000

class CMainDialog : public CDialogEx
{
	DECLARE_DYNAMIC(CMainDialog)

public:
	CMainDialog(CWnd* pFramework, CWnd* pParents, unsigned int nPosX, unsigned int nPosY, unsigned int nDialogWidth, unsigned int nDialogHeight);
	virtual ~CMainDialog();

	enum { IDD = IDD_MAIN_DIALOG };

public:
	CWnd*			m_pFramework;
	CWnd*			m_pParents;
	unsigned int	m_nPosX;
	unsigned int	m_nPosY;
	unsigned int	m_nDialogWidth;
	unsigned int	m_nDialogHeight;

public:
	CTabCtrl m_Tab;

	CString m_strFolderPath;
	CString m_strProgramName;

	unsigned int	m_nBufferIndex;
	CString			m_strMessage;
	unsigned int	m_nTabAddress;
	unsigned int	m_nTabIndex;
	CLogTabDlg*		m_pTabDlg;

	unsigned int	m_nTabCount;

	CString			m_strMainBuffer[LOG_BUFFER_LIMIT];
	unsigned int	m_nMainBuffer[LOG_BUFFER_LIMIT];
	unsigned int	m_nBufferCount;

	unsigned int GetBufferIndex() { return m_nBufferIndex % LOG_BUFFER_LIMIT; }
	void SetBufferIndex() { m_nBufferIndex++; }

	unsigned int GetBufferCount() { return m_nBufferCount % LOG_BUFFER_LIMIT; }
	void SetBufferCount() { m_nBufferCount++; }

	vector<pair<CLogTabDlg*, unsigned int>> m_vecTabDlg;

	CWinThread*				m_pThread;
	HANDLE					m_hEvent;
	BOOL					m_bThreadStatus;
	CRITICAL_SECTION		m_pThreadCritical;
	recursive_mutex			m_Mutex_A;
	BOOL ReCreateThread();
	BOOL DeleteThread();
	BOOL InitDynamicVariable();
	BOOL Run(int nSequence, BOOL bMessage);
	static UINT Thread(LPVOID lpParam);
	BOOL StartThread();
	BOOL StopThread();
	void SetThreadStatus(BOOL bStatus) { m_bThreadStatus = bStatus; }
	BOOL GetThreadStatus()			   { return m_bThreadStatus;	}

public:
	BOOL CreateTabDlg(unsigned int nTabAddress, CString strTabName);
	BOOL SetWindowPosDlg(int cx, int cy);
	BOOL AddLog(unsigned int nTabAddress, CString strMessage);
	BOOL SetTabControlPos(unsigned int nTabAddress);
	BOOL SetProgramName(CString strProgramName);
	BOOL SetLogFolderPath(CString strPath);

public:
	CLogTabDlg* m_fnFindTabIndexItem(unsigned int nTabAddress, unsigned int* nTabIndex);
	void m_fnSetTabPosShowHide();
	void m_fnTabDlgClose();

public:

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();

public:
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnTcnSelchangeTabLogviewer(NMHDR *pNMHDR, LRESULT *pResult);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

