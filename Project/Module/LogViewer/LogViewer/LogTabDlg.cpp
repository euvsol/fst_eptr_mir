﻿// LogTabDlg.cpp: 구현 파일
//

#include "stdafx.h"
#include "LogTabDlg.h"
#include "afxdialogex.h"


// LogTabDlg 대화 상자

IMPLEMENT_DYNAMIC(CLogTabDlg, CDialogEx)

CLogTabDlg::CLogTabDlg(CWnd* pFramework, CWnd* pParents, int nPosX, int nPosY, int nDialogWidth, int nDialogHeight)
{
	m_pFramework	= pFramework;
	m_pParents		= pParents;
	m_nPosX			= nPosX;
	m_nPosY			= nPosY;
	m_nDialogWidth	= nDialogWidth;
	m_nDialogHeight = nDialogHeight;
	m_strTabName	= _T("NAME");

	m_pThread		= nullptr;
	m_hEvent		= NULL;
	m_bThreadStatus = FALSE;
	ReCreateThread();
	StartThread();

	m_nBufferCount = 0;
	m_nBufferIndex = 0;
	m_strLogMessage = _T("");
	m_strFileMessage = _T("");

	m_bUpdateFlag = FALSE;

	m_brushsPaint = CreateSolidBrush(RGB(53, 53, 53));

	for (int i = 0; i < LOG_BUFFER_LIMIT; i++) { m_strTabBuffer[i] = _T(""); }
}

CLogTabDlg::~CLogTabDlg()
{
	DeleteThread();

	if (m_brushsPaint != NULL) { DeleteObject(m_brushsPaint); m_brushsPaint = NULL; }
}

void CLogTabDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_LOG, m_ctlLogViewer);
}


BEGIN_MESSAGE_MAP(CLogTabDlg, CDialogEx)
	ON_WM_SIZE()
	ON_WM_CTLCOLOR()
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()

// LogTabDlg 메시지 처리기

UINT CLogTabDlg::Thread(LPVOID lpParam)
{
	CLogTabDlg* pThread = (CLogTabDlg*)lpParam;

	while (1)
	{
		WaitForSingleObject(pThread->m_hEvent, INFINITE);
		pThread->SetThreadStatus(TRUE);
		pThread->Run(NULL, FALSE);
		pThread->SetThreadStatus(FALSE);
		pThread->InitDynamicVariable();
		ResetEvent(pThread->m_hEvent);
	}

	return FALSE;
}

BOOL CLogTabDlg::Run(int nSequence, BOOL bMessage)
{
	while (GetThreadStatus())
	{
		while (m_nBufferIndex != m_nBufferCount)
		{
			m_strLogMessage = m_strTabBuffer[GetBufferIndex()];
			m_fnListBoxAddMessage(&m_strLogMessage);
			m_fnFileAddMessage(&m_strLogMessage);
			SetBufferIndex();
		}
		Sleep(1);
	}

	return TRUE;
}

BOOL CLogTabDlg::StartThread()
{
	SetEvent(m_hEvent);
	return TRUE;
}

BOOL CLogTabDlg::StopThread()
{
	SetThreadStatus(FALSE);
	return TRUE;
}

BOOL CLogTabDlg::ReCreateThread()
{
	DeleteThread();

	if (m_pThread == nullptr)
	{
		m_hEvent = CreateEventA(NULL, FALSE, FALSE, NULL);
		ResetEvent(m_hEvent);
		m_pThread = AfxBeginThread(Thread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
		m_pThread->ResumeThread();
	}

	return TRUE;
}

BOOL CLogTabDlg::DeleteThread()
{
	StopThread();
	Sleep(100);
	if (m_pThread != nullptr)
	{
		InitDynamicVariable();
		DWORD dw;
		::GetExitCodeThread(m_pThread->m_hThread, &dw);
		if (dw == STILL_ACTIVE)
		{
			m_pThread->SuspendThread();
			TerminateThread(m_pThread->m_hThread, 0);
			CloseHandle(m_pThread->m_hThread);
		}
		m_pThread->m_hThread = NULL;
		delete m_pThread;
		m_pThread = nullptr;
	}

	if (m_hEvent != NULL)
	{
		CloseHandle(m_hEvent);
		m_hEvent = NULL;
	}

	return TRUE;
}

BOOL CLogTabDlg::InitDynamicVariable()
{
	return TRUE;
}

BOOL CLogTabDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
				  // 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CLogTabDlg::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CLogTabDlg::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	CRect rc;
	GetClientRect(rc);

	if (m_ctlLogViewer.GetSafeHwnd()) { m_ctlLogViewer.SetWindowPos(NULL, 0, 0, rc.Width(), rc.Height(), SWP_NOZORDER); }
}

BOOL CLogTabDlg::AddLog(CString* strMessage)
{
	m_Mutex_B.lock();
	m_strTabBuffer[GetBufferCount()] = *strMessage;
	m_nBufferCount++;
	m_Mutex_B.unlock();

	return TRUE;
}

BOOL CLogTabDlg::SetWindowPosDlg(int cx, int cy)
{
	SetWindowPos(NULL, m_nPosX, m_nPosY, m_nDialogWidth + cx, m_nDialogHeight + cy, SWP_NOZORDER);
	return TRUE;
}

BOOL CLogTabDlg::SetTabName(CString strTabName)
{
	m_strTabName = strTabName;
	return TRUE;
}

void CLogTabDlg::m_fnListBoxAddMessage(CString* strMessage)
{
	if (m_ctlLogViewer.GetSafeHwnd() != nullptr)
	{
		unsigned int nCount = m_ctlLogViewer.GetCount();

		if (GetUpdateFlag())
		{
			m_ctlLogViewer.SetRedraw(FALSE);
			while (nCount > LOG_ADD_MAX_LIMIT - 1) { { m_ctlLogViewer.DeleteString(0); } }
			m_ctlLogViewer.AddString((*strMessage));
			m_ctlLogViewer.SetCurSel(m_ctlLogViewer.GetCount() - 1);
			m_fnCreateHorizontalScroll();
			m_ctlLogViewer.SetRedraw(TRUE);
		}
		else
		{
			while (nCount > LOG_ADD_MAX_LIMIT - 1) { { m_ctlLogViewer.DeleteString(0); } }
			m_ctlLogViewer.AddString((*strMessage));
			m_ctlLogViewer.SetCurSel(m_ctlLogViewer.GetCount() - 1);
			m_fnCreateHorizontalScroll();
		}
	}
}

void CLogTabDlg::m_fnFileAddMessage(CString* strMessage)
{
	CFile f;
	if (CreateFile(&f, m_fGetFileAndCheck(), 0)) { WriteFile(&f, (*strMessage)); CloseFile(&f); }
}

CString CLogTabDlg::m_fGetFileAndCheck()
{
	CString strFilePath = m_strFolderPath + m_strProgramName + _T("\\") + m_fnGetCurruntDateFunction();

	if (GetFileAttributes((LPCTSTR)strFilePath) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)strFilePath, NULL); }

	CString strFileName		= m_strTabName + _T(".log");
	CString strTotalPath	= strFilePath + _T("\\") + strFileName;

	return strTotalPath;
}

CString CLogTabDlg::m_fnGetCurruntDateFunction()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"%4d%02d%02d",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay);

	return strTime;
}

CString CLogTabDlg::m_fnGetCurrentTimeFunction()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"[%4d-%02d-%02d %02d:%02d:%02d.%03d] : ",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay,
		SystemTime.wHour,
		SystemTime.wMinute,
		SystemTime.wSecond,
		SystemTime.wMilliseconds);

	return strTime;
}

void CLogTabDlg::m_fnCreateHorizontalScroll()
{
	CString str; CSize sz; int dx = 0;

	CDC* pDC = m_ctlLogViewer.GetDC();

	for (int i = 0; i < m_ctlLogViewer.GetCount(); i++)
	{
		m_ctlLogViewer.GetText(i, str);
		sz = pDC->GetTextExtent(str);

		if (sz.cx > dx)
			dx = sz.cx;
	}
	m_ctlLogViewer.ReleaseDC(pDC);

	if (m_ctlLogViewer.GetHorizontalExtent() < dx)
	{
		m_ctlLogViewer.SetHorizontalExtent(dx);
		ASSERT(m_ctlLogViewer.GetHorizontalExtent() == dx);
	}
}

BOOL CLogTabDlg::CreateFile(CFile* cfile, CString strPath, int nFIleMode)
{
	if (!cfile->Open(
		strPath,
		CFile::modeCreate |
		CFile::modeNoTruncate |
		CFile::modeReadWrite))
	{
		return FALSE;
	}
	else { cfile->SeekToEnd(); return TRUE; }
	return FALSE;
}

BOOL CLogTabDlg::WriteFile(CFile* cfile, CString strText)
{
	strText += _T("\r\n");
	cfile->Write(strText, strText.GetLength() * sizeof(TCHAR));
	return TRUE;
}

BOOL CLogTabDlg::CloseFile(CFile* cfile)
{
	if (INVALID_HANDLE_VALUE == (HANDLE)cfile) { return FALSE; }
	else { cfile->Close(); return TRUE; }
	return FALSE;
}

BOOL CLogTabDlg::SetProgramName(CString strProgramName)
{
	m_strProgramName = strProgramName;
	return TRUE;
}

BOOL CLogTabDlg::SetLogFolderPath(CString strPath)
{
	m_strFolderPath = strPath;
	return TRUE;
}


HBRUSH CLogTabDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	UINT nID = pWnd->GetDlgCtrlID();

	if (nCtlColor == CTLCOLOR_LISTBOX)
	{
		if (nID == IDC_LIST_LOG)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(53, 53, 53));
			hbr = m_brushsPaint;
		}
	}

	return hbr;
}

BOOL CLogTabDlg::OnEraseBkgnd(CDC* pDC)
{
//	return CDialogEx::OnEraseBkgnd(pDC);
	return FALSE;
}
