// SixthRxProjectMain.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ObjectDll.h"
#include "LogViewerMain.h"

extern "C" __declspec(dllexport) BOOL FrameworkInstance(CWnd* pFramework, CWnd* pParents)
{
	return ObjectDll.FrameworkInstance(pFramework, pParents);
}

extern "C" __declspec(dllexport) BOOL OnInitialDialog(unsigned int nChannel, unsigned int nPosX, unsigned int nPosY, unsigned int nDialogWidth, unsigned int nDialogHeight)
{
	return ObjectDll.OnInitialDialog(nChannel, nPosX, nPosY, nDialogWidth, nDialogHeight);
}

extern "C" __declspec(dllexport) BOOL CreateCheckFolderPath(CString strFolderPath)
{
	return ObjectDll.CreateCheckFolderPath(strFolderPath);
}

extern "C" __declspec(dllexport) BOOL CreateTabDlg(unsigned int nChannel, int nTabAddress, CString strTabName)
{
	return ObjectDll.CreateTabDlg(nChannel, nTabAddress, strTabName);
}

extern "C" __declspec(dllexport) BOOL OnSize(unsigned int nChannel, UINT nType, int cx, int cy)
{
	return ObjectDll.OnSize(nChannel, nType, cx, cy);
}

extern "C" __declspec(dllexport) BOOL AddLog(unsigned int nChannel, unsigned int nTabAddress, CString strMessage)
{
	return ObjectDll.AddLog(nChannel, nTabAddress, strMessage);
}

extern "C" __declspec(dllexport) BOOL SetTabControlPos(unsigned int nChannel, unsigned int nTabAddress)
{
	return ObjectDll.SetTabControlPos(nChannel, nTabAddress);
}

extern "C" __declspec(dllexport) BOOL SetProgramName(unsigned int nChannel, CString strProgramName)
{
	return ObjectDll.SetProgramName(nChannel, strProgramName);
}

extern "C" __declspec(dllexport) BOOL SetLogFolderPath(unsigned int nChannel, CString strPath)
{
	return ObjectDll.SetLogFolderPath(nChannel, strPath);
}