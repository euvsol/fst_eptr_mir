// DlgTwentiethEx.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MainDialog.h"
#include "afxdialogex.h"


// CDlgTwentiethEx 대화 상자입니다.

IMPLEMENT_DYNAMIC(CMainDialog, CDialogEx)

CMainDialog::CMainDialog(CWnd* pFramework, CWnd* pParents, unsigned int nPosX, unsigned int nPosY, unsigned int nDialogWidth, unsigned int nDialogHeight)
	: CDialogEx(CMainDialog::IDD, pFramework)
{
	m_pFramework	= pFramework;
	m_pParents		= pParents;
	m_nPosX			= nPosX;
	m_nPosY			= nPosY;
	m_nDialogWidth	= nDialogWidth;
	m_nDialogHeight = nDialogHeight;

	m_pThread		= nullptr;
	m_hEvent		= NULL;
	m_bThreadStatus = FALSE;
	ReCreateThread();
	StartThread();

	m_nTabCount = 0;

	m_nBufferCount = 0;
	m_nBufferIndex = 0;
	m_strMessage = _T("");
	m_nTabAddress = 0;
	m_nTabIndex = 0;
	m_pTabDlg = nullptr;

	for (int i = 0; i < LOG_BUFFER_LIMIT; i++) { m_strMainBuffer[i] = _T(""); m_nMainBuffer[i] = 0; }
}

CMainDialog::~CMainDialog()
{
	DeleteThread();

	if (m_vecTabDlg.size() > 0) { m_fnTabDlgClose(); }
}

void CMainDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_LOGVIEWER, m_Tab);
}


BEGIN_MESSAGE_MAP(CMainDialog, CDialogEx)
	ON_WM_SIZE()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_LOGVIEWER, &CMainDialog::OnTcnSelchangeTabLogviewer)
	ON_WM_CREATE()
	ON_WM_ERASEBKGND()
END_MESSAGE_MAP()


// CDlgTwentiethEx 메시지 처리기입니다.

UINT CMainDialog::Thread(LPVOID lpParam)
{
	CMainDialog* pThread = (CMainDialog*)lpParam;

	while (1)
	{
		WaitForSingleObject(pThread->m_hEvent, INFINITE);
		pThread->SetThreadStatus(TRUE);
		pThread->Run(NULL, FALSE);
		pThread->SetThreadStatus(FALSE);
		pThread->InitDynamicVariable();
		ResetEvent(pThread->m_hEvent);
	}

	return FALSE;
}

BOOL CMainDialog::Run(int nSequence, BOOL bMessage)
{
	while (GetThreadStatus())
	{
		while (m_nBufferIndex != m_nBufferCount)
		{
			m_strMessage = m_strMainBuffer[GetBufferIndex()];
			m_nTabAddress = m_nMainBuffer[GetBufferIndex()];
			m_pTabDlg = m_fnFindTabIndexItem(m_nTabAddress, &m_nTabIndex);
			m_pTabDlg->AddLog(&m_strMessage);
			SetBufferIndex();
		}
		Sleep(1);
	}

	return TRUE;
}

BOOL CMainDialog::StartThread()
{
	SetEvent(m_hEvent);
	return TRUE;
}

BOOL CMainDialog::StopThread()
{
	SetThreadStatus(FALSE);
	return TRUE;
}

BOOL CMainDialog::ReCreateThread()
{
	DeleteThread();

	if (m_pThread == nullptr)
	{
		m_hEvent = CreateEventA(NULL, FALSE, FALSE, NULL);
		ResetEvent(m_hEvent);
		m_pThread = AfxBeginThread(Thread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
		m_pThread->ResumeThread();
	}

	return TRUE;
}

BOOL CMainDialog::DeleteThread()
{
	StopThread();
	Sleep(100);
	if (m_pThread != nullptr)
	{
		InitDynamicVariable();
		DWORD dw;
		::GetExitCodeThread(m_pThread->m_hThread, &dw);
		if (dw == STILL_ACTIVE)
		{
			m_pThread->SuspendThread();
			TerminateThread(m_pThread->m_hThread, 0);
			CloseHandle(m_pThread->m_hThread);
		}
		m_pThread->m_hThread = NULL;
		delete m_pThread;
		m_pThread = nullptr;
	}

	if (m_hEvent != NULL)
	{
		CloseHandle(m_hEvent);
		m_hEvent = NULL;
	}

	return TRUE;
}

BOOL CMainDialog::InitDynamicVariable()
{
	return TRUE;
}

BOOL CMainDialog::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

int CMainDialog::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialogEx::OnCreate(lpCreateStruct) == -1)
		return -1;

	return 0;
}

void CMainDialog::OnSize(UINT nType, int cx, int cy)
{
	CDialogEx::OnSize(nType, cx, cy);

	CRect rc;
	GetClientRect(rc);

	if (m_Tab.GetSafeHwnd()) { m_Tab.SetWindowPos(NULL, 0, 0, rc.Width(), rc.Height(), SWP_NOZORDER); }

	if (m_vecTabDlg.size() > 0)
	{
		int nX = cx - m_nDialogWidth;
		int nY = cy - m_nDialogHeight;

		vector<pair<CLogTabDlg*, unsigned int>>::iterator it;

		for (it = m_vecTabDlg.begin(); it != m_vecTabDlg.end(); it++)
		{
			CLogTabDlg* pTabDlg = it->first;

			pTabDlg->SetWindowPosDlg(nX, nY);
		}
	}
}

BOOL CMainDialog::SetWindowPosDlg(int cx, int cy)
{
	SetWindowPos(NULL, m_nPosX, m_nPosY, m_nDialogWidth + cx, m_nDialogHeight + cy, SWP_NOZORDER);
	return TRUE;
}

BOOL CMainDialog::AddLog(unsigned int nTabAddress, CString strMessage)
{
	m_Mutex_A.lock();
	m_strMainBuffer[GetBufferCount()] = strMessage;
	m_nMainBuffer[GetBufferCount()] = nTabAddress;
	m_nBufferCount++;
	m_Mutex_A.unlock();

	return TRUE;
}

BOOL CMainDialog::CreateTabDlg(unsigned int nTabAddress, CString strTabName)
{
	CRect rect;
	m_Tab.GetWindowRect(&rect);

	CLogTabDlg* pTabDlg = new CLogTabDlg(m_pFramework, m_pParents, 0, 20, rect.Width() - 1, rect.Height() - 21);

	pTabDlg->SetTabName(strTabName);
	pTabDlg->SetProgramName(m_strProgramName);
	pTabDlg->SetLogFolderPath(m_strFolderPath);

	pair<CLogTabDlg*, unsigned int> pPair = make_pair(pTabDlg, nTabAddress);

	m_Tab.InsertItem(m_nTabCount++, strTabName);

	pTabDlg->Create(IDD_TAB_DIALOG, &m_Tab);
	pTabDlg->MoveWindow(0, 20, rect.Width() - 1, rect.Height() - 21);
	pTabDlg->ShowWindow(SW_SHOW);

	m_vecTabDlg.push_back(pPair);

	return TRUE;
}

BOOL CMainDialog::SetTabControlPos(unsigned int nTabAddress)
{
	unsigned int nTabIndex = 0;
	CLogTabDlg* pTabDlg = m_fnFindTabIndexItem(nTabAddress, &nTabIndex);
	m_Tab.SetCurSel(nTabIndex);
	m_fnSetTabPosShowHide();
	return TRUE;
}

CLogTabDlg* CMainDialog::m_fnFindTabIndexItem(unsigned int nTabAddress, unsigned int* nTabIndex)
{
	vector<pair<CLogTabDlg*, unsigned int>>::iterator it;

	CLogTabDlg*		pTabDlg		= nullptr;
	unsigned int	pTabAddress = 0;
	unsigned int	nIndex		= 0;

	for (it = m_vecTabDlg.begin(); it != m_vecTabDlg.end(); it++)
	{
		pTabDlg		= it->first;
		pTabAddress = it->second;

		if (pTabAddress == nTabAddress) { *nTabIndex = nIndex; break; }
		else							{ pTabDlg = nullptr; pTabAddress = 0; }
		nIndex++;
	}

	return pTabDlg;
}

void CMainDialog::m_fnSetTabPosShowHide()
{
	int sel = m_Tab.GetCurSel();

	for (int i = 0; i < m_vecTabDlg.size(); i++)
	{
		CLogTabDlg* pTabDlg = m_vecTabDlg.at(i).first;

		if (i == sel) { pTabDlg->m_ctlLogViewer.SetRedraw(TRUE);  pTabDlg->SetUpdateFlag(TRUE);  pTabDlg->ShowWindow(SW_SHOW); }
		else		  { pTabDlg->m_ctlLogViewer.SetRedraw(FALSE); pTabDlg->SetUpdateFlag(FALSE); pTabDlg->ShowWindow(SW_HIDE); }
	}
}

void CMainDialog::OnTcnSelchangeTabLogviewer(NMHDR *pNMHDR, LRESULT *pResult)
{
	int sel = m_Tab.GetCurSel();

	for (int i = 0; i < m_vecTabDlg.size(); i++)
	{
		CLogTabDlg* pTabDlg = m_vecTabDlg.at(i).first;

		if (i == sel) { pTabDlg->m_ctlLogViewer.SetRedraw(TRUE);  pTabDlg->SetUpdateFlag(TRUE);  pTabDlg->ShowWindow(SW_SHOW); }
		else		  { pTabDlg->m_ctlLogViewer.SetRedraw(FALSE); pTabDlg->SetUpdateFlag(FALSE); pTabDlg->ShowWindow(SW_HIDE); }
	}

	*pResult = 0;
}

void CMainDialog::m_fnTabDlgClose()
{
	vector<pair<CLogTabDlg*, unsigned int>>::iterator it;

	CLogTabDlg*		pTabDlg		= nullptr;
	unsigned int	pTabAddress = 0;

	for (it = m_vecTabDlg.begin(); it != m_vecTabDlg.end(); it++)
	{
		pTabDlg		= it->first;
		pTabAddress = it->second;

		delete pTabDlg;
		pTabDlg = nullptr;
	}
	m_vecTabDlg.clear();
	vector<pair<CLogTabDlg*, unsigned int >>().swap(m_vecTabDlg);
}

BOOL CMainDialog::SetProgramName(CString strProgramName)
{
	m_strProgramName = strProgramName;
	return TRUE;
}

BOOL CMainDialog::SetLogFolderPath(CString strPath)
{
	m_strFolderPath = strPath;
	return TRUE;
}

BOOL CMainDialog::OnEraseBkgnd(CDC* pDC)
{
//	return CDialogEx::OnEraseBkgnd(pDC);
	return FALSE;
}
