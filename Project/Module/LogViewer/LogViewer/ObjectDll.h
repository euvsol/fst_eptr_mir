#pragma once

#include "resource.h"

#include "MainDialog.h"

#define MAIN_DIALOG_COUNT 1

class CObjectDll
{
	
public:
	CObjectDll();
	~CObjectDll();

public:
	CWnd*			m_pFramework;
	CWnd*			m_pParents;

	CMainDialog*	m_MainDialog[MAIN_DIALOG_COUNT];

public:
//	(FUNCTION_RET) (FUNCTION_NAME)							(Type* A, Type* B));
	BOOL			FrameworkInstance						(CWnd* pFramework, CWnd* pParents);
	BOOL			OnInitialDialog							(unsigned int nChannel, unsigned int nPosX, unsigned int nPosY, unsigned int nDialogWidth, unsigned int nDialogHeight);
	BOOL			CreateCheckFolderPath					(CString strFolderPath);
	BOOL			CreateTabDlg							(unsigned int nChannel, unsigned int nTabAddress, CString strTabName);
	BOOL			OnSize									(unsigned int nChannel, UINT nType, int cx, int cy);
	BOOL			AddLog									(unsigned int nChannel, unsigned int nTabAddress, CString strMessage);
	BOOL			SetTabControlPos						(unsigned int nChannel, unsigned int nTabAddress);
	BOOL			SetProgramName							(unsigned int nChannel, CString strProgramName);
	BOOL			SetLogFolderPath						(unsigned int nChannel, CString strPath);
	BOOL			SetWindowLock							(unsigned int nChannel, BOOL bFlag);
};

extern class CObjectDll ObjectDll;