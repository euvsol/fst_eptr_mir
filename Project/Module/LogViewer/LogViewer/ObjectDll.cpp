
#include "stdafx.h"
#include "ObjectDll.h"

class CObjectDll ObjectDll;

CObjectDll::CObjectDll()
{
	m_pFramework	= nullptr;
	m_pParents		= nullptr;

	for (int i = 0; i < MAIN_DIALOG_COUNT; i++) { m_MainDialog[i] = nullptr; }
}

CObjectDll::~CObjectDll()
{
	for (int i = 0; i < MAIN_DIALOG_COUNT; i++) { CMainDialog* pDlg = m_MainDialog[i]; delete pDlg; pDlg = nullptr; }
}

BOOL CObjectDll::FrameworkInstance(CWnd* pFramework, CWnd* pParents)
{
	m_pFramework	= pFramework;
	m_pParents		= pParents;
	return TRUE;
}

BOOL CObjectDll::OnInitialDialog(unsigned int nChannel, unsigned int nPosX, unsigned int nPosY, unsigned int nDialogWidth, unsigned int nDialogHeight)
{
	if (m_MainDialog[nChannel] == nullptr)
	{
		m_MainDialog[nChannel] = new CMainDialog(m_pFramework, m_pParents, nPosX, nPosY, nDialogWidth, nDialogHeight);
		m_MainDialog[nChannel]->Create(IDD_MAIN_DIALOG, m_pParents);
		m_MainDialog[nChannel]->MoveWindow(nPosX, nPosY, nDialogWidth, nDialogHeight);
		m_MainDialog[nChannel]->ShowWindow(SW_SHOW);
	}
	return TRUE;
}

BOOL CObjectDll::CreateCheckFolderPath(CString strFolderPath)
{
	if (GetFileAttributes((LPCTSTR)strFolderPath) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)strFolderPath, NULL); }
	return TRUE;
}

BOOL CObjectDll::CreateTabDlg(unsigned int nChannel, unsigned int nTabAddress, CString strTabName)
{
	if (m_MainDialog[nChannel] != nullptr) { m_MainDialog[nChannel]->CreateTabDlg(nTabAddress, strTabName); }
	return TRUE;
}

BOOL CObjectDll::OnSize(unsigned int nChannel, UINT nType, int cx, int cy)
{
	if (m_MainDialog[nChannel] != nullptr) { m_MainDialog[nChannel]->SetWindowPosDlg(cx, cy); }
	return TRUE;
}

BOOL CObjectDll::AddLog(unsigned int nChannel, unsigned int nTabAddress, CString strMessage)
{
	if (m_MainDialog[nChannel] != nullptr){ m_MainDialog[nChannel]->AddLog(nTabAddress, strMessage); }
	return TRUE;
}

BOOL CObjectDll::SetTabControlPos(unsigned int nChannel, unsigned int nTabAddress)
{
	if (m_MainDialog[nChannel] != nullptr) { m_MainDialog[nChannel]->SetTabControlPos(nTabAddress); }
	return TRUE;
}

BOOL CObjectDll::SetProgramName(unsigned int nChannel, CString strProgramName)
{
	if (m_MainDialog[nChannel] != nullptr) { m_MainDialog[nChannel]->SetProgramName(strProgramName); }
	return TRUE;
}

BOOL CObjectDll::SetLogFolderPath(unsigned int nChannel, CString strPath)
{
	if (m_MainDialog[nChannel] != nullptr) { m_MainDialog[nChannel]->SetLogFolderPath(strPath); }
	return TRUE;
}