#pragma once

#include "MainDialog.h"

/*

DLL(Dynamic-Link Library)
- DLL은 마이크로소프트 윈도에서 구현된 동적 라이브러리이다. 
  내부에는 다른 프로그램이 불러서 쓸 수 있는 다양한 함수들을 가지고 있는데, 
  확장DLL인 경우는 클래스를 가지고 있기도 한다.
  사용하는 방법에는 두가지가 있다.

묵시적 링킹(Implicit linking)
- 실행 파일 자체에 어떤 DLL의 어떤 함수를 사용하겠다는 정보를 포함시키고 
  운영체제가 프로그램 실행 시 해당 함수들을 초기화한 후 그것을 이용하는 방법

명시적 링킹(Explicit linking)
- 프로그램이 실행 중일 때 API를 이용하여 DLL 파일이 있는지 검사하고 
  동적으로 원하는 함수만 불러와서 쓰는 방법

*/

// ********* 명시적 링킹 ********* //

#ifdef _LOGVIEWER
	#define EXT_CLASS_LOGVIEWER __declspec(dllexport)
#else
	#define EXT_CLASS_LOGVIEWER __declspec(dllimport)
#endif

// ret  : 리턴 값
// name : 함수 이름
// def  : 괄호 "()"가 들어간 파라미터 정의
#define DEF_EXTERNAL(ret, name, def) private: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_INTERNAL(ret, name, def) public: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_CHECK(function) if	(!function) { AfxMessageBox	(_T("CLogViewer Failed to get function pointer")); }

class EXT_CLASS_LOGVIEWER CLogViewer
{

private:
//				(FUNCTION_RET) (FUNCTION_NAME)						  (Type* A, Type* B));
	DEF_EXTERNAL(BOOL		 , FrameworkInstance					, (CWnd* pFramework, CWnd* pParents));
	DEF_EXTERNAL(BOOL		 , OnInitialDialog						, (unsigned int nChannel, unsigned int nPosX, unsigned int nPosY, unsigned int nDialogWidth, unsigned int nDialogHeight));
	DEF_EXTERNAL(BOOL		 , CreateCheckFolderPath				, (CString strFolderPath));
	DEF_EXTERNAL(BOOL		 , CreateTabDlg							, (unsigned int nChannel, unsigned int nTabAddress, CString strTabName));
	DEF_EXTERNAL(BOOL		 , OnSize								, (unsigned int nChannel, UINT nType, int cx, int cy));
	DEF_EXTERNAL(BOOL		 , AddLog								, (unsigned int nChannel, unsigned int nTabAddress, CString strMessage));
	DEF_EXTERNAL(BOOL		 , SetTabControlPos						, (unsigned int nChannel, unsigned int nTabAddress));
	DEF_EXTERNAL(BOOL		 , SetProgramName						, (unsigned int nChannel, CString strProgramName));
	DEF_EXTERNAL(BOOL		 , SetLogFolderPath						, (unsigned int nChannel, CString strPath));

public:

	//생성자는 꼭 private나 protected로
	CLogViewer(HINSTANCE hInstance)
	{
		if (hInstance)
		{
//		   (FUNCTION_NAME)				  (FUNCTION_TYPE)					GetProcAddress(hInstance, FUNCTION_NAME)					;
			FrameworkInstance			= (fp_FrameworkInstance)			GetProcAddress(hInstance, "FrameworkInstance")				;
			OnInitialDialog				= (fp_OnInitialDialog)				GetProcAddress(hInstance, "OnInitialDialog")				;
			CreateCheckFolderPath		= (fp_CreateCheckFolderPath)		GetProcAddress(hInstance, "CreateCheckFolderPath")			;
			CreateTabDlg				= (fp_CreateTabDlg)					GetProcAddress(hInstance, "CreateTabDlg")					;
			OnSize						= (fp_OnSize)						GetProcAddress(hInstance, "OnSize")							;
			AddLog						= (fp_AddLog)						GetProcAddress(hInstance, "AddLog")							;
			SetTabControlPos			= (fp_SetTabControlPos)				GetProcAddress(hInstance, "SetTabControlPos")				;
			SetProgramName				= (fp_SetProgramName)				GetProcAddress(hInstance, "SetProgramName")					;
			SetLogFolderPath			= (fp_SetLogFolderPath)				GetProcAddress(hInstance, "SetLogFolderPath")				;

			DEF_CHECK(FrameworkInstance);
			DEF_CHECK(OnInitialDialog);
			DEF_CHECK(CreateCheckFolderPath);
			DEF_CHECK(CreateTabDlg);
			DEF_CHECK(OnSize);
			DEF_CHECK(AddLog);
			DEF_CHECK(SetTabControlPos);
			DEF_CHECK(SetProgramName);
			DEF_CHECK(SetLogFolderPath);
		}
	}
	~CLogViewer() {}
};