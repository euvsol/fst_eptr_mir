// SixthRxProjectMain.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ObjectDll.h"
#include "ImageProcessAlgorithmMain.h"

extern "C" __declspec(dllexport) BOOL Interpolation(BYTE* pInputImage, Mat* pOutputImage, int nWidth, int nHeight, int nMagnification, int nQueueIndex, int nQueueMainIndex)
{
	return ObjectDll.Interpolation(pInputImage, pOutputImage, nWidth, nHeight, nMagnification, nQueueIndex, nQueueMainIndex);
}

extern "C" __declspec(dllexport) BOOL Average(vector<BYTE*>*pVecInputImage, Mat * pOutputImage, int nVectorLengh, int nWidth, int nHeight, int nMagnification, int nQueueIndex)
{
	return ObjectDll.Average(pVecInputImage, pOutputImage, nVectorLengh, nWidth, nHeight, nMagnification, nQueueIndex);
}

extern "C" __declspec(dllexport) double ReMap(double dPos, double dSize, double dMin, double dMax)
{
	return ObjectDll.ReMap(dPos, dSize, dMin, dMax);
}

extern "C" __declspec(dllexport) double GetColorMapParula(unsigned int nColor, unsigned int nIndex)
{
	return ObjectDll.GetColorMapParula(nColor, nIndex);
}

extern "C" __declspec(dllexport) double ArrayAverage(unsigned int nSize, double* pdData)
{
	return ObjectDll.ArrayAverage(nSize, pdData);
}

extern "C" __declspec(dllexport) double MovingAverageFilter(double* dInputArray, double* dOutputArray, unsigned int nLength, unsigned int nWeight)
{
	return ObjectDll.MovingAverageFilter(dInputArray, dOutputArray, nLength, nWeight);
}

extern "C" __declspec(dllexport) BOOL WriteRawScanPositionFile(double* pPosX, double* pPosY, double* pDataD1, double* pDataD2, double* pDataD3, double* pDataT, double* pDataR, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName)
{
	return ObjectDll.WriteRawScanPositionFile(pPosX, pPosY, pDataD1, pDataD2, pDataD3, pDataT, pDataR, nImageWidth, nImageHeight, nImageFOV_X, nImageFOV_Y, nScanGridX, nScanGridY, strFolderName);
}

extern "C" __declspec(dllexport) BOOL WriteRawScanPositionFile2(double* pPosX, double* pPosY, double* pDataD1, double* pDataD2, double* pDataD3, double* pDataT, double* pDataR, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName)
{
	return ObjectDll.WriteRawScanPositionFile2(pPosX, pPosY, pDataD1, pDataD2, pDataD3, pDataT, pDataR, nImageWidth, nImageHeight, nImageFOV_X, nImageFOV_Y, nScanGridX, nScanGridY, strFolderName);
}

extern "C" __declspec(dllexport) BOOL WriteRawScanDetectorFile(double* pPosX, double* pPosY, double* pData, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName)
{
	return ObjectDll.WriteRawScanDetectorFile(pPosX, pPosY, pData, nImageWidth, nImageHeight, nImageFOV_X, nImageFOV_Y, nScanGridX, nScanGridY, strFolderName);
}

extern "C" __declspec(dllexport) BOOL CalColorBuffer(Mat* pDataBuffer, uchar* pDisplayBuffer, unsigned int nPosY, unsigned int nImageWidth, unsigned int nImageHeight)
{
	return ObjectDll.CalColorBuffer(pDataBuffer, pDisplayBuffer, nPosY, nImageWidth, nImageHeight);
}

extern "C" __declspec(dllexport) BOOL CalColorBufferScanMode(double* pDataBufferT, double* pDataBufferR, uchar* pDisplayBuffer, double dTransmittance, double dReflectance, unsigned int nPosX, unsigned int nPosY, unsigned int nImageWidth, unsigned int nImageHeight)
{
	return ObjectDll.CalColorBufferScanMode(pDataBufferT, pDataBufferR, pDisplayBuffer, dTransmittance, dReflectance, nPosX, nPosY, nImageWidth, nImageHeight);
}

extern "C" __declspec(dllexport) int StandbyModeMemoryOptimization()
{
	return ObjectDll.StandbyModeMemoryOptimization();
}

extern "C" __declspec(dllexport) BOOL WriteScanDataBinary(double* pD1, double* pD2, double* pD3, double* pT, double* pR, double* pActualX, double* pActualY, double* pStaticX, double* pStaticY, unsigned int nSize, CString strPath)
{
	return ObjectDll.WriteScanDataBinary(pD1, pD2, pD3, pT, pR, pActualX, pActualY, pStaticX, pStaticY, nSize, strPath);
}

extern "C" __declspec(dllexport) BOOL ReadScanDataBinary(double* pD1, double* pD2, double* pD3, double* pT, double* pR, double* pActualX, double* pActualY, double* pStaticX, double* pStaticY, unsigned int nSize, CString strPath)
{
	return ObjectDll.ReadScanDataBinary(pD1, pD2, pD3, pT, pR, pActualX, pActualY, pStaticX, pStaticY, nSize, strPath);
}

extern "C" __declspec(dllexport) BOOL CreateFile_(CFile* cfile, CString strPath, int nFIleMode)
{
	return ObjectDll.CreateFile_(cfile, strPath, nFIleMode);
}

extern "C" __declspec(dllexport) BOOL WriteFile_(CFile* cfile, CString strText)
{
	return ObjectDll.WriteFile_(cfile, strText);
}

extern "C" __declspec(dllexport) BOOL CloseFile_(CFile* cfile)
{
	return ObjectDll.CloseFile_(cfile);
}