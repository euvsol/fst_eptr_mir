#include "StdAfx.h"
#include "ObjectDll.h"

static const double m_dColormapParulaR[256] =
{ 0.2422, 0.2444, 0.2464, 0.2484, 0.2503, 0.2522, 0.2540, 0.2558, 0.2576, 0.2594, 0.2611, 0.2628, 0.2645, 0.2661, 0.2676, 0.2691, 0.2704, 0.2717, 0.2729, 0.2740,
  0.2749, 0.2758, 0.2766, 0.2774, 0.2781, 0.2788, 0.2794, 0.2798, 0.2802, 0.2806, 0.2809, 0.2811, 0.2813, 0.2814, 0.2814, 0.2813, 0.2811, 0.2809, 0.2807, 0.2803,
  0.2798, 0.2791, 0.2784, 0.2776, 0.2766, 0.2754, 0.2741, 0.2726, 0.2710, 0.2691, 0.2670, 0.2647, 0.2621, 0.2591, 0.2556, 0.2517, 0.2473, 0.2424, 0.2369, 0.2311,
  0.2250, 0.2189, 0.2128, 0.2066, 0.2006, 0.1950, 0.1903, 0.1869, 0.1847, 0.1831, 0.1818, 0.1806, 0.1795, 0.1785, 0.1778, 0.1773, 0.1768, 0.1764, 0.1755, 0.1740,
  0.1716, 0.1686, 0.1649, 0.1610, 0.1573, 0.1540, 0.1513, 0.1492, 0.1475, 0.1461, 0.1446, 0.1429, 0.1408, 0.1383, 0.1354, 0.1321, 0.1288, 0.1253, 0.1219, 0.1185,
  0.1152, 0.1119, 0.1085, 0.1048, 0.1009, 0.0964, 0.0914, 0.0855, 0.0789, 0.0713, 0.0628, 0.0535, 0.0433, 0.0328, 0.0234, 0.0155, 0.0091, 0.0046, 0.0019, 0.0009,
  0.0018, 0.0046, 0.0094, 0.0162, 0.0253, 0.0369, 0.0504, 0.0638, 0.0770, 0.0899, 0.1023, 0.1141, 0.1252, 0.1354, 0.1448, 0.1532, 0.1609, 0.1678, 0.1741, 0.1799,
  0.1853, 0.1905, 0.1954, 0.2003, 0.2061, 0.2118, 0.2178, 0.2244, 0.2318, 0.2401, 0.2491, 0.2589, 0.2695, 0.2809, 0.2929, 0.3052, 0.3176, 0.3301, 0.3424, 0.3548,
  0.3671, 0.3795, 0.3921, 0.4050, 0.4184, 0.4322, 0.4463, 0.4608, 0.4753, 0.4899, 0.5044, 0.5187, 0.5329, 0.5470, 0.5609, 0.5748, 0.5886, 0.6024, 0.6161, 0.6297,
  0.6433, 0.6567, 0.6701, 0.6833, 0.6963, 0.7091, 0.7218, 0.7344, 0.7468, 0.7590, 0.7710, 0.7829, 0.7945, 0.8060, 0.8172, 0.8281, 0.8389, 0.8495, 0.8600, 0.8703,
  0.8804, 0.8903, 0.9000, 0.9093, 0.9184, 0.9272, 0.9357, 0.9440, 0.9523, 0.9606, 0.9689, 0.9770, 0.9842, 0.9900, 0.9946, 0.9966, 0.9971, 0.9972, 0.9971, 0.9969,
  0.9966, 0.9962, 0.9957, 0.9949, 0.9938, 0.9923, 0.9906, 0.9885, 0.9861, 0.9835, 0.9807, 0.9778, 0.9748, 0.9720, 0.9694, 0.9671, 0.9651, 0.9634, 0.9619, 0.9608,
  0.9601, 0.9596, 0.9595, 0.9597, 0.9601, 0.9608, 0.9618, 0.9629, 0.9642, 0.9657, 0.9674, 0.9692, 0.9711, 0.9730, 0.9749, 0.9769 };

static const double m_dColormapParulaG[256] = 
{ 0.1504, 0.1534, 0.1569, 0.1607, 0.1648, 0.1689, 0.1732, 0.1773, 0.1814, 0.1854, 0.1893, 0.1932, 0.1972, 0.2011, 0.2052, 0.2094, 0.2138, 0.2184, 0.2231, 0.2280, 
  0.2330, 0.2382, 0.2435, 0.2489, 0.2543, 0.2598, 0.2653, 0.2708, 0.2764, 0.2819, 0.2875, 0.2930, 0.2985, 0.3040, 0.3095, 0.3150, 0.3204, 0.3259, 0.3313, 0.3367, 
  0.3421, 0.3475, 0.3529, 0.3583, 0.3638, 0.3693, 0.3748, 0.3804, 0.3860, 0.3916, 0.3973, 0.4030, 0.4088, 0.4145, 0.4203, 0.4261, 0.4319, 0.4378, 0.4437, 0.4497, 
  0.4559, 0.4620, 0.4682, 0.4743, 0.4803, 0.4861, 0.4919, 0.4975, 0.5030, 0.5084, 0.5138, 0.5191, 0.5244, 0.5296, 0.5349, 0.5401, 0.5452, 0.5504, 0.5554, 0.5605, 
  0.5655, 0.5705, 0.5755, 0.5805, 0.5854, 0.5902, 0.5950, 0.5997, 0.6043, 0.6089, 0.6135, 0.6180, 0.6226, 0.6272, 0.6317, 0.6363, 0.6408, 0.6453, 0.6497, 0.6541, 
  0.6584, 0.6627, 0.6669, 0.6710, 0.6750, 0.6789, 0.6828, 0.6865, 0.6902, 0.6938, 0.6972, 0.7006, 0.7039, 0.7071, 0.7103, 0.7133, 0.7163, 0.7192, 0.7220, 0.7248, 
  0.7275, 0.7301, 0.7327, 0.7352, 0.7376, 0.7400, 0.7423, 0.7446, 0.7468, 0.7489, 0.7510, 0.7531, 0.7552, 0.7572, 0.7593, 0.7614, 0.7635, 0.7656, 0.7678, 0.7699, 
  0.7721, 0.7743, 0.7765, 0.7787, 0.7808, 0.7828, 0.7849, 0.7869, 0.7887, 0.7905, 0.7922, 0.7937, 0.7951, 0.7964, 0.7975, 0.7985, 0.7994, 0.8002, 0.8009, 0.8016, 
  0.8021, 0.8026, 0.8029, 0.8031, 0.8030, 0.8028, 0.8024, 0.8018, 0.8011, 0.8002, 0.7993, 0.7982, 0.7970, 0.7957, 0.7943, 0.7929, 0.7913, 0.7896, 0.7878, 0.7859, 
  0.7839, 0.7818, 0.7796, 0.7773, 0.7750, 0.7727, 0.7703, 0.7679, 0.7654, 0.7629, 0.7604, 0.7579, 0.7554, 0.7529, 0.7505, 0.7481, 0.7457, 0.7435, 0.7413, 0.7392, 
  0.7372, 0.7353, 0.7336, 0.7321, 0.7308, 0.7298, 0.7290, 0.7285, 0.7284, 0.7285, 0.7292, 0.7304, 0.7330, 0.7365, 0.7407, 0.7458, 0.7513, 0.7569, 0.7626, 0.7683, 
  0.7740, 0.7798, 0.7856, 0.7915, 0.7974, 0.8034, 0.8095, 0.8156, 0.8218, 0.8280, 0.8342, 0.8404, 0.8467, 0.8529, 0.8591, 0.8654, 0.8716, 0.8778, 0.8840, 0.8902, 
  0.8963, 0.9023, 0.9084, 0.9143, 0.9203, 0.9262, 0.9320, 0.9379, 0.9437, 0.9494, 0.9552, 0.9609, 0.9667, 0.9724, 0.9782, 0.9839 };

static const double m_dColormapParulaB[256] = 
{ 0.6600, 0.6720, 0.6840, 0.6960, 0.7070, 0.7170, 0.7280, 0.7390, 0.7500, 0.7610, 0.7710, 0.7820, 0.7930, 0.8040, 0.8140, 0.8240, 0.8340, 0.8430, 0.8520, 0.8610, 
  0.8690, 0.8760, 0.8840, 0.8900, 0.8970, 0.9030, 0.9090, 0.9150, 0.9200, 0.9250, 0.9300, 0.9350, 0.9390, 0.9440, 0.9480, 0.9520, 0.9560, 0.9600, 0.9630, 0.9670, 
  0.9700, 0.9730, 0.9760, 0.9790, 0.9810, 0.9840, 0.9860, 0.9880, 0.9890, 0.9910, 0.9920, 0.9930, 0.9940, 0.9950, 0.9960, 0.9970, 0.9980, 0.9990, 0.9990, 0.9990, 
  0.9980, 0.9960, 0.9940, 0.9920, 0.9900, 0.9880, 0.9860, 0.9840, 0.9810, 0.9790, 0.9760, 0.9730, 0.9700, 0.9670, 0.9640, 0.9600, 0.9560, 0.9510, 0.9470, 0.9430, 
  0.9390, 0.9350, 0.9320, 0.9280, 0.9250, 0.9210, 0.9180, 0.9140, 0.9110, 0.9080, 0.9050, 0.9020, 0.8990, 0.8970, 0.8950, 0.8930, 0.8910, 0.8880, 0.8860, 0.8830, 
  0.8800, 0.8770, 0.8730, 0.8690, 0.8650, 0.8600, 0.8560, 0.8510, 0.8460, 0.8400, 0.8350, 0.8290, 0.8240, 0.8180, 0.8120, 0.8060, 0.8000, 0.7940, 0.7870, 0.7810, 
  0.7750, 0.7680, 0.7620, 0.7550, 0.7490, 0.7420, 0.7350, 0.7290, 0.7220, 0.7150, 0.7080, 0.7010, 0.6950, 0.6880, 0.6810, 0.6740, 0.6670, 0.6590, 0.6520, 0.6450, 
  0.6370, 0.6300, 0.6220, 0.6140, 0.6060, 0.5980, 0.5890, 0.5810, 0.5720, 0.5630, 0.5540, 0.5450, 0.5360, 0.5260, 0.5170, 0.5070, 0.4970, 0.4870, 0.4770, 0.4660, 
  0.4560, 0.4450, 0.4340, 0.4230, 0.4120, 0.4010, 0.3900, 0.3790, 0.3690, 0.3580, 0.3480, 0.3370, 0.3260, 0.3150, 0.3050, 0.2940, 0.2830, 0.2720, 0.2620, 0.2520, 
  0.2420, 0.2320, 0.2230, 0.2150, 0.2070, 0.1990, 0.1920, 0.1850, 0.1780, 0.1710, 0.1650, 0.1600, 0.1570, 0.1540, 0.1530, 0.1530, 0.1540, 0.1560, 0.1580, 0.1610, 
  0.1650, 0.1690, 0.1740, 0.1810, 0.1890, 0.1970, 0.2060, 0.2150, 0.2230, 0.2310, 0.2370, 0.2410, 0.2440, 0.2420, 0.2390, 0.2350, 0.2300, 0.2260, 0.2220, 0.2180, 
  0.2130, 0.2090, 0.2050, 0.2010, 0.1970, 0.1930, 0.1900, 0.1870, 0.1840, 0.1810, 0.1780, 0.1750, 0.1720, 0.1690, 0.1660, 0.1630, 0.1600, 0.1580, 0.1550, 0.1530, 
  0.1500, 0.1480, 0.1450, 0.1410, 0.1380, 0.1340, 0.1300, 0.1260, 0.1210, 0.1160, 0.1110, 0.1060, 0.1000, 0.0930, 0.0870, 0.0800 };

typedef enum _SYSTEM_INFORMATION_CLASS
{
	SystemFileCacheInformation = 21,
	SystemMemoryListInformation = 80
};

typedef enum _SYSTEM_MEMORY_LIST_COMMAND
{
	MemoryCaptureAccessedBits,
	MemoryCaptureAndResetAccessedBits,
	MemoryEmptyWorkingSets,
	MemoryFlushModifiedList,
	MemoryPurgeStandbyList,
	MemoryPurgeLowPriorityStandbyList,
	MemoryCommandMax
} SYSTEM_MEMORY_LIST_COMMAND;

typedef struct _SYSTEM_FILECACHE_INFORMATION
{
	SIZE_T CurrentSize;
	SIZE_T PeakSize;
	ULONG PageFaultCount;
	SIZE_T MinimumWorkingSet;
	SIZE_T MaximumWorkingSet;
	SIZE_T CurrentSizeIncludingTransitionInPages;
	SIZE_T PeakSizeIncludingTransitionInPages;
	ULONG TransitionRePurposeCount;
	ULONG Flags;
} SYSTEM_FILECACHE_INFORMATION;

class CObjectDll ObjectDll;

CObjectDll::CObjectDll()
{
}

CObjectDll::~CObjectDll()
{
}

BOOL CObjectDll::Interpolation(BYTE* pInputImage, Mat* pOutputImage, int nWidth, int nHeight, int nMagnification, int nQueueIndex, int nQueueMainIndex)
{
	CString strPathTemp;

	Mat RawSRC = Mat(nHeight, nWidth, CV_8UC3, pInputImage).clone();

	Mat InterpolationSRC;

	resize(RawSRC, InterpolationSRC, cv::Size(nWidth * nMagnification, nHeight * nMagnification), INTER_LINEAR);

	memcpy(pOutputImage->data, InterpolationSRC.data, (nWidth * nMagnification) * (nHeight * nMagnification) * 3);

	return TRUE;
}

BOOL CObjectDll::Average(vector<BYTE*>* pVecInputImage, Mat* pOutputImage, int nVectorLengh, int nWidth, int nHeight, int nMagnification, int nQueueIndex)
{
	Mat MatAverage = Mat::zeros((nHeight * nMagnification), (nWidth * nMagnification), CV_8UC3);

	uchar* cAverageData = new uchar[(nWidth * nMagnification) * (nHeight * nMagnification) * 3];

	int nVectorSize = nVectorLengh;

	unsigned int nValueResult_0 = 0;
	unsigned int nValueResult_1 = 0;
	unsigned int nValueResult_2 = 0;

	memset(cAverageData, 0x00, sizeof((nWidth * nMagnification) * (nHeight * nMagnification) * 3));

	for (int row = 0; row < (nHeight * nMagnification); row++)
	{
		for (int col = 0; col < (nWidth * nMagnification); col++)
		{
			for (int i = 0; i < nVectorSize; i++)
			{
				BYTE* pByte = pVecInputImage->at(i);

				nValueResult_0 += pByte[(row * (nWidth * nMagnification)) * 3 + col * 3 + 0];
				nValueResult_1 += pByte[(row * (nWidth * nMagnification)) * 3 + col * 3 + 1];
				nValueResult_2 += pByte[(row * (nWidth * nMagnification)) * 3 + col * 3 + 2];
			}

			cAverageData[(row * (nWidth * nMagnification)) * 3 + col * 3 + 0] = (nValueResult_0 / nVectorSize);
			cAverageData[(row * (nWidth * nMagnification)) * 3 + col * 3 + 1] = (nValueResult_1 / nVectorSize);
			cAverageData[(row * (nWidth * nMagnification)) * 3 + col * 3 + 2] = (nValueResult_2 / nVectorSize);

			nValueResult_0 = 0;
			nValueResult_1 = 0;
			nValueResult_2 = 0;
		}
	}

	for (int i = 0; i < (nHeight * nMagnification); i++)
	{
		memcpy(MatAverage.data + i * MatAverage.step, &cAverageData[i * (MatAverage.cols * 3)], MatAverage.cols * 3);
	}

	memcpy(pOutputImage->data, MatAverage.data, (nWidth * nMagnification) * (nHeight * nMagnification) * 3);

	delete[] cAverageData;
	cAverageData = nullptr;

	return TRUE;
}

double CObjectDll::ReMap(double dPos, double dSize, double dMin, double dMax)
{
	double dRet = 0.0;

	if		(dPos <= dMin) { dRet = 0.0;   }
	else if (dPos >= dMax) { dRet = 255.0; }
	else				   { dRet = ((dPos * (256 / 256) - dMin) / (dMax - dMin)) * 255; }

	return dRet;
}

double CObjectDll::GetColorMapParula(unsigned int nColor, unsigned int nIndex)
{
	double dRet = 0.0;

	switch (nColor)
	{
		case COLOR_MAP_PARULA_R: { dRet = m_dColormapParulaR[nIndex]; break; }
		case COLOR_MAP_PARULA_G: { dRet = m_dColormapParulaG[nIndex]; break; }
		case COLOR_MAP_PARULA_B: { dRet = m_dColormapParulaB[nIndex]; break; }
	}

	return dRet;
}

double CObjectDll::ArrayAverage(unsigned int nSize, double* pdData)
{
	double sum = 0;

	for (unsigned int i = 0; i < nSize; i++) { sum = sum + pdData[i]; }

	return sum / (double)nSize;
}

BOOL CObjectDll::MovingAverageFilter(double* dInputArray, double* dOutputArray, unsigned int nLength, unsigned int nWeight)
{
	unsigned int nTempWeight = 0;
	unsigned int nCoordIndex = 0;
	unsigned int nCoordLoopIndex = 0;

	double dAverageFIlter = 0;

	double dWeightHalf = floor(((double)nWeight / 2) + 0.5);

	double* dAverageFilter = new double[nLength];

	for (int nIndex = 0; nIndex < nLength; nIndex++)
	{
		if		(nCoordIndex < (dWeightHalf - 1))					  { nTempWeight = dWeightHalf + nCoordIndex; }
		else if ((((nLength) - 1) - nCoordIndex) < (dWeightHalf - 1)) { nTempWeight = dWeightHalf + (((nLength) - 1) - nCoordIndex); }
		else														  { nTempWeight = nWeight; }

		for (int i = 0; i < nTempWeight; i++)
		{
			if		(nCoordIndex < (dWeightHalf - 1))					  { dAverageFIlter += dInputArray[0 + i]; }
			else if ((((nLength) - 1) - nCoordIndex) < (dWeightHalf - 1)) { dAverageFIlter += dInputArray[((nLength) - 1) - i]; }
			else														  { nCoordLoopIndex = nCoordIndex - (dWeightHalf - 1); dAverageFIlter += dInputArray[nCoordLoopIndex + i]; }
		}

		dAverageFIlter /= (double)nTempWeight;
		dAverageFilter[nCoordIndex] = dAverageFIlter;
		dAverageFIlter = 0;
		nCoordIndex++;
	}

	memcpy(dOutputArray, dAverageFilter, sizeof(double) * nLength);

	delete[] dAverageFilter;

	return TRUE;
}

BOOL CObjectDll::WriteRawScanPositionFile(double* pPosX, double* pPosY, double* pDataD1, double* pDataD2, double* pDataD3, double* pDataT, double* pDataR, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName)
{
	auto t = std::thread([&]()
	{

	CFile f;

	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"%4d-%02d-%02d %02d:%02d:%02d.%03d",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay,
		SystemTime.wHour,
		SystemTime.wMinute,
		SystemTime.wSecond,
		SystemTime.wMilliseconds);

	if (CreateFile(&f, GetFileAndCheck(_T("RawScanPosition.csv"), strFolderName), 0))
	{ 
		CString strAdd, strSum, strTemp;

		double dTemp = 0;

		strTemp.Format(_T("Scan Raw Data Start")); WriteFile(&f, strTemp);
		strTemp.Format(_T("Scan Time : %s"), strTime); WriteFile(&f, strTemp);
		strTemp.Format(_T("Image Width : %d"), nImageWidth); WriteFile(&f, strTemp);
		strTemp.Format(_T("Image Height : %d"), nImageHeight); WriteFile(&f, strTemp);
		strTemp.Format(_T("Image FOV X : %d"), nImageFOV_X); WriteFile(&f, strTemp);
		strTemp.Format(_T("Image FOV Y : %d"), nImageFOV_Y); WriteFile(&f, strTemp);
		strTemp.Format(_T("Scan Grid X : %d"), nScanGridX); WriteFile(&f, strTemp);
		strTemp.Format(_T("Scan Grid Y : %d"), nScanGridY); WriteFile(&f, strTemp);
		strTemp.Format(_T("")); WriteFile(&f, strTemp);
		strTemp.Format(_T("Position X Scan Data")); WriteFile(&f, strTemp);

		strAdd.Empty(); strSum.Empty(); strTemp.Empty();

		for (int y = 0; y < nImageHeight; y++)
		{
			for (int x = 0; x < nImageWidth; x++) { dTemp = pPosX[(y * nImageWidth) + x]; strAdd.Format(_T("%lf, "), dTemp); strSum += strAdd; }
			strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
			strAdd.Empty(); strSum.Empty(); strTemp.Empty();
		}

		strTemp.Format(_T("\n")); WriteFile(&f, strTemp);
		strTemp.Format(_T("Position Y Scan Data")); WriteFile(&f, strTemp);

		for (int y = 0; y < nImageHeight; y++)
		{
			for (int x = 0; x < nImageWidth; x++) { dTemp = pPosY[(y * nImageWidth) + x]; strAdd.Format(_T("%lf, "), dTemp); strSum += strAdd; }
			strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
			strAdd.Empty(); strSum.Empty(); strTemp.Empty();
		}

		strTemp.Format(_T("")); WriteFile(&f, strTemp);
		strTemp.Format(_T("D1 Scan Data")); WriteFile(&f, strTemp);

		for (int y = 0; y < nImageHeight; y++)
		{
			for (int x = 0; x < nImageWidth; x++){ dTemp = pDataD1[(y * nImageWidth) + x]; strAdd.Format(_T("%lf, "), dTemp); strSum += strAdd; }
			strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
			strAdd.Empty(); strSum.Empty(); strTemp.Empty();
		}

		strTemp.Format(_T("")); WriteFile(&f, strTemp);
		strTemp.Format(_T("D2 Scan Data")); WriteFile(&f, strTemp);

		for (int y = 0; y < nImageHeight; y++)
		{
			for (int x = 0; x < nImageWidth; x++) { dTemp = pDataD2[(y * nImageWidth) + x]; strAdd.Format(_T("%lf, "), dTemp); strSum += strAdd; }
			strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
			strAdd.Empty(); strSum.Empty(); strTemp.Empty();
		}

		strTemp.Format(_T("")); WriteFile(&f, strTemp);
		strTemp.Format(_T("D3 Scan Data")); WriteFile(&f, strTemp);

		for (int y = 0; y < nImageHeight; y++)
		{
			for (int x = 0; x < nImageWidth; x++) { dTemp = pDataD3[(y * nImageWidth) + x]; strAdd.Format(_T("%lf, "), dTemp); strSum += strAdd; }
			strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
			strAdd.Empty(); strSum.Empty(); strTemp.Empty();
		}

		strTemp.Format(_T("")); WriteFile(&f, strTemp);
		strTemp.Format(_T("Transmittance Scan Data")); WriteFile(&f, strTemp);

		for (int y = 0; y < nImageHeight; y++)
		{
			for (int x = 0; x < nImageWidth; x++) { dTemp = pDataT[(y * nImageWidth) + x]; strAdd.Format(_T("%lf, "), dTemp); strSum += strAdd; }
			strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
			strAdd.Empty(); strSum.Empty(); strTemp.Empty();
		}

		strTemp.Format(_T("")); WriteFile(&f, strTemp);
		strTemp.Format(_T("Reflectance Scan Data")); WriteFile(&f, strTemp);

		for (int y = 0; y < nImageHeight; y++)
		{
			for (int x = 0; x < nImageWidth; x++) { dTemp = pDataR[(y * nImageWidth) + x]; strAdd.Format(_T("%lf, "), dTemp); strSum += strAdd; }
			strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
			strAdd.Empty(); strSum.Empty(); strTemp.Empty();
		}

		strTemp.Format(_T("")); WriteFile(&f, strTemp);
		strTemp.Format(_T("Scan Raw Data Finish")); WriteFile(&f, strTemp);
		strTemp.Format(_T("\n")); WriteFile(&f, strTemp);

		CloseFile(&f); 
	}

	});

	t.join();

	return TRUE;
}

BOOL CObjectDll::WriteRawScanPositionFile2(double* pPosX, double* pPosY, double* pDataD1, double* pDataD2, double* pDataD3, double* pDataT, double* pDataR, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName)
{
	auto t = std::thread([&]()
	{

		CFile f;

		SYSTEMTIME		SystemTime;
		CString			strTime;

		::GetLocalTime(&SystemTime);

		strTime.Format(
			"%4d-%02d-%02d %02d:%02d:%02d.%03d",
			SystemTime.wYear,
			SystemTime.wMonth,
			SystemTime.wDay,
			SystemTime.wHour,
			SystemTime.wMinute,
			SystemTime.wSecond,
			SystemTime.wMilliseconds);

		if (CreateFile(&f, GetFileAndCheck(_T("RawScanPosition2.csv"), strFolderName), 0))
		{
			CString strAdd, strSum, strTemp;

			double dTemp = 0;
			double dTemp2 = 0;
			double dTemp3 = 0;
			double dTemp4 = 0;
			double dTemp5 = 0;
			double dTemp6 = 0;
			double dTemp7 = 0;


			strTemp.Format(_T("Scan Raw Data Start")); WriteFile(&f, strTemp);
			strTemp.Format(_T("Scan Time : %s"), strTime); WriteFile(&f, strTemp);
			strTemp.Format(_T("Image Width : %d"), nImageWidth); WriteFile(&f, strTemp);
			strTemp.Format(_T("Image Height : %d"), nImageHeight); WriteFile(&f, strTemp);
			strTemp.Format(_T("Image FOV X : %d"), nImageFOV_X); WriteFile(&f, strTemp);
			strTemp.Format(_T("Image FOV Y : %d"), nImageFOV_Y); WriteFile(&f, strTemp);
			strTemp.Format(_T("Scan Grid X : %d"), nScanGridX); WriteFile(&f, strTemp);
			strTemp.Format(_T("Scan Grid Y : %d"), nScanGridY); WriteFile(&f, strTemp);
			strTemp.Format(_T("")); WriteFile(&f, strTemp);
			strTemp.Format(_T("Position X Scan Data")); WriteFile(&f, strTemp);

			strAdd.Empty(); strSum.Empty(); strTemp.Empty();

			for (int y = 0; y < nImageHeight; y++)
			{
				for (int x = 0; x < nImageWidth; x++) 
				{ 
					dTemp  = pPosX[(y * nImageWidth) + x]; 
					dTemp2 = pPosY[(y * nImageWidth) + x];
					dTemp3 = pDataD1[(y * nImageWidth) + x];
					dTemp4 = pDataD2[(y * nImageWidth) + x];
					dTemp5 = pDataD3[(y * nImageWidth) + x];
					dTemp6 = pDataT[(y * nImageWidth) + x];
					dTemp7 = pDataR[(y * nImageWidth) + x];
					strAdd.Format(_T("%lf,%lf,%lf,%lf,%lf,%lf,%lf"), dTemp, dTemp2, dTemp3, dTemp4, dTemp5, dTemp6, dTemp7);
					strSum += strAdd; 
					strTemp.Format(_T("%s"), strSum);
					WriteFile(&f, strTemp);
					strAdd.Empty();
					strSum.Empty();
					strTemp.Empty();
				}
			}

		/*	strTemp.Format(_T("\t")); WriteFile(&f, strTemp);
			strTemp.Format(_T("Position Y Scan Data")); WriteFile(&f, strTemp);

			for (int y = 0; y < nImageHeight; y++)
			{
				for (int x = 0; x < nImageWidth; x++) 
				{ 
					dTemp = pPosY[(y * nImageWidth) + x]; 
					strAdd.Format(_T("%lf"), dTemp); 
					strSum += strAdd; 
					strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
					strAdd.Empty(); strSum.Empty(); strTemp.Empty();
				}
			}

			strTemp.Format(_T("")); WriteFile(&f, strTemp);
			strTemp.Format(_T("D1 Scan Data")); WriteFile(&f, strTemp);

			for (int y = 0; y < nImageHeight; y++)
			{
				for (int x = 0; x < nImageWidth; x++) 
				{ 
					dTemp = pDataD1[(y * nImageWidth) + x]; 
					strAdd.Format(_T("%lf"), dTemp); 
					strSum += strAdd; 
					strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
					strAdd.Empty(); strSum.Empty(); strTemp.Empty();
				}
			}

			strTemp.Format(_T("")); WriteFile(&f, strTemp);
			strTemp.Format(_T("D2 Scan Data")); WriteFile(&f, strTemp);

			for (int y = 0; y < nImageHeight; y++)
			{
				for (int x = 0; x < nImageWidth; x++) 
				{ 
					dTemp = pDataD2[(y * nImageWidth) + x]; 
					strAdd.Format(_T("%lf"), dTemp); 
					strSum += strAdd; 
					strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
					strAdd.Empty(); strSum.Empty(); strTemp.Empty();
				}
			}

			strTemp.Format(_T("")); WriteFile(&f, strTemp);
			strTemp.Format(_T("D3 Scan Data")); WriteFile(&f, strTemp);

			for (int y = 0; y < nImageHeight; y++)
			{
				for (int x = 0; x < nImageWidth; x++) 
				{ 
					dTemp = pDataD3[(y * nImageWidth) + x]; 
					strAdd.Format(_T("%lf"), dTemp); 
					strSum += strAdd; 
					strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
					strAdd.Empty(); strSum.Empty(); strTemp.Empty();
				}
			}

			strTemp.Format(_T("")); WriteFile(&f, strTemp);
			strTemp.Format(_T("Transmittance Scan Data")); WriteFile(&f, strTemp);

			for (int y = 0; y < nImageHeight; y++)
			{
				for (int x = 0; x < nImageWidth; x++) 
				{ 
					dTemp = pDataT[(y * nImageWidth) + x]; 
					strAdd.Format(_T("%lf"), dTemp); 
					strSum += strAdd; 
					strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
					strAdd.Empty(); strSum.Empty(); strTemp.Empty();
				}
			}

			strTemp.Format(_T("")); WriteFile(&f, strTemp);
			strTemp.Format(_T("Reflectance Scan Data")); WriteFile(&f, strTemp);

			for (int y = 0; y < nImageHeight; y++)
			{
				for (int x = 0; x < nImageWidth; x++) 
				{ 
					dTemp = pDataR[(y * nImageWidth) + x]; 
					strAdd.Format(_T("%lf"), dTemp); 
					strSum += strAdd; 
					strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
					strAdd.Empty(); strSum.Empty(); strTemp.Empty();
				}
			}*/

			strTemp.Format(_T("")); WriteFile(&f, strTemp);
			strTemp.Format(_T("Scan Raw Data Finish")); WriteFile(&f, strTemp);
			strTemp.Format(_T("\t")); WriteFile(&f, strTemp);

			CloseFile(&f);
		}

	});

	t.join();

	return TRUE;
}

BOOL CObjectDll::WriteRawScanDetectorFile(double* pPosX, double* pPosY, double* pData, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName)
{
	auto t = std::thread([&]()
	{

	CFile f;

	if (CreateFile(&f, GetFileAndCheck(_T("RawScanDetector.txt"), strFolderName), 0))
	{ 
		CString strAdd, strSum, strTemp;

		double dPosX = 0;
		double dPosY = 0;
		double dDetector = 0;
		double dEtc = 0;

		strTemp.Format(_T("[Header]")); WriteFile(&f, strTemp);
		strTemp.Format(_T("FILE TYPE: Filtered: (1)")); WriteFile(&f, strTemp);
		strTemp.Format(_T("%d"), nImageWidth); WriteFile(&f, strTemp);
		strTemp.Format(_T("%d"), nImageHeight); WriteFile(&f, strTemp);
		strTemp.Format(_T("[Image]")); WriteFile(&f, strTemp);

		strAdd.Empty(); strSum.Empty(); strTemp.Empty();

		for (int y = 0; y < nImageHeight; y++)
		{
			for (int x = 0; x < nImageWidth; x++) 
			{ 
				dPosX		= pPosX[(y * nImageWidth) + x];
				dPosY		= pPosY[(y * nImageWidth) + x];
				dDetector	= pData[(y * nImageWidth) + x];
				strAdd.Format(_T("%.3lf\t\t%.3lf\t\t%lf\t\t%lf"), dPosX, dPosY, dDetector, dEtc);
				strSum += strAdd; 
				strTemp.Format(_T("%s"), strSum); WriteFile(&f, strTemp);
				strAdd.Empty(); strSum.Empty(); strTemp.Empty();
			}
		}

		CloseFile(&f); 
	}

	});

	t.join();

	return TRUE;
}

CString CObjectDll::GetFileAndCheck(CString strFileNameFormat, CString strFolderName)
{
	CString strFileScanPath;
	CString strFileScanDatePath;

	TCHAR chFilePath[256] = { 0, };
	GetModuleFileName(NULL, chFilePath, 256);
	
	CString strFolderTemp(chFilePath);
	CString strFolderPath;
	CString strFilePath;
	CString strFileName = _T("MaskImageReconstructor");
	CString strFileTemp = _T("\\");

	strFolderTemp	= strFolderTemp.Left(strFolderTemp.ReverseFind('\\'));
	strFolderTemp	= strFolderTemp.Left(strFolderTemp.ReverseFind('\\'));
	strFolderTemp	= strFolderTemp.Left(strFolderTemp.ReverseFind('\\'));
	strFolderPath	= strFolderTemp + _T("\\Log\\") + strFileName + strFileTemp + GetCurruntDateFunction();

	if (GetFileAttributes((LPCTSTR)strFolderPath) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)strFolderPath, NULL); }

	strFileScanPath = strFolderPath + _T("\\ScanResult");

	if (GetFileAttributes((LPCTSTR)strFileScanPath) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)strFileScanPath, NULL); }

	strFileScanDatePath = strFileScanPath + _T("\\") + strFolderName;

	if (GetFileAttributes((LPCTSTR)strFileScanDatePath) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)strFileScanDatePath, NULL); }

	CString strCreateFilePath = strFileScanDatePath + _T("\\") + strFileNameFormat;

	return strCreateFilePath;
}

BOOL CObjectDll::CreateFile(CFile* cfile, CString strPath, int nFIleMode)
{
	if (!cfile->Open(
		strPath,
		CFile::modeCreate |
		CFile::modeNoTruncate |
		CFile::modeReadWrite))
	{
		return FALSE;
	}
	else { cfile->SeekToEnd(); return TRUE; }
	return FALSE;
}

BOOL CObjectDll::WriteFile(CFile* cfile, CString strText)
{
	strText += _T("\r\n");
	cfile->Write(strText, strText.GetLength() * sizeof(TCHAR));
	return TRUE;
}

BOOL CObjectDll::CloseFile(CFile* cfile)
{
	if (INVALID_HANDLE_VALUE == (HANDLE)cfile) { return FALSE; }
	else { cfile->Close(); return TRUE; }
	return FALSE;
}

CString CObjectDll::GetCurruntDateFunction()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"%4d%02d%02d",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay);

	return strTime;
}

CString CObjectDll::GetCurrentTimeFunction()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"[%4d-%02d-%02d %02d:%02d:%02d.%03d] : ",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay,
		SystemTime.wHour,
		SystemTime.wMinute,
		SystemTime.wSecond,
		SystemTime.wMilliseconds);

	return strTime;
}

BOOL CObjectDll::CalColorBuffer(Mat* pDataBuffer, uchar* pDisplayBuffer, unsigned int nPosY, unsigned int nImageWidth, unsigned int nImageHeight)
{
	Mat MatRsize;

	resize(*pDataBuffer, MatRsize, cv::Size(nImageWidth * IMAGE_MAGNIFICATION, IMAGE_MAGNIFICATION), INTER_LINEAR);

	double* pInterpolationData	= (double*)MatRsize.data;
	double	dTransmittance		= 0;

	unsigned int nValueT = 0;

	for (int i = 0; i < IMAGE_MAGNIFICATION; i++)
	{
		for (int ii = 0; ii < nImageWidth * IMAGE_MAGNIFICATION; ii++)
		{
			dTransmittance	= *&pInterpolationData[(i * nImageWidth * IMAGE_MAGNIFICATION) + ii];
			nValueT			= (unsigned int)ReMap((dTransmittance * 100), 256, 60, 100);

			pDisplayBuffer[((nPosY * IMAGE_MAGNIFICATION) + i) * (nImageWidth * IMAGE_MAGNIFICATION) * 3 + ii * 3 + 0] = ReMap(GetColorMapParula(COLOR_MAP_PARULA_B, nValueT), 256, 0.0, 1.0);
			pDisplayBuffer[((nPosY * IMAGE_MAGNIFICATION) + i) * (nImageWidth * IMAGE_MAGNIFICATION) * 3 + ii * 3 + 1] = ReMap(GetColorMapParula(COLOR_MAP_PARULA_G, nValueT), 256, 0.0, 1.0);
			pDisplayBuffer[((nPosY * IMAGE_MAGNIFICATION) + i) * (nImageWidth * IMAGE_MAGNIFICATION) * 3 + ii * 3 + 2] = ReMap(GetColorMapParula(COLOR_MAP_PARULA_R, nValueT), 256, 0.0, 1.0);
		}
	}

	return TRUE;
}

BOOL CObjectDll::CalColorBufferScanMode(double* pDataBufferT, double* pDataBufferR, uchar* pDisplayBuffer, double dTransmittance, double dReflectance, unsigned int nPosX, unsigned int nPosY,  unsigned int nImageWidth, unsigned int nImageHeight)
{
	pDataBufferT[(nPosY * nImageWidth * 1) + nPosX] = dTransmittance;
	pDataBufferR[(nPosY * nImageWidth * 1) + nPosX] = dReflectance;

	unsigned int nValueT = (unsigned int)ReMap((dTransmittance  * 100), 256, 60, 100);
	unsigned int nValueR = (unsigned int)ReMap((dReflectance	* 100), 256, 60, 100);

	pDisplayBuffer[nPosY * nImageWidth * 3 + nPosX * 3 + 0] = ReMap(GetColorMapParula(COLOR_MAP_PARULA_B, nValueT), 256, 0.0, 1.0);
	pDisplayBuffer[nPosY * nImageWidth * 3 + nPosX * 3 + 1] = ReMap(GetColorMapParula(COLOR_MAP_PARULA_G, nValueT), 256, 0.0, 1.0);
	pDisplayBuffer[nPosY * nImageWidth * 3 + nPosX * 3 + 2] = ReMap(GetColorMapParula(COLOR_MAP_PARULA_R, nValueT), 256, 0.0, 1.0);

	return TRUE;
}

int CObjectDll::StandbyModeMemoryOptimization()
{
	return FlushFileCache();
}

BOOL CObjectDll::SetPrivilege(HANDLE hToken, LPCTSTR lpszPrivilege, BOOL bEnablePrivilege)
{
	TOKEN_PRIVILEGES tp;
	LUID luid;

	if (!LookupPrivilegeValue(NULL, lpszPrivilege, &luid)) { return -1; }

	tp.PrivilegeCount = 1;
	tp.Privileges[0].Luid = luid;
	tp.Privileges[0].Attributes = bEnablePrivilege ? SE_PRIVILEGE_ENABLED : 0;

	if (!AdjustTokenPrivileges(hToken, FALSE, &tp, sizeof(TOKEN_PRIVILEGES), (PTOKEN_PRIVILEGES)NULL, (PDWORD)NULL)) { return -2; }

	if (GetLastError() == ERROR_NOT_ALL_ASSIGNED) { return -3; }

	return FALSE;
}

int CObjectDll::FlushFileCache()
{
	HMODULE ntdll = LoadLibrary("ntdll.dll");

	if (!ntdll) { return -1; }

	NTSTATUS(WINAPI *NtSetSystemInformation)(INT, PVOID, ULONG) = (NTSTATUS(WINAPI *)(INT, PVOID, ULONG))GetProcAddress(ntdll, "NtSetSystemInformation");

	if (!NtSetSystemInformation) { return -2; }

	HANDLE processToken;

	if (OpenProcessToken(GetCurrentProcess(), TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &processToken) == FALSE) { return -3; }

	// Clear File Cache(working set)
	if (!SetPrivilege(processToken, "SeIncreaseQuotaPrivilege", TRUE))
	{
		SYSTEM_FILECACHE_INFORMATION info;
		ZeroMemory(&info, sizeof(info));
		info.MinimumWorkingSet = -1;
		info.MaximumWorkingSet = -1;
		NTSTATUS ret = NtSetSystemInformation(SystemFileCacheInformation, &info, sizeof(info));
		if (ret < 0) { return -1; }
	}
	else { return -1; }

	// Empty Standby List
	if (!SetPrivilege(processToken, "SeProfileSingleProcessPrivilege", TRUE))
	{
		SYSTEM_MEMORY_LIST_COMMAND command = MemoryPurgeStandbyList;
		NTSTATUS ret = NtSetSystemInformation(SystemMemoryListInformation, &command, sizeof(command));
		if (ret < 0) { return -1; }
	}
	else { return -1; }

	return TRUE;
}

BOOL CObjectDll::WriteScanDataBinary(double* pD1, double* pD2, double* pD3, double* pT, double* pR, double* pActualX, double* pActualY, double* pStaticX, double* pStaticY, unsigned int nSize, CString strPath)
{
	CFile fp_w;
	fp_w.Open(strPath, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeWrite | CFile::typeBinary);

	fp_w.Write(pD1, sizeof(double) * nSize);
	fp_w.Write(pD2, sizeof(double) * nSize);
	fp_w.Write(pD3, sizeof(double) * nSize);
	fp_w.Write(pT, sizeof(double) * nSize);
	fp_w.Write(pR, sizeof(double) * nSize);
	fp_w.Write(pActualX, sizeof(double) * nSize);
	fp_w.Write(pActualY, sizeof(double) * nSize);
	fp_w.Write(pStaticX, sizeof(double) * nSize);
	fp_w.Write(pStaticY, sizeof(double) * nSize);

	fp_w.Close();

	return TRUE;
}

BOOL CObjectDll::ReadScanDataBinary(double* pD1, double* pD2, double* pD3, double* pT, double* pR, double* pActualX, double* pActualY, double* pStaticX, double* pStaticY, unsigned int nSize, CString strPath)
{
	CFile fp_r;
	fp_r.Open(strPath, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeRead | CFile::typeBinary);

	pD1 = (double*)malloc(sizeof(double) * nSize);
	pD2 = (double*)malloc(sizeof(double) * nSize);
	pD3 = (double*)malloc(sizeof(double) * nSize);
	pT = (double*)malloc(sizeof(double) * nSize);
	pR = (double*)malloc(sizeof(double) * nSize);
	pActualX = (double*)malloc(sizeof(double) * nSize);
	pActualY = (double*)malloc(sizeof(double) * nSize);
	pStaticX = (double*)malloc(sizeof(double) * nSize);
	pStaticY = (double*)malloc(sizeof(double) * nSize);

	fp_r.Read(pD1, sizeof(double) * nSize);
	fp_r.Read(pD2, sizeof(double) * nSize);
	fp_r.Read(pD3, sizeof(double) * nSize);
	fp_r.Read(pT, sizeof(double) * nSize);
	fp_r.Read(pR, sizeof(double) * nSize);
	fp_r.Read(pActualX, sizeof(double) * nSize);
	fp_r.Read(pActualY, sizeof(double) * nSize);
	fp_r.Read(pStaticX, sizeof(double) * nSize);
	fp_r.Read(pStaticY, sizeof(double) * nSize);

	fp_r.Close();

	return TRUE;
}

BOOL CObjectDll::CreateFile_(CFile* cfile, CString strPath, int nFIleMode)
{
	if (!cfile->Open(
		strPath,
		CFile::modeCreate |
		CFile::modeNoTruncate |
		CFile::modeReadWrite))
	{
		return FALSE;
	}
	else { cfile->SeekToEnd(); return TRUE; }
	return FALSE;
}

BOOL CObjectDll::WriteFile_(CFile* cfile, CString strText)
{
	strText += _T("\r\n");
	cfile->Write(strText, strText.GetLength() * sizeof(TCHAR));
	return TRUE;
}

BOOL CObjectDll::CloseFile_(CFile* cfile)
{
	if (INVALID_HANDLE_VALUE == (HANDLE)cfile) { return FALSE; }
	else { cfile->Close(); return TRUE; }
	return FALSE;
}