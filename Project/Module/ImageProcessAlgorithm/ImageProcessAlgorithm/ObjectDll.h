#pragma once

#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementDefine.h"

#include <process.h>
#include <vector>
#include <ppl.h>
#include <opencv2/opencv.hpp>

using namespace concurrency;
using namespace std;
using namespace cv;

class CObjectDll
{
	
public:
	CObjectDll();
	~CObjectDll();

public:
//	(FUNCTION_RET) (FUNCTION_NAME)							(Type* A, Type* B));
	BOOL			Interpolation							(BYTE* pInputImage, Mat* pOutputImage, int nWidth, int nHeight, int nMagnification, int nQueueIndex, int nQueueMainIndex);
	BOOL			Average									(vector<BYTE*>* pVecInputImage, Mat* pOutputImage, int nVectorLengh, int nWidth, int nHeight, int nMagnification, int nQueueIndex);
	double			ReMap									(double dPos, double dSize, double dMin, double dMax);
	double			GetColorMapParula						(unsigned int nColor, unsigned int nIndex);
	double			ArrayAverage							(unsigned int nSize, double* pdData);
	BOOL			MovingAverageFilter						(double* dInputArray, double* dOutputArray, unsigned int nLength, unsigned int nWeight);
	BOOL			WriteRawScanPositionFile				(double* pPosX, double* pPosY, double* pDataD1, double* pDataD2, double* pDataD3, double* pDataT, double* pDataR, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName);
	BOOL			WriteRawScanPositionFile2				(double* pPosX, double* pPosY, double* pDataD1, double* pDataD2, double* pDataD3, double* pDataT, double* pDataR, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName);
	BOOL			WriteRawScanDetectorFile				(double* pPosX, double* pPosY, double* pData, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName);
	CString			GetFileAndCheck							(CString strFileNameFormat, CString strFolderName);
	BOOL			CreateFile								(CFile* cfile, CString strPath, int nFIleMode);
	BOOL			WriteFile								(CFile* cfile, CString strText);
	BOOL			CloseFile								(CFile* cfile);
	CString			GetCurruntDateFunction					();
	CString			GetCurrentTimeFunction					();
	BOOL			CalColorBuffer							(Mat* pDataBuffer, uchar* pDisplayBuffer, unsigned int nPosY, unsigned int nImageWidth, unsigned int nImageHeight);
	BOOL			CalColorBufferScanMode					(double* pDataBufferT, double* pDataBufferR, uchar* pDisplayBuffer, double dTransmittance, double dReflectance, unsigned int nPosX, unsigned int nPosY, unsigned int nImageWidth, unsigned int nImageHeight);
	int				StandbyModeMemoryOptimization			();
	BOOL			SetPrivilege							(HANDLE hToken, LPCTSTR lpszPrivilege, BOOL bEnablePrivilege);
	int				FlushFileCache							();
	BOOL			WriteScanDataBinary						(double* pD1, double* pD2, double* pD3, double* pT, double* pR, double* pActualX, double* pActualY, double* pStaticX, double* pStaticY, unsigned int nSize, CString strPath);
	BOOL			ReadScanDataBinary						(double* pD1, double* pD2, double* pD3, double* pT, double* pR, double* pActualX, double* pActualY, double* pStaticX, double* pStaticY, unsigned int nSize, CString strPath);
	BOOL			CreateFile_								(CFile* cfile, CString strPath, int nFIleMode);
	BOOL			WriteFile_								(CFile* cfile, CString strText);
	BOOL			CloseFile_								(CFile* cfile);
};

extern class CObjectDll ObjectDll;