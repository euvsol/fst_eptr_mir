#pragma once

#include <vector>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

/*

DLL(Dynamic-Link Library)
- DLL은 마이크로소프트 윈도에서 구현된 동적 라이브러리이다. 
  내부에는 다른 프로그램이 불러서 쓸 수 있는 다양한 함수들을 가지고 있는데, 
  확장DLL인 경우는 클래스를 가지고 있기도 한다.
  사용하는 방법에는 두가지가 있다.

묵시적 링킹(Implicit linking)
- 실행 파일 자체에 어떤 DLL의 어떤 함수를 사용하겠다는 정보를 포함시키고 
  운영체제가 프로그램 실행 시 해당 함수들을 초기화한 후 그것을 이용하는 방법

명시적 링킹(Explicit linking)
- 프로그램이 실행 중일 때 API를 이용하여 DLL 파일이 있는지 검사하고 
  동적으로 원하는 함수만 불러와서 쓰는 방법

*/

// ********* 명시적 링킹 ********* //

#ifdef _IMAGEPROCESSALGORITHM
	#define EXT_CLASS_IMAGEPROCESSALGORITHM __declspec(dllexport)
#else
	#define EXT_CLASS_IMAGEPROCESSALGORITHM __declspec(dllimport)
#endif

// ret  : 리턴 값
// name : 함수 이름
// def  : 괄호 "()"가 들어간 파라미터 정의
#define DEF_EXTERNAL(ret, name, def) private: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_INTERNAL(ret, name, def) public: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_CHECK(function) if	(!function) { AfxMessageBox	(_T("CImageProcess Failed to get function pointer")); }

class EXT_CLASS_IMAGEPROCESSALGORITHM CImageProcessAlgorithm
{

private:
//				(FUNCTION_RET) (FUNCTION_NAME)						  (Type* A, Type* B));
	DEF_EXTERNAL(BOOL		 , Interpolation						, (BYTE* pInputImage, Mat* pOutputImage, int nWidth, int nHeight, int nMagnification, int nQueueIndex, int nQueueMainIndex));
	DEF_EXTERNAL(BOOL		 , Average								, (vector<BYTE*>* pVecInputImage, Mat* pOutputImage, int nVectorLengh, int nWidth, int nHeight, int nMagnification, int nQueueIndex));
	DEF_EXTERNAL(double		 , ReMap								, (double dPos, double dSize, double dMin, double dMax));
	DEF_EXTERNAL(double		 , GetColorMapParula					, (unsigned int nColor, unsigned int nIndex));
	DEF_EXTERNAL(double		 , ArrayAverage							, (unsigned int nSize, double* pdData));
	DEF_EXTERNAL(BOOL		 , MovingAverageFilter					, (double* dInputArray, double* dOutputArray, unsigned int nLength, unsigned int nWeight));
	DEF_EXTERNAL(BOOL		 , WriteRawScanPositionFile				, (double* pPosX, double* pPosY, double* pDataD1, double* pDataD2, double* pDataD3, double* pDataT, double* pDataR, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName));
	DEF_EXTERNAL(BOOL		 , WriteRawScanPositionFile2			, (double* pPosX, double* pPosY, double* pDataD1, double* pDataD2, double* pDataD3, double* pDataT, double* pDataR, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName));
	DEF_EXTERNAL(BOOL		 , WriteRawScanDetectorFile				, (double* pPosX, double* pPosY, double* pData, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY, CString strFolderName));
	DEF_EXTERNAL(BOOL		 , CalColorBuffer						, (Mat* pDataBuffer, uchar* pDisplayBuffer, unsigned int nPosY, unsigned int nImageWidth, unsigned int nImageHeight));
	DEF_EXTERNAL(BOOL		 , CalColorBufferScanMode				, (double* pDataBufferT, double* pDataBufferR, uchar* pDisplayBuffer, double dTransmittance, double dReflectance, unsigned int nPosX, unsigned int nPosY, unsigned int nImageWidth, unsigned int nImageHeight));
	DEF_EXTERNAL(int		 , StandbyModeMemoryOptimization		, ());
	DEF_EXTERNAL(BOOL		 , WriteScanDataBinary					, (double* pD1, double* pD2, double* pD3, double* pT, double* pR, double* pActualX, double* pActualY, double* pStaticX, double* pStaticY, unsigned int nSize, CString strPath));
	DEF_EXTERNAL(BOOL		 , ReadScanDataBinary					, (double* pD1, double* pD2, double* pD3, double* pT, double* pR, double* pActualX, double* pActualY, double* pStaticX, double* pStaticY, unsigned int nSize, CString strPath));
	DEF_EXTERNAL(BOOL		 , CreateFile_							, (CFile* cfile, CString strPath, int nFIleMode));
	DEF_EXTERNAL(BOOL		 , WriteFile_							, (CFile* cfile, CString strText));
	DEF_EXTERNAL(BOOL		 , CloseFile_							, (CFile* cfile));

public:

	//생성자는 꼭 private나 protected로
	CImageProcessAlgorithm(HINSTANCE hInstance)
	{
		if (hInstance)
		{
//		   (FUNCTION_NAME)					  (FUNCTION_TYPE)						GetProcAddress(hInstance, FUNCTION_NAME)					;
			Interpolation					= (fp_Interpolation)					GetProcAddress(hInstance, "Interpolation")					;
			Average							= (fp_Average)							GetProcAddress(hInstance, "Average")						;
			ReMap							= (fp_ReMap)							GetProcAddress(hInstance, "ReMap")							;
			GetColorMapParula				= (fp_GetColorMapParula)				GetProcAddress(hInstance, "GetColorMapParula")				;
			ArrayAverage					= (fp_ArrayAverage)						GetProcAddress(hInstance, "ArrayAverage")					;
			MovingAverageFilter				= (fp_MovingAverageFilter)				GetProcAddress(hInstance, "MovingAverageFilter")			;
			WriteRawScanPositionFile		= (fp_WriteRawScanPositionFile)			GetProcAddress(hInstance, "WriteRawScanPositionFile")		;
			WriteRawScanPositionFile2		= (fp_WriteRawScanPositionFile)			GetProcAddress(hInstance, "WriteRawScanPositionFile2")		;
			WriteRawScanDetectorFile		= (fp_WriteRawScanDetectorFile)			GetProcAddress(hInstance, "WriteRawScanDetectorFile")		;
			CalColorBuffer					= (fp_CalColorBuffer)					GetProcAddress(hInstance, "CalColorBuffer")					;
			CalColorBufferScanMode			= (fp_CalColorBufferScanMode)			GetProcAddress(hInstance, "CalColorBufferScanMode")			;
			StandbyModeMemoryOptimization	= (fp_StandbyModeMemoryOptimization)	GetProcAddress(hInstance, "StandbyModeMemoryOptimization")	;
			WriteScanDataBinary				= (fp_WriteScanDataBinary)				GetProcAddress(hInstance, "WriteScanDataBinary")			;
			ReadScanDataBinary				= (fp_ReadScanDataBinary)				GetProcAddress(hInstance, "ReadScanDataBinary")				;
			CreateFile_						= (fp_CreateFile_)						GetProcAddress(hInstance, "CreateFile_")					;
			WriteFile_						= (fp_WriteFile_)						GetProcAddress(hInstance, "WriteFile_")						;
			CloseFile_						= (fp_CloseFile_)						GetProcAddress(hInstance, "CloseFile_")						;

			DEF_CHECK(Interpolation);
			DEF_CHECK(Average);
			DEF_CHECK(ReMap);
			DEF_CHECK(GetColorMapParula);
			DEF_CHECK(ArrayAverage);
			DEF_CHECK(MovingAverageFilter);
			DEF_CHECK(WriteRawScanPositionFile);
			DEF_CHECK(WriteRawScanPositionFile2);
			DEF_CHECK(WriteRawScanDetectorFile);
			DEF_CHECK(CalColorBuffer);
			DEF_CHECK(CalColorBufferScanMode);
			DEF_CHECK(StandbyModeMemoryOptimization);
			DEF_CHECK(WriteScanDataBinary);
			DEF_CHECK(ReadScanDataBinary);
			DEF_CHECK(CreateFile_);
			DEF_CHECK(WriteFile_);
			DEF_CHECK(CloseFile_);
		}
	}
	~CImageProcessAlgorithm() {}
};