#pragma once

#include <process.h>

#include <vector>

using namespace std;

class CObjectDll
{
	
public:
	CObjectDll();
	~CObjectDll();

public:
//	(FUNCTION_RET) (FUNCTION_NAME)							(Type* A, Type* B));
	BOOL			SetMapCreateFileMapping					(HANDLE* hHandler, int nMemorySize, CString strObjectName);
	BOOL			SetMapViewOfFileAddress_Byte			(HANDLE* hHandler, BYTE** pAddress, BYTE** pCopyOriginalData, int nMemorySize);
	BOOL			GetMapOpenFileMapping					(HANDLE* hHandler, CString strObjectName);
	BOOL			GetMapViewOfFileAddress_Byte			(HANDLE* hHandler, BYTE** pAddress, BYTE** pCopyOtherData, int nMemorySize);
	BOOL			SendMessageIPC							(CString strProcess, COPYDATASTRUCT* pData);
};

extern class CObjectDll ObjectDll;