#pragma once

#include <vector>

using namespace std;

/*

DLL(Dynamic-Link Library)
- DLL은 마이크로소프트 윈도에서 구현된 동적 라이브러리이다. 
  내부에는 다른 프로그램이 불러서 쓸 수 있는 다양한 함수들을 가지고 있는데, 
  확장DLL인 경우는 클래스를 가지고 있기도 한다.
  사용하는 방법에는 두가지가 있다.

묵시적 링킹(Implicit linking)
- 실행 파일 자체에 어떤 DLL의 어떤 함수를 사용하겠다는 정보를 포함시키고 
  운영체제가 프로그램 실행 시 해당 함수들을 초기화한 후 그것을 이용하는 방법

명시적 링킹(Explicit linking)
- 프로그램이 실행 중일 때 API를 이용하여 DLL 파일이 있는지 검사하고 
  동적으로 원하는 함수만 불러와서 쓰는 방법

*/

// ********* 명시적 링킹 ********* //

#ifdef _SHAREDMEMORY
	#define EXT_CLASS_SHAREDMEMORY __declspec(dllexport)
#else
	#define EXT_CLASS_SHAREDMEMORY __declspec(dllimport)
#endif

// ret  : 리턴 값
// name : 함수 이름
// def  : 괄호 "()"가 들어간 파라미터 정의
#define DEF_EXTERNAL(ret, name, def) private: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_INTERNAL(ret, name, def) public: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_CHECK(function) if	(!function) { AfxMessageBox	(_T("CSharedMemory Failed to get function pointer")); }

class EXT_CLASS_SHAREDMEMORY CSharedMemory
{

private:
//				(FUNCTION_RET) (FUNCTION_NAME)						  (Type* A, Type* B));
	DEF_EXTERNAL(BOOL		  , SetMapCreateFileMapping				, (HANDLE* hHandler, int nMemorySize, CString strObjectName));
	DEF_EXTERNAL(BOOL		  , SetMapViewOfFileAddress_Byte		, (HANDLE* hHandler, BYTE** pAddress, BYTE** pCopyOriginalData, int nMemorySize));
	DEF_EXTERNAL(BOOL		  , GetMapOpenFileMapping				, (HANDLE* hHandler, CString strObjectName));
	DEF_EXTERNAL(BOOL		  , GetMapViewOfFileAddress_Byte		, (HANDLE* hHandler, BYTE** pAddress, BYTE** pCopyOtherData, int nMemorySize));
	DEF_EXTERNAL(BOOL		  , SendMessageIPC						, (CString strProcess, COPYDATASTRUCT* pData));

public:

	//생성자는 꼭 private나 protected로
	CSharedMemory(HINSTANCE hInstance)
	{
		if (hInstance)
		{
//		   (FUNCTION_NAME)					  (FUNCTION_TYPE)						GetProcAddress(hInstance, FUNCTION_NAME)						;
			SetMapCreateFileMapping			= (fp_SetMapCreateFileMapping)			GetProcAddress(hInstance, "SetMapCreateFileMapping")			;
			SetMapViewOfFileAddress_Byte	= (fp_SetMapViewOfFileAddress_Byte)		GetProcAddress(hInstance, "SetMapViewOfFileAddress_Byte")		;
			GetMapOpenFileMapping			= (fp_GetMapOpenFileMapping)			GetProcAddress(hInstance, "GetMapOpenFileMapping")				;
			GetMapViewOfFileAddress_Byte	= (fp_GetMapViewOfFileAddress_Byte)		GetProcAddress(hInstance, "GetMapViewOfFileAddress_Byte")		;
			SendMessageIPC					= (fp_SendMessageIPC)					GetProcAddress(hInstance, "SendMessageIPC")						;

			DEF_CHECK(SetMapCreateFileMapping);
			DEF_CHECK(SetMapViewOfFileAddress_Byte);
			DEF_CHECK(GetMapOpenFileMapping);
			DEF_CHECK(GetMapViewOfFileAddress_Byte);
			DEF_CHECK(SendMessageIPC);
		}
	}
	~CSharedMemory() {}
};