#include "StdAfx.h"
#include "ObjectDll.h"

class CObjectDll ObjectDll;

CObjectDll::CObjectDll()
{
}

CObjectDll::~CObjectDll()
{
}

BOOL CObjectDll::SetMapCreateFileMapping(HANDLE* hHandler, int nMemorySize, CString strObjectName)
{
	int nLastError = 0;

//	*hHandler = OpenFileMapping(FILE_MAP_ALL_ACCESS, FALSE, strObjectName);

	if (*hHandler == NULL)
	{
		*hHandler = CreateFileMapping(
			INVALID_HANDLE_VALUE,
			NULL,
			PAGE_READWRITE,
			0,
			nMemorySize,
			strObjectName);

		if (*hHandler == NULL)
		{
			nLastError = GetLastError();
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CObjectDll::SetMapViewOfFileAddress_Byte(HANDLE* hHandler, BYTE** pAddress, BYTE** pCopyOriginalData, int nMemorySize)
{
	int nLastError = 0;

	if (*pAddress == NULL)
	{
		*pAddress = (BYTE*)MapViewOfFile(
			*hHandler,
			FILE_MAP_ALL_ACCESS,
			0,
			0,
			nMemorySize);

		if (*pAddress == NULL)
		{
			nLastError = GetLastError();
			return FALSE;
		}
	}

	memcpy(*pAddress, *pCopyOriginalData , nMemorySize);

	return TRUE;
}

BOOL CObjectDll::GetMapOpenFileMapping(HANDLE* hHandler, CString strObjectName)
{
	int nLastError = 0;

	if (*hHandler == NULL)
	{
		*hHandler = OpenFileMapping(
			FILE_MAP_ALL_ACCESS, 
			FALSE, 
			strObjectName);

		if (*hHandler == NULL)
		{
			nLastError = GetLastError();
			return FALSE;
		}
	}

	return TRUE;
}

BOOL CObjectDll::GetMapViewOfFileAddress_Byte(HANDLE* hHandler, BYTE** pAddress, BYTE** pCopyOtherData, int nMemorySize)
{
	int nLastError = 0;

	if (*pAddress == NULL)
	{
		*pAddress = (BYTE*)MapViewOfFile(
			*hHandler,
			FILE_MAP_ALL_ACCESS,
			0,
			0,
			nMemorySize);

		if (*pAddress == NULL)
		{
			nLastError = GetLastError();
			return FALSE;
		}
	}

	memcpy(*pCopyOtherData, *pAddress, nMemorySize);

	return TRUE;
}

BOOL CObjectDll::SendMessageIPC(CString strProcess, COPYDATASTRUCT* pData)
{
	CWnd* pWnd = CWnd::FindWindow(NULL, strProcess);

	if (pWnd != nullptr)
	{
		pWnd->SendMessage(WM_COPYDATA, (WPARAM)AfxGetApp()->m_pMainWnd->GetSafeHwnd(), (LPARAM)&*pData);
	}

	return TRUE;
}