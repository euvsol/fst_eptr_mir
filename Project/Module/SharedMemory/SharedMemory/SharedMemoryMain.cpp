// SixthRxProjectMain.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ObjectDll.h"
#include "SharedMemoryMain.h"

extern "C" __declspec(dllexport) BOOL SetMapCreateFileMapping(HANDLE* hHandler, int nMemorySize, CString strObjectName)
{
	return ObjectDll.SetMapCreateFileMapping(hHandler, nMemorySize, strObjectName);
}

extern "C" __declspec(dllexport) BOOL SetMapViewOfFileAddress_Byte(HANDLE* hHandler, BYTE** pAddress, BYTE** pCopyOriginalData, int nMemorySize)
{
	return ObjectDll.SetMapViewOfFileAddress_Byte(hHandler, pAddress, pCopyOriginalData, nMemorySize);
}

extern "C" __declspec(dllexport) __inline BOOL GetMapOpenFileMapping(HANDLE* hHandler, CString strObjectName)
{
	return ObjectDll.GetMapOpenFileMapping(hHandler, strObjectName);
}

extern "C" __declspec(dllexport) __inline BOOL GetMapViewOfFileAddress_Byte(HANDLE* hHandler, BYTE** pAddress, BYTE** pCopyOtherData, int nMemorySize)
{
	return ObjectDll.GetMapViewOfFileAddress_Byte(hHandler, pAddress, pCopyOtherData, nMemorySize);
}

extern "C" __declspec(dllexport) __inline BOOL SendMessageIPC(CString strProcess, COPYDATASTRUCT* pData)
{
	return ObjectDll.SendMessageIPC(strProcess, pData);
}