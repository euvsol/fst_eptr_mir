#pragma once

#include "PTR_SocketProtocolStructData.h"
#include "PTR_SocketProtocolStructByte.h"

class CTcpClientSendData
{

public:
	CTcpClientSendData(void* pFramework, void* pParents);
	virtual ~CTcpClientSendData();

public:
	void*			m_pFramework;
	void*			m_pParents;

	BYTE*			m_byPacketBuffer;
	unsigned int	m_nPacketBufferSize;

public:
	// Send Scan Start Run & Stop
	BOOL SendRunStopPacket(int nChannel, PACKET_RUN_STOP_DATA* pPacketData);

	// Send Scan Recipe
	BOOL SendScanRecipe(int nChannel, PACKET_RECIPE_DATA* pPacketData);

	// Send TCP Connect Ping Packet
	BOOL SendSocketDataPacket(int nChannel, PACKET_SOCKET_PING_DATA* pPacketData);

	// Send Average Run
	BOOL SendAPacketRunPacket(int nChannel, PACKET_A_PACKET_RUN_DATA* pPacketData);

	// Send Average Count
	BOOL SendAverageCountPacket(int nChannel, PACKET_AVERAGE_COUNT_DATA* pPacketData);

	// Send EM1024 Reset
	BOOL SendEm1024ResetPacket(int nChannel, PACKET_EM1024_RESET_DATA* pPacketData);

	// Send M Packet Run
	BOOL SendMPacketRunPacket(int nChannel, PACKET_M_PACKET_RUN_DATA* pPacketData);

	BOOL BufferSizeChange(unsigned int nBufferSize);

	CString ConvertToHex(CString data);

	void ConvertToHexDebugging(BYTE* pByte, int nLength);
};