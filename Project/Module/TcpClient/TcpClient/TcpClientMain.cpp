// SixthRxProjectMain.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "ObjectDll.h"
#include "TcpClientMain.h"

extern "C" __declspec(dllexport) BOOL FrameworkInstance(void* pFramework, void* pParents)
{
	return ObjectDll.FrameworkInstance(pFramework, pParents);
}

extern "C" __declspec(dllexport) BOOL CreateClient(unsigned int nChannel, CString strIP, unsigned int nPort)
{
	return ObjectDll.CreateClient(nChannel, strIP, nPort);
}

extern "C" __declspec(dllexport) BOOL CloseClient(unsigned int nChannel, CString strIP, unsigned int nPort)
{
	return ObjectDll.CloseClient(nChannel, strIP, nPort);
}

extern "C" __declspec(dllexport) BOOL BufferSizeChange(unsigned int nChannel, unsigned int nBufferSize)
{
	return ObjectDll.BufferSizeChange(nChannel, nBufferSize);
}

extern "C" __declspec(dllexport) unsigned int GetRingBufferReceiveIndex(unsigned int nChannel)
{
	return ObjectDll.GetRingBufferReceiveIndex(nChannel);
}

extern "C" __declspec(dllexport) unsigned int GetRingBufferIndex(unsigned int nChannel)
{
	return ObjectDll.GetRingBufferIndex(nChannel);
}

extern "C" __declspec(dllexport) BYTE* GetCurrentByteRingBuffer(unsigned int nChannel)
{
	return ObjectDll.GetCurrentByteRingBuffer(nChannel);
}

extern "C" __declspec(dllexport) BYTE* GetReceiveByteRingBuffer(unsigned int nChannel)
{
	return ObjectDll.GetReceiveByteRingBuffer(nChannel);
}

extern "C" __declspec(dllexport) BOOL ByteToMImageData(unsigned int nChannel, BYTE* pByte, PACKET_M_IMAGE_DATA* pPacketData)
{
	return ObjectDll.ByteToMImageData(nChannel, pByte, pPacketData);
}

extern "C" __declspec(dllexport) BOOL ByteToAImageData(unsigned int nChannel, BYTE * pByte, PACKET_A_IMAGE_DATA * pPacketData)
{
	return ObjectDll.ByteToAImageData(nChannel, pByte, pPacketData);
}

extern "C" __declspec(dllexport) BOOL RingBufferReceiveIndexIncrement(unsigned int nChannel)
{
	return ObjectDll.RingBufferReceiveIndexIncrement(nChannel);
}

extern "C" __declspec(dllexport) BOOL BufferIndexInit(unsigned int nChannel)
{
	return ObjectDll.BufferIndexInit(nChannel);
}

extern "C" __declspec(dllexport) BOOL SendScanRecipe(unsigned int nChannel, PACKET_RECIPE_DATA* pPacketData)
{
	return ObjectDll.SendScanRecipe(nChannel, pPacketData);
}

extern "C" __declspec(dllexport) BOOL SendRunStopPacket(unsigned int nChannel, PACKET_RUN_STOP_DATA* pPacketData)
{
	return ObjectDll.SendRunStopPacket(nChannel, pPacketData);
}

extern "C" __declspec(dllexport) BOOL SendSocketDataPacket(unsigned int nChannel, PACKET_SOCKET_PING_DATA* pData)
{
	return ObjectDll.SendSocketDataPacket(nChannel, pData);
}

extern "C" __declspec(dllexport) BOOL GetSocketStatus(unsigned int nChannel)
{
	return ObjectDll.GetSocketStatus(nChannel);
}

extern "C" __declspec(dllexport) BOOL SendAPacketRunPacket(unsigned int nChannel, PACKET_A_PACKET_RUN_DATA* pPacketData)
{
	return ObjectDll.SendAPacketRunPacket(nChannel, pPacketData);
}

extern "C" __declspec(dllexport) BOOL SendAverageCountPacket(unsigned int nChannel, PACKET_AVERAGE_COUNT_DATA* pPacketData)
{
	return ObjectDll.SendAverageCountPacket(nChannel, pPacketData);
}

extern "C" __declspec(dllexport) BOOL SendEm1024ResetPacket(unsigned int nChannel, PACKET_EM1024_RESET_DATA* pPacketData)
{
	return ObjectDll.SendEm1024ResetPacket(nChannel, pPacketData);
}

extern "C" __declspec(dllexport) BOOL SendMPacketRunPacket(unsigned int nChannel, PACKET_M_PACKET_RUN_DATA* pPacketData)
{
	return ObjectDll.SendMPacketRunPacket(nChannel, pPacketData);
}

extern "C" __declspec(dllexport) BOOL SetScanPacketLengh(unsigned int nChannel, unsigned int nLength)
{
	return ObjectDll.SetScanPacketLengh(nChannel, nLength);
}

extern "C" __declspec(dllexport) BOOL SetScanPacketMode(unsigned int nChannel, BOOL bFlag)
{
	return ObjectDll.SetScanPacketMode(nChannel, bFlag);
}

extern "C" __declspec(dllexport) BOOL GetScanPacketLast(unsigned int nChannel)
{
	return ObjectDll.GetScanPacketLast(nChannel);
}

extern "C" __declspec(dllexport) unsigned int GetRingBufferTotalIndex(unsigned int nChannel)
{
	return ObjectDll.GetRingBufferTotalIndex(nChannel);
}