#pragma once

#include <afxsock.h>
#include "TcpClientSocket.h"
#include <queue>


class CTcpSocketThread : public CWinThread
{
	DECLARE_DYNCREATE(CTcpSocketThread)

protected:
	CTcpSocketThread();
	virtual ~CTcpSocketThread();

public:
	SOCKET					m_hSocket;
	CTcpClientSocket*		m_pClient;
	void*					m_pParent;

protected:
	DECLARE_MESSAGE_MAP()

public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
};


