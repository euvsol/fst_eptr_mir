#pragma once

#include <afxsock.h>
#include "TcpClientProtocol.h"
#include "TcpClientSendData.h"
#include "PTR_UserMessage.h"
#include "PTR_SocketProtocolDefine.h"
#include "PTR_SocketProtocolStructData.h"
#include "PTR_SocketProtocolStructByte.h"
#include "PTR_DataManagementMacro.h"

#include <vector>
#include <queue>

#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

class CTcpClientSocket : public CSocket
{

public:
	CTcpClientSocket(void* pFramework, void* pParents, unsigned int nChannel, CString strIP, unsigned int nPort);
	virtual ~CTcpClientSocket();

public:
	void*					m_pFramework;
	void*					m_pParents;

	unsigned int			m_nChannel;
	CString					m_strIP;
	unsigned int			m_nPort;

	SOCKET					m_hSocket;
	CTcpClientProtocol*		m_pClientProtocol;
	CTcpClientSendData*		m_pClientSendData;

	BYTE*					m_byPacketBuffer;
	BYTE**					m_byPacketBufferRing;
	unsigned int			m_nPacketBufferSize;

	unsigned int			m_nRingBufferIndex;
	unsigned int			m_nRingBufferReceiveIndex;

	unsigned int			m_nBufferTotalIndex;

	unsigned int			m_nScanPacketBufferIndex;
	unsigned int			m_nScanPacketTotalLengh;
	unsigned int			m_nScanPacketLenghTemp;
	BOOL					m_bScanPacketMode;
	BOOL					m_bScanPacketLast;

public:
	virtual void OnReceive(int nErrorCode);
	virtual void OnClose(int nErrorCode);

public:
	BOOL BufferSizeChange(unsigned int nBufferSize);
	BOOL SetScanPacketLengh(unsigned int nLength);
	BOOL SetScanPacketMode(BOOL bFlag);
	BOOL GetScanPacketLast();
	unsigned int GetScanPacketLengh1000();
	void Sequence_StartPacket();
	void Sequence_A_TO_B_SAME(unsigned int* nLength);
	void Sequence_A_TO_B_LARGE(unsigned int* nLength);
	void Sequence_A_TO_B_SMALL(unsigned int* nLength, unsigned int* nOverIndex = nullptr);
};


