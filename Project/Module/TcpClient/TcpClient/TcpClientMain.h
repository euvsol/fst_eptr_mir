#pragma once

#include <vector>

using namespace std;

/*

DLL(Dynamic-Link Library)
- DLL은 마이크로소프트 윈도에서 구현된 동적 라이브러리이다. 
  내부에는 다른 프로그램이 불러서 쓸 수 있는 다양한 함수들을 가지고 있는데, 
  확장DLL인 경우는 클래스를 가지고 있기도 한다.
  사용하는 방법에는 두가지가 있다.

묵시적 링킹(Implicit linking)
- 실행 파일 자체에 어떤 DLL의 어떤 함수를 사용하겠다는 정보를 포함시키고 
  운영체제가 프로그램 실행 시 해당 함수들을 초기화한 후 그것을 이용하는 방법

명시적 링킹(Explicit linking)
- 프로그램이 실행 중일 때 API를 이용하여 DLL 파일이 있는지 검사하고 
  동적으로 원하는 함수만 불러와서 쓰는 방법

*/

// ********* 명시적 링킹 ********* //

#ifdef _TCP_CLIENT
	#define EXT_CLASS_TCP_CLIENT __declspec(dllexport)
#else
	#define EXT_CLASS_TCP_CLIENT __declspec(dllimport)
#endif

// ret  : 리턴 값
// name : 함수 이름
// def  : 괄호 "()"가 들어간 파라미터 정의
#define DEF_EXTERNAL(ret, name, def) private: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_INTERNAL(ret, name, def) public: typedef ret (*fp_##name)def; public: fp_##name name;
#define DEF_CHECK(function) if	(!function) { AfxMessageBox	(_T("CTcpClient Failed to get function pointer")); }

class EXT_CLASS_TCP_CLIENT CTcpClient
{

private:
//				(FUNCTION_RET)	  (FUNCTION_NAME)						  (Type* A, Type* B));
	DEF_EXTERNAL(BOOL			, FrameworkInstance						, (void* pFramework, void* pParents));
	DEF_EXTERNAL(BOOL			, CreateClient							, (unsigned int nChannel, CString strIP, unsigned int nPort));
	DEF_EXTERNAL(BOOL			, CloseClient							, (unsigned int nChannel, CString strIP, unsigned int nPort));
	DEF_EXTERNAL(BOOL			, BufferSizeChange						, (unsigned int nChannel, unsigned int nBufferSize));
	DEF_EXTERNAL(unsigned int	, GetRingBufferReceiveIndex				, (unsigned int nChannel));
	DEF_EXTERNAL(unsigned int	, GetRingBufferIndex					, (unsigned int nChannel));
	DEF_EXTERNAL(BYTE*			, GetCurrentByteRingBuffer				, (unsigned int nChannel));
	DEF_EXTERNAL(BYTE*			, GetReceiveByteRingBuffer				, (unsigned int nChannel));
	DEF_EXTERNAL(BOOL			, ByteToMImageData						, (unsigned int nChannel, BYTE* pByte, PACKET_M_IMAGE_DATA* pPacketData));
	DEF_EXTERNAL(BOOL			, ByteToAImageData						, (unsigned int nChannel, BYTE* pByte, PACKET_A_IMAGE_DATA* pPacketData));
	DEF_EXTERNAL(BOOL			, RingBufferReceiveIndexIncrement		, (unsigned int nChannel));
	DEF_EXTERNAL(BOOL			, BufferIndexInit						, (unsigned int nChannel));
	DEF_EXTERNAL(BOOL			, SendScanRecipe						, (unsigned int nChannel, PACKET_RECIPE_DATA* pPacketData));
	DEF_EXTERNAL(BOOL			, SendSocketDataPacket					, (unsigned int nChannel, PACKET_SOCKET_PING_DATA* pData));
	DEF_EXTERNAL(BOOL			, GetSocketStatus						, (unsigned int nChannel));
	DEF_EXTERNAL(BOOL			, SendRunStopPacket						, (unsigned int nChannel, PACKET_RUN_STOP_DATA* pPacketData));
	DEF_EXTERNAL(BOOL			, SendAPacketRunPacket					, (unsigned int nChannel, PACKET_A_PACKET_RUN_DATA* pPacketData));
	DEF_EXTERNAL(BOOL			, SendAverageCountPacket				, (unsigned int nChannel, PACKET_AVERAGE_COUNT_DATA* pPacketData));
	DEF_EXTERNAL(BOOL			, SendEm1024ResetPacket					, (unsigned int nChannel, PACKET_EM1024_RESET_DATA* pPacketData));
	DEF_EXTERNAL(BOOL			, SendMPacketRunPacket					, (unsigned int nChannel, PACKET_M_PACKET_RUN_DATA* pPacketData));
	DEF_EXTERNAL(BOOL			, SetScanPacketLengh					, (unsigned int nChannel, unsigned int nLength));
	DEF_EXTERNAL(BOOL			, SetScanPacketMode						, (unsigned int nChannel, BOOL bFlag));
	DEF_EXTERNAL(BOOL			, GetScanPacketLast						, (unsigned int nChannel));
	DEF_EXTERNAL(unsigned int	, GetRingBufferTotalIndex				, (unsigned int nChannel));

public:

	//생성자는 꼭 private나 protected로
	CTcpClient(HINSTANCE hInstance)
	{
		if (hInstance)
		{
//		   (FUNCTION_NAME)					  (FUNCTION_TYPE)						GetProcAddress(hInstance, FUNCTION_NAME)						;
			FrameworkInstance				= (fp_FrameworkInstance)				GetProcAddress(hInstance, "FrameworkInstance")					;
			CreateClient					= (fp_CreateClient)						GetProcAddress(hInstance, "CreateClient")						;
			CloseClient						= (fp_CloseClient)						GetProcAddress(hInstance, "CloseClient")						;
			BufferSizeChange				= (fp_BufferSizeChange)					GetProcAddress(hInstance, "BufferSizeChange")					;
			GetRingBufferReceiveIndex		= (fp_GetRingBufferReceiveIndex)		GetProcAddress(hInstance, "GetRingBufferReceiveIndex")			;
			GetRingBufferIndex				= (fp_GetRingBufferIndex)				GetProcAddress(hInstance, "GetRingBufferIndex")					;
			GetCurrentByteRingBuffer		= (fp_GetCurrentByteRingBuffer)			GetProcAddress(hInstance, "GetCurrentByteRingBuffer")			;
			GetReceiveByteRingBuffer		= (fp_GetReceiveByteRingBuffer)			GetProcAddress(hInstance, "GetReceiveByteRingBuffer")			;
			ByteToMImageData				= (fp_ByteToMImageData)					GetProcAddress(hInstance, "ByteToMImageData")					;
			ByteToAImageData				= (fp_ByteToAImageData)					GetProcAddress(hInstance, "ByteToAImageData")					;
			RingBufferReceiveIndexIncrement = (fp_RingBufferReceiveIndexIncrement)	GetProcAddress(hInstance, "RingBufferReceiveIndexIncrement")	;
			BufferIndexInit					= (fp_BufferIndexInit)					GetProcAddress(hInstance, "BufferIndexInit")					;
			SendScanRecipe					= (fp_SendScanRecipe)					GetProcAddress(hInstance, "SendScanRecipe")						;
			SendRunStopPacket				= (fp_SendRunStopPacket)				GetProcAddress(hInstance, "SendRunStopPacket")					;
			SendSocketDataPacket			= (fp_SendSocketDataPacket)				GetProcAddress(hInstance, "SendSocketDataPacket")				;
			GetSocketStatus					= (fp_GetSocketStatus)					GetProcAddress(hInstance, "GetSocketStatus")					;
			SendAPacketRunPacket			= (fp_SendAPacketRunPacket)				GetProcAddress(hInstance, "SendAPacketRunPacket")				;
			SendAverageCountPacket			= (fp_SendAverageCountPacket)			GetProcAddress(hInstance, "SendAverageCountPacket")				;
			SendEm1024ResetPacket			= (fp_SendEm1024ResetPacket)			GetProcAddress(hInstance, "SendEm1024ResetPacket")				;
			SendMPacketRunPacket			= (fp_SendMPacketRunPacket)				GetProcAddress(hInstance, "SendMPacketRunPacket")				;
			SetScanPacketLengh				= (fp_SetScanPacketLengh)				GetProcAddress(hInstance, "SetScanPacketLengh")					;
			SetScanPacketMode				= (fp_SetScanPacketMode)				GetProcAddress(hInstance, "SetScanPacketMode")					;
			GetScanPacketLast				= (fp_GetScanPacketLast)				GetProcAddress(hInstance, "GetScanPacketLast")					;
			GetRingBufferTotalIndex			= (fp_GetRingBufferTotalIndex)			GetProcAddress(hInstance, "GetRingBufferTotalIndex")			;

			DEF_CHECK(FrameworkInstance);
			DEF_CHECK(CreateClient);
			DEF_CHECK(CloseClient);
			DEF_CHECK(BufferSizeChange);
			DEF_CHECK(GetRingBufferReceiveIndex);
			DEF_CHECK(GetRingBufferIndex);
			DEF_CHECK(GetCurrentByteRingBuffer);
			DEF_CHECK(GetReceiveByteRingBuffer);
			DEF_CHECK(ByteToMImageData);
			DEF_CHECK(ByteToAImageData);
			DEF_CHECK(RingBufferReceiveIndexIncrement);
			DEF_CHECK(BufferIndexInit);
			DEF_CHECK(SendScanRecipe);
			DEF_CHECK(SendRunStopPacket);
			DEF_CHECK(SendSocketDataPacket);
			DEF_CHECK(GetSocketStatus);
			DEF_CHECK(SendAPacketRunPacket);
			DEF_CHECK(SendAverageCountPacket);
			DEF_CHECK(SendEm1024ResetPacket);
			DEF_CHECK(SendMPacketRunPacket);
			DEF_CHECK(SetScanPacketLengh);
			DEF_CHECK(SetScanPacketMode);
			DEF_CHECK(GetScanPacketLast);
			DEF_CHECK(GetRingBufferTotalIndex);
		}
	}
	~CTcpClient() {}
};