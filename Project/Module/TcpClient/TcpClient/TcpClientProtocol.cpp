#include "stdafx.h"
#include "TcpClientProtocol.h"
#include "TcpClientSocket.h"

#include "PTR_SocketProtocolDefine.h"

CTcpClientProtocol::CTcpClientProtocol(void* pFramework, void* pParents)
{
	m_pFramework	= pFramework;
	m_pParents		= pParents;
}

CTcpClientProtocol::~CTcpClientProtocol()
{
}

BOOL CTcpClientProtocol::ByteToSocketData(BYTE* pByte, PACKET_SOCKET_PING_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Data	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	return TRUE;
}

BOOL CTcpClientProtocol::ByteToMImageData(BYTE* pByte, PACKET_M_IMAGE_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);

	int nHeaderSize		= PROTOCOL_HEADER_SIZE;
	int nContentSize	= sizeof(PACKET_M_IMAGE_BYTE_CONTENT);
	int nVectorSize		= pPacketData->Len / PROTOCOL_M_PACKET_SIZE;
	int nSendSize		= nHeaderSize + (nVectorSize * nContentSize);

	for (int i = 0; i < nVectorSize; i++)
	{
		PACKET_M_IMAGE_DATA_CONTENT* pDataContent = new PACKET_M_IMAGE_DATA_CONTENT;

		memcpy(&pDataContent->PacketNumber	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
		memcpy(&pDataContent->EM1024X		, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->EM1024Y		, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Detector_1	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Detector_2	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Detector_3	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Detector_4	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Zr_IO			, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->BS_IO			, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);

		pPacketData->QueueData.push(pDataContent);
	}

	return TRUE;
}

BOOL CTcpClientProtocol::MImageDataToMImageByte(PACKET_M_IMAGE_DATA* pPacketData, PACKET_M_IMAGE_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));	

	int nVectorSize = pPacketData->QueueData.size();

	for (int i=0; i< nVectorSize; i++)
	{
		PACKET_M_IMAGE_BYTE_CONTENT* pByteContent = new PACKET_M_IMAGE_BYTE_CONTENT;
		
		PACKET_M_IMAGE_DATA_CONTENT* pDataContent = pPacketData->QueueData.front();

		memcpy(&pByteContent->PacketNumber		, &pDataContent->PacketNumber	, sizeof(unsigned int));
		memcpy(&pByteContent->EM1024X			, &pDataContent->EM1024X		, sizeof(double));
		memcpy(&pByteContent->EM1024Y			, &pDataContent->EM1024Y		, sizeof(double));
		memcpy(&pByteContent->Detector_1		, &pDataContent->Detector_1		, sizeof(double));	
		memcpy(&pByteContent->Detector_2		, &pDataContent->Detector_2		, sizeof(double));	
		memcpy(&pByteContent->Detector_3		, &pDataContent->Detector_3		, sizeof(double));	
		memcpy(&pByteContent->Detector_4		, &pDataContent->Detector_4		, sizeof(double));	
		memcpy(&pByteContent->Zr_IO				, &pDataContent->Zr_IO			, sizeof(double));	
		memcpy(&pByteContent->BS_IO				, &pDataContent->BS_IO			, sizeof(double));

		pPacketByte->QueueByte.push(pByteContent);

		delete pDataContent;
		pDataContent = nullptr;

		pPacketData->QueueData.pop();
	}

	return TRUE;
}

BOOL CTcpClientProtocol::ByteToAImageData(BYTE* pByte, PACKET_A_IMAGE_DATA* pPacketData)
{
	int nIndex = 0;

	memcpy(&pPacketData->Stx	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pPacketData->Len	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pPacketData->Cmd	, &pByte[nIndex], sizeof(BYTE));			nIndex += sizeof(BYTE);

	int nHeaderSize		= PROTOCOL_HEADER_SIZE;
	int nContentSize	= sizeof(PACKET_A_IMAGE_BYTE_CONTENT);
	int nVectorSize		= pPacketData->Len / PROTOCOL_A_PACKET_SIZE;
	int nSendSize		= nHeaderSize + (nVectorSize * nContentSize);

	for (int i = 0; i < nVectorSize; i++)
	{
		PACKET_A_IMAGE_DATA_CONTENT* pDataContent = new PACKET_A_IMAGE_DATA_CONTENT;

		memcpy(&pDataContent->PacketNumber	, &pByte[nIndex], sizeof(unsigned int));	nIndex += sizeof(unsigned int);
		memcpy(&pDataContent->EM1024X		, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->EM1024Y		, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Detector_1	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Detector_2	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Detector_3	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Detector_4	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->Zr_IO			, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->BS_IO			, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->DetectorSub_1	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->DetectorSub_2	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->DetectorSub_3	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);
		memcpy(&pDataContent->DetectorSub_4	, &pByte[nIndex], sizeof(double));			nIndex += sizeof(double);

		pPacketData->QueueData.push(pDataContent);
	}

	return TRUE;
}

BOOL CTcpClientProtocol::AImageDataToAImageByte(PACKET_A_IMAGE_DATA* pPacketData, PACKET_A_IMAGE_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));	

	int nVectorSize = pPacketData->QueueData.size();

	for (int i=0; i< nVectorSize; i++)
	{
		PACKET_A_IMAGE_BYTE_CONTENT* pByteContent = new PACKET_A_IMAGE_BYTE_CONTENT;
		
		PACKET_A_IMAGE_DATA_CONTENT* pDataContent = pPacketData->QueueData.front();

		memcpy(&pByteContent->PacketNumber		, &pDataContent->PacketNumber	, sizeof(unsigned int));
		memcpy(&pByteContent->EM1024X			, &pDataContent->EM1024X		, sizeof(double));
		memcpy(&pByteContent->EM1024Y			, &pDataContent->EM1024Y		, sizeof(double));
		memcpy(&pByteContent->Detector_1		, &pDataContent->Detector_1		, sizeof(double));	
		memcpy(&pByteContent->Detector_2		, &pDataContent->Detector_2		, sizeof(double));	
		memcpy(&pByteContent->Detector_3		, &pDataContent->Detector_3		, sizeof(double));	
		memcpy(&pByteContent->Detector_4		, &pDataContent->Detector_4		, sizeof(double));	
		memcpy(&pByteContent->Zr_IO				, &pDataContent->Zr_IO			, sizeof(double));	
		memcpy(&pByteContent->BS_IO				, &pDataContent->BS_IO			, sizeof(double));
		memcpy(&pByteContent->DetectorSub_1		, &pDataContent->DetectorSub_1	, sizeof(double));
		memcpy(&pByteContent->DetectorSub_2		, &pDataContent->DetectorSub_2	, sizeof(double));
		memcpy(&pByteContent->DetectorSub_3		, &pDataContent->DetectorSub_3	, sizeof(double));
		memcpy(&pByteContent->DetectorSub_4		, &pDataContent->DetectorSub_4	, sizeof(double));

		pPacketByte->QueueByte.push(pByteContent);

		delete pDataContent;
		pDataContent = nullptr;

		pPacketData->QueueData.pop();
	}

	return TRUE;
}

BOOL CTcpClientProtocol::RunStopDataToRunStopByte(PACKET_RUN_STOP_DATA* pPacketData, PACKET_RUN_STOP_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));
	memcpy(&pPacketByte->Data	, &pPacketData->Data	, sizeof(unsigned int));

	return TRUE;
}

BOOL CTcpClientProtocol::SocketDataToSocketByte(PACKET_SOCKET_PING_DATA* pPacketData, PACKET_SOCKET_PING_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));
	memcpy(&pPacketByte->Data	, &pPacketData->Data	, sizeof(unsigned int));

	return TRUE;
}

BOOL CTcpClientProtocol::RecipeDataToRecipeByte(PACKET_RECIPE_DATA* pPacketData, PACKET_RECIPE_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx				, &pPacketData->Stx					, sizeof(BYTE));	
	memcpy(&pPacketByte->Len				, &pPacketData->Len					, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd				, &pPacketData->Cmd					, sizeof(BYTE));
	memcpy(&pPacketByte->ImageFOV_X			, &pPacketData->ImageFOV_X			, sizeof(unsigned int));
	memcpy(&pPacketByte->ImageFOV_Y			, &pPacketData->ImageFOV_Y			, sizeof(unsigned int));
	memcpy(&pPacketByte->ScanGrid_X			, &pPacketData->ScanGrid_X			, sizeof(unsigned int));
	memcpy(&pPacketByte->ScanGrid_Y			, &pPacketData->ScanGrid_Y			, sizeof(unsigned int));
	memcpy(&pPacketByte->InterpolationCount	, &pPacketData->InterpolationCount	, sizeof(unsigned int));
	memcpy(&pPacketByte->AverageCount		, &pPacketData->AverageCount		, sizeof(unsigned int));
	memcpy(&pPacketByte->SetCount			, &pPacketData->SetCount			, sizeof(unsigned int));
	memcpy(&pPacketByte->ScanDirection		, &pPacketData->ScanDirection		, sizeof(unsigned int));
	memcpy(&pPacketByte->PacketCount		, &pPacketData->PacketCount			, sizeof(unsigned int));

	return TRUE;
}

unsigned int CTcpClientProtocol::RecipeByteToByte(PACKET_RECIPE_BYTE* pPacketByte, BYTE* pByte)
{
	int nIndex = 0;

	memcpy(&pByte[nIndex], &pPacketByte->Stx				, sizeof(BYTE));				nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Len				, sizeof(unsigned int));		nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->Cmd				, sizeof(BYTE));				nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->ImageFOV_X			, sizeof(unsigned int));		nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->ImageFOV_Y			, sizeof(unsigned int));		nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->ScanGrid_X			, sizeof(unsigned int));		nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->ScanGrid_Y			, sizeof(unsigned int));		nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->InterpolationCount	, sizeof(unsigned int));		nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->AverageCount		, sizeof(unsigned int));		nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->SetCount			, sizeof(unsigned int));		nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->ScanDirection		, sizeof(unsigned int));		nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->PacketCount		, sizeof(unsigned int));		nIndex += sizeof(unsigned int);

	return nIndex;
}

unsigned int CTcpClientProtocol::RunStopByteToByte(PACKET_RUN_STOP_BYTE* pPacketByte, BYTE* pByte)
{
	int nIndex = 0;

	memcpy(&pByte[nIndex], &pPacketByte->Stx	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Len	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->Cmd	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Data	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	return nIndex;
}

unsigned int CTcpClientProtocol::SocketByteToByte(PACKET_SOCKET_PING_BYTE* pPacketData, BYTE* pByte)
{
	int nIndex = 0;

	memcpy(&pByte[nIndex], &pPacketData->Stx	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketData->Len	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketData->Cmd	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketData->Data	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	return nIndex;
}

BOOL CTcpClientProtocol::APacketRunDataToAPacketRunByte(PACKET_A_PACKET_RUN_DATA* pPacketData, PACKET_A_PACKET_RUN_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));
	memcpy(&pPacketByte->Data	, &pPacketData->Data	, sizeof(unsigned char));

	return TRUE;
}

BOOL CTcpClientProtocol::AverageCountDataToAverageCountByte(PACKET_AVERAGE_COUNT_DATA* pPacketData, PACKET_AVERAGE_COUNT_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));
	memcpy(&pPacketByte->Data	, &pPacketData->Data	, sizeof(unsigned int));

	return TRUE;
}

BOOL CTcpClientProtocol::Em1024ResetDataToEm1024ResetByte(PACKET_EM1024_RESET_DATA* pPacketData, PACKET_EM1024_RESET_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));
	memcpy(&pPacketByte->Data	, &pPacketData->Data	, sizeof(unsigned char));

	return TRUE;
}

BOOL CTcpClientProtocol::MPacketRunDataToMPacketRunByte(PACKET_M_PACKET_RUN_DATA* pPacketData, PACKET_M_PACKET_RUN_BYTE* pPacketByte)
{
	memcpy(&pPacketByte->Stx	, &pPacketData->Stx		, sizeof(BYTE));	
	memcpy(&pPacketByte->Len	, &pPacketData->Len		, sizeof(unsigned int));
	memcpy(&pPacketByte->Cmd	, &pPacketData->Cmd		, sizeof(BYTE));
	memcpy(&pPacketByte->Data	, &pPacketData->Data	, sizeof(unsigned int));

	return TRUE;
}

unsigned int CTcpClientProtocol::APacketRunByteToByte(PACKET_A_PACKET_RUN_BYTE* pPacketByte, BYTE* pByte)
{
	int nIndex = 0;

	memcpy(&pByte[nIndex], &pPacketByte->Stx	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Len	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->Cmd	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Data	, sizeof(unsigned char));	nIndex += sizeof(unsigned char);

	return nIndex;
}

unsigned int CTcpClientProtocol::AverageCountByteToByte(PACKET_AVERAGE_COUNT_BYTE* pPacketByte, BYTE* pByte)
{
	int nIndex = 0;

	memcpy(&pByte[nIndex], &pPacketByte->Stx	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Len	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->Cmd	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Data	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	int nSendSize = nIndex;

	return nSendSize;
}

unsigned int CTcpClientProtocol::Em1024ResetByteToByte(PACKET_EM1024_RESET_BYTE* pPacketByte, BYTE* pByte)
{
	int nIndex = 0;

	memcpy(&pByte[nIndex], &pPacketByte->Stx	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Len	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->Cmd	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Data	, sizeof(unsigned char));	nIndex += sizeof(unsigned char);

	return nIndex;
}

unsigned int CTcpClientProtocol::MPacketRunByteToByte(PACKET_M_PACKET_RUN_BYTE* pPacketByte, BYTE* pByte)
{
	int nIndex = 0;

	memcpy(&pByte[nIndex], &pPacketByte->Stx	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Len	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);
	memcpy(&pByte[nIndex], &pPacketByte->Cmd	, sizeof(BYTE));			nIndex += sizeof(BYTE);
	memcpy(&pByte[nIndex], &pPacketByte->Data	, sizeof(unsigned int));	nIndex += sizeof(unsigned int);

	return nIndex;
}