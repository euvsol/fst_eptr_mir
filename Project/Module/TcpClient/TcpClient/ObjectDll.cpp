#include "StdAfx.h"
#include "ObjectDll.h"


class CObjectDll ObjectDll;

CObjectDll::CObjectDll()
{
	m_pFramework		= nullptr;
	m_pParents			= nullptr;

	SetSocketConnecting(FALSE);

	for (int i = 0; i < CLIENT_NETWORK_COUNT; i++)
	{ 
		m_stSocketThread[i].m_pClient		= nullptr;
		m_stSocketThread[i].m_pSocketThread	= nullptr;
		m_stSocketThread[i].m_nPort			= 0;
		m_stSocketThread[i].m_strIP			= _T("");
		m_stSocketThread[i].m_bConnect		= FALSE;
	}
}

CObjectDll::~CObjectDll()
{
	while (1) { if (!GetSocketConnecting()) { break; } Sleep(1); }

	for (int i = 0; i < CLIENT_NETWORK_COUNT; i++)
	{
		if (m_stSocketThread[i].m_pClient != nullptr)
		{
			if (m_stSocketThread[i].m_pSocketThread != nullptr)
			{
				m_stSocketThread[i].m_pClient->m_hSocket = m_stSocketThread[i].m_pSocketThread->m_hSocket;
				m_stSocketThread[i].m_pClient->Attach(m_stSocketThread[i].m_pClient->m_hSocket);
				m_stSocketThread[i].m_pClient->ShutDown(SD_BOTH);
				m_stSocketThread[i].m_pClient->Close();
			}
			delete m_stSocketThread[i].m_pClient;
			m_stSocketThread[i].m_pClient = nullptr;
			m_stSocketThread[i].m_bConnect = FALSE;
		}

		if (m_stSocketThread[i].m_pSocketThread != nullptr)
		{
			m_stSocketThread[i].m_pSocketThread->PostThreadMessage(WM_QUIT, 0, 0);
			m_stSocketThread[i].m_pSocketThread = nullptr;
			Sleep(100);
		}
	}
}

BOOL CObjectDll::FrameworkInstance(void* pFramework, void* pParents)
{
	m_pFramework	= pFramework;
	m_pParents		= pParents;
	return TRUE;
}

BOOL CObjectDll::CreateClient(unsigned int nChannel, CString strIP, unsigned int nPort)
{
	if (!AfxSocketInit())
	{
		AfxMessageBox(_T("Windows 소켓을 초기화하지 못했습니다."));
		return FALSE;
	}

	if (m_stSocketThread[nChannel].m_pClient == nullptr)
	{
		SetSocketConnecting(TRUE);

		m_stSocketThread[nChannel].m_pClient = new CTcpClientSocket(m_pParents, this, nChannel, strIP, nPort);

		m_stSocketThread[nChannel].m_strIP = strIP;
		m_stSocketThread[nChannel].m_nPort = nPort;

		if (m_stSocketThread[nChannel].m_pClient->Create())
		{
			if (!m_stSocketThread[nChannel].m_pClient->Connect(m_stSocketThread[nChannel].m_strIP, m_stSocketThread[nChannel].m_nPort))
			{
				m_stSocketThread[nChannel].m_pClient->ShutDown(SD_BOTH);
				m_stSocketThread[nChannel].m_pClient->Close();
				delete m_stSocketThread[nChannel].m_pClient;
				m_stSocketThread[nChannel].m_pClient = nullptr;
			}
			else
			{
				CWnd*	pWnd	= (CWnd*)m_pParents;
				WPARAM	wParam	= 0;
				LPARAM	lParam	= (LPARAM)m_stSocketThread[nChannel].m_pClient;

				if (SendMessage(pWnd->m_hWnd, WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_ACCEPT, wParam, lParam))
				{
					m_stSocketThread[nChannel].m_bConnect = TRUE;
					m_stSocketThread[nChannel].m_pSocketThread = (CTcpSocketThread*)AfxBeginThread(RUNTIME_CLASS(CTcpSocketThread), THREAD_PRIORITY_NORMAL, 0, CREATE_SUSPENDED);
					m_stSocketThread[nChannel].m_pSocketThread->m_pClient = m_stSocketThread[nChannel].m_pClient;
					m_stSocketThread[nChannel].m_pSocketThread->m_hSocket = m_stSocketThread[nChannel].m_pClient->Detach();
					m_stSocketThread[nChannel].m_pSocketThread->m_pParent = this;
					m_stSocketThread[nChannel].m_pSocketThread->ResumeThread();

					BufferSizeChange(nChannel, SOCKET_BUFFER_SIZE);
					SetSocketConnecting(FALSE);
					return TRUE;
				}
			}
		}
		else
		{
			if (m_stSocketThread[nChannel].m_pClient != nullptr)
			{
				delete m_stSocketThread[nChannel].m_pClient;
				m_stSocketThread[nChannel].m_pClient = nullptr;
			}
		}

		SetSocketConnecting(FALSE);
	}

	return FALSE;
}

BOOL CObjectDll::CloseClient(unsigned int nChannel, CString strIP, unsigned int nPort)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr)
	{
		m_stSocketThread[nChannel].m_pClient->m_hSocket = m_stSocketThread[nChannel].m_pSocketThread->m_hSocket;
		m_stSocketThread[nChannel].m_pClient->Attach(m_stSocketThread[nChannel].m_pClient->m_hSocket);
		m_stSocketThread[nChannel].m_pClient->ShutDown(SD_BOTH);
		m_stSocketThread[nChannel].m_pClient->Close();
		delete m_stSocketThread[nChannel].m_pClient;
		m_stSocketThread[nChannel].m_pClient = nullptr;
		m_stSocketThread[nChannel].m_bConnect = FALSE;
	}

	if (m_stSocketThread[nChannel].m_pSocketThread != nullptr)
	{
		m_stSocketThread[nChannel].m_pSocketThread->PostThreadMessage(WM_QUIT, 0, 0);
		m_stSocketThread[nChannel].m_pSocketThread = nullptr;
		Sleep(100);
	}

	return TRUE;
}

BOOL CObjectDll::BufferSizeChange(unsigned int nChannel, unsigned int nBufferSize)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { m_stSocketThread[nChannel].m_pClient->BufferSizeChange(nBufferSize); m_stSocketThread[nChannel].m_pClient->m_pClientSendData->BufferSizeChange(nBufferSize); return TRUE; }
	return FALSE;
}

unsigned int CObjectDll::GetRingBufferReceiveIndex(unsigned int nChannel)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_nRingBufferReceiveIndex; }
	return 0;
}

unsigned int CObjectDll::GetRingBufferIndex(unsigned int nChannel)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_nRingBufferIndex; }
	return 0;
}

BYTE* CObjectDll::GetCurrentByteRingBuffer(unsigned int nChannel)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_byPacketBufferRing[GetRingBufferIndex(nChannel)]; }
	return nullptr;
}

BYTE* CObjectDll::GetReceiveByteRingBuffer(unsigned int nChannel)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { unsigned int nIndex = m_stSocketThread[nChannel].m_pClient->m_nRingBufferReceiveIndex % RING_PACKET_SIZE; return m_stSocketThread[nChannel].m_pClient->m_byPacketBufferRing[nIndex]; }
	return nullptr;
}

BOOL CObjectDll::ByteToMImageData(unsigned int nChannel, BYTE* pByte, PACKET_M_IMAGE_DATA* pPacketData)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { m_stSocketThread[nChannel].m_pClient->m_pClientProtocol->ByteToMImageData(pByte, pPacketData); }
	return FALSE;
}

BOOL CObjectDll::ByteToAImageData(unsigned int nChannel, BYTE* pByte, PACKET_A_IMAGE_DATA* pPacketData)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_pClientProtocol->ByteToAImageData(pByte, pPacketData); }
	return FALSE;
}

BOOL CObjectDll::RingBufferReceiveIndexIncrement(unsigned int nChannel)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { m_stSocketThread[nChannel].m_pClient->m_nRingBufferReceiveIndex++; }
	return TRUE;
}

BOOL CObjectDll::BufferIndexInit(unsigned int nChannel)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr)
	{
		m_stSocketThread[nChannel].m_pClient->m_nRingBufferIndex			= 0;
		m_stSocketThread[nChannel].m_pClient->m_nRingBufferReceiveIndex		= 0;
		m_stSocketThread[nChannel].m_pClient->m_nBufferTotalIndex			= 0;
		m_stSocketThread[nChannel].m_pClient->m_nScanPacketBufferIndex		= 0;
		m_stSocketThread[nChannel].m_pClient->m_nScanPacketTotalLengh		= 0;
		m_stSocketThread[nChannel].m_pClient->m_nScanPacketLenghTemp		= 0;
		m_stSocketThread[nChannel].m_pClient->m_bScanPacketMode				= FALSE;
		m_stSocketThread[nChannel].m_pClient->m_bScanPacketLast				= FALSE;
	}
	return TRUE;
}

BOOL CObjectDll::SendScanRecipe(unsigned int nChannel, PACKET_RECIPE_DATA* pPacketData)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { if (m_stSocketThread[nChannel].m_pClient->m_pClientSendData != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_pClientSendData->SendScanRecipe(nChannel, pPacketData); } }
	return FALSE;
}

BOOL CObjectDll::SendRunStopPacket(unsigned int nChannel, PACKET_RUN_STOP_DATA* pPacketData)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { if (m_stSocketThread[nChannel].m_pClient->m_pClientSendData != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_pClientSendData->SendRunStopPacket(nChannel, pPacketData); } }
	return FALSE;
}

BOOL CObjectDll::SendSocketDataPacket(unsigned int nChannel, PACKET_SOCKET_PING_DATA* pPacketData)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { if (m_stSocketThread[nChannel].m_pClient->m_pClientSendData != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_pClientSendData->SendSocketDataPacket(nChannel, pPacketData); } }
	return FALSE;
}

BOOL CObjectDll::GetSocketStatus(unsigned int nChannel)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { return m_stSocketThread[nChannel].m_bConnect; }
	return FALSE;
}

BOOL CObjectDll::SendAPacketRunPacket(unsigned int nChannel, PACKET_A_PACKET_RUN_DATA* pPacketData)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { if (m_stSocketThread[nChannel].m_pClient->m_pClientSendData != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_pClientSendData->SendAPacketRunPacket(nChannel, pPacketData); } }
	return FALSE;
}

BOOL CObjectDll::SendAverageCountPacket(unsigned int nChannel, PACKET_AVERAGE_COUNT_DATA* pPacketData)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { if (m_stSocketThread[nChannel].m_pClient->m_pClientSendData != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_pClientSendData->SendAverageCountPacket(nChannel, pPacketData); } }
	return FALSE;
}

BOOL CObjectDll::SendEm1024ResetPacket(unsigned int nChannel, PACKET_EM1024_RESET_DATA* pPacketData)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { if (m_stSocketThread[nChannel].m_pClient->m_pClientSendData != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_pClientSendData->SendEm1024ResetPacket(nChannel, pPacketData); } }
	return FALSE;
}

BOOL CObjectDll::SendMPacketRunPacket(unsigned int nChannel, PACKET_M_PACKET_RUN_DATA* pPacketData)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { if (m_stSocketThread[nChannel].m_pClient->m_pClientSendData != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_pClientSendData->SendMPacketRunPacket(nChannel, pPacketData); } }
	return FALSE;
}

BOOL CObjectDll::SetScanPacketLengh(unsigned int nChannel, unsigned int nLength)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { if (m_stSocketThread[nChannel].m_pClient->m_pClientSendData != nullptr) { return m_stSocketThread[nChannel].m_pClient->SetScanPacketLengh(nLength); } }
	return FALSE;
}

BOOL CObjectDll::SetScanPacketMode(unsigned int nChannel, BOOL bFlag)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { if (m_stSocketThread[nChannel].m_pClient->m_pClientSendData != nullptr) { return m_stSocketThread[nChannel].m_pClient->SetScanPacketMode(bFlag); } }
	return FALSE;
}

BOOL CObjectDll::GetScanPacketLast(unsigned int nChannel)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { return m_stSocketThread[nChannel].m_pClient->GetScanPacketLast(); }
	return FALSE;
}

unsigned int CObjectDll::GetRingBufferTotalIndex(unsigned int nChannel)
{
	if (m_stSocketThread[nChannel].m_pClient != nullptr) { return m_stSocketThread[nChannel].m_pClient->m_nBufferTotalIndex; }
	return 0;
}