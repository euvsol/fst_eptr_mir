#pragma once

#include "PTR_SocketProtocolDefine.h"
#include "PTR_SocketProtocolMessage.h"
#include "PTR_SocketProtocolStructData.h"
#include "PTR_SocketProtocolStructByte.h"

#include <queue>

using namespace std;

class CTcpClientProtocol
{

public:
	CTcpClientProtocol(void* pFramework, void* pParents);
	virtual ~CTcpClientProtocol();

public:
	void*			m_pFramework;
	void*			m_pParents;

public:
	void Protocol(BYTE* pByte);

	// Image M Packet Data
	BOOL			MImageDataToMImageByte(PACKET_M_IMAGE_DATA* pPacketData, PACKET_M_IMAGE_BYTE* pPacketByte);
	BOOL			ByteToMImageData(BYTE* pByte, PACKET_M_IMAGE_DATA* pPacketData);
	unsigned int	MImageByteToByte(PACKET_M_IMAGE_BYTE* pPacketByte, BYTE* pByte);

	// Image A Packet Data
	BOOL			AImageDataToAImageByte(PACKET_A_IMAGE_DATA* pPacketData, PACKET_A_IMAGE_BYTE* pPacketByte);
	BOOL			ByteToAImageData(BYTE* pByte, PACKET_A_IMAGE_DATA* pPacketData);
	unsigned int	AImageByteToByte(PACKET_A_IMAGE_BYTE* pPacketByte, BYTE* pByte);

	// RunStop Data
	BOOL			RunStopDataToRunStopByte(PACKET_RUN_STOP_DATA* pPacketData, PACKET_RUN_STOP_BYTE* pPacketByte);
//	BOOL			ByteToRunStopData(BYTE* pByte, PACKET_RUN_STOP_DATA* pPacketData);
	unsigned int	RunStopByteToByte(PACKET_RUN_STOP_BYTE* pPacketByte, BYTE* pByte);

	// A Packet Run Data
	BOOL			APacketRunDataToAPacketRunByte(PACKET_A_PACKET_RUN_DATA* pPacketData, PACKET_A_PACKET_RUN_BYTE* pPacketByte);
//	BOOL			ByteToAPacketRunData(BYTE* pByte, PACKET_A_PACKET_RUN_DATA* pPacketData);
	unsigned int	APacketRunByteToByte(PACKET_A_PACKET_RUN_BYTE* pPacketByte, BYTE* pByte);

	// Average Count Data
	BOOL			AverageCountDataToAverageCountByte(PACKET_AVERAGE_COUNT_DATA* pPacketData, PACKET_AVERAGE_COUNT_BYTE* pPacketByte);
//	BOOL			ByteToAverageCountData(BYTE* pByte, PACKET_AVERAGE_COUNT_DATA* pPacketData);
	unsigned int	AverageCountByteToByte(PACKET_AVERAGE_COUNT_BYTE* pPacketByte, BYTE* pByte);

	// EM1024 Reset Data
	BOOL			Em1024ResetDataToEm1024ResetByte(PACKET_EM1024_RESET_DATA* pPacketData, PACKET_EM1024_RESET_BYTE* pPacketByte);
//	BOOL			ByteToEm1024ResetData(BYTE* pByte, PACKET_EM1024_RESET_DATA* pPacketData);
	unsigned int	Em1024ResetByteToByte(PACKET_EM1024_RESET_BYTE* pPacketByte, BYTE* pByte);

	// M Packet Run Data
	BOOL			MPacketRunDataToMPacketRunByte(PACKET_M_PACKET_RUN_DATA* pPacketData, PACKET_M_PACKET_RUN_BYTE* pPacketByte);
//	BOOL			ByteToMPacketRunData(BYTE* pByte, PACKET_M_PACKET_RUN_DATA* pPacketData);
	unsigned int	MPacketRunByteToByte(PACKET_M_PACKET_RUN_BYTE* pPacketByte, BYTE* pByte);

	// Recipe Data
	BOOL			RecipeDataToRecipeByte(PACKET_RECIPE_DATA* pPacketData, PACKET_RECIPE_BYTE* pPacketByte);
//	BOOL			ByteToRecipeData(BYTE* pByte, PACKET_RECIPE_DATA* pPacketData);
	unsigned int	RecipeByteToByte(PACKET_RECIPE_BYTE* pPacketByte, BYTE* pByte);

	// Socket Ping Data
	BOOL			SocketDataToSocketByte(PACKET_SOCKET_PING_DATA* pPacketData, PACKET_SOCKET_PING_BYTE* pPacketByte);
	BOOL			ByteToSocketData(BYTE* pByte, PACKET_SOCKET_PING_DATA* pPacketData);
	unsigned int	SocketByteToByte(PACKET_SOCKET_PING_BYTE* pPacketData, BYTE* pByte);
};