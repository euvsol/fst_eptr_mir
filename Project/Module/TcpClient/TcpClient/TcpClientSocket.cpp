#include "stdafx.h"
#include "TcpClientSocket.h"
#include "TcpSocketThread.h"
#include "ObjectDll.h"

void TraceRelease(LPCTSTR lpszFormat, ...)
{
	CString sResult;

	va_list vlMarker = NULL;
	va_start(vlMarker, lpszFormat);

	sResult.FormatV(lpszFormat, vlMarker);

	va_end(vlMarker);
	OutputDebugString(sResult);
}

template <class Function>
__int64 time_call(Function&& f)
{
	__int64 begin = GetTickCount();
	f();
	return GetTickCount() - begin;
}

CTcpClientSocket::CTcpClientSocket(void* pFramework, void* pParents, unsigned int nChannel, CString strIP, unsigned int nPort)
{
	m_pFramework				= pFramework;
	m_pParents					= pParents;

	m_nChannel					= nChannel;
	m_strIP						= strIP;
	m_nPort						= nPort;

	m_hSocket					= NULL;
	m_pClientProtocol			= nullptr;
	m_pClientSendData			= nullptr;
	m_byPacketBuffer			= nullptr;
	m_byPacketBufferRing		= nullptr;
	m_nPacketBufferSize			= 0;
	m_nRingBufferIndex			= 0;
	m_nRingBufferReceiveIndex	= 0;
	m_nBufferTotalIndex			= 0;

	m_nScanPacketBufferIndex = 0;
	m_nScanPacketTotalLengh = 0;
	m_nScanPacketLenghTemp = 0;
	m_bScanPacketMode = FALSE;
	m_bScanPacketLast = FALSE;

	m_pClientProtocol = new CTcpClientProtocol(m_pFramework, this);
	m_pClientSendData = new CTcpClientSendData(m_pFramework, this);
}

CTcpClientSocket::~CTcpClientSocket()
{
	if (m_pClientProtocol != nullptr) { delete m_pClientProtocol; m_pClientProtocol = nullptr; }
	if (m_pClientSendData != nullptr) { delete m_pClientSendData; m_pClientSendData = nullptr; }
	if (m_byPacketBuffer != nullptr) { free(m_byPacketBuffer); m_byPacketBuffer = nullptr; }
	if (m_byPacketBufferRing != nullptr) { for (int i = 0; i < RING_PACKET_SIZE; i++) { if (m_byPacketBufferRing[i] != nullptr) { free(m_byPacketBufferRing[i]); m_byPacketBufferRing[i] = nullptr; } } free(m_byPacketBufferRing); m_byPacketBufferRing = nullptr; }
}

void CTcpClientSocket::OnReceive(int nErrorCode)
{
	CObjectDll* pObject = (CObjectDll*)m_pParents;

	unsigned int nLength = Receive(m_byPacketBuffer, m_nPacketBufferSize);

	if (nLength > 0)
	{
		// ****************** Buffer Merge Secnario ****************** //
		//
		// 1. TempBuffer + Length == 68006 일때
		//
		// 2. TempBuffer + Length > 68006 일때
		//		2-1. LengthTemp == 68006 일때, 반복
		//		2-2. TempBuffer + LengthTemp < 68006 일때
		//			2-2-1. 나머지 LengthTemp Buffer 저장
		//			2-2-2. 마지막 Packet 확인
		//
		// 3. TempBuffer + Length < 68006 일때
		//		3-1. 나머지 Length Buffer 저장
		//		3-2. 마지막 Packet 확인
		//
		// ****************** Buffer Merge Secnario ****************** //

		if (m_bScanPacketMode)
		{
			if		(MACRO_COMPARE_A_TO_B_SAME	(m_nScanPacketBufferIndex + nLength, GetScanPacketLengh1000())) { Sequence_A_TO_B_SAME	(&nLength); }
			else if (MACRO_COMPARE_A_TO_B_LARGE	(m_nScanPacketBufferIndex + nLength, GetScanPacketLengh1000())) { Sequence_A_TO_B_LARGE	(&nLength); }
			else if (MACRO_COMPARE_A_TO_B_SMALL	(m_nScanPacketBufferIndex + nLength, GetScanPacketLengh1000())) { Sequence_A_TO_B_SMALL	(&nLength); }
		}
		else
		{
			memcpy(m_byPacketBufferRing[m_nRingBufferIndex], m_byPacketBuffer, nLength);
			m_nRingBufferIndex++;
			m_nBufferTotalIndex++;
			if (m_nRingBufferIndex == RING_PACKET_SIZE) { m_nRingBufferIndex = 0; }
		}
	}

	CSocket::OnReceive(nErrorCode);
}

void CTcpClientSocket::OnClose(int nErrorCode)
{
	CObjectDll* pObject = (CObjectDll*)m_pParents;
	CWnd*		pWnd	= (CWnd*)pObject->m_pParents;
	WPARAM		wParam	= 0;
	LPARAM		lParam	= (LPARAM)this;

	SendMessage(pWnd->m_hWnd, WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_CLOSE, wParam, lParam);

	CSocket::OnClose(nErrorCode);
}

BOOL CTcpClientSocket::BufferSizeChange(unsigned int nBufferSize)
{
	if (m_nPacketBufferSize != nBufferSize)
	{
		if (m_byPacketBuffer != nullptr) { free(m_byPacketBuffer); m_byPacketBuffer = nullptr; }
		m_byPacketBuffer = (BYTE*)malloc(sizeof(BYTE) * nBufferSize);
		if (m_byPacketBufferRing != nullptr) { for (int i = 0; i < RING_PACKET_SIZE; i++) { if (m_byPacketBufferRing[i] != nullptr) { free(m_byPacketBufferRing[i]); m_byPacketBufferRing[i] = nullptr; } } free(m_byPacketBufferRing); m_byPacketBufferRing = nullptr; }
		m_byPacketBufferRing = (BYTE**)malloc(sizeof(BYTE*) * RING_PACKET_SIZE);
		for (int i = 0; i < RING_PACKET_SIZE; i++) { m_byPacketBufferRing[i] = (BYTE*)malloc(sizeof(BYTE) * nBufferSize); } 
		m_nPacketBufferSize = nBufferSize;
	}

	return TRUE;
}

unsigned int CTcpClientSocket::GetScanPacketLengh1000()
{
	return PROTOCOL_HEADER_SIZE + PROTOCOL_PACKET_SIZE_1000;
}

BOOL CTcpClientSocket::SetScanPacketLengh(unsigned int nLength)
{
	m_nScanPacketTotalLengh = nLength;
	return TRUE;
}

BOOL CTcpClientSocket::SetScanPacketMode(BOOL bFlag)
{
	return m_bScanPacketMode = bFlag;
}

BOOL CTcpClientSocket::GetScanPacketLast()
{
	return m_bScanPacketLast;
}

void CTcpClientSocket::Sequence_A_TO_B_SAME(unsigned int* nLength)
{
	// 1. TempBuffer + Length == 68006 일때
	memcpy(m_byPacketBufferRing[m_nRingBufferIndex] + m_nScanPacketBufferIndex, m_byPacketBuffer, GetScanPacketLengh1000());
	m_nScanPacketLenghTemp += *nLength;
	Sequence_StartPacket();
}

void CTcpClientSocket::Sequence_A_TO_B_LARGE(unsigned int* nLength)
{
	// 2. TempBuffer + Length > 68006 일때
	unsigned int nLengthTemp	= m_nScanPacketBufferIndex + *nLength;
	unsigned int nOverIndex		= 0;
	unsigned int nLengthDiv		= 0;
	unsigned int nOverSize		= 0;

	while (1)
	{
		nLengthDiv = nLengthTemp / GetScanPacketLengh1000();

		// 2-1. LengthTemp == 68006 일때, 반복
		if (nLengthDiv != 0)
		{
			nOverSize = GetScanPacketLengh1000() - m_nScanPacketBufferIndex;
			memcpy(m_byPacketBufferRing[m_nRingBufferIndex] + m_nScanPacketBufferIndex, m_byPacketBuffer + nOverIndex, GetScanPacketLengh1000());
			nOverIndex += nOverSize;
			m_nScanPacketLenghTemp += nOverSize;
			Sequence_StartPacket();
			nLengthTemp -= GetScanPacketLengh1000();
			if (m_nScanPacketTotalLengh == m_nScanPacketLenghTemp) { break; }
		}
		// 2-2. TempBuffer + LengthTemp < 68006 일때
		if (MACRO_COMPARE_A_TO_B_SMALL(m_nScanPacketBufferIndex + nLengthTemp, GetScanPacketLengh1000())) { Sequence_A_TO_B_SMALL(&nLengthTemp, &nOverIndex); break; }
	}
}

void CTcpClientSocket::Sequence_A_TO_B_SMALL(unsigned int* nLength, unsigned int* nOverIndex)
{
	// 3. TempBuffer + Length < 68006 일때

	// 3-1. 나머지 Length Buffer 저장
	if (MACRO_COMPARE_A_TO_B_SAME(m_nScanPacketLenghTemp + *nLength, m_nScanPacketTotalLengh))
	{
		if (nOverIndex == nullptr) { memcpy(m_byPacketBufferRing[m_nRingBufferIndex] + m_nScanPacketBufferIndex, m_byPacketBuffer, *nLength); }
		else					   { memcpy(m_byPacketBufferRing[m_nRingBufferIndex] + m_nScanPacketBufferIndex, m_byPacketBuffer + *nOverIndex, *nLength); }
		m_nScanPacketLenghTemp += *nLength;
		m_bScanPacketLast		= TRUE;
		Sequence_StartPacket();
	}
	// 3-2. 마지막 Packet 확인
	else
	{
		if (nOverIndex == nullptr) { memcpy(m_byPacketBufferRing[m_nRingBufferIndex] + m_nScanPacketBufferIndex, m_byPacketBuffer, *nLength); }
		else					   { memcpy(m_byPacketBufferRing[m_nRingBufferIndex] + m_nScanPacketBufferIndex, m_byPacketBuffer + *nOverIndex, *nLength); }
		m_nScanPacketBufferIndex	+= *nLength;
		m_nScanPacketLenghTemp		+= *nLength;
	}
}

void CTcpClientSocket::Sequence_StartPacket()
{
	m_nScanPacketBufferIndex = 0;
	m_nRingBufferIndex++;
	m_nBufferTotalIndex++;
	if (m_nRingBufferIndex == RING_PACKET_SIZE) { m_nRingBufferIndex = 0; }
}
