#include "stdafx.h"
#include "TcpClientSendData.h"
#include "TcpClientSocket.h"

CTcpClientSendData::CTcpClientSendData(void* pFramework, void* pParents)
{
	m_pFramework		= pFramework;
	m_pParents			= pParents;
	m_byPacketBuffer	= nullptr;
	m_nPacketBufferSize = 0;
}

CTcpClientSendData::~CTcpClientSendData()
{
	if (m_byPacketBuffer != nullptr) { free(m_byPacketBuffer); m_byPacketBuffer = nullptr; }
}

BOOL CTcpClientSendData::SendRunStopPacket(int nChannel, PACKET_RUN_STOP_DATA* pPacketData)
{
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)m_pParents;
	CTcpClientProtocol* pProtocol	= pClient->m_pClientProtocol;

	PACKET_RUN_STOP_BYTE PacketByte;

	pProtocol->RunStopDataToRunStopByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize		= pProtocol->RunStopByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	ConvertToHexDebugging(m_byPacketBuffer, nLength);

	return TRUE;
}

BOOL CTcpClientSendData::SendScanRecipe(int nChannel, PACKET_RECIPE_DATA* pPacketData)
{
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)m_pParents;
	CTcpClientProtocol* pProtocol	= pClient->m_pClientProtocol;

	PACKET_RECIPE_BYTE PacketByte;

	pProtocol->RecipeDataToRecipeByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize		= pProtocol->RecipeByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	ConvertToHexDebugging(m_byPacketBuffer, nLength);

	return TRUE;
}

BOOL CTcpClientSendData::SendSocketDataPacket(int nChannel, PACKET_SOCKET_PING_DATA* pPacketData)
{
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)m_pParents;
	CTcpClientProtocol* pProtocol	= pClient->m_pClientProtocol;

	PACKET_SOCKET_PING_BYTE PacketByte;

	pProtocol->SocketDataToSocketByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize		= pProtocol->SocketByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	ConvertToHexDebugging(m_byPacketBuffer, nLength);

	return TRUE;
}

BOOL CTcpClientSendData::SendAPacketRunPacket(int nChannel, PACKET_A_PACKET_RUN_DATA* pPacketData)
{
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)m_pParents;
	CTcpClientProtocol* pProtocol	= pClient->m_pClientProtocol;

	PACKET_A_PACKET_RUN_BYTE PacketByte;

	pProtocol->APacketRunDataToAPacketRunByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize		= pProtocol->APacketRunByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	ConvertToHexDebugging(m_byPacketBuffer, nLength);

	return TRUE;
}

BOOL CTcpClientSendData::SendAverageCountPacket(int nChannel, PACKET_AVERAGE_COUNT_DATA* pPacketData)
{
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)m_pParents;
	CTcpClientProtocol* pProtocol	= pClient->m_pClientProtocol;

	PACKET_AVERAGE_COUNT_BYTE PacketByte;

	pProtocol->AverageCountDataToAverageCountByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize		= pProtocol->AverageCountByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	ConvertToHexDebugging(m_byPacketBuffer, nLength);

	return TRUE;
}

BOOL CTcpClientSendData::SendEm1024ResetPacket(int nChannel, PACKET_EM1024_RESET_DATA* pPacketData)
{
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)m_pParents;
	CTcpClientProtocol* pProtocol	= pClient->m_pClientProtocol;

	PACKET_EM1024_RESET_BYTE PacketByte;

	pProtocol->Em1024ResetDataToEm1024ResetByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize		= pProtocol->Em1024ResetByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	ConvertToHexDebugging(m_byPacketBuffer, nLength);

	return TRUE;
}

BOOL CTcpClientSendData::SendMPacketRunPacket(int nChannel, PACKET_M_PACKET_RUN_DATA* pPacketData)
{
	CTcpClientSocket*	pClient		= (CTcpClientSocket*)m_pParents;
	CTcpClientProtocol* pProtocol	= pClient->m_pClientProtocol;

	PACKET_M_PACKET_RUN_BYTE PacketByte;

	pProtocol->MPacketRunDataToMPacketRunByte(pPacketData, &PacketByte);

	memset(m_byPacketBuffer, 0x00, m_nPacketBufferSize);

	unsigned int nSendSize		= pProtocol->MPacketRunByteToByte(&PacketByte, m_byPacketBuffer);
	unsigned int nLength		= pClient->Send(m_byPacketBuffer, nSendSize);

	ConvertToHexDebugging(m_byPacketBuffer, nLength);

	return TRUE;
}

BOOL CTcpClientSendData::BufferSizeChange(unsigned int nBufferSize)
{
	if (m_nPacketBufferSize != nBufferSize)
	{
		if (m_byPacketBuffer != nullptr) { free(m_byPacketBuffer); m_byPacketBuffer = nullptr; }
		m_byPacketBuffer = (BYTE*)malloc(sizeof(BYTE) * nBufferSize);
		m_nPacketBufferSize = nBufferSize;
	}

	return TRUE;
}

CString CTcpClientSendData::ConvertToHex(CString data)
{
	CString returnvalue;
	for (int x = 0; x < data.GetLength(); x++)
	{
		CString temporary;
		int value = (int)(data[x]);
		temporary.Format(_T("%02X "), value);
		returnvalue += temporary;
	}
	return returnvalue;
}

void CTcpClientSendData::ConvertToHexDebugging(BYTE* pByte, int nLength)
{
	CString strResult;
	CString strTokData;

	strResult.Format(_T("[Client][Send][%d Byte] : "), nLength);

	for (int i = 0; i < nLength; i++)
	{
		strTokData.Format(_T("%c"), pByte[i]);
		strTokData = ConvertToHex(strTokData);
		strResult += strTokData;
	}

	TRACE(_T("* %s\n"), strResult);
}