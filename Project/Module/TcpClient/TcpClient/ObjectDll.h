#pragma once

#include "TcpClientSocket.h"
#include "TcpSocketThread.h"

#include "PTR_SocketProtocolMessage.h"
#include "PTR_SocketProtocolStruct.h"


class CObjectDll
{
	
public:
	CObjectDll();
	~CObjectDll();

public:
	void*				m_pFramework;
	void*				m_pParents;
	
	SOCKET_THREAD		m_stSocketThread[CLIENT_NETWORK_COUNT];
	BOOL				m_bSocketConnecting;

public:
	void SetSocketConnecting(BOOL bFlag) { m_bSocketConnecting = bFlag; }
	BOOL GetSocketConnecting()			 { return m_bSocketConnecting; }

public:
//	(FUNCTION_RET)		(FUNCTION_NAME)							(Type* A, Type* B));
	BOOL				FrameworkInstance						(void* pFramework, void* pParents);
	BOOL				CreateClient							(unsigned int nChannel, CString strIP, unsigned int nPort);
	BOOL				CloseClient								(unsigned int nChannel, CString strIP, unsigned int nPort);
	BOOL				BufferSizeChange						(unsigned int nChannel, unsigned int nBufferSize);
	unsigned int		GetRingBufferReceiveIndex				(unsigned int nChannel);
	unsigned int		GetRingBufferIndex						(unsigned int nChannel);
	BYTE*				GetCurrentByteRingBuffer				(unsigned int nChannel);
	BYTE*				GetReceiveByteRingBuffer				(unsigned int nChannel);
	BOOL				ByteToMImageData						(unsigned int nChannel, BYTE* pByte, PACKET_M_IMAGE_DATA* pPacketData);
	BOOL				ByteToAImageData						(unsigned int nChannel, BYTE* pByte, PACKET_A_IMAGE_DATA* pPacketData);
	BOOL				RingBufferReceiveIndexIncrement			(unsigned int nChannel);
	BOOL				BufferIndexInit							(unsigned int nChannel);
	BOOL				SendScanRecipe							(unsigned int nChannel, PACKET_RECIPE_DATA* pPacketData);
	BOOL				SendRunStopPacket						(unsigned int nChannel, PACKET_RUN_STOP_DATA* pPacketData);
	BOOL				SendSocketDataPacket					(unsigned int nChannel, PACKET_SOCKET_PING_DATA* pData);
	BOOL				GetSocketStatus							(unsigned int nChannel);
	BOOL				SendAPacketRunPacket					(unsigned int nChannel, PACKET_A_PACKET_RUN_DATA* pPacketData);
	BOOL				SendAverageCountPacket					(unsigned int nChannel, PACKET_AVERAGE_COUNT_DATA* pPacketData);
	BOOL				SendEm1024ResetPacket					(unsigned int nChannel, PACKET_EM1024_RESET_DATA* pPacketData);
	BOOL				SendMPacketRunPacket					(unsigned int nChannel, PACKET_M_PACKET_RUN_DATA* pPacketData);
	BOOL				SetScanPacketLengh						(unsigned int nChannel, unsigned int nLength);
	BOOL				SetScanPacketMode						(unsigned int nChannel, BOOL bFlag);
	BOOL				GetScanPacketLast						(unsigned int nChannel);
	unsigned int		GetRingBufferTotalIndex					(unsigned int nChannel);
};

extern class CObjectDll ObjectDll;