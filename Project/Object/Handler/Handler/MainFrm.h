
// MainFrm.h : CMainFrame 클래스의 인터페이스
//

#pragma once

#include "HandlerDoc.h"
#include "HandlerView.h"

#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_ProtocolMessageIPC.h"

class CMainFrame : public CFrameWnd
{
	
protected: // serialization에서만 만들어집니다.
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 특성입니다.
public:

	//CHandlerView* pSeventhExProject_AView;

// 작업입니다.
public:
	CString m_strMessage;
	
public:
	BOOL OnMessageAdamRunPacketTimeOutUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamPingUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamRecipeUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamRunUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamStopUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamAverageCountUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamAPacketRunUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamMPacketRunUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamDisplayInitUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamRawDisplayUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamInterpolationDisplayUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamAverageDisplayUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamTDataUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamRDataUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureBGUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureWO_PUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureBSUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureP_As_PointUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureP_As_ScanUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureP_As_Scan_StopUpdate(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureEndUpdate(COPYDATASTRUCT* pCopyDataStruct);
	

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 구현입니다.
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 컨트롤 모음이 포함된 멤버입니다.
	CStatusBar        m_wndStatusBar;

// 생성된 메시지 맵 함수
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
};


