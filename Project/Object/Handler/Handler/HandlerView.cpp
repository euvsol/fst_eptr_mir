
// SeventhExProject_AView.cpp : CHandlerView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "Handler.h"
#endif

#include "MainFrm.h"
#include "HandlerDoc.h"
#include "HandlerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CHandlerView

IMPLEMENT_DYNCREATE(CHandlerView, CFormView)

BEGIN_MESSAGE_MAP(CHandlerView, CFormView)
	ON_WM_DESTROY()
	ON_WM_PAINT()
	ON_WM_MEASUREITEM()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RECIEPE, &CHandlerView::OnNMDblclkListReciepe)
	ON_NOTIFY(NM_CLICK, IDC_LIST_RECIEPE, &CHandlerView::OnNMClickListReciepe)
	ON_WM_TIMER()
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_RECIEPE, &CHandlerView::OnNMCustomdrawListReciepe)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_RUN, &CHandlerView::OnBnClickedButtonProtocolAdamRun)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_STOP, &CHandlerView::OnBnClickedButtonProtocolAdamStop)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_RECIPE, &CHandlerView::OnBnClickedButtonProtocolAdamRecipe)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_M_PACKET_RUN, &CHandlerView::OnBnClickedButtonProtocolMPacketRun)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_A_PACKET_RUN, &CHandlerView::OnBnClickedButtonProtocolAPacketRun)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_EM1024_RESET, &CHandlerView::OnBnClickedButtonProtocolEm1024Reset)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_MEASURE_BG, &CHandlerView::OnBnClickedButtonProtocolAdamMeasureBg)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_MEASURE_WO_P, &CHandlerView::OnBnClickedButtonProtocolAdamMeasureWoP)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_MEASURE_BS, &CHandlerView::OnBnClickedButtonProtocolAdamMeasureBs)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_MEASURE_P_AS_POINT, &CHandlerView::OnBnClickedButtonProtocolAdamMeasurePAsPoint)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_MEASURE_P_AS_SCAN, &CHandlerView::OnBnClickedButtonProtocolAdamMeasurePAsScan)
	ON_WM_DRAWITEM()
	ON_WM_CTLCOLOR()
	ON_WM_ERASEBKGND()

	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_RUN_PACKET_TIMEOUT_UPDATE, &CHandlerView::OnMessageAdamRunPacketTimeOutUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_PING_UPDATE, &CHandlerView::OnMessageAdamPingUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_RECIPE_UPDATE, &CHandlerView::OnMessageAdamRecipeUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_RUN_UPDATE, &CHandlerView::OnMessageAdamRunUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_STOP_UPDATE, &CHandlerView::OnMessageAdamStopUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_AVERAGE_COUNT_UPDATE, &CHandlerView::OnMessageAdamAverageCountUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_A_PACKET_RUN_UPDATE, &CHandlerView::OnMessageAdamAPacketRunUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_M_PACKET_RUN_UPDATE, &CHandlerView::OnMessageAdamMPacketRunUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_DISPLAY_INIT_UPDATE, &CHandlerView::OnMessageAdamDisplayInitUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_RAW_DISPLAY_UPDATE, &CHandlerView::OnMessageAdamRawDisplayUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_INTERPOLATION_DISPLAY_UPDATE, &CHandlerView::OnMessageAdamInterpolationDisplayUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_AVERAGE_DISPLAY_UPDATE, &CHandlerView::OnMessageAdamAverageDisplayUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_T_DATA_UPDATE, &CHandlerView::OnMessageAdamTDataUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_ADAM_R_DATA_UPDATE, &CHandlerView::OnMessageAdamRDataUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_MEASURE_END_UPDATE, &CHandlerView::OnMessageMeasureEndUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_MEASURE_BG_UPDATE, &CHandlerView::OnMessageMeasureBGUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_MEASURE_WO_P_UPDATE, &CHandlerView::OnMessageMeasureWO_PUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_MEASURE_BS_UPDATE, &CHandlerView::OnMessageMeasureBSUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_MEASURE_P_AS_POINT_UPDATE, &CHandlerView::OnMessageMeasureP_As_PointUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_MEASURE_P_AS_SCAN_UPDATE, &CHandlerView::OnMessageMeasureP_As_ScanUpdate)
	ON_MESSAGE(WM_USER_MESSAGE_CP_MEASURE_P_AS_SCAN_STOP_UPDATE, &CHandlerView::OnMessageMeasureP_As_Scan_StopUpdate)
END_MESSAGE_MAP()

// CHandlerView 생성/소멸

CHandlerView::CHandlerView()
	: CFormView(CHandlerView::IDD)
{
	m_nSavedItem = -1;
	m_nSavedSubitem = -1;

	m_nRawCountCheck = 0;
	m_nInterpolationCountCheck = 0;
	m_nAverageCountCheck = 0;
	m_nScanSetCountCheck = 0;

	m_nMPacketCount = 1;
	m_nAPacketCount = 1;

	m_nBeforeImageWidth = 0;
	m_nBeforeImageHeight = 0;

	m_bEqpStatus = FALSE;

	m_nPacketMode = MODE_RUN_PACKET;

	m_dMat_T = nullptr;
	m_dMat_R = nullptr;
	m_MatRawSRC = nullptr;
	m_MatRawInterpolationSRC = nullptr;
	m_MatInterpolationSRC = nullptr;
	m_MatAverageSRC = nullptr;

	m_bySharedMemoryRaw = nullptr;
	m_bySharedMemoryRawInterpolation = nullptr;
	m_bySharedMemoryInterpolation = nullptr;
	m_bySharedMemoryAverage = nullptr;

	m_bySharedMemoryT = nullptr;
	m_bySharedMemoryR = nullptr;
	m_bySharedMemoryPosX = nullptr;
	m_bySharedMemoryPosY = nullptr;

	m_nMatRawInterpolationControlX = 0;
	m_nMatRawInterpolationControlY = 0;
	m_nMatRawInterpolationResizeX = 0;
	m_nMatRawInterpolationResizeY = 0;
	m_bitmapInfoMatRawInterpolation = nullptr;

	m_nMatRawControlX = 0;
	m_nMatRawControlY = 0;
	m_nMatRawResizeX = 0;
	m_nMatRawResizeY = 0;
	m_bitmapInfoMatRaw = nullptr;

	m_nMatInterpolationControlX = 0;
	m_nMatInterpolationControlY = 0;
	m_nMatInterpolationResizeX = 0;
	m_nMatInterpolationResizeY = 0;
	m_bitmapInfoMatInterpolation = nullptr;

	m_nMatAverageControlX = 0;
	m_nMatAverageControlY = 0;
	m_nMatAverageResizeX = 0;
	m_nMatAverageResizeY = 0;
	m_bitmapInfoMatAverage = nullptr;

	memset(&m_hTransmittanceMapping, 0x00, sizeof(m_hTransmittanceMapping));
	memset(&m_pTransmittanceAddress, 0x00, sizeof(m_pTransmittanceAddress));

	memset(&m_hReflectanceMapping, 0x00, sizeof(m_hReflectanceMapping));
	memset(&m_pReflectanceAddress, 0x00, sizeof(m_pReflectanceAddress));

	memset(&m_hRawMapping, 0x00, sizeof(m_hRawMapping));
	memset(&m_pRawAddress, 0x00, sizeof(m_pRawAddress));

	memset(&m_hRawInterpolationMapping, 0x00, sizeof(m_hRawInterpolationMapping));
	memset(&m_pRawInterpolationAddress, 0x00, sizeof(m_pRawInterpolationAddress));

	memset(&m_hInterpolationMapping, 0x00, sizeof(m_hInterpolationMapping));
	memset(&m_pInterpolationAddress, 0x00, sizeof(m_pInterpolationAddress));

	memset(&m_hAverageMapping, 0x00, sizeof(m_hAverageMapping));
	memset(&m_pAverageAddress, 0x00, sizeof(m_pAverageAddress));

	m_pThread		= nullptr;
	m_hEvent		= NULL;
	m_bThreadStatus = FALSE;
	ReCreateThread();
	InitializeCriticalSection(&m_pThreadCritical);

	CMainFrame*	pFrame = (CMainFrame *)AfxGetMainWnd();

	m_hSharedMemory = LoadLibrary(_T("SharedMemory"));
	m_pSharedMemory = new CSharedMemory(m_hSharedMemory);

	m_hLogViewer = LoadLibrary(_T("LogViewer"));
	m_pLogViewer = new CLogViewer(m_hLogViewer);
	m_pLogViewer->FrameworkInstance(pFrame, this);
}

CHandlerView::~CHandlerView()
{
	if (m_pSharedMemory != nullptr) { delete m_pSharedMemory; m_pSharedMemory = nullptr; FreeLibrary(m_hSharedMemory); m_hSharedMemory = NULL; }
	if (m_pLogViewer != nullptr) { delete m_pLogViewer; m_pLogViewer = nullptr; FreeLibrary(m_hLogViewer); m_hLogViewer = NULL; }

	if (m_pThread != nullptr) { StopThread(); m_pThread->SuspendThread(); DWORD dw; ::GetExitCodeThread(m_pThread->m_hThread, &dw); delete m_pThread; m_pThread = nullptr; }

	if (m_brushsMenubar != NULL) { DeleteObject(m_brushsMenubar); m_brushsMenubar = NULL; }
	if (m_brushsStatic != NULL) { DeleteObject(m_brushsStatic); m_brushsStatic = NULL; }
	if (m_brushsEdit != NULL) { DeleteObject(m_brushsEdit); m_brushsEdit = NULL; }
	if (m_brushsEqpStatusOn != NULL) { DeleteObject(m_brushsEqpStatusOn); m_brushsEqpStatusOn = NULL; }
	if (m_brushsEqpStatusOff != NULL) { DeleteObject(m_brushsEqpStatusOff); m_brushsEqpStatusOff = NULL; }

	if (m_dMat_T != nullptr) { delete m_dMat_T; m_dMat_T = nullptr; }
	if (m_dMat_R != nullptr) { delete m_dMat_R; m_dMat_R = nullptr; }
	if (m_MatRawSRC != nullptr) { delete m_MatRawSRC; m_MatRawSRC = nullptr; }
	if (m_MatRawInterpolationSRC != nullptr) { delete m_MatRawInterpolationSRC; m_MatRawInterpolationSRC = nullptr; }
	if (m_MatInterpolationSRC != nullptr) { delete m_MatInterpolationSRC; m_MatInterpolationSRC = nullptr; }
	if (m_MatAverageSRC != nullptr) { delete m_MatAverageSRC; m_MatAverageSRC = nullptr; }

	if (m_bySharedMemoryRaw != nullptr) { free(m_bySharedMemoryRaw); m_bySharedMemoryRaw = nullptr; }
	if (m_bySharedMemoryRawInterpolation != nullptr) { free(m_bySharedMemoryRawInterpolation); m_bySharedMemoryRawInterpolation = nullptr; }
	if (m_bySharedMemoryInterpolation != nullptr) { free(m_bySharedMemoryInterpolation); m_bySharedMemoryInterpolation = nullptr; }
	if (m_bySharedMemoryAverage != nullptr) { free(m_bySharedMemoryAverage); m_bySharedMemoryAverage = nullptr; }

	if (m_bySharedMemoryT != nullptr) { free(m_bySharedMemoryT); m_bySharedMemoryT = nullptr; }
	if (m_bySharedMemoryR != nullptr) { free(m_bySharedMemoryR); m_bySharedMemoryR = nullptr; }
	if (m_bySharedMemoryPosX != nullptr) { free(m_bySharedMemoryPosX); m_bySharedMemoryPosX = nullptr; }
	if (m_bySharedMemoryPosY != nullptr) { free(m_bySharedMemoryPosY); m_bySharedMemoryPosY = nullptr; }

	if (m_pTransmittanceAddress != NULL) ::UnmapViewOfFile(m_pTransmittanceAddress);
	if (m_hTransmittanceMapping != NULL) ::CloseHandle(m_hTransmittanceMapping);

	if (m_pReflectanceAddress != NULL) ::UnmapViewOfFile(m_pReflectanceAddress);
	if (m_hReflectanceMapping != NULL) ::CloseHandle(m_hReflectanceMapping);

	if (m_pRawAddress != NULL) ::UnmapViewOfFile(m_pRawAddress);
	if (m_hRawMapping != NULL) ::CloseHandle(m_hRawMapping);

	if (m_pRawInterpolationAddress != NULL) ::UnmapViewOfFile(m_pRawInterpolationAddress); m_pRawInterpolationAddress = NULL;
	if (m_hRawInterpolationMapping != NULL) ::CloseHandle(m_hRawInterpolationMapping); m_hRawInterpolationMapping = NULL;

	if (m_pInterpolationAddress != NULL) ::UnmapViewOfFile(m_pInterpolationAddress);
	if (m_hInterpolationMapping != NULL) ::CloseHandle(m_hInterpolationMapping);

	if (m_pAverageAddress != NULL) ::UnmapViewOfFile(m_pAverageAddress);
	if (m_hAverageMapping != NULL) ::CloseHandle(m_hAverageMapping);
}

void CHandlerView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_RECIEPE, m_ctlImageRecipe);
	DDX_Control(pDX, IDC_EDIT_IMAGE_RECIPE, m_ctlEditImageRecipe);
	DDX_Control(pDX, IDC_STATIC_PROGRAM_NAME, m_ctlProjectName);
}

BOOL CHandlerView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CHandlerView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	CRect rc;
	GetClientRect(rc);

	m_nDialogWidth = rc.Width();
	m_nDialogHeight = rc.Height();

	m_fnCreateBrushInit();

	m_fnCreateLogViewerInit();

	m_fnMenuBarInit();

	m_fnImageInfoList();

	m_fnCreateImageInit();
}


// CHandlerView 진단

#ifdef _DEBUG
void CHandlerView::AssertValid() const
{
	CFormView::AssertValid();
}

void CHandlerView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CHandlerDoc* CHandlerView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CHandlerDoc)));
	return (CHandlerDoc*)m_pDocument;
}
#endif //_DEBUG


// CHandlerView 메시지 처리기


void CHandlerView::OnDestroy()
{
	CFormView::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

}

void CHandlerView::m_fnDisplayImage(Mat* pMatImage, unsigned int nControlView, int* nPictureControlX, int* nPictureControlY, int* nResizeX, int* nResizeY, BITMAPINFO*& bitmapInfo)
{
	Mat MatManualResize;

//	EnterCriticalSection(&m_pImageThreadCritical);
	resize(*pMatImage, MatManualResize, cv::Size(*nResizeX - 2, *nResizeY), INTER_LINEAR);
//	LeaveCriticalSection(&m_pImageThreadCritical);

	CWnd* pWnd_ImageTraget;
	pWnd_ImageTraget = GetDlgItem(nControlView);

	CClientDC dcImageTraget(pWnd_ImageTraget);

	dcImageTraget.SetStretchBltMode(MAXSTRETCHBLTMODE);
	::StretchDIBits(
		dcImageTraget.GetSafeHdc(),
		0,
		0,
		*nPictureControlX,
		*nPictureControlY,
		0,
		0,
		MatManualResize.cols,
		MatManualResize.rows,
		MatManualResize.data,
		bitmapInfo,
		DIB_RGB_COLORS,
		SRCCOPY
	);
}

void CHandlerView::m_fnDisplayImageInfoUpdate(Mat* pMatImage, unsigned int nControlView, int* nPictureControlX, int* nPictureControlY, int* nResizeX, int* nResizeY, BITMAPINFO*& bitmapInfo)
{
	RECT rcImageTraget;

	CWnd* pWnd_ImageTraget;
	pWnd_ImageTraget = GetDlgItem(nControlView);

	CClientDC dcImageTraget(pWnd_ImageTraget);
	pWnd_ImageTraget->GetClientRect(&rcImageTraget);

	*nPictureControlX = rcImageTraget.right;
	*nPictureControlY = rcImageTraget.bottom;

	double dMagnificationX = (double)pMatImage->cols / (double)*nPictureControlX;
	double dMagnificationY = (double)pMatImage->rows / (double)*nPictureControlY;

	double dMagnification = (dMagnificationX < dMagnificationY) ? dMagnificationY : dMagnificationX;

	*nResizeX = ((int)(floor((double)pMatImage->cols / dMagnification)));
	*nResizeY = ((int)(floor((double)pMatImage->rows / dMagnification)));

	if (*nResizeX % 2 == TRUE)  { *nResizeX -= 1; }
	if (*nResizeY % 2 == FALSE) { *nResizeY -= 1; }

	int nDummyX = (*nResizeX < *nPictureControlX) ? (*nPictureControlX - *nResizeX) / 2 : 0;
	int nDummyY = (*nResizeY < *nPictureControlY) ? (*nPictureControlY - *nResizeY) / 2 : 0;

	if (bitmapInfo != nullptr) { free(bitmapInfo); }

	bitmapInfo = (BITMAPINFO*)malloc(sizeof(BITMAPINFO));

	bitmapInfo->bmiHeader.biSize			= sizeof(BITMAPINFOHEADER);				 
	bitmapInfo->bmiHeader.biPlanes			= 1;									 
	bitmapInfo->bmiHeader.biCompression		= BI_RGB;								 
	bitmapInfo->bmiHeader.biSizeImage		= 0;									 
	bitmapInfo->bmiHeader.biXPelsPerMeter	= 0;									 
	bitmapInfo->bmiHeader.biYPelsPerMeter	= 0;									 
	bitmapInfo->bmiHeader.biClrUsed			= 0;									 
	bitmapInfo->bmiHeader.biClrImportant	= 0;									 
	bitmapInfo->bmiHeader.biWidth			= *nResizeX - 2;
	bitmapInfo->bmiHeader.biHeight			= -(*nResizeY);
	bitmapInfo->bmiHeader.biBitCount		= 3 * 8;		 
}

UINT CHandlerView::Thread(LPVOID lpParam)
{
	CHandlerView* pThread = (CHandlerView*)lpParam;

	while (1)
	{
		WaitForSingleObject(pThread->m_hEvent, INFINITE);

		pThread->SetThreadStatus(TRUE);

		pThread->Run(NULL, FALSE);

		pThread->SetThreadStatus(FALSE);

		ResetEvent(pThread->m_hEvent);
	}

	return 0;
}

BOOL CHandlerView::Run(int nSequence, BOOL bMessage)
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Image Display Sequence!! **********")));

	while (GetThreadStatus())
	{
//		SendMessage(WM_USER_MESSAGE_CP_ADAM_RAW_DISPLAY_UPDATE_COPY, NULL, NULL);
//		SendMessage(WM_USER_MESSAGE_CP_ADAM_RAW_INTERPOLATION_DISPLAY_UPDATE_COPY, NULL, NULL);

		OnMessageAdamRawDisplayUpdate_Copy(NULL, NULL);
		OnMessageAdamRawInterpolationDisplayUpdate_Copy(NULL, NULL);
		Sleep(1);
	}

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Finish Image Display Sequence!! **********")));

	return TRUE;
}

BOOL CHandlerView::ReCreateThread()
{
	StopThread();

	Sleep(100);

	if (m_pThread != nullptr)
		m_pThread->SuspendThread();

	if (m_pThread != nullptr)
	{
		DWORD dw;
		::GetExitCodeThread(m_pThread->m_hThread, &dw);
		if (dw == STILL_ACTIVE)
		{
			TerminateThread(m_pThread->m_hThread, 0);
			CloseHandle(m_pThread->m_hThread);
		}
		m_pThread = nullptr;
	}

	if (m_pThread == nullptr)
	{
		m_hEvent = CreateEventA(NULL, FALSE, FALSE, NULL);
		ResetEvent(m_hEvent);
		m_pThread = AfxBeginThread(Thread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
		m_pThread->ResumeThread();
	}

	return TRUE;
}

BOOL CHandlerView::StartThread()
{
	SetThreadStatus(TRUE);
	SetEvent(m_hEvent);
	return TRUE;
}

BOOL CHandlerView::StopThread()
{
	SetThreadStatus(FALSE);
	return TRUE;
}

CString CHandlerView::m_fnGetCurrentTimeFunction()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"[%4d-%02d-%02d %02d:%02d:%02d.%03d] : ",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay,
		SystemTime.wHour,
		SystemTime.wMinute,
		SystemTime.wSecond,
		SystemTime.wMilliseconds);

	return strTime;
}

void CHandlerView::m_fnImageInfoList()
{
	m_ctlImageRecipe.DeleteAllItems();

	m_ctlImageRecipe.ModifyStyle(LVS_OWNERDRAWFIXED, 0, 0);
	m_ctlImageRecipe.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_ctlImageRecipe.InsertColumn(0, _T("                 Menu"),	LVCFMT_LEFT,	120, -1); 
	m_ctlImageRecipe.InsertColumn(1, _T("Value"),					LVCFMT_CENTER,	70 , -1); 
	m_ctlImageRecipe.InsertColumn(2, _T("Description"),				LVCFMT_CENTER,	144, -1); 

	CString strTemp;

	m_nImageFOV_X = 99900;
	strTemp.Format(_T("%d"), m_nImageFOV_X);

	m_ctlImageRecipe.InsertItem	(0, _T("Image X FOV")); 
	m_ctlImageRecipe.SetItem	(0, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(0, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nImageFOV_Y = 99900;
	strTemp.Format(_T("%d"), m_nImageFOV_Y);

	m_ctlImageRecipe.InsertItem	(1, _T("Image FOV")); 
	m_ctlImageRecipe.SetItem	(1, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(1, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nScanGrid_X = 100;
	strTemp.Format(_T("%d"), m_nScanGrid_X);

	m_ctlImageRecipe.InsertItem	(2, _T("Scan Grid")); 
	m_ctlImageRecipe.SetItem	(2, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(2, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nScanGrid_Y = 100;
	strTemp.Format(_T("%d"), m_nScanGrid_Y);

	m_ctlImageRecipe.InsertItem	(3, _T("Scan Grid")); 
	m_ctlImageRecipe.SetItem	(3, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(3, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nInterpolationCount = 1;
	strTemp.Format(_T("%d"), m_nInterpolationCount);

	m_ctlImageRecipe.InsertItem	(4, _T("Interpolation Count")); 
	m_ctlImageRecipe.SetItem	(4, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(4, 2, LVIF_TEXT, _T("Interplation Image Count"), 0, 0, 0, NULL );

	m_nAverageCount = 3;
	strTemp.Format(_T("%d"), m_nAverageCount);

	m_ctlImageRecipe.InsertItem	(5, _T("Average Count")); 
	m_ctlImageRecipe.SetItem	(5, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(5, 2, LVIF_TEXT, _T("Average Image Count"), 0, 0, 0, NULL );

	m_nSetCount = 1;
	strTemp.Format(_T("%d"), m_nSetCount);

	m_ctlImageRecipe.InsertItem	(6, _T("Set Count")); 
	m_ctlImageRecipe.SetItem	(6, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(6, 2, LVIF_TEXT, _T("Set Image Count"), 0, 0, 0, NULL );

	m_nScanDirection = 1;
	strTemp.Format(_T("%d"), m_nScanDirection);

	m_ctlImageRecipe.InsertItem	(7, _T("Scan Direction")); 
	m_ctlImageRecipe.SetItem	(7, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(7, 2, LVIF_TEXT, _T("Ex) 1 : V / 0 : H"), 0, 0, 0, NULL );

	m_nPacketCount = 1000;
	strTemp.Format(_T("%d"), m_nPacketCount);

	m_ctlImageRecipe.InsertItem	(8, _T("Packet Count"));
	m_ctlImageRecipe.SetItem	(8, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL);
	m_ctlImageRecipe.SetItem	(8, 2, LVIF_TEXT, _T("Data Packet Count"), 0, 0, 0, NULL);

	SetDlgItemText(IDC_EDIT_M_PACKET_COUNT, _T("1000"));
	SetDlgItemText(IDC_EDIT_A_PACKET_COUNT, _T("1000"));
	SetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, _T("5000"));
}


void CHandlerView::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	// TODO: 여기에 메시지 처리기 코드를 추가 및/또는 기본값을 호출합니다.
	if (nIDCtl == IDC_LIST_RECIEPE || nIDCtl == IDC_LIST_SOCKET_CONNECT)
	{
		lpMeasureItemStruct->itemHeight += 10;
	}

	CFormView::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}


void CHandlerView::OnNMDblclkListReciepe(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	LPNMITEMACTIVATE pNMITEM = (LPNMITEMACTIVATE)pNMHDR;

	int m_nSavedItem = pNMITEM->iItem;
	int m_nSavedSubitem = pNMITEM->iSubItem;

	if (pNMITEM->iItem != -1)
	{
		if (m_nSavedSubitem == 1)
		{
			CRect rect;

			if (pNMITEM->iSubItem == 0)
			{
				m_ctlImageRecipe.GetItemRect(pNMITEM->iItem, rect, LVIR_BOUNDS);
				rect.right = rect.left + m_ctlImageRecipe.GetColumnWidth(0);
			}
			else
			{
				m_ctlImageRecipe.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			}

			m_ctlImageRecipe.ClientToScreen(rect);
			this->ScreenToClient(rect);

			m_ctlEditImageRecipe.ShowWindow(TRUE);
			m_ctlEditImageRecipe.SetWindowText(m_ctlImageRecipe.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			m_ctlEditImageRecipe.SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
			m_ctlEditImageRecipe.SetFocus();
		}
	}

	*pResult = 0;
}


void CHandlerView::OnNMClickListReciepe(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	LPNMITEMACTIVATE pNMITEM = (LPNMITEMACTIVATE)pNMHDR;

	m_nSavedItem = m_nSavedSubitem = -1;
	m_ctlEditImageRecipe.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

	if (pNMITEM->iItem != -1)
	{
		m_nSavedItem = pNMITEM->iItem;
		m_nSavedSubitem = pNMITEM->iSubItem;
	}

	*pResult = 0;
}

BOOL CHandlerView::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN) {
		if (pMsg->wParam == VK_RETURN) {

			if (pMsg->hwnd == GetDlgItem(IDC_EDIT_IMAGE_RECIPE)->GetSafeHwnd())
			{
				CString str;
				GetDlgItemText(IDC_EDIT_IMAGE_RECIPE, str);
				m_ctlImageRecipe.SetItemText(m_nSavedItem, m_nSavedSubitem, str);

				switch (m_nSavedItem)
				{
					case 0: { m_nImageFOV_X			= _ttof(str);  break; }
					case 1: { m_nImageFOV_Y			= _ttof(str);  break; }
					case 2: { m_nScanGrid_X			= _ttoi(str);  break; }
					case 3: { m_nScanGrid_Y			= _ttoi(str);  break; }
					case 4: { m_nInterpolationCount	= _ttoi(str);  break; }
					case 5: { m_nAverageCount		= _ttoi(str);  break; }
					case 6: { m_nSetCount			= _ttoi(str);  break; }
					case 7: { m_nScanDirection		= _ttoi(str);  break; }
					case 8: { m_nPacketCount		= _ttoi(str);  break; }
				}

				GetDlgItem(IDC_EDIT_IMAGE_RECIPE)->ShowWindow(SW_HIDE);
			}

			return TRUE;
		}

		if (pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}

	return CFormView::PreTranslateMessage(pMsg);
}

void CHandlerView::m_fnScanRecipeUpdate()
{
	CString strTemp;
	strTemp.Format(_T("%d"), m_nImageFOV_X);
	m_ctlImageRecipe.SetItemText(0, 1, strTemp);

	strTemp.Format(_T("%d"), m_nImageFOV_Y);
	m_ctlImageRecipe.SetItemText(1, 1, strTemp);

	strTemp.Format(_T("%d"), m_nScanGrid_X);
	m_ctlImageRecipe.SetItemText(2, 1, strTemp);

	strTemp.Format(_T("%d"), m_nScanGrid_Y);
	m_ctlImageRecipe.SetItemText(3, 1, strTemp);

	strTemp.Format(_T("%d"), m_nInterpolationCount);
	m_ctlImageRecipe.SetItemText(4, 1, strTemp);

	strTemp.Format(_T("%d"), m_nAverageCount);
	m_ctlImageRecipe.SetItemText(5, 1, strTemp);

	strTemp.Format(_T("%d"), m_nSetCount);
	m_ctlImageRecipe.SetItemText(6, 1, strTemp);

	strTemp.Format(_T("%d"), m_nScanDirection);
	m_ctlImageRecipe.SetItemText(7, 1, strTemp);

	strTemp.Format(_T("%d"), m_nPacketCount);
	m_ctlImageRecipe.SetItemText(8, 1, strTemp);
}

void CHandlerView::OnNMCustomdrawListReciepe(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMCUSTOMDRAW pNMCD;

	pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	 
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	int iRow = (int)lplvcd->nmcd.dwItemSpec;
	 
	switch(lplvcd->nmcd.dwDrawStage)
	{
		case CDDS_PREPAINT :
		{
			*pResult = CDRF_NOTIFYITEMDRAW;
			return;
		}
			 
		// Modify item text and or background
		case CDDS_ITEMPREPAINT:
		{
			lplvcd->clrText = RGB(0,0,0);

			// If you want the sub items the same as the item,
			// set *pResult to CDRF_NEWFONT
			*pResult = CDRF_NOTIFYSUBITEMDRAW;
			return;
		}
			 
		// Modify sub item text and/or background
		case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		{
			// 홀수열, 짝수열의 배경색 재설정
			if(iRow % 2)	{ lplvcd->clrTextBk = RGB(255, 255, 255); }
			else			{ lplvcd->clrTextBk = RGB(230, 230, 230); }
				
			*pResult = CDRF_DODEFAULT;
			return;
		}
	}

	*pResult = 0;
}

int CHandlerView::m_fnPixelWidth()
{
	return (m_nImageFOV_X / m_nScanGrid_X) + 1;
}

int CHandlerView::m_fnPixelHeight()
{
	return (m_nImageFOV_Y / m_nScanGrid_Y) + 1;
}

void CHandlerView::OnBnClickedButtonProtocolAdamRun()
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Sequence!! **********")));

	m_fnImageSizeChangeAndInit();

	IPC_ADAM_RUN_DATA PacketData;

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_ADAM_RUN;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Adam Run.")));

	m_bEqpStatus = TRUE;
	m_fnRefreshEqpStatusRect();
}


void CHandlerView::OnBnClickedButtonProtocolAdamStop()
{
	IPC_ADAM_STOP_DATA PacketData;

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_ADAM_STOP;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Adam Stop.")));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Finish Sequence!! **********")));

	m_bEqpStatus = FALSE;
	m_fnRefreshEqpStatusRect();
}


void CHandlerView::OnBnClickedButtonProtocolMPacketRun()
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Sequence!! **********")));

	CString strCount;
	GetDlgItemText(IDC_EDIT_M_PACKET_COUNT, strCount);

	IPC_ADAM_M_PACKET_RUN_DATA PacketData;

	PacketData.nCount = _ttoi(strCount);

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_ADAM_M_PACKET_RUN;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Receive M Packet[%d] Count : 1"), _ttoi(strCount)));

	m_bEqpStatus = TRUE;
	m_fnRefreshEqpStatusRect();
}


void CHandlerView::OnBnClickedButtonProtocolAPacketRun()
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Sequence!! **********")));

	CString strCount;
	GetDlgItemText(IDC_EDIT_A_PACKET_COUNT, strCount);

	IPC_ADAM_AVERAGE_COUNT_DATA PacketDataA;

	PacketDataA.nCount = _ttoi(strCount);

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_ADAM_AVERAGE_COUNT;
	stCopyDataStruct.cbData = sizeof(PacketDataA);
	stCopyDataStruct.lpData = &PacketDataA;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Average Count.")));

	IPC_ADAM_A_PACKET_RUN_DATA PacketDataB;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_ADAM_A_PACKET_RUN;
	stCopyDataStruct.cbData = sizeof(PacketDataB);
	stCopyDataStruct.lpData = &PacketDataB;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Receive A Packet[%d] Count : 1"), _ttoi(strCount)));

	m_bEqpStatus = TRUE;
	m_fnRefreshEqpStatusRect();
}


void CHandlerView::OnBnClickedButtonProtocolEm1024Reset()
{
	IPC_ADAM_EM1024_RESET_DATA PacketData;

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_ADAM_EM1024_RESET;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Em1024 Reset.")));
}


void CHandlerView::OnBnClickedButtonProtocolAdamRecipe()
{
	IPC_ADAM_RECIPE_DATA PacketData;

	PacketData.ImageFOV_X			= m_nImageFOV_X;
	PacketData.ImageFOV_Y			= m_nImageFOV_Y;
	PacketData.ScanGrid_X			= m_nScanGrid_X;
	PacketData.ScanGrid_Y			= m_nScanGrid_Y;
	PacketData.InterpolationCount	= m_nInterpolationCount;
	PacketData.AverageCount			= m_nAverageCount;
	PacketData.SetCount				= m_nSetCount;
	PacketData.PacketCount			= m_nPacketCount;

	m_fnSharedMemoryAllInit();

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_ADAM_RECIPE;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Send Recipe.")));
}

void CHandlerView::m_fnImageSizeChangeAndInit()
{
	int nImageWidth		= m_fnPixelWidth();
	int nImageHeight	= m_fnPixelHeight();

	if (m_nBeforeImageHeight != nImageHeight || m_nBeforeImageWidth != nImageWidth)
	{
		delete m_dMat_T; m_dMat_T = nullptr;
		delete m_dMat_R; m_dMat_R = nullptr;

		delete m_MatRawSRC; m_MatRawSRC = nullptr;
		delete m_MatRawInterpolationSRC; m_MatRawInterpolationSRC = nullptr;
		delete m_MatInterpolationSRC; m_MatInterpolationSRC = nullptr;
		delete m_MatAverageSRC; m_MatAverageSRC = nullptr;

		free(m_bySharedMemoryRaw); m_bySharedMemoryRaw = nullptr;
		free(m_bySharedMemoryRawInterpolation); m_bySharedMemoryRawInterpolation = nullptr;
		free(m_bySharedMemoryInterpolation); m_bySharedMemoryInterpolation = nullptr;
		free(m_bySharedMemoryAverage); m_bySharedMemoryAverage = nullptr;

		free(m_bySharedMemoryT); m_bySharedMemoryT = nullptr;
		free(m_bySharedMemoryR); m_bySharedMemoryR = nullptr;
		free(m_bySharedMemoryPosX); m_bySharedMemoryPosX = nullptr;
		free(m_bySharedMemoryPosY); m_bySharedMemoryPosY = nullptr;

		m_dMat_T					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMat_R					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_MatRawSRC					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_8UC3));
		m_MatRawInterpolationSRC	= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_8UC3));
		m_MatInterpolationSRC		= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_8UC3));
		m_MatAverageSRC				= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_8UC3));

		m_bySharedMemoryRaw					= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * nImageHeight) * 3);
		m_bySharedMemoryRawInterpolation	= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		m_bySharedMemoryInterpolation		= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		m_bySharedMemoryAverage				= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);

		m_bySharedMemoryT					= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		m_bySharedMemoryR					= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		m_bySharedMemoryPosX				= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		m_bySharedMemoryPosY				= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));

		m_fnDisplayImageInfoUpdate(m_MatRawInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION, &m_nMatRawInterpolationControlX, &m_nMatRawInterpolationControlY, &m_nMatRawInterpolationResizeX, &m_nMatRawInterpolationResizeY, m_bitmapInfoMatRawInterpolation);
		m_fnDisplayImageInfoUpdate(m_MatRawSRC, IDC_IMAGE_DISPLAY_THREAD_RAW, &m_nMatRawControlX, &m_nMatRawControlY, &m_nMatRawResizeX, &m_nMatRawResizeY, m_bitmapInfoMatRaw);
		m_fnDisplayImageInfoUpdate(m_MatInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_INTERPOLATION, &m_nMatInterpolationControlX, &m_nMatInterpolationControlY, &m_nMatInterpolationResizeX, &m_nMatInterpolationResizeY, m_bitmapInfoMatInterpolation);
		m_fnDisplayImageInfoUpdate(m_MatAverageSRC, IDC_IMAGE_DISPLAY_THREAD_AVERAGE, &m_nMatAverageControlX, &m_nMatAverageControlY, &m_nMatAverageResizeX, &m_nMatAverageResizeY, m_bitmapInfoMatAverage);
	}
	else
	{
		*m_dMat_T					= Mat::zeros(m_dMat_T->rows, m_dMat_T->cols, CV_64F);
		*m_dMat_R					= Mat::zeros(m_dMat_R->rows, m_dMat_R->cols, CV_64F);
		*m_MatRawSRC				= Mat::zeros(m_MatRawSRC->rows, m_MatRawSRC->cols, CV_8UC3);
		*m_MatRawInterpolationSRC	= Mat::zeros(m_MatRawInterpolationSRC->rows, m_MatRawInterpolationSRC->cols, CV_8UC3);
		*m_MatInterpolationSRC		= Mat::zeros(m_MatInterpolationSRC->rows, m_MatInterpolationSRC->cols, CV_8UC3);
		*m_MatAverageSRC			= Mat::zeros(m_MatAverageSRC->rows, m_MatAverageSRC->cols, CV_8UC3);

		memset(m_bySharedMemoryRaw				, 0x00, sizeof(BYTE) * (nImageWidth * nImageHeight) * 3);
		memset(m_bySharedMemoryRawInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		memset(m_bySharedMemoryInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		memset(m_bySharedMemoryAverage			, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);

		memset(m_bySharedMemoryT				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		memset(m_bySharedMemoryR				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		memset(m_bySharedMemoryPosX				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		memset(m_bySharedMemoryPosY				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	}

	m_nBeforeImageWidth		= nImageWidth;
	m_nBeforeImageHeight	= nImageHeight;
}

void CHandlerView::m_fnCreateImageInit()
{
	int nImageWidth		= m_fnPixelWidth();
	int nImageHeight	= m_fnPixelHeight();

	m_dMat_T					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMat_R					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_MatRawSRC					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_8UC3));
	m_MatRawInterpolationSRC	= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageHeight * IMAGE_MAGNIFICATION, CV_8UC3));
	m_MatInterpolationSRC		= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageHeight * IMAGE_MAGNIFICATION, CV_8UC3));
	m_MatAverageSRC				= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageHeight * IMAGE_MAGNIFICATION, CV_8UC3));

	m_bySharedMemoryRaw					= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * nImageHeight) * 3);
	m_bySharedMemoryRawInterpolation	= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
	m_bySharedMemoryInterpolation		= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
	m_bySharedMemoryAverage				= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);

	m_bySharedMemoryT					= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	m_bySharedMemoryR					= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	m_bySharedMemoryPosX				= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	m_bySharedMemoryPosY				= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));

	memset(m_bySharedMemoryRaw				, 0x00, sizeof(BYTE) * (nImageWidth * nImageHeight) * 3);
	memset(m_bySharedMemoryRawInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
	memset(m_bySharedMemoryInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
	memset(m_bySharedMemoryAverage			, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);

	memset(m_bySharedMemoryT				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	memset(m_bySharedMemoryR				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	memset(m_bySharedMemoryPosX				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	memset(m_bySharedMemoryPosY				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));

	m_fnDisplayImageInfoUpdate(m_MatRawInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION, &m_nMatRawInterpolationControlX, &m_nMatRawInterpolationControlY, &m_nMatRawInterpolationResizeX, &m_nMatRawInterpolationResizeY, m_bitmapInfoMatRawInterpolation);
	m_fnDisplayImageInfoUpdate(m_MatRawSRC, IDC_IMAGE_DISPLAY_THREAD_RAW, &m_nMatRawControlX, &m_nMatRawControlY, &m_nMatRawResizeX, &m_nMatRawResizeY, m_bitmapInfoMatRaw);
	m_fnDisplayImageInfoUpdate(m_MatInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_INTERPOLATION, &m_nMatInterpolationControlX, &m_nMatInterpolationControlY, &m_nMatInterpolationResizeX, &m_nMatInterpolationResizeY, m_bitmapInfoMatInterpolation);
	m_fnDisplayImageInfoUpdate(m_MatAverageSRC, IDC_IMAGE_DISPLAY_THREAD_AVERAGE, &m_nMatAverageControlX, &m_nMatAverageControlY, &m_nMatAverageResizeX, &m_nMatAverageResizeY, m_bitmapInfoMatAverage);

	m_nBeforeImageWidth		= nImageWidth;
	m_nBeforeImageHeight	= nImageHeight;
}

LRESULT CHandlerView::OnMessageAdamPingUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_PING_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Ping!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamRecipeUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_RECIPE_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Recipe!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamRunUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_STOP_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	StartThread();

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Run!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamStopUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_STOP_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	StopThread();

	m_fnImageSizeChangeAndInit();

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Stop!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamAverageCountUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_STOP_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Average Count!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamMPacketRunUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	PACKET_M_IMAGE_DATA_CONTENT PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack M Packet Sequence!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamAPacketRunUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	PACKET_A_IMAGE_DATA_CONTENT PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack A Packet Sequence!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamTDataUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_T_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack T Data!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamRDataUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_R_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack R Data!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamDisplayInitUpdate(WPARAM wParam, LPARAM lParam)
{
	m_fnDisplayImage(m_MatRawInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION, &m_nMatRawInterpolationControlX, &m_nMatRawInterpolationControlY, &m_nMatRawInterpolationResizeX, &m_nMatRawInterpolationResizeY, m_bitmapInfoMatRawInterpolation);
	m_fnDisplayImage(m_MatRawSRC, IDC_IMAGE_DISPLAY_THREAD_RAW, &m_nMatRawControlX, &m_nMatRawControlY, &m_nMatRawResizeX, &m_nMatRawResizeY, m_bitmapInfoMatRaw);
	m_fnDisplayImage(m_MatInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_INTERPOLATION, &m_nMatInterpolationControlX, &m_nMatInterpolationControlY, &m_nMatInterpolationResizeX, &m_nMatInterpolationResizeY, m_bitmapInfoMatInterpolation);
	m_fnDisplayImage(m_MatAverageSRC, IDC_IMAGE_DISPLAY_THREAD_AVERAGE, &m_nMatAverageControlX, &m_nMatAverageControlY, &m_nMatAverageResizeX, &m_nMatAverageResizeY, m_bitmapInfoMatAverage);

	m_fnImageCountDataIntit();

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Display Init Sequence!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamRawDisplayUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_RAW_DATA PacketData;

	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	CString strMemorySpace	= PacketData.cMemorySpace;
	int		nBufferAddress	= PacketData.nBufferAddress;
	int		nImageWidth		= PacketData.nImageWidth;
	int		nImageHeight	= PacketData.nImageHeight;
	int		nSize			= PacketData.nSize;
	int		nRow			= PacketData.nRow;
	int		nCol			= PacketData.nCol;
	int		nCount			= PacketData.nCount;

	m_pSharedMemory->GetMapOpenFileMapping(
		&m_hRawMapping,
		strMemorySpace);

	m_pSharedMemory->GetMapViewOfFileAddress_Byte(
		&m_hRawMapping,
		&m_pRawAddress,
		&m_bySharedMemoryRaw,
		nSize);

	memcpy(m_MatRawSRC->data, m_bySharedMemoryRaw, sizeof(BYTE) * nSize);

	m_fnDisplayImage(m_MatRawSRC, IDC_IMAGE_DISPLAY_THREAD_RAW, &m_nMatRawControlX, &m_nMatRawControlY, &m_nMatRawResizeX, &m_nMatRawResizeY, m_bitmapInfoMatRaw);

	m_nRawCountCheck = nCount;

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamRawInterpolationDisplayUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_RAW_INTERPOLATION_DATA PacketData;

	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	CString strMemorySpace	= PacketData.cMemorySpace;
	int		nBufferAddress	= PacketData.nBufferAddress;
	int		nImageWidth		= PacketData.nImageWidth;
	int		nImageHeight	= PacketData.nImageHeight;
	int		nSize			= PacketData.nSize;
	int		nRow			= PacketData.nRow;
	int		nCol			= PacketData.nCol;
	int		nCount			= PacketData.nCount;

	m_pSharedMemory->GetMapOpenFileMapping(
		&m_hInterpolationMapping,
		strMemorySpace);

	m_pSharedMemory->GetMapViewOfFileAddress_Byte(
		&m_hInterpolationMapping,
		&m_pInterpolationAddress,
		&m_bySharedMemoryRawInterpolation,
		nSize);

	memcpy(m_MatRawInterpolationSRC->data, m_bySharedMemoryRawInterpolation, sizeof(BYTE) * nSize);

	m_fnDisplayImage(m_MatRawInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION, &m_nMatRawInterpolationControlX, &m_nMatRawInterpolationControlY, &m_nMatRawInterpolationResizeX, &m_nMatRawInterpolationResizeY, m_bitmapInfoMatRawInterpolation);

	m_nInterpolationCountCheck = nCount;

	m_fnImageCountDataUpdate();

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamInterpolationDisplayUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_INTERPOLATION_DATA PacketData;

	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	CString strMemorySpace	= PacketData.cMemorySpace;
	int		nBufferAddress	= PacketData.nBufferAddress;
	int		nImageWidth		= PacketData.nImageWidth;
	int		nImageHeight	= PacketData.nImageHeight;
	int		nSize			= PacketData.nSize;
	int		nRow			= PacketData.nRow;
	int		nCol			= PacketData.nCol;
	int		nCount			= PacketData.nCount;

	m_pSharedMemory->GetMapOpenFileMapping(
		&m_hInterpolationMapping,
		strMemorySpace);

	m_pSharedMemory->GetMapViewOfFileAddress_Byte(
		&m_hInterpolationMapping,
		&m_pInterpolationAddress,
		&m_bySharedMemoryInterpolation,
		nSize);

	memcpy(m_MatInterpolationSRC->data, m_bySharedMemoryInterpolation, sizeof(BYTE) * nSize);

	m_fnDisplayImage(m_MatInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_INTERPOLATION, &m_nMatInterpolationControlX, &m_nMatInterpolationControlY, &m_nMatInterpolationResizeX, &m_nMatInterpolationResizeY, m_bitmapInfoMatInterpolation);

	m_nInterpolationCountCheck = nCount;

	m_fnImageCountDataUpdate();

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("* CCD Image Success : %d"), m_nRawCountCheck));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamAverageDisplayUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_ADAM_AVERAGE_DATA PacketData;

	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	CString strMemorySpace	= PacketData.cMemorySpace;
	int		nBufferAddress	= PacketData.nBufferAddress;
	int		nImageWidth		= PacketData.nImageWidth;
	int		nImageHeight	= PacketData.nImageHeight;
	int		nSize			= PacketData.nSize;
	int		nRow			= PacketData.nRow;
	int		nCol			= PacketData.nCol;
	int		nCount			= PacketData.nCount;

	m_pSharedMemory->GetMapOpenFileMapping(
		&m_hAverageMapping,
		strMemorySpace);

	m_pSharedMemory->GetMapViewOfFileAddress_Byte(
		&m_hAverageMapping,
		&m_pAverageAddress,
		&m_bySharedMemoryAverage,
		nSize);

	memcpy(m_MatAverageSRC->data, m_bySharedMemoryAverage, sizeof(BYTE) * nSize);

	m_fnDisplayImage(m_MatAverageSRC, IDC_IMAGE_DISPLAY_THREAD_AVERAGE, &m_nMatAverageControlX, &m_nMatAverageControlY, &m_nMatAverageResizeX, &m_nMatAverageResizeY, m_bitmapInfoMatAverage);

	m_nAverageCountCheck = nCount;

	m_fnImageCountDataUpdate();

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("* CCD Image Average Success : %d"), m_nAverageCountCheck));

	return TRUE;
}

LRESULT CHandlerView::OnMessageMeasureEndUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_MEASURE_END_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Measure End!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageMeasureBGUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_MEASURE_BG_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Measure BG!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageMeasureWO_PUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_MEASURE_WO_P_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Measure WO_P!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageMeasureBSUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_MEASURE_BS_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Measure BS!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageMeasureP_As_PointUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_MEASURE_P_AS_POINT_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Measure P_As_Point!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageMeasureP_As_ScanUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_MEASURE_P_AS_SCAN_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	m_fnSharedMemoryAllInit();

	StartThread();

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Measure P_As_Scan!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageMeasureP_As_Scan_StopUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	IPC_MEASURE_P_AS_SCAN_STOP_DATA PacketData;
	memcpy(&PacketData, pCopyDataStruct->lpData, sizeof(PacketData));

	StopThread();

	OnBnClickedButtonProtocolAdamStop();

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Measure P_As_Scan_Stop!! **********")));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** End Sequence!! **********")));

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamRunPacketTimeOutUpdate(WPARAM wParam, LPARAM lParam)
{
	COPYDATASTRUCT* pCopyDataStruct = (COPYDATASTRUCT*)lParam;

	SendMessage(WM_USER_MESSAGE_CP_MEASURE_P_AS_SCAN_STOP_UPDATE, NULL, NULL);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Ack Run Packet TimeOut Finish!! **********")));

	return TRUE;
}

void CHandlerView::m_fnImageCountDataIntit()
{
	m_nRawCountCheck = 0;
	m_nRawInterpolationCountCheck = 0;
	m_nInterpolationCountCheck = 0;
	m_nAverageCountCheck = 0;
	m_nScanSetCountCheck = 0;

	m_fnImageCountDataUpdate();
}

void CHandlerView::m_fnImageCountDataUpdate()
{
	CString strTemp;

	strTemp.Format(_T("Raw Image Count : %d"), m_nRawCountCheck);
	SetDlgItemText(IDC_STATIC_RAW_COUNT, strTemp);

	strTemp.Format(_T("Raw Interpolation Image Count : %d"), m_nRawInterpolationCountCheck);
	SetDlgItemText(IDC_STATIC_RAW_INTERPOLATION_COUNT, strTemp);

	strTemp.Format(_T("Interpolation Image Count : %d"), m_nInterpolationCountCheck);
	SetDlgItemText(IDC_STATIC_INTERPOLATION_COUNT, strTemp);

	strTemp.Format(_T("Average Image Count : %d"), m_nAverageCountCheck);
	SetDlgItemText(IDC_STATIC_AVERAGE_COUNT, strTemp);
}

void CHandlerView::OnBnClickedButtonProtocolAdamMeasureBg()
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Sequence!! **********")));

	CString strCount;
	GetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, strCount);

	IPC_MEASURE_BG_DATA PacketData;

	PacketData.nCount = _ttoi(strCount);

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_MEASURE_BG;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Measure BG.")));

	m_bEqpStatus = TRUE;
	m_fnRefreshEqpStatusRect();
}


void CHandlerView::OnBnClickedButtonProtocolAdamMeasureWoP()
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Sequence!! **********")));

	CString strCount;
	GetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, strCount);

	IPC_MEASURE_WO_P_DATA PacketData;

	PacketData.nCount = _ttoi(strCount);

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_MEASURE_WO_P;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Measure WO P.")));

	m_bEqpStatus = TRUE;
	m_fnRefreshEqpStatusRect();
}


void CHandlerView::OnBnClickedButtonProtocolAdamMeasureBs()
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Sequence!! **********")));

	CString strCount;
	GetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, strCount);

	IPC_MEASURE_BS_DATA PacketData;

	PacketData.nCount = _ttoi(strCount);

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_MEASURE_BS;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Measure BS.")));

	m_bEqpStatus = TRUE;
	m_fnRefreshEqpStatusRect();
}


void CHandlerView::OnBnClickedButtonProtocolAdamMeasurePAsPoint()
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Sequence!! **********")));

	CString strCount;
	GetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, strCount);

	IPC_MEASURE_P_AS_POINT_DATA PacketData;

	PacketData.nCount = _ttoi(strCount);

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_MEASURE_P_AS_POINT;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Measure P As Point.")));

	m_bEqpStatus = TRUE;
	m_fnRefreshEqpStatusRect();
}


void CHandlerView::OnBnClickedButtonProtocolAdamMeasurePAsScan()
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Sequence!! **********")));

	IPC_MEASURE_P_AS_SCAN_DATA PacketData;

	COPYDATASTRUCT stCopyDataStruct;

	stCopyDataStruct.dwData = WM_USER_MESSAGE_PROTOCOL_IPC_CP_TO_IP_MEASURE_P_AS_SCAN;
	stCopyDataStruct.cbData = sizeof(PacketData);
	stCopyDataStruct.lpData = &PacketData;

	m_pSharedMemory->SendMessageIPC(IPC_NAME_IMAGE_PROCESS, &stCopyDataStruct);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Protocol Measure P As Scan.")));

	m_bEqpStatus = TRUE;
	m_fnRefreshEqpStatusRect();
}

void CHandlerView::m_fnSharedMemoryAllInit()
{
	if (m_pTransmittanceAddress != NULL) ::UnmapViewOfFile(m_pTransmittanceAddress); m_pTransmittanceAddress = NULL;
	if (m_hTransmittanceMapping != NULL) ::CloseHandle(m_hTransmittanceMapping); m_hTransmittanceMapping = NULL;

	if (m_pReflectanceAddress != NULL) ::UnmapViewOfFile(m_pReflectanceAddress); m_pReflectanceAddress = NULL;
	if (m_hReflectanceMapping != NULL) ::CloseHandle(m_hReflectanceMapping); m_hReflectanceMapping = NULL;

	if (m_pRawAddress != NULL) ::UnmapViewOfFile(m_pRawAddress); m_pRawAddress = NULL;
	if (m_hRawMapping != NULL) ::CloseHandle(m_hRawMapping); m_hRawMapping = NULL;

	if (m_pRawInterpolationAddress != NULL) ::UnmapViewOfFile(m_pRawInterpolationAddress); m_pRawInterpolationAddress = NULL;
	if (m_hRawInterpolationMapping != NULL) ::CloseHandle(m_hRawInterpolationMapping); m_hRawInterpolationMapping = NULL;

	if (m_pInterpolationAddress != NULL) ::UnmapViewOfFile(m_pInterpolationAddress); m_pInterpolationAddress = NULL;
	if (m_hInterpolationMapping != NULL) ::CloseHandle(m_hInterpolationMapping); m_hInterpolationMapping = NULL;

	if (m_pAverageAddress != NULL) ::UnmapViewOfFile(m_pAverageAddress); m_pAverageAddress = NULL;
	if (m_hAverageMapping != NULL) ::CloseHandle(m_hAverageMapping); m_hAverageMapping = NULL;
}

CString CHandlerView::m_fnFormattedString(LPCTSTR lpszFormat, ...)
{
	CString sResult;

	va_list vlMarker = NULL;
	va_start(vlMarker, lpszFormat);

	sResult.FormatV(lpszFormat, vlMarker);

	va_end(vlMarker);

	return sResult;
}

void CHandlerView::m_fnCreateLogViewerInit()
{
	CString strFileName;
	CString strFilePath;
	CString strTotalPath;
	CString strFileTemp = _T("\\");

	strFileName		= _T("Handler");
	strFilePath		= RESULT_LOG_FOLDER_PATH + strFileTemp;
	strTotalPath	= strFilePath + strFileName;

	m_pLogViewer->OnInitialDialog(static_cast<UINT>(LOG_VIEWER::CH_1), 820, 460, 715, 505);
	m_pLogViewer->SetProgramName(static_cast<UINT>(LOG_VIEWER::CH_1), strFileName);
	m_pLogViewer->SetLogFolderPath(static_cast<UINT>(LOG_VIEWER::CH_1), strFilePath);
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_SEQUENCE, _T("Sequence"));
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_TEST, _T("TEST"));
	m_pLogViewer->SetTabControlPos(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_SEQUENCE);
}

void CHandlerView::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if ((nIDCtl == IDC_BUTTON_PROTOCOL_ADAM_RUN)					||
		(nIDCtl == IDC_BUTTON_PROTOCOL_ADAM_STOP)					||
		(nIDCtl == IDC_BUTTON_PROTOCOL_ADAM_MEASURE_BG)				||
		(nIDCtl == IDC_BUTTON_PROTOCOL_ADAM_MEASURE_WO_P)			||
		(nIDCtl == IDC_BUTTON_PROTOCOL_M_PACKET_RUN)				||
		(nIDCtl == IDC_BUTTON_PROTOCOL_A_PACKET_RUN)				||
		(nIDCtl == IDC_BUTTON_PROTOCOL_ADAM_MEASURE_BS)				||
		(nIDCtl == IDC_BUTTON_PROTOCOL_ADAM_MEASURE_P_AS_POINT)		||
		(nIDCtl == IDC_BUTTON_PROTOCOL_EM1024_RESET)				||
		(nIDCtl == IDC_BUTTON_PROTOCOL_ADAM_RECIPE)					||
		(nIDCtl == IDC_BUTTON_PROTOCOL_ADAM_MEASURE_P_AS_SCAN))
	{
		m_fnDrawDesignButtonSelect(lpDrawItemStruct, RGB(65, 65, 65), RGB(38, 38, 38), RGB(255, 255, 255));
	}

	CFormView::OnDrawItem(nIDCtl, lpDrawItemStruct);
}


HBRUSH CHandlerView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	UINT nID = pWnd->GetDlgCtrlID();

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (nID == IDC_STATIC_PROGRAM_PROJECT || 
			nID == IDC_STATIC_PROGRAM_VERSION)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(91, 91, 91));
			hbr = m_brushsMenubar;
		}

		if (nID == IDC_STATIC ||
			nID == IDC_STATIC_AVERAGE_COUNT ||
			nID == IDC_STATIC_RAW_COUNT ||
			nID == IDC_STATIC_RAW_INTERPOLATION_COUNT ||
			nID == IDC_STATIC_INTERPOLATION_COUNT)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(38, 38, 38));
			hbr = m_brushsStatic;
		}

		if ((nID == IDC_STATIC_EQP_STATUS) && m_bEqpStatus)
		{
			GetDlgItem(IDC_STATIC_EQP_STATUS)->SetWindowText(_T("ADAM SCAN RUNNING..."));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(0, 0, 0));
			pDC->SetBkColor(RGB(0, 255, 0));
			hbr = m_brushsEqpStatusOn;
		}
		else if ((nID == IDC_STATIC_EQP_STATUS) && !m_bEqpStatus)
		{
			GetDlgItem(IDC_STATIC_EQP_STATUS)->SetWindowText(_T("ADAM SCAN STOP STATUS"));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(255, 0, 0));
			hbr = m_brushsEqpStatusOff;
		}
	}
	else if (nCtlColor == CTLCOLOR_EDIT)
	{
		if (nID == IDC_EDIT_M_PACKET_COUNT ||
			nID == IDC_EDIT_A_PACKET_COUNT ||
			nID == IDC_EDIT_BS_PACKET_COUNT)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(38, 38, 38));
			hbr = m_brushsEdit;
		}
	}

	return hbr;
}

void CHandlerView::m_fnCreateBrushInit()
{
	m_brushsMenubar = CreateSolidBrush(RGB(91, 91, 91));
	m_brushsStatic = CreateSolidBrush(RGB(38, 38, 38));
	m_brushsEdit = CreateSolidBrush(RGB(53, 53, 53));
	m_brushsEqpStatusOn = CreateSolidBrush(RGB(0, 255, 0));
	m_brushsEqpStatusOff = CreateSolidBrush(RGB(255, 0, 0));
}

void CHandlerView::m_fnMenuBarInit()
{
	BITMAP	size;
	HBITMAP hBmp;

	m_ctlProjectName.ModifyStyle(0xF, SS_BITMAP | SS_CENTERIMAGE);

	hBmp = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_PROJECT_NAME));

	m_ctlProjectName.SetBitmap(hBmp);

	::GetObject(hBmp, sizeof(BITMAP), &size);

	m_fStaticBold.CreateFont(20, 8, 0, 0,
		FW_HEAVY, FALSE, FALSE, FALSE,
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH, "굴림");

	GetDlgItem(IDC_STATIC_PROGRAM_VERSION)->SetWindowText(_T("2.0.0 Ver"));
	GetDlgItem(IDC_STATIC_PROGRAM_VERSION)->SetFont(&m_fStaticBold);

	GetDlgItem(IDC_STATIC_PROGRAM_PROJECT)->SetWindowText(_T("PTR"));
	GetDlgItem(IDC_STATIC_PROGRAM_PROJECT)->SetFont(&m_fStaticBold);

	GetDlgItem(IDC_STATIC_EQP_STATUS)->SetWindowText(_T("ADAM SCAN STOP STATUS"));
	GetDlgItem(IDC_STATIC_EQP_STATUS)->SetFont(&m_fStaticBold);
	GetDlgItem(IDC_STATIC_EQP_STATUS)->GetWindowRect(m_rcEqpStatus);
	ScreenToClient(&m_rcEqpStatus);
}

void CHandlerView::m_fnDrawDesignButtonStay(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font, BOOL bFlag)
{
	if (bFlag)
    {
        CDC dc;
        RECT rect;
        dc.Attach(lpDrawItemStruct -> hDC);
        rect = lpDrawItemStruct->rcItem;
		dc.SetBkMode(TRANSPARENT);
        dc.Draw3dRect(&rect,color_edge,color_edge);
        dc.FillSolidRect(&rect, color_bk);

        UINT state	= lpDrawItemStruct-> itemState;
		UINT action = lpDrawItemStruct->itemAction;

//        if((state &ODS_SELECTED)) { dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT); }
//        else						{ dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT); }

        dc.SetBkColor(color_bk);
        dc.SetTextColor(color_font);
        TCHAR buffer[MAX_PATH];
        ZeroMemory(buffer,MAX_PATH);
        ::GetWindowTextA(lpDrawItemStruct->hwndItem,buffer,MAX_PATH);
        dc.DrawText(buffer,&rect,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
        dc.Detach();
    }
	else
	{
		CDC dc;
        RECT rect;
        dc.Attach(lpDrawItemStruct -> hDC);
        rect = lpDrawItemStruct->rcItem;
		dc.SetBkMode(TRANSPARENT);
		dc.Draw3dRect(&rect,color_edge,color_edge);
		dc.FillSolidRect(&rect,color_bk);

        UINT state = lpDrawItemStruct-> itemState;

//        if((state &ODS_SELECTED)) { dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT); }
//        else						{ dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT); }

        dc.SetBkColor(color_bk);
        dc.SetTextColor(color_font);
        TCHAR buffer[MAX_PATH];
        ZeroMemory(buffer,MAX_PATH);
        ::GetWindowTextA(lpDrawItemStruct->hwndItem,buffer,MAX_PATH);
        dc.DrawText(buffer,&rect,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
        dc.Detach();
	}
}

void CHandlerView::m_fnDrawDesignButtonSelect(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font)
{
	CDC dc;
	RECT rect;
	dc.Attach(lpDrawItemStruct->hDC);
	rect = lpDrawItemStruct->rcItem;
	dc.SetBkMode(TRANSPARENT);
	dc.Draw3dRect(&rect, color_edge, color_edge);
	dc.FillSolidRect(&rect, color_bk);

	UINT state = lpDrawItemStruct->itemState;

//    if((state &ODS_SELECTED))	{ dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT); }
//    else						{ dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT); }

	if ((state & ODS_SELECTED))
	{
		dc.Draw3dRect(&rect, RGB(53, 53, 53), RGB(53, 53, 53));
		dc.FillSolidRect(&rect, RGB(103, 103, 103));
		dc.SetBkColor(RGB(103, 103, 103));
		dc.SetTextColor(color_font);
	}

	dc.SetBkColor(color_bk);
	dc.SetTextColor(color_font);
	TCHAR buffer[MAX_PATH];
	ZeroMemory(buffer, MAX_PATH);
	::GetWindowTextA(lpDrawItemStruct->hwndItem, buffer, MAX_PATH);
	dc.DrawText(buffer, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	dc.Detach();
}

BOOL CHandlerView::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);
	pDC->FillSolidRect(rect, RGB(38, 38, 38));

	return TRUE;
	//return CFormView::OnEraseBkgnd(pDC);
}

void CHandlerView::m_fnRefreshEqpStatusRect()
{
	InvalidateRect(m_rcEqpStatus, FALSE);
}

LRESULT CHandlerView::OnMessageAdamRawDisplayUpdate_Copy(WPARAM wParam, LPARAM lParam)
{
	int		nImageWidth		= m_fnPixelWidth();
	int		nImageHeight	= m_fnPixelHeight();
	int		nSize			= m_fnPixelWidth() * m_fnPixelHeight() * 3;
	int		nCount			= 0;

	m_pSharedMemory->GetMapOpenFileMapping(
		&m_hRawMapping,
		SHARED_MEMORY_RAW_SPACE);

	m_pSharedMemory->GetMapViewOfFileAddress_Byte(
		&m_hRawMapping,
		&m_pRawAddress,
		&m_bySharedMemoryRaw,
		nSize);

	memcpy(m_MatRawSRC->data, m_bySharedMemoryRaw, sizeof(BYTE) * nSize);

	m_fnDisplayImage(m_MatRawSRC, IDC_IMAGE_DISPLAY_THREAD_RAW, &m_nMatRawControlX, &m_nMatRawControlY, &m_nMatRawResizeX, &m_nMatRawResizeY, m_bitmapInfoMatRaw);

	return TRUE;
}

LRESULT CHandlerView::OnMessageAdamRawInterpolationDisplayUpdate_Copy(WPARAM wParam, LPARAM lParam)
{
	int		nImageWidth		= m_fnPixelWidth();
	int		nImageHeight	= m_fnPixelHeight();
	int		nSize			= (m_fnPixelWidth() * IMAGE_MAGNIFICATION) * (m_fnPixelHeight() * IMAGE_MAGNIFICATION) * 3;
	int		nCount			= 0;

	m_pSharedMemory->GetMapOpenFileMapping(
		&m_hRawInterpolationMapping,
		SHARED_MEMORY_RAW_INTERPOLATION_SPACE);

	m_pSharedMemory->GetMapViewOfFileAddress_Byte(
		&m_hRawInterpolationMapping,
		&m_pRawInterpolationAddress,
		&m_bySharedMemoryRawInterpolation,
		nSize);

	memcpy(m_MatRawInterpolationSRC->data, m_bySharedMemoryRawInterpolation, sizeof(BYTE) * nSize);

	m_fnDisplayImage(m_MatRawInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION, &m_nMatRawInterpolationControlX, &m_nMatRawInterpolationControlY, &m_nMatRawInterpolationResizeX, &m_nMatRawInterpolationResizeY, m_bitmapInfoMatRawInterpolation);

	return TRUE;
}