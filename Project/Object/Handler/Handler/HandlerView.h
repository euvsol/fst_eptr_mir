
// SeventhExProject_AView.h : CHandlerView 클래스의 인터페이스
//

#pragma once

#include "resource.h"
#include "afxwin.h"

#include <opencv2/opencv.hpp>
#include "../../../Module/SharedMemory/SharedMemory/SharedMemoryMain.h"
#include "../../../Module/LogViewer/LogViewer/LogViewerMain.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementDefine.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementStruct.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_ProtocolStructIPC.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_UserMessage.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolDefine.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolMessage.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolStructData.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolStructByte.h"

using namespace std;
using namespace cv;

class CHandlerView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CHandlerView();
	DECLARE_DYNCREATE(CHandlerView)

public:
	enum{ IDD = IDD_Handler_FORM };

// 특성입니다.
public:
	CHandlerDoc* GetDocument() const;

public:
	HMODULE			m_hSharedMemory;
	CSharedMemory*	m_pSharedMemory;

	HMODULE			m_hLogViewer;
	CLogViewer*		m_pLogViewer;

// 작업입니다.
public:
	HANDLE			m_hTransmittanceMapping;
	BYTE*			m_pTransmittanceAddress;

	HANDLE			m_hReflectanceMapping;
	BYTE*			m_pReflectanceAddress;

	HANDLE			m_hRawMapping;
	BYTE*			m_pRawAddress;

	HANDLE			m_hRawInterpolationMapping;
	BYTE*			m_pRawInterpolationAddress;

	HANDLE			m_hInterpolationMapping;
	BYTE*			m_pInterpolationAddress;

	HANDLE			m_hAverageMapping;
	BYTE*			m_pAverageAddress;

	Mat*	m_dMat_T;
	Mat*	m_dMat_R;
	Mat*	m_MatRawSRC;
	Mat*	m_MatRawInterpolationSRC;
	Mat*	m_MatInterpolationSRC;
	Mat*	m_MatAverageSRC;

	BYTE*	m_bySharedMemoryRaw;
	BYTE*	m_bySharedMemoryRawInterpolation;
	BYTE*	m_bySharedMemoryInterpolation;
	BYTE*	m_bySharedMemoryAverage;

	BYTE*	m_bySharedMemoryT;
	BYTE*	m_bySharedMemoryR;
	BYTE*	m_bySharedMemoryPosX;
	BYTE*	m_bySharedMemoryPosY;

	int m_nMatRawInterpolationControlX;
	int m_nMatRawInterpolationControlY;
	int m_nMatRawInterpolationResizeX;
	int m_nMatRawInterpolationResizeY;
	BITMAPINFO* m_bitmapInfoMatRawInterpolation;

	int m_nMatRawControlX;
	int m_nMatRawControlY;
	int m_nMatRawResizeX;
	int m_nMatRawResizeY;
	BITMAPINFO* m_bitmapInfoMatRaw;

	int m_nMatInterpolationControlX;
	int m_nMatInterpolationControlY;
	int m_nMatInterpolationResizeX;
	int m_nMatInterpolationResizeY;
	BITMAPINFO* m_bitmapInfoMatInterpolation;

	int m_nMatAverageControlX;
	int m_nMatAverageControlY;
	int m_nMatAverageResizeX;
	int m_nMatAverageResizeY;
	BITMAPINFO* m_bitmapInfoMatAverage;

	int m_nBeforeImageWidth;
	int m_nBeforeImageHeight;

	int m_nImageFOV_X;
	int m_nImageFOV_Y;
	int m_nScanGrid_X;
	int m_nScanGrid_Y;
	int m_nInterpolationCount;
	int m_nAverageCount;
	int m_nSetCount;
	int m_nScanDirection;
	int m_nPacketCount;

	int m_nPacketMode;

	int m_nMPacketCount;
	int m_nAPacketCount;

	int m_nRawCountCheck;
	int m_nRawInterpolationCountCheck;
	int m_nInterpolationCountCheck;
	int m_nAverageCountCheck;
	int m_nScanSetCountCheck;

	int m_nSavedItem, m_nSavedSubitem;

	int	m_nDialogWidth;
	int	m_nDialogHeight;

	HBRUSH m_brushsMenubar;
	HBRUSH m_brushsStatic;
	HBRUSH m_brushsEdit;
	HBRUSH m_brushsEqpStatusOn;
	HBRUSH m_brushsEqpStatusOff;

	CFont m_fStaticBold;

	CRect m_rcEqpStatus;

	BOOL m_bEqpStatus;

	CListCtrl m_ctlImageRecipe;

	CEdit m_ctlEditImageRecipe;

	CStatic m_ctlProjectName;

	CWinThread*				m_pThread;
	HANDLE					m_hEvent;
	BOOL					m_bThreadStatus;
	CRITICAL_SECTION		m_pThreadCritical;
	BOOL ReCreateThread();
	BOOL Run(int nSequence, BOOL bMessage);
	static UINT Thread(LPVOID lpParam);
	BOOL StartThread();
	BOOL StopThread();
	void SetThreadStatus(BOOL bStatus) { m_bThreadStatus = bStatus; }
	BOOL GetThreadStatus()			   { return m_bThreadStatus;	}

public:
	afx_msg LRESULT OnMessageAdamRunPacketTimeOutUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamPingUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamRecipeUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamRunUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamStopUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamAverageCountUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamAPacketRunUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamMPacketRunUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamDisplayInitUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamRawDisplayUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamRawInterpolationDisplayUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamInterpolationDisplayUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamAverageDisplayUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamTDataUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamRDataUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageMeasureEndUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageMeasureBGUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageMeasureWO_PUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageMeasureBSUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageMeasureP_As_PointUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageMeasureP_As_ScanUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageMeasureP_As_Scan_StopUpdate(WPARAM wParam, LPARAM lParam);

	afx_msg LRESULT OnMessageAdamRawDisplayUpdate_Copy(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAdamRawInterpolationDisplayUpdate_Copy(WPARAM wParam, LPARAM lParam);

public:
	void m_fnDisplayImage(Mat* pMatImage, unsigned int nControlView, int* nPictureControlX, int* nPictureControlY, int* nResizeX, int* nResizeY, BITMAPINFO*& bitmapInfo);
	void m_fnDisplayImageInfoUpdate(Mat* pMatImage, unsigned int nControlView, int* nPictureControlX, int* nPictureControlY, int* nResizeX, int* nResizeY, BITMAPINFO*& bitmapInfo);
	CString m_fnGetCurrentTimeFunction();
	void m_fnImageInfoList();
	void m_fnScanRecipeUpdate();
	int m_fnPixelWidth();
	int m_fnPixelHeight();
	void m_fnImageSizeChangeAndInit();
	void m_fnImageCountDataIntit();
	void m_fnImageCountDataUpdate();
	void m_fnSharedMemoryAllInit();
	CString m_fnFormattedString(LPCTSTR lpszFormat, ...);
	void m_fnCreateLogViewerInit();
	void m_fnCreateImageInit();
	void m_fnCreateBrushInit();
	void m_fnMenuBarInit();
	void m_fnDrawDesignButtonStay(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font, BOOL bFlag);
	void m_fnDrawDesignButtonSelect(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font);
	void m_fnRefreshEqpStatusRect();

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CHandlerView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
protected:

public:
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnNMDblclkListReciepe(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMClickListReciepe(NMHDR* pNMHDR, LRESULT* pResult);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnNMCustomdrawListReciepe(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedButtonProtocolAdamRun();
	afx_msg void OnBnClickedButtonProtocolAdamStop();
	afx_msg void OnBnClickedButtonProtocolAdamRecipe();
	afx_msg void OnBnClickedButtonProtocolMPacketRun();
	afx_msg void OnBnClickedButtonProtocolAPacketRun();
	afx_msg void OnBnClickedButtonProtocolEm1024Reset();
	afx_msg void OnBnClickedButtonProtocolAdamMeasureBg();
	afx_msg void OnBnClickedButtonProtocolAdamMeasureWoP();
	afx_msg void OnBnClickedButtonProtocolAdamMeasureBs();
	afx_msg void OnBnClickedButtonProtocolAdamMeasurePAsPoint();
	afx_msg void OnBnClickedButtonProtocolAdamMeasurePAsScan();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

#ifndef _DEBUG  // SeventhExProject_AView.cpp의 디버그 버전
inline CHandlerDoc* CHandlerView::GetDocument() const
   { return reinterpret_cast<CHandlerDoc*>(m_pDocument); }
#endif

