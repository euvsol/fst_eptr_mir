
// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "Handler.h"
#include "HandlerDoc.h"
#include "HandlerView.h"

#include "MainFrm.h"
#include "HandlerDoc.h"
#include "HandlerView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
{
	// TODO: 여기에 멤버 초기화 코드를 추가합니다.
	m_strMessage = _T("");
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	//if (!m_wndStatusBar.Create(this))
	//{
	//	TRACE0("상태 표시줄을 만들지 못했습니다.\n");
	//	return -1;      // 만들지 못했습니다.
	//}
	//m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	//int nWindowWidth	= 1200;
	//int nWindowHeight	= 900;

	//CPoint pos;

	//pos.x = GetSystemMetrics(SM_CXSCREEN) / 2.0f - nWindowWidth / 2.0f;
	//pos.y = GetSystemMetrics(SM_CYSCREEN) / 2.0f - nWindowHeight / 2.0f;

	//// -------- Set Frame size & Remove Menu -------- //
	cs.x		= 0;										// 메인프레임 첫 시작 x축 위치
	cs.y		= 0;										// 메인프레임 첫 시작 y축 위치
	//cs.cx		= nWindowWidth;									// 자신이 원하는 메인 프레임의 x축 크기
	//cs.cy		= nWindowHeight;								// 자신이 원하는 메인 프레임의 y축 크기
	cs.hMenu	= NULL;											// 메뉴 제거
	cs.lpszName = _T("Handler");								// 메인프레임 제목명

	cs.style = WS_OVERLAPPED | WS_CAPTION | /*FWS_ADDTOTITLE |*/ WS_THICKFRAME | WS_MINIMIZEBOX | WS_SYSMENU;

	return TRUE;
}

// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	switch (pCopyDataStruct->dwData) 
	{
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_RUN_PACKET_TIMEOUT_UPDATE		: { OnMessageAdamRunPacketTimeOutUpdate(pCopyDataStruct);		break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_PING_UPDATE						: { OnMessageAdamPingUpdate(pCopyDataStruct);					break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_RECIPE_UPDATE					: { OnMessageAdamRecipeUpdate(pCopyDataStruct);					break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_RUN_UPDATE						: { OnMessageAdamRunUpdate(pCopyDataStruct);					break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_STOP_UPDATE						: { OnMessageAdamStopUpdate(pCopyDataStruct);					break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_AVERAGE_COUNT_UPDATE			: { OnMessageAdamAverageCountUpdate(pCopyDataStruct);			break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_A_PACKET_RUN_UPDATE				: { OnMessageAdamAPacketRunUpdate(pCopyDataStruct);				break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_M_PACKET_RUN_UPDATE				: { OnMessageAdamMPacketRunUpdate(pCopyDataStruct);				break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_DISPLAY_INIT_UPDATE				: { OnMessageAdamDisplayInitUpdate(pCopyDataStruct);			break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_RAW_DISPLAY_UPDATE				: { OnMessageAdamRawDisplayUpdate(pCopyDataStruct);				break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_INTERPOLATION_DISPLAY_UPDATE	: { OnMessageAdamInterpolationDisplayUpdate(pCopyDataStruct);	break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_AVERAGE_DISPLAY_UPDATE			: { OnMessageAdamAverageDisplayUpdate(pCopyDataStruct);			break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_T_DATA_UPDATE					: { OnMessageAdamTDataUpdate(pCopyDataStruct);					break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_ADAM_R_DATA_UPDATE					: { OnMessageAdamRDataUpdate(pCopyDataStruct);					break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_MEASURE_BG_UPDATE					: { OnMessageMeasureBGUpdate(pCopyDataStruct);					break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_MEASURE_WO_P_UPDATE					: { OnMessageMeasureWO_PUpdate(pCopyDataStruct);				break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_MEASURE_BS_UPDATE					: { OnMessageMeasureBSUpdate(pCopyDataStruct);					break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_MEASURE_P_AS_POINT_UPDATE			: { OnMessageMeasureP_As_PointUpdate(pCopyDataStruct);			break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_MEASURE_P_AS_SCAN_UPDATE				: { OnMessageMeasureP_As_ScanUpdate(pCopyDataStruct);			break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_MEASURE_P_AS_SCAN_STOP_UPDATE		: { OnMessageMeasureP_As_Scan_StopUpdate(pCopyDataStruct);		break; }
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_CP_MEASURE_END_UPDATE					: { OnMessageMeasureEndUpdate(pCopyDataStruct);					break; }
		
	}

	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

BOOL CMainFrame::OnMessageAdamRunPacketTimeOutUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame = (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView = (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_RUN_PACKET_TIMEOUT_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamPingUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_PING_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamRecipeUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_RECIPE_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamRunUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_RUN_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamStopUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_STOP_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamAverageCountUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_AVERAGE_COUNT_UPDATE, wParam, lParam);

	return TRUE;
}


BOOL CMainFrame::OnMessageAdamAPacketRunUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_A_PACKET_RUN_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamMPacketRunUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_M_PACKET_RUN_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamDisplayInitUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_DISPLAY_INIT_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamRawDisplayUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_RAW_DISPLAY_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamInterpolationDisplayUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_INTERPOLATION_DISPLAY_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamAverageDisplayUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_AVERAGE_DISPLAY_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamTDataUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_T_DATA_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageAdamRDataUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_ADAM_R_DATA_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageMeasureBGUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_MEASURE_BG_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageMeasureWO_PUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_MEASURE_WO_P_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageMeasureBSUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_MEASURE_BS_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageMeasureP_As_PointUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_MEASURE_P_AS_POINT_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageMeasureP_As_ScanUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_MEASURE_P_AS_SCAN_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageMeasureP_As_Scan_StopUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_MEASURE_P_AS_SCAN_STOP_UPDATE, wParam, lParam);

	return TRUE;
}

BOOL CMainFrame::OnMessageMeasureEndUpdate(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*		pFrame	= (CMainFrame*)AfxGetMainWnd();
	CHandlerView*	pView	= (CHandlerView*)pFrame->GetActiveView();

	WPARAM wParam = 0;
	LPARAM lParam = (LPARAM)pCopyDataStruct;

	pView->SendMessage(WM_USER_MESSAGE_CP_MEASURE_END_UPDATE, wParam, lParam);

	return TRUE;
}