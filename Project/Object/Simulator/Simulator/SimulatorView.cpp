
// SeventhExProject_AView.cpp : CSimulatorView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "Simulator.h"
#endif

#include "MainFrm.h"
#include "SimulatorDoc.h"
#include "SimulatorView.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CSimulatorView

IMPLEMENT_DYNCREATE(CSimulatorView, CFormView)

BEGIN_MESSAGE_MAP(CSimulatorView, CFormView)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_BUTTON_SERVER_CREATE, &CSimulatorView::OnBnClickedButtonServerCreate)
	ON_BN_CLICKED(IDC_BUTTON_IMAGE_LOAD, &CSimulatorView::OnBnClickedButtonImageLoad)
	ON_BN_CLICKED(IDC_BUTTON_SEND_DATA, &CSimulatorView::OnBnClickedButtonSendData)
	ON_WM_PAINT()
	ON_BN_CLICKED(IDC_BUTTON_IMAGE_THREAD_START, &CSimulatorView::OnBnClickedButtonImageThreadStart)
	ON_BN_CLICKED(IDC_BUTTON_IMAGE_THREAD_STOP, &CSimulatorView::OnBnClickedButtonImageThreadStop)
	ON_WM_MEASUREITEM()
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RECIEPE, &CSimulatorView::OnNMDblclkListReciepe)
	ON_NOTIFY(NM_CLICK, IDC_LIST_RECIEPE, &CSimulatorView::OnNMClickListReciepe)
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_RECIEPE, &CSimulatorView::OnNMCustomdrawListReciepe)
	ON_BN_CLICKED(IDC_BUTTON_IMAGE_LOAD_SET, &CSimulatorView::OnBnClickedButtonImageLoadSet)
	ON_BN_CLICKED(IDC_BUTTON_START_SCAN, &CSimulatorView::OnBnClickedButtonStartScan)
	ON_BN_CLICKED(IDC_BUTTON_SCAN_RESET, &CSimulatorView::OnBnClickedButtonScanReset)
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_BUTTON_SIGNAL_LOAD_SET, &CSimulatorView::OnBnClickedButtonSignalLoadSet)
	ON_WM_SIZE()
	ON_WM_CTLCOLOR()
	ON_WM_ERASEBKGND()

	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_ACCEPT, &CSimulatorView::OnMessageServerClientAccept)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_RECEIVE, &CSimulatorView::OnMessageServerClientReceive)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_CLOSE, &CSimulatorView::OnMessageServerClientSocketClose)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_RUN, &CSimulatorView::OnMessageRun)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_STOP, &CSimulatorView::OnMessageStop)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_RECIPE, &CSimulatorView::OnMessageScanRecipe)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_PING, &CSimulatorView::OnMessageSocketPing)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_AVERAGE_RUN, &CSimulatorView::OnMessageAPacketeRun)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_AVERAGE_COUNT, &CSimulatorView::OnMessageAverageCount)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_EM1024_RESET, &CSimulatorView::OnMessageEm1024Reset)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_M_PACKET_RUN, &CSimulatorView::OnMessageMPacketRun)
	ON_MESSAGE(WM_USER_MESSAGE_SM_CHART_POS_VIEW_UPDATE, &CSimulatorView::OnMessageStartChartPosView)
	ON_MESSAGE(WM_USER_MESSAGE_SM_CHART_SIGNAL_VIEW_UPDATE, &CSimulatorView::OnMessageStartChartSignalView)
END_MESSAGE_MAP()

// CSimulatorView 생성/소멸

CSimulatorView::CSimulatorView()
	: CFormView(CSimulatorView::IDD)
{
	m_nDialogWidth = 0;
	m_nDialogHeight = 0;

	m_bImageLoad = FALSE;

	m_nDummyX = 0;
	m_nDummyY = 0;

	m_nPixelPosX = 0;
	m_nPixelPosY = 0;

	m_bEqpStatus = FALSE;
	m_bSocketConnect = FALSE;

	m_bStartScan = FALSE;

	m_nSavedItem = -1;
	m_nSavedSubitem = -1;

	m_nMPacketCount = 0;
	m_nAPacketCount = 0;

	m_nBeforeImageWidth = 0;
	m_nBeforeImageHeight = 0;

	m_nImageCountCCD = 0;

	m_bTimeUpdate = FALSE;

	m_nPacketMode = MODE_RUN_PACKET;

	m_bManualModeStatus = FALSE;

	m_dMat = nullptr;

	memset(&m_dAxisX_Pos, 0x00, sizeof(m_dAxisX_Pos));
	memset(&m_dAxisY_Pos, 0x00, sizeof(m_dAxisY_Pos));

	memset(&m_dAxisX_Signal, 0x00, sizeof(m_dAxisX_Signal));
	memset(&m_dAxisY_Signal, 0x00, sizeof(m_dAxisY_Signal));

	m_pThread		= nullptr;
	m_hEvent		= NULL;
	m_bThreadStatus = FALSE;
	ReCreateThread();
	InitializeCriticalSection(&m_pThreadCritical);

	CMainFrame*	pFrame = (CMainFrame *)AfxGetMainWnd();

	m_hImageProcessAlgorithm = LoadLibrary(_T("ImageProcessAlgorithm"));
	m_pImageProcessAlgorithm = new CImageProcessAlgorithm(m_hImageProcessAlgorithm);

	m_hLogViewer = LoadLibrary(_T("LogViewer"));
	m_pLogViewer = new CLogViewer(m_hLogViewer);
	m_pLogViewer->FrameworkInstance(pFrame, this);

	m_hTcpServer = LoadLibrary(_T("TcpServer"));
	m_pTcpServer = new CTcpServer(m_hTcpServer);
}

CSimulatorView::~CSimulatorView()
{
	if (m_pImageProcessAlgorithm != nullptr) { delete m_pImageProcessAlgorithm; m_pImageProcessAlgorithm = nullptr; FreeLibrary(m_hImageProcessAlgorithm); m_hImageProcessAlgorithm = NULL; }
	if (m_pLogViewer != nullptr) { delete m_pLogViewer; m_pLogViewer = nullptr; FreeLibrary(m_hLogViewer); m_hLogViewer = NULL; }
	if (m_pTcpServer != nullptr) { delete m_pTcpServer; m_pTcpServer = nullptr; FreeLibrary(m_hTcpServer); m_hTcpServer = NULL; }

	if (m_pThread != nullptr) { m_pThread->SuspendThread(); DWORD dw; ::GetExitCodeThread(m_pThread->m_hThread, &dw); delete m_pThread; m_pThread = nullptr; }

	if (m_brushsMenubar != NULL) { DeleteObject(m_brushsMenubar); m_brushsMenubar = NULL; }
	if (m_brushsStatic != NULL) { DeleteObject(m_brushsStatic); m_brushsStatic = NULL; }
	if (m_brushsEdit != NULL) { DeleteObject(m_brushsEdit); m_brushsEdit = NULL; }
	if (m_brushsEqpStatusOn != NULL) { DeleteObject(m_brushsEqpStatusOn); m_brushsEqpStatusOn = NULL; }
	if (m_brushsEqpStatusOff != NULL) { DeleteObject(m_brushsEqpStatusOff); m_brushsEqpStatusOff = NULL; }
	if (m_brushsSocketConnectOn != NULL) { DeleteObject(m_brushsSocketConnectOn); m_brushsSocketConnectOn = NULL; }
	if (m_brushsSocketConnectOff != NULL) { DeleteObject(m_brushsSocketConnectOff); m_brushsSocketConnectOff = NULL; }
}

void CSimulatorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_RECIEPE, m_ctlImageRecipe);
	DDX_Control(pDX, IDC_EDIT_IMAGE_RECIPE, m_ctlEditImageRecipe);
	DDX_Control(pDX, IDC_GRAPH_POS_VIEW, m_chartPosView);
	DDX_Control(pDX, IDC_GRAPH_SIGNAL_VIEW, m_chartSignalView);
	DDX_Control(pDX, IDC_STATIC_PROGRAM_NAME, m_ctlProjectName);
}

BOOL CSimulatorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void CSimulatorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();

	CRect rc;
	GetClientRect(rc);

	m_nDialogWidth = rc.Width();
	m_nDialogHeight = rc.Height();

	m_fnCreateBrushInit();

	m_fnCreateLogViewerInit();

	m_fnMenuBarInit();

	m_fnImageInfoList();

	m_fnCreateServer();

	m_fnImageLoadInit();

	m_fnCreateImageInit();

	m_fnControlInit();

	m_fnChartPosVIew(m_dAxisX_Pos, m_dAxisY_Pos, X_AXIS_COUNT);
	m_fnChartSignalVIew(m_dAxisX_Signal, m_dAxisY_Signal, X_AXIS_COUNT);

	srand((double)(time(NULL)));

	//SetTimer(100, 5000, NULL);
}


// CSimulatorView 진단

#ifdef _DEBUG
void CSimulatorView::AssertValid() const
{
	CFormView::AssertValid();
}

void CSimulatorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

CSimulatorDoc* CSimulatorView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSimulatorDoc)));
	return (CSimulatorDoc*)m_pDocument;
}
#endif //_DEBUG


// CSimulatorView 메시지 처리기


void CSimulatorView::OnDestroy()
{
	CFormView::OnDestroy();

	// TODO: 여기에 메시지 처리기 코드를 추가합니다.

}

void CSimulatorView::OnBnClickedButtonServerCreate()
{
	m_fnCreateServer();
}


void CSimulatorView::OnBnClickedButtonImageLoad()
{
	CMainFrame* pFrame = (CMainFrame*)AfxGetMainWnd();

	// 파일 대화 상자에서 BMP 형식의 파일만 보이도록 하는 필터의 정의
	char szFilter[] = "BMP File (*.BMP) | *.BMP; | All Files(*.*) | *.* ||";

	// 파일 대화 상자의 생성
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

	if (IDOK == dlg.DoModal())
	{
		CString strImageName	= dlg.GetFileName();
		CString strFolderName	= dlg.GetPathName();

		try
		{
			if (m_ImageMatSRC.data != NULL)
			{
				m_ImageMatSRC.release();
				m_ImageMatSRC = NULL;
			}

			if (m_ImageTempMatSRC.data != NULL)
			{
				m_ImageTempMatSRC.release();
				m_ImageTempMatSRC = NULL;
			}

			if (m_Image != NULL)
			{
				m_Image.Destroy();
			}

			CString strName, strFolder, strPath;

			int nImageNameLength		= strImageName.GetLength();
			int nFolderNameLength	= strFolderName.GetLength();

			strName		= strImageName;
			strFolder	= strFolderName.Left(nFolderNameLength - nImageNameLength);

			GetDlgItem(IDC_EDIT_IMAGE_NAME)->SetWindowText(strName);
			GetDlgItem(IDC_EDIT_IMAGE_FOLDER_PATH)->SetWindowText(strFolder);

			strPath.Format(_T("%s%s"), strFolder, strName);

			m_bImageLoad = TRUE;

			m_Image.Load(strPath);

			m_ImageMatSRC		= imread(string(strPath), IMREAD_COLOR);
			m_ImageTempMatSRC	= imread(string(strPath), IMREAD_COLOR);

			m_fnDisplayResize();

			m_fnRefreshViewRect();
		}
		catch (cv::Exception& e)
		{
			MessageBox(_T("이미지 읽기에 실패 하였습니다."), _T("cvLoadImage"), MB_ICONERROR);
			return;
		}
	}
}

void CSimulatorView::m_fnCreateServer()
{
	if (m_pTcpServer->CreateServer(static_cast<UINT>(SERVER_SOCKET::CH_1), SERVER_LOCAL, SERVER_ADAM_PORT))
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedString(_T("Create Server[%d] IP : %s / PORT : %d"), static_cast<UINT>(SERVER_SOCKET::CH_1), SERVER_LOCAL, SERVER_ADAM_PORT));
	}
	else
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedString(_T("Create Server[%d] Fail"), static_cast<UINT>(SERVER_SOCKET::CH_1)));
	}
}


void CSimulatorView::OnBnClickedButtonSendData()
{
}


void CSimulatorView::OnPaint()
{
	CPaintDC dc(this); // device context for painting
					   // TODO: 여기에 메시지 처리기 코드를 추가합니다.
					   // 그리기 메시지에 대해서는 CFormView::OnPaint()을(를) 호출하지 마십시오.

	if (m_bImageLoad)
	{
		//그릴 영역 설정
		CRect rect;
		CDC* dc = GetDlgItem(IDC_IMAGE_DISPLAY)->GetDC();

		if (dc == NULL) { return; }

		GetDlgItem(IDC_IMAGE_DISPLAY)->GetClientRect(rect);

		// ------------------- 더블 버퍼링 처리 -------------------//
		CDC* pMemDC = new CDC;
		pMemDC->CreateCompatibleDC(dc);
		CBitmap memBitmap;
		memBitmap.CreateCompatibleBitmap(dc, rect.Width(), rect.Height());
		CBitmap* pOldBitmap = pMemDC->SelectObject(&memBitmap);

		// ------------------- 이미지 그리기 -------------------//

		m_fnDrawAllItems(pMemDC, &memBitmap);

		// ------------------- 이미지 그리기 -------------------//

		dc->BitBlt(0, 0, rect.right, rect.bottom, pMemDC, 0, 0, SRCCOPY);

		pMemDC->SelectObject(pOldBitmap);
		DeleteObject(memBitmap);
		ReleaseDC(pMemDC);
		ReleaseDC(dc);
		// ------------------- 더블 버퍼링 처리 -------------------//
	}
}

void CSimulatorView::m_fnRefreshViewRect()
{
	InvalidateRect(m_rcView, FALSE);
}

void CSimulatorView::m_fnRefreshSocketRect()
{
	InvalidateRect(m_rcSocketConnect, FALSE);
}

void CSimulatorView::m_fnRefreshEqpStatusRect()
{
	InvalidateRect(m_rcEqpStatus, FALSE);
}

void CSimulatorView::m_fnRefreshManualRect()
{
	InvalidateRect(m_rcManualMode, FALSE);
}

void CSimulatorView::m_fnDrawAllItems(CDC*& pMemDC, CBitmap* memBitmap)
{
	pMemDC->SetStretchBltMode(HALFTONE);

	m_Image.StretchBlt(
		pMemDC->m_hDC,
		m_nDummyX,
		m_nDummyY,
		m_rcView.Width(),
		m_rcView.Height(),
		0,
		0,
		m_rcView.Width(),
		m_rcView.Height(),
		SRCCOPY);
}

void CSimulatorView::m_fnDisplayResize()
{
	CWnd* pWnd_ImageTraget;

	RECT rcImageTraget;

	// Picture 컨트롤 Rect, DC 값
	pWnd_ImageTraget = GetDlgItem(IDC_IMAGE_DISPLAY);

	CClientDC dcImageTraget(pWnd_ImageTraget);
	pWnd_ImageTraget->GetClientRect(&rcImageTraget);

	int nPictureControlX = rcImageTraget.right;
	int nPictureControlY = rcImageTraget.bottom;

	double dMagnificationX = (double)m_ImageMatSRC.cols / (double)nPictureControlX;
	double dMagnificationY = (double)m_ImageMatSRC.rows / (double)nPictureControlY;

	double dMagnification = (dMagnificationX < dMagnificationY) ? dMagnificationY : dMagnificationX;

	int nResizeX = ((int)(floor((double)m_ImageMatSRC.cols / dMagnification)));
	int nResizeY = ((int)(floor((double)m_ImageMatSRC.rows / dMagnification)));

	if (nResizeX % 2 == TRUE)  { nResizeX -= 1; }
	if (nResizeY % 2 == FALSE) { nResizeY -= 1; }

	m_nDummyX = (nResizeX < nPictureControlX) ? (nPictureControlX - nResizeX) / 2 : 0;
	m_nDummyY = (nResizeY < nPictureControlY) ? (nPictureControlY - nResizeY) / 2 : 0;

	resize(m_ImageMatSRC, m_ImageMatResize, cv::Size(nResizeX, nResizeY), INTER_LINEAR);

	m_fnMatToCImage(m_ImageMatResize, m_Image);
}

void CSimulatorView::m_fnMatToCImage(Mat& mat, CImage& cimage)
{
	if (0 == mat.total()) { return; }

	int nChannels = mat.channels();

	int nWidth = mat.cols;
	int nHeight = mat.rows;

	cimage.Destroy();
	cimage.Create(nWidth, nHeight, 8 * nChannels);

	uchar* pData = (uchar*)cimage.GetBits();
	uchar* pData_;

	int nStep = cimage.GetPitch();

	if (1 == nChannels)
	{
		RGBQUAD* rgbquadColorTable;
		int nMaxColors = 256;
		rgbquadColorTable = new RGBQUAD[nMaxColors];
		cimage.GetColorTable(0, nMaxColors, rgbquadColorTable);

		for (int nColor = 0; nColor < nMaxColors; nColor++)
		{
			rgbquadColorTable[nColor].rgbBlue = (uchar)nColor;
			rgbquadColorTable[nColor].rgbGreen = (uchar)nColor;
			rgbquadColorTable[nColor].rgbRed = (uchar)nColor;
		}

		cimage.SetColorTable(0, nMaxColors, rgbquadColorTable);
		delete[]rgbquadColorTable;
	}

	for (int row = 0; row < nHeight; row++)
	{
		pData_ = (mat.ptr<uchar>(row));
		for (int col = 0; col < nWidth; col++)
		{
			if (1 == nChannels)
			{
				*(pData + row * nStep + col) = pData_[col];
			}
			else if (3 == nChannels)
			{
				for (int nCha = 0; nCha < 3; nCha++)
				{
					*(pData + row * nStep + col * 3 + nCha) = pData_[col * 3 + nCha];
				}
			}
		}
	}
}

UINT CSimulatorView::Thread(LPVOID lpParam)
{
	CSimulatorView* pThread = (CSimulatorView*)lpParam;

	while (1)
	{
		WaitForSingleObject(pThread->m_hEvent, INFINITE);

		pThread->SetThreadStatus(TRUE);

		pThread->Run(NULL, FALSE);

		pThread->SetThreadStatus(FALSE);

		ResetEvent(pThread->m_hEvent);
	}

	return 0;
}

BOOL CSimulatorView::Run(int nSequence, BOOL bMessage)
{
	m_bEqpStatus = TRUE;
	m_fnRefreshEqpStatusRect();

	m_fnBufferSizeChange();

	OnBnClickedButtonScanReset();

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Start Sequence!! **********")));

	while (GetThreadStatus())
	{
		switch (m_nPacketMode)
		{
			case MODE_M_PACKET		: { m_fnMImagePacketDataSend(); break; }
			case MODE_A_PACKET		: { m_fnAImagePacketDataSend(); break; }
			case MODE_RUN_PACKET	: 
			{ 
				while (GetThreadStatus())
				{
					if (!m_fnRunImagePacketDataSend()) { break; }
					Sleep(1000);
				}
				break;
			}
		}
		break;
	}

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("********** Finish Sequence!! **********")));

	m_bEqpStatus = FALSE;
	m_fnRefreshEqpStatusRect();

	return TRUE;
}

BOOL CSimulatorView::StartThread()
{
	SetThreadStatus(TRUE);
	SetEvent(m_hEvent);
	return TRUE;
}

BOOL CSimulatorView::StopThread()
{
	SetThreadStatus(FALSE);
	return TRUE;
}

BOOL CSimulatorView::ReCreateThread()
{
	StopThread();

	Sleep(100);

	if (m_pThread != nullptr)
		m_pThread->SuspendThread();

	if (m_pThread != nullptr)
	{
		DWORD dw;
		::GetExitCodeThread(m_pThread->m_hThread, &dw);
		if (dw == STILL_ACTIVE)
		{
			TerminateThread(m_pThread->m_hThread, 0);
			CloseHandle(m_pThread->m_hThread);
		}
		m_pThread = nullptr;
	}

	if (m_pThread == nullptr)
	{
		m_hEvent = CreateEventA(NULL, FALSE, FALSE, NULL);
		ResetEvent(m_hEvent);
		m_pThread = AfxBeginThread(Thread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
		m_pThread->ResumeThread();
	}

	return TRUE;
}

void CSimulatorView::OnBnClickedButtonImageThreadStart()
{
	StartThread();
}


void CSimulatorView::OnBnClickedButtonImageThreadStop()
{
	StopThread();
}

CString CSimulatorView::m_fnGetCurrentTimeFunction()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"[%4d-%02d-%02d %02d:%02d:%02d.%03d] : ",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay,
		SystemTime.wHour,
		SystemTime.wMinute,
		SystemTime.wSecond,
		SystemTime.wMilliseconds);

	return strTime;
}

void CSimulatorView::m_fnImageInfoList()
{
	m_ctlImageRecipe.DeleteAllItems();

	m_ctlImageRecipe.ModifyStyle(LVS_OWNERDRAWFIXED, 0, 0);
	m_ctlImageRecipe.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_ctlImageRecipe.InsertColumn(0, _T("                 Menu"),	LVCFMT_LEFT,	120, -1); 
	m_ctlImageRecipe.InsertColumn(1, _T("Value"),					LVCFMT_CENTER,	70 , -1); 
	m_ctlImageRecipe.InsertColumn(2, _T("Description"),				LVCFMT_CENTER,	144, -1); 

	CString strTemp;

	m_nImageFOV_X = 99900;
	strTemp.Format(_T("%d"), m_nImageFOV_X);

	m_ctlImageRecipe.InsertItem	(0, _T("Image X FOV")); 
	m_ctlImageRecipe.SetItem	(0, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(0, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nImageFOV_Y = 99900;
	strTemp.Format(_T("%d"), m_nImageFOV_Y);

	m_ctlImageRecipe.InsertItem	(1, _T("Image Y FOV"));
	m_ctlImageRecipe.SetItem	(1, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL);
	m_ctlImageRecipe.SetItem	(1, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL);

	m_nScanGrid_X = 100;
	strTemp.Format(_T("%d"), m_nScanGrid_X);

	m_ctlImageRecipe.InsertItem	(2, _T("Scan Grid")); 
	m_ctlImageRecipe.SetItem	(2, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(2, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nScanGrid_Y = 100;
	strTemp.Format(_T("%d"), m_nScanGrid_Y);

	m_ctlImageRecipe.InsertItem	(3, _T("Scan Grid")); 
	m_ctlImageRecipe.SetItem	(3, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(3, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nInterpolationCount = 1;
	strTemp.Format(_T("%d"), m_nInterpolationCount);

	m_ctlImageRecipe.InsertItem	(4, _T("Interpolation Count")); 
	m_ctlImageRecipe.SetItem	(4, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(4, 2, LVIF_TEXT, _T("Interplation Image Count"), 0, 0, 0, NULL );

	m_nAverageCount = 3;
	strTemp.Format(_T("%d"), m_nAverageCount);

	m_ctlImageRecipe.InsertItem	(5, _T("Average Count")); 
	m_ctlImageRecipe.SetItem	(5, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(5, 2, LVIF_TEXT, _T("Average Image Count"), 0, 0, 0, NULL );

	m_nSetCount = 1;
	strTemp.Format(_T("%d"), m_nSetCount);

	m_ctlImageRecipe.InsertItem	(6, _T("Set Count")); 
	m_ctlImageRecipe.SetItem	(6, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(6, 2, LVIF_TEXT, _T("Set Image Count"), 0, 0, 0, NULL );

	m_nScanDirection = 1;
	strTemp.Format(_T("%d"), m_nScanDirection);

	m_ctlImageRecipe.InsertItem	(7, _T("Scan Direction")); 
	m_ctlImageRecipe.SetItem	(7, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(7, 2, LVIF_TEXT, _T("Ex) 1 : V / 0 : H"), 0, 0, 0, NULL );

	m_nPacketCount = 1000;
	strTemp.Format(_T("%d"), m_nPacketCount);

	m_ctlImageRecipe.InsertItem	(8, _T("Packet Count"));
	m_ctlImageRecipe.SetItem	(8, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL);
	m_ctlImageRecipe.SetItem	(8, 2, LVIF_TEXT, _T("Data Packet Count"), 0, 0, 0, NULL);
}

void CSimulatorView::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (nIDCtl == IDC_LIST_RECIEPE)
	{
		lpMeasureItemStruct->itemHeight += 10;
	}

	CFormView::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}


void CSimulatorView::OnNMDblclkListReciepe(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	LPNMITEMACTIVATE pNMITEM = (LPNMITEMACTIVATE)pNMHDR;

	int m_nSavedItem = pNMITEM->iItem;
	int m_nSavedSubitem = pNMITEM->iSubItem;

	if (pNMITEM->iItem != -1)
	{
		if (m_nSavedSubitem == 1)
		{
			CRect rect;

			if (pNMITEM->iSubItem == 0)
			{
				m_ctlImageRecipe.GetItemRect(pNMITEM->iItem, rect, LVIR_BOUNDS);
				rect.right = rect.left + m_ctlImageRecipe.GetColumnWidth(0);
			}
			else
			{
				m_ctlImageRecipe.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			}

			m_ctlImageRecipe.ClientToScreen(rect);
			this->ScreenToClient(rect);

			m_ctlEditImageRecipe.ShowWindow(TRUE);
			m_ctlEditImageRecipe.SetWindowText(m_ctlImageRecipe.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			m_ctlEditImageRecipe.SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
			m_ctlEditImageRecipe.SetFocus();
		}
	}

	*pResult = 0;
}


void CSimulatorView::OnNMClickListReciepe(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);
	
	LPNMITEMACTIVATE pNMITEM = (LPNMITEMACTIVATE)pNMHDR;

	m_nSavedItem = m_nSavedSubitem = -1;
	m_ctlEditImageRecipe.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

	if (pNMITEM->iItem != -1)
	{
		m_nSavedItem = pNMITEM->iItem;
		m_nSavedSubitem = pNMITEM->iSubItem;
	}

	*pResult = 0;
}

void CSimulatorView::m_fnScanRecipeUpdate()
{
	CString strTemp;
	strTemp.Format(_T("%d"), m_nImageFOV_X);
	m_ctlImageRecipe.SetItemText(0, 1, strTemp);

	strTemp.Format(_T("%d"), m_nImageFOV_Y);
	m_ctlImageRecipe.SetItemText(1, 1, strTemp);

	strTemp.Format(_T("%d"), m_nScanGrid_X);
	m_ctlImageRecipe.SetItemText(2, 1, strTemp);

	strTemp.Format(_T("%d"), m_nScanGrid_Y);
	m_ctlImageRecipe.SetItemText(3, 1, strTemp);

	strTemp.Format(_T("%d"), m_nInterpolationCount);
	m_ctlImageRecipe.SetItemText(4, 1, strTemp);

	strTemp.Format(_T("%d"), m_nAverageCount);
	m_ctlImageRecipe.SetItemText(5, 1, strTemp);

	strTemp.Format(_T("%d"), m_nSetCount);
	m_ctlImageRecipe.SetItemText(6, 1, strTemp);

	strTemp.Format(_T("%d"), m_nScanDirection);
	m_ctlImageRecipe.SetItemText(7, 1, strTemp);

	strTemp.Format(_T("%d"), m_nPacketCount);
	m_ctlImageRecipe.SetItemText(8, 1, strTemp);
}

void CSimulatorView::OnNMCustomdrawListReciepe(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMCUSTOMDRAW pNMCD;

	pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	 
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	int iRow = (int)lplvcd->nmcd.dwItemSpec;
	 
	switch(lplvcd->nmcd.dwDrawStage)
	{
		case CDDS_PREPAINT :
		{
			*pResult = CDRF_NOTIFYITEMDRAW;
			return;
		}
			 
		// Modify item text and or background
		case CDDS_ITEMPREPAINT:
		{
			lplvcd->clrText = RGB(0,0,0);

			// If you want the sub items the same as the item,
			// set *pResult to CDRF_NEWFONT
			*pResult = CDRF_NOTIFYSUBITEMDRAW;
			return;
		}
			 
		// Modify sub item text and/or background
		case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		{
			// 홀수열, 짝수열의 배경색 재설정
			if(iRow % 2)	{ lplvcd->clrTextBk = RGB(255, 255, 255); }
			else			{ lplvcd->clrTextBk = RGB(230, 230, 230); }
				
			*pResult = CDRF_DODEFAULT;
			return;
		}
	}

	*pResult = 0;
}

void CSimulatorView::m_fnReceiveTackTimeUpdate()
{
	__int64	elapsed = m_lEndtime.QuadPart - m_lBeginTime.QuadPart;
	double	duringtime = (double)elapsed / (double)m_lFrequency.QuadPart * 1000;

	CString strTimeTemp;

	strTimeTemp.Format(_T("1 Image Send Time : %.3lf (Sec)"), (duringtime / 1000));
	SetDlgItemText(IDC_STATIC_PACKET_SEND_TIME, strTimeTemp);

	int nHeaderSize		= PROTOCOL_HEADER_SIZE;
	int nContentSize	= 0;

	if		(m_nPacketMode == MODE_M_PACKET)	{ nContentSize = sizeof(PACKET_M_IMAGE_BYTE_CONTENT); }
	else if (m_nPacketMode == MODE_A_PACKET)	{ nContentSize = sizeof(PACKET_A_IMAGE_BYTE_CONTENT); }
	else if (m_nPacketMode == MODE_RUN_PACKET)	{ nContentSize = sizeof(PACKET_M_IMAGE_BYTE_CONTENT); }

	int nSendSize		= (nHeaderSize + (nContentSize * m_fnPixelWidth())) * m_fnPixelHeight();
	int nTotalByte		= m_fnPixelWidth() * m_fnPixelHeight();
	double dByteBps		= (nTotalByte / (duringtime / 1000));
	double dByteKbps	= dByteBps / 1024;
	double dByteMbps	= dByteKbps / 1024;

	strTimeTemp.Format(_T("1 Sec Send Byte Time : %.3lf (Kbps)"), dByteKbps);
	SetDlgItemText(IDC_STATIC_BYTE_BPS_TIME, strTimeTemp);
}

void CSimulatorView::OnBnClickedButtonImageLoadSet()
{
	CString strName, strFolder, strPath;

	GetDlgItem(IDC_EDIT_IMAGE_NAME)->GetWindowText(strName);
	GetDlgItem(IDC_EDIT_IMAGE_FOLDER_PATH)->GetWindowText(strFolder);

	strPath.Format(_T("%s%s"), strFolder, strName);

	if (m_ImageMatSRC.data != NULL)
	{
		m_ImageMatSRC.release();
		m_ImageMatSRC = NULL;
	}

	if (m_ImageTempMatSRC.data != NULL)
	{
		m_ImageTempMatSRC.release();
		m_ImageTempMatSRC = NULL;
	}

	if (m_Image != NULL)
	{
		m_Image.Destroy();
	}

	m_bImageLoad = TRUE;

	m_Image.Load(strPath);

	m_ImageMatSRC = imread(string(strPath), IMREAD_COLOR);
	m_ImageTempMatSRC = imread(string(strPath), IMREAD_COLOR);

	m_fnDisplayResize();

	m_fnRefreshViewRect();
}

void CSimulatorView::m_fnImageLoadInit()
{
	CString strName, strFolder;

	strName = _T("SampleImage1_1000.bmp");
	strFolder = _T("..\\..\\..\\Document\\");

	GetDlgItem(IDC_EDIT_IMAGE_NAME)->SetWindowText(strName);
	GetDlgItem(IDC_EDIT_IMAGE_FOLDER_PATH)->SetWindowText(strFolder);

	OnBnClickedButtonImageLoadSet();
}

void CSimulatorView::m_fnBufferSizeChange()
{
	if (m_pTcpServer->GetClientStatus(static_cast<UINT>(SERVER_SOCKET::CH_1), static_cast<UINT>(CLIENT_SOCKET::CH_1)))
	{
		if		(m_nPacketCount <= 10  ) { m_pTcpServer->BufferSizeChange(static_cast<UINT>(SERVER_SOCKET::CH_1), static_cast<UINT>(CLIENT_SOCKET::CH_1), 2048);				}
		else if (m_nPacketCount <= 100 ) { m_pTcpServer->BufferSizeChange(static_cast<UINT>(SERVER_SOCKET::CH_1), static_cast<UINT>(CLIENT_SOCKET::CH_1), 20480);				}
		else if (m_nPacketCount <= 1000) { m_pTcpServer->BufferSizeChange(static_cast<UINT>(SERVER_SOCKET::CH_1), static_cast<UINT>(CLIENT_SOCKET::CH_1), SOCKET_BUFFER_SIZE);  }
	}
}

void CSimulatorView::m_fnImageSizeChangeAndInit()
{
	int nImageWidth		= m_fnPixelWidth();
	int nImageHeight	= m_fnPixelHeight();

	if (m_nBeforeImageHeight != nImageHeight || m_nBeforeImageWidth != nImageWidth)
	{
		delete m_dMat; m_dMat = nullptr;

		m_dMat = new Mat(Mat::zeros(nImageWidth, nImageHeight, CV_64F));
	}
	else
	{
		*m_dMat = Mat::zeros(m_dMat->rows, m_dMat->cols, CV_64F);
	}

	m_nBeforeImageWidth		= nImageWidth;
	m_nBeforeImageHeight	= nImageHeight;
}

void CSimulatorView::m_fnImageResize(int nWidth, int nHeight)
{
	Mat ImageMatResize;

	resize(m_ImageTempMatSRC, ImageMatResize, cv::Size(nWidth, nHeight), INTER_LINEAR);

	if (m_ImageMatSRC.data != NULL)
	{
		m_ImageMatSRC.release();
		m_ImageMatSRC = NULL;
	}

	if (m_Image != NULL)
	{
		m_Image.Destroy();
	}

	m_ImageMatSRC = ImageMatResize.clone();

	m_fnDisplayResize();

	m_fnRefreshViewRect();
}

BOOL CSimulatorView::m_fnRunImagePacketDataSend()
{
	int		nCount			= m_nPacketCount;
	int		nTotalSize		= PROTOCOL_M_PACKET_SIZE * nCount;
	int		nRepeat			= nTotalSize / PROTOCOL_M_PACKET_SIZE;
	__int64 nPacketCount	= 1;

	if (m_bTimeUpdate)
	{
		QueryPerformanceFrequency(&m_lFrequency);
		QueryPerformanceCounter(&m_lBeginTime);
	}
	
	m_bStartScan = TRUE;

	while (GetThreadStatus())
	{
		if (!m_bStartScan)
		{
			m_fnStartSiganlDataSignal(&nPacketCount, FALSE);

			if (!GetThreadStatus()) { break; }
		}
		else
		{
			for (int nSetCount = 0; nSetCount < m_nSetCount; nSetCount++)
			{
				for (int nInterpolation = 0; nInterpolation < m_nInterpolationCount; nInterpolation++)
				{
					nPacketCount = 1;

					for (int row = 0; row < m_fnPixelHeight(); row++)
					{
						m_fnStartSiganlDataSignal(&nPacketCount, TRUE);

						m_nPixelPosX = 0;
						m_nPixelPosY++;

						if (!GetThreadStatus()) { break; }
					}

					m_nPixelPosY = 0;
					m_nPixelPosX = 0;

					std::memset(&m_dAxisX_Pos, 0x00, sizeof(m_dAxisX_Pos));
					std::memset(&m_dAxisY_Pos, 0x00, sizeof(m_dAxisY_Pos));

					std::memset(&m_dAxisX_Signal, 0x00, sizeof(m_dAxisX_Signal));
					std::memset(&m_dAxisY_Signal, 0x00, sizeof(m_dAxisY_Signal));

					m_nImageCountCCD++;

					m_pLogViewer->AddLog(
						static_cast<UINT>(LOG_VIEWER::CH_1),
						LOG_SEQUENCE,
						m_fnFormattedString(_T("* CCD Image Success : %d"), m_nImageCountCCD));

					if (!GetThreadStatus()) { break; }
				}
				if (!GetThreadStatus()) { break; }

				Sleep(100);
			}

			SetThreadStatus(FALSE);
		}
	}

	if (m_bTimeUpdate)
	{
		QueryPerformanceCounter(&m_lEndtime);
		m_fnReceiveTackTimeUpdate();
	}

	return TRUE;
}

BOOL CSimulatorView::m_fnMImagePacketDataSend()
{
	PACKET_M_IMAGE_DATA PacketData;

	int nPixelCols = 0;
	int nPacketNum = 0;

	if (m_bTimeUpdate)
	{
		QueryPerformanceFrequency(&m_lFrequency);
		QueryPerformanceCounter(&m_lBeginTime);
	}

	PacketData.Stx = ADAM_TO_MAIN_ID;
	PacketData.Len = PROTOCOL_M_PACKET_SIZE * PROTOCOLM_PACKET_COUNT_FIX;
	PacketData.Cmd = ADAM_TO_MAIN_CMD_M_PACKET_RUN;

	int nRepeatCount = m_nMPacketCount / PROTOCOLM_PACKET_COUNT_FIX;
	int nPacketCount = m_nMPacketCount % PROTOCOLM_PACKET_COUNT_FIX;
	int nPacketTotalCount = 0;
	
	for (int i = 0; i < nRepeatCount; i++)
	{
		for (int nPacketLength = 0; nPacketLength < PROTOCOLM_PACKET_COUNT_FIX; nPacketLength++)
		{
			PACKET_M_IMAGE_DATA_CONTENT* pDataContent = new PACKET_M_IMAGE_DATA_CONTENT;

			srand((double)(time(NULL)));

			double dRand = rand() % 10;

			double dDitector = (dRand / 10);

			pDataContent->PacketNumber = nPacketNum;
			pDataContent->EM1024X = 1.1;
			pDataContent->EM1024Y = 1.2;
			pDataContent->Detector_1 = dDitector;
			pDataContent->Detector_2 = dDitector;
			pDataContent->Detector_3 = dDitector;
			pDataContent->Detector_4 = dDitector;
			pDataContent->Zr_IO = 5.1;
			pDataContent->BS_IO = 5.2;

			PacketData.QueueData.push(pDataContent);

			nPacketNum++;

			if (!GetThreadStatus()) { break; }
		}

		m_pTcpServer->SendMImagePacket(static_cast<UINT>(SERVER_SOCKET::CH_1), static_cast<UINT>(CLIENT_SOCKET::CH_1), &PacketData);

		if (m_bTimeUpdate)
		{
			QueryPerformanceCounter(&m_lEndtime);
			m_fnReceiveTackTimeUpdate();
		}

		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedString(_T("* Send M Packet[%d] Count : %d"), PROTOCOLM_PACKET_COUNT_FIX, i + 1));

		nPacketTotalCount++;
	}

	if (nPacketCount != 0)
	{
		PacketData.Stx = ADAM_TO_MAIN_ID;
		PacketData.Len = PROTOCOL_M_PACKET_SIZE * nPacketCount;
		PacketData.Cmd = ADAM_TO_MAIN_CMD_M_PACKET_RUN;

		for (int nPacketLength = 0; nPacketLength < nPacketCount; nPacketLength++)
		{
			PACKET_M_IMAGE_DATA_CONTENT* pDataContent = new PACKET_M_IMAGE_DATA_CONTENT;

			srand((double)(time(NULL)));

			double dRand = rand() % 10;

			double dDitector = (dRand / 10);

			pDataContent->PacketNumber = nPacketNum;
			pDataContent->EM1024X = 1.1;
			pDataContent->EM1024Y = 1.2;
			pDataContent->Detector_1 = dDitector;
			pDataContent->Detector_2 = dDitector;
			pDataContent->Detector_3 = dDitector;
			pDataContent->Detector_4 = dDitector;
			pDataContent->Zr_IO = 5.1;
			pDataContent->BS_IO = 5.2;

			PacketData.QueueData.push(pDataContent);

			nPacketNum++;

			if (!GetThreadStatus()) { break; }
		}

		m_pTcpServer->SendMImagePacket(static_cast<UINT>(SERVER_SOCKET::CH_1), static_cast<UINT>(CLIENT_SOCKET::CH_1), &PacketData);

		if (m_bTimeUpdate)
		{
			QueryPerformanceCounter(&m_lEndtime);
			m_fnReceiveTackTimeUpdate();
		}

		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedString(_T("* Send M Packet[%d] Count : %d"), nPacketCount, nPacketTotalCount + 1));
	}

	return TRUE;
}

BOOL CSimulatorView::m_fnAImagePacketDataSend()
{
	PACKET_A_IMAGE_DATA PacketData;

	int nCount = m_nPacketCount;
	int nTotalSize = PROTOCOL_A_PACKET_SIZE * nCount;
	int nRepeat = nTotalSize / PROTOCOL_A_PACKET_SIZE;

	int nPixelCols = 0;
	int nPacketCount = 0;

	if (m_bTimeUpdate)
	{
		QueryPerformanceFrequency(&m_lFrequency);
		QueryPerformanceCounter(&m_lBeginTime);
	}

	PacketData.Stx = ADAM_TO_MAIN_ID;
	PacketData.Len = nTotalSize;
	PacketData.Cmd = ADAM_TO_MAIN_CMD_A_PACKET_RUN;

	int nRow = 0;
	int nCol = 0;

	uchar* pData = m_ImageMatSRC.data;

	uchar b = pData[nRow * m_fnPixelWidth() * 1 + (nCol)];

	PACKET_A_IMAGE_DATA_CONTENT* pDataContent = new PACKET_A_IMAGE_DATA_CONTENT;

	srand((double)(time(NULL)));

	double dRand = rand() % 10;

	double dDitector = (unsigned int)b + (dRand / 10);

	pDataContent->PacketNumber = nPacketCount;
	pDataContent->EM1024X = 1.1;
	pDataContent->EM1024Y = 1.2;
	pDataContent->Detector_1 = dDitector;
	pDataContent->Detector_2 = dDitector;
	pDataContent->Detector_3 = dDitector;
	pDataContent->Detector_4 = dDitector;
	pDataContent->Zr_IO = 5.1;
	pDataContent->BS_IO = 5.2;
	pDataContent->DetectorSub_1 = dDitector;
	pDataContent->DetectorSub_2 = dDitector;
	pDataContent->DetectorSub_3 = dDitector;
	pDataContent->DetectorSub_4 = dDitector;

	PacketData.QueueData.push(pDataContent);

	nPacketCount++;

	m_pTcpServer->SendAImagePacket(static_cast<UINT>(SERVER_SOCKET::CH_1), static_cast<UINT>(CLIENT_SOCKET::CH_1), &PacketData);

	if (m_bTimeUpdate)
	{
		QueryPerformanceCounter(&m_lEndtime);
		m_fnReceiveTackTimeUpdate();
	}

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("* Send A Packet[%d] Count : 1"), m_nAPacketCount));

	return TRUE;
}

int CSimulatorView::m_fnPixelWidth()
{
	return (m_nImageFOV_X / m_nScanGrid_X) + 1;
}

int CSimulatorView::m_fnPixelHeight()
{
	return (m_nImageFOV_Y / m_nScanGrid_Y) + 1;
}

LRESULT CSimulatorView::OnMessageServerClientAccept(WPARAM wParam, LPARAM lParam)
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Client OnAccept Success.")));

	m_bSocketConnect = TRUE;
	m_fnRefreshSocketRect();
	return TRUE;
}

LRESULT CSimulatorView::OnMessageServerClientReceive(WPARAM wParam, LPARAM lParam)
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Client Receive Socket.")));

	return TRUE;
}

LRESULT CSimulatorView::OnMessageServerClientSocketClose(WPARAM wParam, LPARAM lParam)
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Client Close Socket.")));

	m_bSocketConnect = FALSE;
	m_fnRefreshSocketRect();
	return TRUE;
}

LRESULT CSimulatorView::OnMessageRun(WPARAM wParam, LPARAM lParam)
{
	PACKET_RUN_STOP_DATA PacketData;

	PacketData = *(PACKET_RUN_STOP_DATA*)lParam;

	m_nPacketMode = MODE_RUN_PACKET;

	SetThreadStatus(TRUE);
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Run Sequence.")));
	SetEvent(m_hEvent);

	return TRUE;
}

LRESULT CSimulatorView::OnMessageStop(WPARAM wParam, LPARAM lParam)
{
	PACKET_RUN_STOP_DATA PacketData;

	PacketData = *(PACKET_RUN_STOP_DATA*)lParam;

	SetThreadStatus(FALSE);
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Stop Sequence.")));

	return TRUE;
}

LRESULT CSimulatorView::OnMessageAPacketeRun(WPARAM wParam, LPARAM lParam)
{
	PACKET_A_PACKET_RUN_DATA PacketData;

	PacketData = *(PACKET_A_PACKET_RUN_DATA*)lParam;

	m_nPacketMode = MODE_A_PACKET;

	SetThreadStatus(TRUE);
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("A Packet Run Sequence.")));
	SetEvent(m_hEvent);

	return TRUE;
}

LRESULT CSimulatorView::OnMessageAverageCount(WPARAM wParam, LPARAM lParam)
{
	PACKET_AVERAGE_COUNT_DATA PacketData;

	PacketData = *(PACKET_AVERAGE_COUNT_DATA*)lParam;

	m_nAPacketCount = PacketData.Data;

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("OnAverageCount")));

	return TRUE;
}

LRESULT CSimulatorView::OnMessageEm1024Reset(WPARAM wParam, LPARAM lParam)
{
	PACKET_EM1024_RESET_DATA PacketData;

	PacketData = *(PACKET_EM1024_RESET_DATA*)lParam;

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("OnLifReset")));

	return TRUE;
}

LRESULT CSimulatorView::OnMessageMPacketRun(WPARAM wParam, LPARAM lParam)
{
	PACKET_M_PACKET_RUN_DATA PacketData;

	PacketData = *(PACKET_M_PACKET_RUN_DATA*)lParam;

	m_nPacketMode = MODE_M_PACKET;

	m_nMPacketCount = PacketData.Data;

	SetThreadStatus(TRUE);
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("M Packet Run Sequence.")));
	SetEvent(m_hEvent);
	
	return TRUE;
}

LRESULT CSimulatorView::OnMessageScanRecipe(WPARAM wParam, LPARAM lParam)
{
	PACKET_RECIPE_DATA PacketData;

	PacketData = *(PACKET_RECIPE_DATA*)lParam;

	m_nImageFOV_X			= PacketData.ImageFOV_X;
	m_nImageFOV_Y			= PacketData.ImageFOV_Y;
	m_nScanGrid_X			= PacketData.ScanGrid_X;
	m_nScanGrid_Y			= PacketData.ScanGrid_Y;
	m_nInterpolationCount	= PacketData.InterpolationCount;
	m_nAverageCount			= PacketData.AverageCount;
	m_nSetCount				= PacketData.SetCount;
	m_nScanDirection		= PacketData.ScanDirection;
	m_nPacketCount			= PacketData.PacketCount;

	m_fnScanRecipeUpdate();
	m_fnImageResize(m_fnPixelWidth(), m_fnPixelHeight());

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedString(_T("Scan Recipe.")));

	return TRUE;
}

LRESULT CSimulatorView::OnMessageSocketPing(WPARAM wParam, LPARAM lParam)
{
	InvalidateRect(m_rcSocketConnect, FALSE);

	return TRUE;
}

void CSimulatorView::m_fnChartPosVIew(double* dAxisX, double* dAxisY, int nSize)
{
	XYChart* c = new XYChart(570, 380, 0xeeeeff, 0x000000, 1);

	c->setRoundedFrame();

	c->setPlotArea(50, 50, 500, 280, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);

	c->addLegend(50, 20, false, "arial.ttf", 9)->setBackground(Chart::Transparent);

	c->addTitle("X,Y Position Graph", "Times New Roman Bold Italic", 10)->setBackground(0xccccff, 0x000000, Chart::glassEffect());

	c->yAxis()->setTitle("X Axis");

	c->xAxis()->setLabels(DoubleArray(dAxisY, nSize));

	c->xAxis()->setLabelStep(((int)(nSize / 10)));

	c->xAxis()->setTitle("Y Axis");

	LineLayer *layer = c->addLineLayer();

	layer->setLineWidth(1);

	layer->addDataSet(DoubleArray(dAxisX, nSize), 0xff0000, "Position #1");

	m_chartPosView.setChart(c);

	delete c;
}

void CSimulatorView::m_fnChartSignalVIew(double* dAxisX, double* dAxisY, int nSize)
{
	XYChart* c = new XYChart(570, 380, 0xeeeeff, 0x000000, 1);

	c->setRoundedFrame();

	c->setPlotArea(50, 50, 500, 280, 0xffffff, -1, -1, 0xcccccc, 0xcccccc);

	c->addLegend(50, 20, false, "arial.ttf", 9)->setBackground(Chart::Transparent);

	c->addTitle("Ditector Signal Graph", "Times New Roman Bold Italic", 10)->setBackground(0xccccff, 0x000000, Chart::glassEffect());

	c->yAxis()->setTitle("X Axis");

	c->xAxis()->setLabels(DoubleArray(dAxisY, nSize));

	c->xAxis()->setLabelStep(((int)(nSize / 10)));

	c->xAxis()->setTitle("Y Axis");

	LineLayer *layer = c->addLineLayer();

	layer->setLineWidth(1);

	layer->addDataSet(DoubleArray(dAxisX, nSize), 0x0000ff, "Ditector #1");

	m_chartSignalView.setChart(c);

	delete c;
}

LRESULT CSimulatorView::OnMessageStartChartPosView(WPARAM wParam, LPARAM lParam)
{
	m_fnChartPosVIew(m_dAxisX_Pos, m_dAxisY_Pos, X_AXIS_COUNT);
	return TRUE;
}

LRESULT CSimulatorView::OnMessageStartChartSignalView(WPARAM wParam, LPARAM lParam)
{
	m_fnChartSignalVIew(m_dAxisX_Signal, m_dAxisY_Signal, X_AXIS_COUNT);
	return TRUE;
}

int CSimulatorView::m_fnRandomData(int nLimit)
{
	int start_number = 1, end_number = (nLimit - 1);
	int random_number = (rand() % (end_number - start_number + 1)) + start_number;

	return random_number;
}

double CSimulatorView::m_fnRandomDataPos(BOOL bNoSignal)
{
	double dDataA = 0;
	double dDataB = 0;
	double dDataC = 0;

	if (bNoSignal)
	{
		dDataA = (double)m_fnRandomData(10);
		dDataB = ((double)m_fnRandomData(100)) / 1000;
		dDataC = dDataA + dDataB;
	}
	else
	{
		dDataA = 0;
		dDataB = ((double)m_fnRandomData(100)) / 1000;
		dDataC = dDataA + dDataB;
	}

	return dDataC;
}

void CSimulatorView::OnBnClickedButtonStartScan()
{
	m_bStartScan = TRUE;
}

void CSimulatorView::OnBnClickedButtonScanReset()
{
	memset(&m_dAxisX_Pos, 0x00, sizeof(m_dAxisX_Pos));
	memset(&m_dAxisY_Pos, 0x00, sizeof(m_dAxisY_Pos));

	memset(&m_dAxisX_Signal, 0x00, sizeof(m_dAxisX_Signal));
	memset(&m_dAxisY_Signal, 0x00, sizeof(m_dAxisY_Signal));

	m_nPixelPosX = 0;
	m_nPixelPosY = 0;

	m_nImageCountCCD = 0;

	m_bStartScan = FALSE;

	m_fnChartPosVIew(m_dAxisX_Pos, m_dAxisY_Pos, X_AXIS_COUNT);
	m_fnChartSignalVIew(m_dAxisX_Signal, m_dAxisY_Signal, X_AXIS_COUNT);
}

void CSimulatorView::m_fnStartSiganlDataSignal(__int64* nPacketCount, BOOL bStartScan)
{
	PACKET_M_IMAGE_DATA PacketData;

	int nColSplitCount		= m_nPacketCount;
	int nTotalSize			= PROTOCOL_M_PACKET_SIZE * nColSplitCount;
	int nRepeat				= nTotalSize / PROTOCOL_M_PACKET_SIZE;
	int nChartCount			= 0;

	PacketData.Stx = ADAM_TO_MAIN_ID;
	PacketData.Len = nTotalSize;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_ADAM_RUN_STOP;

	m_fnSetChartAxis_Y(m_dAxisY_Pos, 1000);
	m_fnSetChartAxis_Y(m_dAxisY_Signal, 1000);

	uchar* pData = m_ImageMatSRC.data;

	double* pDetector = (double*)m_dMat->data;

//	for (int nColSplit = 0; nColSplit < m_fnPixelWidth(); nColSplit++)
	for (int nColSplit = 0; nColSplit < 1; nColSplit++)
	{
		for (int nCol = 0; nCol < nColSplitCount; nCol++)
		{
			if (nChartCount == X_AXIS_COUNT) { nChartCount = 0; }

			double dPosX		= 0;
			double dPosY		= 0;
			double dDitector_1	= 0;
			double dDitector_2	= 0;
			double dDitector_3	= 0;
			double dDitector_4	= 0;

			unsigned int nPixelX = 0;
			unsigned int nPixelY = 0;

			if (bStartScan)
			{
				if (m_nPixelPosY % 2 == FALSE)
				{
					dPosX = (m_nPixelPosX * m_nScanGrid_X);
					dPosY = (m_nPixelPosY * m_nScanGrid_Y);

					nPixelX = m_nPixelPosX;
					nPixelY = m_nPixelPosY;
				}
				else
				{
					dPosX = ((m_fnPixelWidth() - 1) * m_nScanGrid_X) - (m_nPixelPosX * m_nScanGrid_X);
					dPosY = (m_nPixelPosY * m_nScanGrid_Y);

					nPixelX = (m_fnPixelWidth() - 1) - m_nPixelPosX;
					nPixelY = m_nPixelPosY;
				}

				dDitector_1 = (double)pData[nPixelY * m_fnPixelWidth() * 3 + nPixelX * 3 + 0];
				dDitector_2 = (double)pDetector[nPixelY * m_fnPixelWidth() + nPixelX];

				m_dAxisX_Pos[m_nPixelPosY]		= dPosY;
				m_dAxisX_Signal[m_nPixelPosX]	= dDitector_1;
			}
			else
			{
				dPosX = m_nPixelPosX + m_fnRandomDataPos(FALSE);
				dPosY = m_nPixelPosY + m_fnRandomDataPos(FALSE);

				dDitector_1 = m_fnRandomDataPos(FALSE);
				dDitector_2 = m_fnRandomDataPos(FALSE);

				m_dAxisX_Pos[nChartCount]		= dPosX;
				m_dAxisX_Signal[nChartCount]	= dDitector_1;
			}

			PACKET_M_IMAGE_DATA_CONTENT* pDataContent = new PACKET_M_IMAGE_DATA_CONTENT;

			pDataContent->PacketNumber = *nPacketCount;
			pDataContent->EM1024X = (-(dPosX)) / 1000 / 1000;
			pDataContent->EM1024Y = (dPosY) / 1000 / 1000;
			pDataContent->Detector_1 = dDitector_1;
			pDataContent->Detector_2 = dDitector_2;
			pDataContent->Detector_3 = 4.1;
			pDataContent->Detector_4 = 4.2;
			pDataContent->Zr_IO = 5.1;
			pDataContent->BS_IO = 5.2;

			PacketData.QueueData.push(pDataContent);

			(*nPacketCount)++;

			nChartCount++;

			if (bStartScan) { m_nPixelPosX++; }
		}

		if (m_pTcpServer->GetClientStatus(static_cast<UINT>(SERVER_SOCKET::CH_1), static_cast<UINT>(CLIENT_SOCKET::CH_1)))
		{
			m_pTcpServer->SendRunImagePacket(static_cast<UINT>(SERVER_SOCKET::CH_1), static_cast<UINT>(CLIENT_SOCKET::CH_1), &PacketData);
//			Sleep(1);
		}
		else
		{
			int nVectorSize = PacketData.QueueData.size();

			for (int i = 0; i < nVectorSize; i++)
			{
				PACKET_M_IMAGE_DATA_CONTENT* pDataContent = PacketData.QueueData.front();

				delete pDataContent;
				pDataContent = nullptr;

				PacketData.QueueData.pop();
			}
		}

//		if (bStartScan) { m_nPixelPosX++; }

		if (!GetThreadStatus()) { break; }
	}

	SendMessage(WM_USER_MESSAGE_SM_CHART_POS_VIEW_UPDATE, NULL, NULL);
	SendMessage(WM_USER_MESSAGE_SM_CHART_SIGNAL_VIEW_UPDATE, NULL, NULL);
}

void CSimulatorView::m_fnSetChartAxis_Y(double* dAxis, int nSize)
{
	for (int i = 0; i < nSize; i++) { dAxis[i] = i; }
}

void CSimulatorView::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if ((nIDCtl == IDC_BUTTON_TEST_MANUAL_MODE) && m_bManualModeStatus)
    {
		m_fnDrawDesignButtonStay(lpDrawItemStruct, RGB(0, 255, 0), RGB(50, 255, 50), RGB(0, 0, 0), TRUE);
    }
	else
	{
		m_fnDrawDesignButtonStay(lpDrawItemStruct, RGB(255, 0, 0), RGB(255, 50, 50), RGB(255, 255, 255), FALSE);
	}

	if ((nIDCtl == IDC_BUTTON_SERVER_CREATE)		||
		(nIDCtl == IDC_BUTTON_SEND_DATA)			||
		(nIDCtl == IDC_BUTTON_IMAGE_THREAD_START)	||
		(nIDCtl == IDC_BUTTON_IMAGE_THREAD_STOP)	||
		(nIDCtl == IDC_BUTTON_START_SCAN)			||
		(nIDCtl == IDC_BUTTON_SCAN_RESET)			||
		(nIDCtl == IDC_BUTTON_IMAGE_LOAD)			||
		(nIDCtl == IDC_BUTTON_SIGNAL_LOAD_SET)		||
		(nIDCtl == IDC_BUTTON_IMAGE_LOAD_SET))
	{
		m_fnDrawDesignButtonSelect(lpDrawItemStruct, RGB(65, 65, 65), RGB(38, 38, 38), RGB(255, 255, 255));
	}

	CFormView::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CSimulatorView::m_fnDrawDesignButtonStay(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font, BOOL bFlag)
{
	if (bFlag)
    {
        CDC dc;
        RECT rect;
        dc.Attach(lpDrawItemStruct -> hDC);
        rect = lpDrawItemStruct->rcItem;
		dc.SetBkMode(TRANSPARENT);
        dc.Draw3dRect(&rect,color_edge,color_edge);
        dc.FillSolidRect(&rect, color_bk);

        UINT state	= lpDrawItemStruct-> itemState;
		UINT action = lpDrawItemStruct->itemAction;

//        if((state &ODS_SELECTED)) { dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT); }
//        else						{ dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT); }

        dc.SetBkColor(color_bk);
        dc.SetTextColor(color_font);
        TCHAR buffer[MAX_PATH];
        ZeroMemory(buffer,MAX_PATH);
        ::GetWindowTextA(lpDrawItemStruct->hwndItem,buffer,MAX_PATH);
        dc.DrawText(buffer,&rect,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
        dc.Detach();
    }
	else
	{
		CDC dc;
        RECT rect;
        dc.Attach(lpDrawItemStruct -> hDC);
        rect = lpDrawItemStruct->rcItem;
		dc.SetBkMode(TRANSPARENT);
		dc.Draw3dRect(&rect,color_edge,color_edge);
		dc.FillSolidRect(&rect,color_bk);

        UINT state = lpDrawItemStruct-> itemState;

//        if((state &ODS_SELECTED)) { dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT); }
//        else						{ dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT); }

        dc.SetBkColor(color_bk);
        dc.SetTextColor(color_font);
        TCHAR buffer[MAX_PATH];
        ZeroMemory(buffer,MAX_PATH);
        ::GetWindowTextA(lpDrawItemStruct->hwndItem,buffer,MAX_PATH);
        dc.DrawText(buffer,&rect,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
        dc.Detach();
	}
}

void CSimulatorView::m_fnDrawDesignButtonSelect(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font)
{
	CDC dc;
	RECT rect;
	dc.Attach(lpDrawItemStruct->hDC);
	rect = lpDrawItemStruct->rcItem;
	dc.SetBkMode(TRANSPARENT);
	dc.Draw3dRect(&rect, color_edge, color_edge);
	dc.FillSolidRect(&rect, color_bk);

	UINT state = lpDrawItemStruct->itemState;

//    if((state &ODS_SELECTED))	{ dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT); }
//    else						{ dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT); }

	if ((state & ODS_SELECTED))
	{
		dc.Draw3dRect(&rect, RGB(53, 53, 53), RGB(53, 53, 53));
		dc.FillSolidRect(&rect, RGB(103, 103, 103));
		dc.SetBkColor(RGB(103, 103, 103));
		dc.SetTextColor(color_font);
	}

	dc.SetBkColor(color_bk);
	dc.SetTextColor(color_font);
	TCHAR buffer[MAX_PATH];
	ZeroMemory(buffer, MAX_PATH);
	::GetWindowTextA(lpDrawItemStruct->hwndItem, buffer, MAX_PATH);
	dc.DrawText(buffer, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	dc.Detach();
}

void CSimulatorView::m_fnSetManualMode(BOOL bFlag)
{
	m_bManualModeStatus = bFlag;

	m_fnRefreshManualRect();
}

BOOL CSimulatorView::OnMessageManualMode(BOOL bFlag)
{
	m_fnSetManualMode(bFlag);
	return TRUE;
}

void CSimulatorView::OnBnClickedButtonSignalLoadSet()
{
	// 파일 대화 상자에서 BMP 형식의 파일만 보이도록 하는 필터의 정의
	char szFilter[] = "TEXT File (*.txt) | *.txt; | All Files(*.*) | *.* ||";

	// 파일 대화 상자의 생성
	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

	CStdioFile file;

	if (IDOK == dlg.DoModal())
	{
		CString strSignalName	= dlg.GetFileName();
		CString strFolderName	= dlg.GetPathName();

		GetDlgItem(IDC_EDIT_SIGNAL_NAME)->SetWindowText(strSignalName);
		GetDlgItem(IDC_EDIT_SIGNAL_FOLDER_PATH)->SetWindowText(strFolderName);

		BOOL bOpen = file.Open(strFolderName, CStdioFile::modeReadWrite);

		if (bOpen)
		{
			unsigned int nLine = 0;
			unsigned int nPixelPosX = 0;
			unsigned int nPixelPosY = 0;

			CString strRead;
			CString strA, strB, strC, strD;

			while (file.ReadString(strRead))
			{
				m_fnStartLineData(nLine, strRead, &nPixelPosX, &nPixelPosY);
				nLine++;
			}
			file.Close();
		}
		if (m_ImageTempMatSRC.data != NULL)
		{
			m_ImageTempMatSRC.release();
			m_ImageTempMatSRC = NULL;
		}

		m_ImageTempMatSRC = m_ImageMatSRC.clone();

		m_fnDisplayResize();
		m_fnRefreshViewRect();
	}
}

BOOL CSimulatorView::m_fnStartLineData(unsigned int nLine, CString strLineData, unsigned int* pPosX, unsigned int* pPosY)
{
	switch (nLine)
	{
		case 0: { break; }
		case 1: { break; }
		case 2: { m_nImageFOV_X = (_ttoi(strLineData) - 1) * 100; break; }
		case 3: { m_nImageFOV_Y = (_ttoi(strLineData) - 1) * 100; break; }
		case 4: 
		{ 
			m_fnScanRecipeUpdate();
			m_fnImageResize(m_fnPixelWidth(), m_fnPixelHeight());
			m_fnImageSizeChangeAndInit();
			break; 
		}
		default:
		{
			double dDetector = _ttof(m_fnStringDataParsing(strLineData, 2, '\t'));

			if (_ttof(strLineData) == 0) { break; }

			CImageProcessAlgorithm* pPA = m_pImageProcessAlgorithm;

			uchar* pImageRawData = m_ImageMatSRC.data;

			unsigned int nValueT = (unsigned int)pPA->ReMap((dDetector * 100), 256, 60, 100);

			double* pDetector = (double*)m_dMat->data;

			pDetector[*pPosY * m_dMat->cols + *pPosX] = dDetector;

			pImageRawData[*pPosY * m_ImageMatSRC.cols * 3 + *pPosX * 3 + 0] = pPA->ReMap(pPA->GetColorMapParula(COLOR_MAP_PARULA_B, nValueT), 256, 0.0, 1.0);
			pImageRawData[*pPosY * m_ImageMatSRC.cols * 3 + *pPosX * 3 + 1] = pPA->ReMap(pPA->GetColorMapParula(COLOR_MAP_PARULA_G, nValueT), 256, 0.0, 1.0);
			pImageRawData[*pPosY * m_ImageMatSRC.cols * 3 + *pPosX * 3 + 2] = pPA->ReMap(pPA->GetColorMapParula(COLOR_MAP_PARULA_R, nValueT), 256, 0.0, 1.0);
			(*pPosX)++;

			if (*pPosX == m_fnPixelHeight())
			{ 
				*pPosX = 0;
				(*pPosY)++;
				break;
			}
			break;
		}
	}
	return TRUE;
}

CString CSimulatorView::m_fnStringDataParsing(CString strLineData, unsigned int nIndex, TCHAR cCSV)
{
	CString strData;
	AfxExtractSubString(strData, strLineData, nIndex, cCSV);
	return strData;
}

void CSimulatorView::m_fnCreateImageInit()
{
	int nImageWidth		= m_fnPixelWidth();
	int nImageHeight	= m_fnPixelHeight();

	m_dMat = new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

	m_nBeforeImageWidth		= nImageWidth;
	m_nBeforeImageHeight	= nImageHeight;
}

void CSimulatorView::m_fnMenuBarInit()
{
	BITMAP	size;
	HBITMAP hBmp;

	m_ctlProjectName.ModifyStyle(0xF, SS_BITMAP | SS_CENTERIMAGE);

	hBmp = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_PROJECT_NAME));

	m_ctlProjectName.SetBitmap(hBmp);

	::GetObject(hBmp, sizeof(BITMAP), &size);

	m_fStaticBold.CreateFont(20, 8, 0, 0,
		FW_HEAVY, FALSE, FALSE, FALSE,
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH, "굴림");

	GetDlgItem(IDC_STATIC_PROGRAM_VERSION)->SetWindowText(_T("2.0.0 Ver"));
	GetDlgItem(IDC_STATIC_PROGRAM_VERSION)->SetFont(&m_fStaticBold);

	GetDlgItem(IDC_STATIC_PROGRAM_PROJECT)->SetWindowText(_T("PTR"));
	GetDlgItem(IDC_STATIC_PROGRAM_PROJECT)->SetFont(&m_fStaticBold);

	GetDlgItem(IDC_STATIC_EQP_STATUS)->SetWindowText(_T("ADAM SCAN STOP STATUS"));
	GetDlgItem(IDC_STATIC_EQP_STATUS)->SetFont(&m_fStaticBold);
	GetDlgItem(IDC_STATIC_EQP_STATUS)->GetWindowRect(m_rcEqpStatus);
	ScreenToClient(&m_rcEqpStatus);

	GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->SetWindowText(_T("SERVER CONNECT"));
	GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->SetFont(&m_fStaticBold);
	GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->GetWindowRect(m_rcSocketConnect);
	ScreenToClient(&m_rcSocketConnect);
}

void CSimulatorView::m_fnControlInit()
{
	m_fManualBold.CreateFont(14, 6, 0, 0,
		FW_HEAVY, FALSE, FALSE, FALSE,
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH, "굴림");

	GetDlgItem(IDC_BUTTON_TEST_MANUAL_MODE)->SetFont(&m_fManualBold);
	GetDlgItem(IDC_BUTTON_TEST_MANUAL_MODE)->GetWindowRect(m_rcManualMode);
	ScreenToClient(&m_rcManualMode);

	GetDlgItem(IDC_IMAGE_DISPLAY)->GetWindowRect(m_rcView);
	ScreenToClient(&m_rcView);
}

CString CSimulatorView::m_fnFormattedString(LPCTSTR lpszFormat, ...)
{
	CString sResult;

	va_list vlMarker = NULL;
	va_start(vlMarker, lpszFormat);

	sResult.FormatV(lpszFormat, vlMarker);

	va_end(vlMarker);

	return sResult;
}

void CSimulatorView::m_fnCreateLogViewerInit()
{
	CString strFileName;
	CString strFilePath;
	CString strTotalPath;
	CString strFileTemp = _T("\\");

	strFileName		= _T("Simulator");
	strFilePath		= RESULT_LOG_FOLDER_PATH + strFileTemp;
	strTotalPath	= strFilePath + strFileName;

	m_pLogViewer->CreateCheckFolderPath(strTotalPath);
	m_pLogViewer->OnInitialDialog(static_cast<UINT>(LOG_VIEWER::CH_1), 1180, 175, 458, 835);
	m_pLogViewer->SetProgramName(static_cast<UINT>(LOG_VIEWER::CH_1), strFileName);
	m_pLogViewer->SetLogFolderPath(static_cast<UINT>(LOG_VIEWER::CH_1), strFilePath);
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_SEQUENCE, _T("Sequence"));
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_TEST, _T("TEST"));
	m_pLogViewer->SetTabControlPos(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_SEQUENCE);
}

void CSimulatorView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	int nX = cx - m_nDialogWidth;
	int nY = cy - m_nDialogHeight;

	m_pLogViewer->OnSize(static_cast<UINT>(LOG_VIEWER::CH_1), nType, nX, nY);
}


HBRUSH CSimulatorView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	UINT nID = pWnd->GetDlgCtrlID();

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (nID == IDC_STATIC_PROGRAM_PROJECT || 
			nID == IDC_STATIC_PROGRAM_VERSION)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(91, 91, 91));
			hbr = m_brushsMenubar;
		}

		if (nID == IDC_STATIC ||
			nID == IDC_STATIC_PACKET_SEND_TIME ||
			nID == IDC_STATIC_BYTE_BPS_TIME)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(38, 38, 38));
			hbr = m_brushsStatic;
		}

		if ((nID == IDC_STATIC_EQP_STATUS) && m_bEqpStatus)
		{
			GetDlgItem(IDC_STATIC_EQP_STATUS)->SetWindowText(_T("ADAM SCAN RUNNING..."));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(0, 0, 0));
			pDC->SetBkColor(RGB(0, 255, 0));
			hbr = m_brushsEqpStatusOn;
		}
		else if ((nID == IDC_STATIC_EQP_STATUS) && !m_bEqpStatus)
		{
			GetDlgItem(IDC_STATIC_EQP_STATUS)->SetWindowText(_T("ADAM SCAN STOP STATUS"));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(255, 0, 0));
			hbr = m_brushsEqpStatusOff;
		}

		if ((nID == IDC_STATIC_SOCKET_CONNECT) && m_bSocketConnect)
		{
			GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->SetWindowText(_T("CLIENT CONNECT"));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(0, 0, 0));
			pDC->SetBkColor(RGB(0, 255, 0));
			hbr = m_brushsSocketConnectOn;
		}
		else if ((nID == IDC_STATIC_SOCKET_CONNECT) && !m_bSocketConnect)
		{
			GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->SetWindowText(_T("CLIENT CONNECT"));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(255, 0, 0));
			hbr = m_brushsSocketConnectOff;
		}
	}
	else if (nCtlColor == CTLCOLOR_EDIT)
	{
		if (nID == IDC_EDIT_SIGNAL_NAME ||
			nID == IDC_EDIT_SIGNAL_FOLDER_PATH ||
			nID == IDC_EDIT_IMAGE_NAME ||
			nID == IDC_EDIT_IMAGE_FOLDER_PATH)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(38, 38, 38));
			hbr = m_brushsEdit;
		}
	}

	return hbr;
}

void CSimulatorView::m_fnCreateBrushInit()
{
	m_brushsMenubar = CreateSolidBrush(RGB(91, 91, 91));
	m_brushsStatic = CreateSolidBrush(RGB(38, 38, 38));
	m_brushsEdit = CreateSolidBrush(RGB(53, 53, 53));
	m_brushsEqpStatusOn = CreateSolidBrush(RGB(0, 255, 0));
	m_brushsEqpStatusOff = CreateSolidBrush(RGB(255, 0, 0));
	m_brushsSocketConnectOn = CreateSolidBrush(RGB(0, 255, 0));
	m_brushsSocketConnectOff = CreateSolidBrush(RGB(255, 0, 0));
}

BOOL CSimulatorView::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);
	pDC->FillSolidRect(rect, RGB(38, 38, 38));

	return TRUE;
	//return CFormView::OnEraseBkgnd(pDC);
}
