
// SeventhExProject_AView.h : CSimulatorView 클래스의 인터페이스
//

#pragma once

#include "resource.h"
#include "afxwin.h"

#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementDefine.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementEnum.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_UserMessage.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolDefine.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolMessage.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolStructData.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolStructByte.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementMacro.h"
#include "../../../Module/TcpServer/TcpServer/TcpServerMain.h"
#include "../../../Module/LogViewer/LogViewer/LogViewerMain.h"
#include "../../../Module/ImageProcessAlgorithm/ImageProcessAlgorithm/ImageProcessAlgorithmMain.h"

#include "chartdir.h"
#include "ChartViewer.h"

#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;

#define X_AXIS_COUNT 1000
#define Y_AXIS_COUNT 1000

class CSimulatorView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	CSimulatorView();
	DECLARE_DYNCREATE(CSimulatorView)

public:
	enum{ IDD = IDD_SIMULATOR_FORM };

// 특성입니다.
public:
	CSimulatorDoc* GetDocument() const;

// 작업입니다.

public:
	HMODULE					m_hImageProcessAlgorithm;
	CImageProcessAlgorithm*	m_pImageProcessAlgorithm;

	HMODULE					m_hLogViewer;
	CLogViewer*				m_pLogViewer;

	HMODULE		m_hTcpServer;
	CTcpServer* m_pTcpServer;

public:
	int	m_nDialogWidth;
	int	m_nDialogHeight;

	double		m_dAxisX_Pos[X_AXIS_COUNT];
	double		m_dAxisY_Pos[Y_AXIS_COUNT];

	double		m_dAxisX_Signal[X_AXIS_COUNT];
	double		m_dAxisY_Signal[Y_AXIS_COUNT];

	Mat		m_ImageMatSRC;
	Mat		m_ImageTempMatSRC;
	Mat		m_ImageMatResize;
	CRect	m_rcView;
	BOOL	m_bImageLoad;
	CImage	m_Image;

	Mat*	m_dMat;

	int m_nImageFOV_X;
	int m_nImageFOV_Y;
	int m_nScanGrid_X;
	int m_nScanGrid_Y;
	int m_nInterpolationCount;
	int m_nAverageCount;
	int m_nSetCount;
	int m_nScanDirection;
	int m_nPacketCount;

	int m_nPacketMode;

	int m_nMPacketCount;
	int m_nAPacketCount;

	int m_nImageCountCCD;

	int m_nDummyX;
	int m_nDummyY;

	int m_nPixelPosX;
	int m_nPixelPosY;

	int m_nBeforeImageWidth;
	int m_nBeforeImageHeight;

	BOOL m_bStartScan;

	HBRUSH m_brushsMenubar;
	HBRUSH m_brushsStatic;
	HBRUSH m_brushsEdit;
	HBRUSH m_brushsEqpStatusOn;
	HBRUSH m_brushsEqpStatusOff;
	HBRUSH m_brushsSocketConnectOn;
	HBRUSH m_brushsSocketConnectOff;

	int m_nSavedItem, m_nSavedSubitem;

	CString m_strSignalFolderPath;

	CListCtrl m_ctlImageRecipe;

	CStatic m_ctlProjectName;

	CEdit m_ctlEditImageRecipe;

	CRect	m_rcEqpStatus;
	CRect	m_rcSocketConnect;
	CRect	m_rcManualMode;

	BOOL m_bEqpStatus;
	BOOL m_bSocketConnect;
	BOOL m_bManualModeStatus;

	LARGE_INTEGER m_lFrequency;
	LARGE_INTEGER m_lBeginTime;
	LARGE_INTEGER m_lEndtime;

	BOOL m_bTimeUpdate;

	CFont m_fStaticBold;
	CFont m_fManualBold;

	CChartViewer m_chartPosView;
	CChartViewer m_chartSignalView;

	CWinThread*				m_pThread;
	HANDLE					m_hEvent;
	BOOL					m_bThreadStatus;
	CRITICAL_SECTION		m_pThreadCritical;
	BOOL ReCreateThread();
	BOOL Run(int nSequence, BOOL bMessage);
	static UINT Thread(LPVOID lpParam);
	BOOL StartThread();
	BOOL StopThread();
	void SetThreadStatus(BOOL bStatus) { m_bThreadStatus = bStatus; }
	BOOL GetThreadStatus()			   { return m_bThreadStatus;	}

public:
	afx_msg LRESULT OnMessageServerClientAccept(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageServerClientReceive(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageServerClientSocketClose(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageScanRecipe(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageSocketPing(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageRun(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageStop(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAPacketeRun(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageAverageCount(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageEm1024Reset(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageMPacketRun(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageStartChartPosView(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageStartChartSignalView(WPARAM wParam, LPARAM lParam);

public:
	BOOL OnMessageManualMode(BOOL bFlag);

public:
	void m_fnCreateServer();
	void m_fnRefreshViewRect();
	void m_fnDrawAllItems(CDC*& pMemDC, CBitmap* memBitmap);
	void m_fnDisplayResize();
	void m_fnMatToCImage(Mat& mat, CImage& cimage);
	CString m_fnGetCurrentTimeFunction();
	CString m_fnFormattedString(LPCTSTR lpszFormat, ...);
	void m_fnImageInfoList();
	void m_fnRefreshSocketRect();
	void m_fnRefreshEqpStatusRect();
	void m_fnRefreshManualRect();
	void m_fnScanRecipeUpdate();
	void m_fnReceiveTackTimeUpdate();
	void m_fnImageLoadInit();
	void m_fnBufferSizeChange();
	void m_fnImageResize(int nWidth, int nHeight);
	BOOL m_fnRunImagePacketDataSend();
	BOOL m_fnMImagePacketDataSend();
	BOOL m_fnAImagePacketDataSend();
	int m_fnPixelWidth();
	int m_fnPixelHeight();
	void m_fnChartPosVIew(double* dAxisX, double* dAxisY, int nSize);
	void m_fnChartSignalVIew(double* dAxisX, double* dAxisY, int nSize);
	void m_fnImageSizeChangeAndInit();
	int m_fnRandomData(int nLimit);
	double m_fnRandomDataPos(BOOL bNoSignal);
	void m_fnStartSiganlDataSignal(__int64* nPacketCount, BOOL bStartScan);
	void m_fnSetChartAxis_Y(double* dAxis, int nSize);
	void m_fnSetManualMode(BOOL bFlag);
	BOOL m_fnStartLineData(unsigned int nLine, CString strLineData, unsigned int* pPosX, unsigned int* pPosY);
	CString m_fnStringDataParsing(CString strLineData, unsigned int nIndex, TCHAR cCSV);
	void m_fnCreateImageInit();
	void m_fnMenuBarInit();
	void m_fnControlInit();
	void m_fnCreateLogViewerInit();
	void m_fnCreateBrushInit();
	void m_fnDrawDesignButtonStay(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font, BOOL bFlag);
	void m_fnDrawDesignButtonSelect(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font);

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~CSimulatorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnDestroy();
protected:
public:
	
	afx_msg void OnBnClickedButtonServerCreate();
	afx_msg void OnBnClickedButtonImageLoad();
	afx_msg void OnBnClickedButtonSendData();
	afx_msg void OnPaint();
	afx_msg void OnBnClickedButtonImageThreadStart();
	afx_msg void OnBnClickedButtonImageThreadStop();
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	afx_msg void OnNMDblclkListReciepe(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMClickListReciepe(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMCustomdrawListReciepe(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedButtonImageLoadSet();
	afx_msg void OnBnClickedButtonStartScan();
	afx_msg void OnBnClickedButtonScanReset();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnBnClickedButtonSignalLoadSet();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
};

#ifndef _DEBUG  // SeventhExProject_AView.cpp의 디버그 버전
inline CSimulatorDoc* CSimulatorView::GetDocument() const
   { return reinterpret_cast<CSimulatorDoc*>(m_pDocument); }
#endif

