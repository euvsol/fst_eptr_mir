
// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "Simulator.h"
#include "SimulatorDoc.h"
#include "SimulatorView.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
{
	// TODO: 여기에 멤버 초기화 코드를 추가합니다.
	m_strMessage = _T("");
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	//if (!m_wndStatusBar.Create(this))
	//{
	//	TRACE0("상태 표시줄을 만들지 못했습니다.\n");
	//	return -1;      // 만들지 못했습니다.
	//}
	//m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	//int nWindowWidth	= 1200;
	//int nWindowHeight	= 900;

	//CPoint pos;

	//pos.x = GetSystemMetrics(SM_CXSCREEN) / 2.0f - nWindowWidth / 2.0f;
	//pos.y = GetSystemMetrics(SM_CYSCREEN) / 2.0f - nWindowHeight / 2.0f;

	//// -------- Set Frame size & Remove Menu -------- //
	cs.x		= 0;										// 메인프레임 첫 시작 x축 위치
	cs.y		= 0;										// 메인프레임 첫 시작 y축 위치
	//cs.cx		= nWindowWidth;									// 자신이 원하는 메인 프레임의 x축 크기
	//cs.cy		= nWindowHeight;								// 자신이 원하는 메인 프레임의 y축 크기
	cs.hMenu	= NULL;											// 메뉴 제거
	cs.lpszName = _T("Simulator");								// 메인프레임 제목명

	cs.style = WS_OVERLAPPED | WS_CAPTION | /*FWS_ADDTOTITLE |*/ WS_THICKFRAME | WS_MINIMIZEBOX | WS_SYSMENU;

	return TRUE;
}

// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	switch (pCopyDataStruct->dwData) 
	{
		case WM_USER_MESSAGE_PROTOCOL_IPC_IP_TO_SM_MANUAL_MODE : { OnMessageManualMode(pCopyDataStruct); break; }
	}

	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

BOOL CMainFrame::OnMessageManualMode(COPYDATASTRUCT* pCopyDataStruct)
{
	CMainFrame*			pFrame	= (CMainFrame*)AfxGetMainWnd();
	CSimulatorView*		pView	= (CSimulatorView*)pFrame->GetActiveView();

	BOOL bFlag = FALSE;

	memcpy(&bFlag, pCopyDataStruct->lpData, sizeof(bFlag));

	pView->OnMessageManualMode(bFlag);

	return TRUE;
}