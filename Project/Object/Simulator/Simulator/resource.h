//{{NO_DEPENDENCIES}}
// Microsoft Visual C++에서 생성한 포함 파일입니다.
// Simulator.rc에서 사용되고 있습니다.
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_SIMULATOR_FORM              101
#define IDP_SOCKETS_INIT_FAILED         104
#define IDR_MAINFRAME                   128
#define IDR_SeventhExProjecTYPE         130
#define IDB_BITMAP_PROJECT_NAME         311
#define IDC_LIST1                       1000
#define IDC_LIST2                       1001
#define IDC_LIST_RECIEPE                1001
#define IDC_EDIT_INPUT                  1002
#define IDC_LIST_RECIEPE2               1002
#define IDC_LIST_SOCKET_CONNECT         1002
#define IDC_BUTTON_SEND                 1003
#define IDC_BUTTON_SERVER_CREATE        1004
#define IDC_BUTTON_IMAGE_LOAD           1005
#define IDC_BUTTON_IMAGE_NOISE          1006
#define IDC_BUTTON_SEND_DATA            1007
#define IDC_IMAGE_DISPLAY               1008
#define IDC_BUTTON_Y_AXIS_SET           1008
#define IDC_BUTTON_IMAGE_THREAD_START   1009
#define IDC_BUTTON_IMAGE_THREAD_STOP    1010
#define IDC_EDIT_IMAGE_RECIPE           1011
#define IDC_STATIC_PACKET_SEND_TIME     1012
#define IDC_STATIC_PACKET_SEND_TIME2    1013
#define IDC_STATIC_BYTE_BPS_TIME        1013
#define IDC_EDIT_IMAGE_NAME             1014
#define IDC_EDIT_IMAGE_FOLDER_PATH      1015
#define IDC_BUTTON1                     1016
#define IDC_BUTTON_IMAGE_LOAD_SET       1016
#define IDC_GRAPH_SIGNAL                1017
#define IDC_GRAPH_POS_VIEW              1017
#define IDC_BUTTON_X_AXIS_SET           1018
#define IDC_BUTTON_START_SCAN           1018
#define IDC_BUTTON_Y_AXIS_SET2          1019
#define IDC_BUTTON_AXIS_RESET           1019
#define IDC_BUTTON_SCAN_RESET           1019
#define IDC_GRAPH_SIGNAL_VIEW           1020
#define IDC_BUTTON_SCAN_TEST            1021
#define IDC_STATIC_PROGRAM_NAME         1021
#define IDC_BUTTON_TEST_MANUAL_MODE     1022
#define IDC_STATIC_PROGRAM_PROJECT      1023
#define IDC_STATIC_PROGRAM_VERSION      1024
#define IDC_STATIC_SOCKET_CONNECT       1025
#define IDC_STATIC_EQP_STATUS           1026
#define IDC_EDIT_SIGNAL_NAME            1027
#define IDC_EDIT_SIGNAL_FOLDER_PATH     1028
#define IDC_BUTTON_SIGNAL_LOAD_SET      1029

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        312
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1022
#define _APS_NEXT_SYMED_VALUE           310
#endif
#endif
