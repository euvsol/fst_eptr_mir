
// MainFrm.h : CMainFrame 클래스의 인터페이스
//

#pragma once

#include "SimulatorDoc.h"
#include "SimulatorView.h"

#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_ProtocolMessageIPC.h"

class CMainFrame : public CFrameWnd
{
	
protected: // serialization에서만 만들어집니다.
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 특성입니다.
public:

	//CSimulatorView* pSeventhExProject_AView;

// 작업입니다.
public:
	CString m_strMessage;

public:
	BOOL OnMessageManualMode(COPYDATASTRUCT* pCopyDataStruct);

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 구현입니다.
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 컨트롤 모음이 포함된 멤버입니다.
	CStatusBar        m_wndStatusBar;

// 생성된 메시지 맵 함수
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
};


