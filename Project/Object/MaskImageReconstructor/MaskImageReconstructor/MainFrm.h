
// MainFrm.h : CMainFrame 클래스의 인터페이스
//

#pragma once

#include "MaskImageReconstructorDoc.h"
#include "MaskImageReconstructorView.h"
#include "DlgVersion.h"

#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_ProtocolMessageIPC.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_TRAY_NOTIFICATION WM_USER + 1

const UINT WM_TASKBARCREATED = ::RegisterWindowMessage(_T("TaskbarCreated"));

class CMainFrame : public CFrameWnd
{
	
protected: // serialization에서만 만들어집니다.
	CMainFrame();
	DECLARE_DYNCREATE(CMainFrame)

// 특성입니다.
public:

	//MaskImageReconstructorView* pSeventhExProject_BView;

// 작업입니다.
public:
	BOOL m_bOnlyOnce;
	BOOL m_bTrayIcon;

	CDlgVersion* m_dDlgVersion;

public:
	BOOL OnMessageAdamRecipe(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamRun(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamStop(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamAverageCount(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamAPacketRun(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamMPacketRun(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageAdamEm1024Reset(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureBG(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureWO_P(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureBS(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureP_As_Point(COPYDATASTRUCT* pCopyDataStruct);
	BOOL OnMessageMeasureP_As_Scan(COPYDATASTRUCT* pCopyDataStruct);

public:
	void m_fnAddTaskbarIcon();

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);

// 구현입니다.
public:
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:  // 컨트롤 모음이 포함된 멤버입니다.
	CStatusBar        m_wndStatusBar;

// 생성된 메시지 맵 함수
protected:
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	DECLARE_MESSAGE_MAP()
public:
	afx_msg LRESULT OnTrayNotification(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnTrayRegisterShow(WPARAM wParam, LPARAM lParam);
	afx_msg BOOL OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct);
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnTrayEnd();
	afx_msg void OnTrayHide();
	afx_msg void OnTrayShow();
	afx_msg void OnDestroy();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
};


