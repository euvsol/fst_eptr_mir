#pragma once

#include <queue>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


class CQueueMainThread
{
public:

	CQueueMainThread(void* pView, void* pClientSocket, void* pQueueThread, int nQueueIndex, int nQueueMainIndex);
	~CQueueMainThread();

public:
	void* m_pView;
	void* m_pClientSocket;
	void* m_pQueueThread;

	int m_nQueueIndex;
	int m_nQueueMainIndex;

	int m_nImageWidth;
	int m_nImageHeight;
	int m_nImageFOV_X;
	int m_nImageFOV_Y;
	int m_nScanGridX;
	int m_nScanGridY;

	BYTE* m_pRawByteD1;
	BYTE* m_pRawByteD2;
	BYTE* m_pRawByteD3;
	BYTE* m_pRawByteT;
	BYTE* m_pRawByteR;
	BYTE* m_pRawActualScanBytePosX;
	BYTE* m_pRawActualScanBytePosY;
	BYTE* m_pRawStaticScanBytePosX;
	BYTE* m_pRawStaticScanBytePosY;

	BOOL m_bComplete;

	CWinThread*				m_pThread;
	HANDLE					m_hEvent;
	BOOL					m_bThreadStatus;
	CRITICAL_SECTION		m_pThreadCritical;
	BOOL ReCreateThread();
	BOOL DeleteThread();
	BOOL InitDynamicVariable();
	BOOL Run(int nSequence, BOOL bMessage);
	static UINT Thread(LPVOID lpParam);
	BOOL StartThread();
	BOOL StopThread();
	void SetThreadStatus(BOOL bStatus) { m_bThreadStatus = bStatus; }
	BOOL GetThreadStatus()			   { return m_bThreadStatus;	}

public:
	void SetRawImageByte(BYTE* pRawByteD1, BYTE* pRawByteD2, BYTE* pRawByteD3, BYTE* pRawByteT, BYTE* pRawByteR, BYTE* pRawActualScanBytenPosX, BYTE* pRawActualScanBytenPosY, BYTE* pRawStaticScanBytenPosX, BYTE* pRawStaticScanBytenPosY, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY);
	CString m_fnGetCurrentTime();
};

