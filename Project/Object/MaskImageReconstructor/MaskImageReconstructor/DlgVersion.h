#pragma once

// CImageTab1 대화 상자입니다.

class CDlgVersion : public CDialogEx
{
	DECLARE_DYNAMIC(CDlgVersion)

public:
	CDlgVersion(void* pFramework, void* pParents, CWnd* pParent = NULL);   // 표준 생성자입니다.
	virtual ~CDlgVersion();

// 대화 상자 데이터입니다.
	enum { IDD = IDD_DLG_VERSION };

public:
	void* m_pFramework;
	void* m_pParents;

	CEdit m_ctlTextBox;

	HBRUSH m_brushsPaint;

	CString m_strVersionPath;
	CString m_strVersionTextPath;

public:
	void m_fnControlInit();
	void m_fnDrawDesignButtonSelect(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.

	DECLARE_MESSAGE_MAP()
public:
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedButtonModify();
	afx_msg void OnBnClickedButtonSave();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnBnClickedButtonClose();
};
