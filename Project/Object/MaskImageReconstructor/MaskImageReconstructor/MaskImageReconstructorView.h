
// SeventhExProject_BView.h : MaskImageReconstructorView 클래스의 인터페이스
//

#pragma once

#include "resource.h"
#include "afxwin.h"

#include "QueueThread.h"

#include <numeric>
#include <vector>
#include "float.h"

#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementMacro.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementDefine.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementStruct.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_DataManagementEnum.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_UserMessage.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_ProtocolMessageIPC.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_ProtocolStructIPC.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolDefine.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolMessage.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolStructByte.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolStructData.h"
#include "../../../Module/ImageProcessAlgorithm/ImageProcessAlgorithm/ImageProcessAlgorithmMain.h"
#include "../../../Module/LogViewer/LogViewer/LogViewerMain.h"
#include "../../../Module/TcpClient/TcpClient/TcpClientMain.h"
#include "../../../Module/SharedMemory/SharedMemory/SharedMemoryMain.h"
#include "../../../Module/ImageProcessPython/ImageProcessPython/ImageProcessPythonMain.h"
#include "../../../Module/TcpClient/TcpClient/TcpClientSocket.h"

#include "../../../../CommonModuleMIR/PTR/Module/TcpComClient/TcpComClient/Com_SocketProtocolEnum.h"
#include "../../../../CommonModuleMIR/PTR/Module/TcpComClient/TcpComClient/Com_SocketProtocolMessage.h"
#include "../../../../CommonModuleMIR/PTR/Module/TcpComClient/TcpComClient/Com_SocketProtocolStructData.h"
#include "../../../../CommonModuleMIR/PTR/Module/TcpComClient/TcpComClient/TcpComClientMain.h"
#include "../../../../CommonModuleMIR/PTR/Module/TcpComClient/TcpComClient/TcpComClientSocket.h"

#include <tuple>
#include <ppl.h>
#include <ppltasks.h>
#include <opencv2/opencv.hpp>

using namespace Concurrency;
using namespace std;
using namespace cv;

class MaskImageReconstructorView : public CFormView
{
protected: // serialization에서만 만들어집니다.
	MaskImageReconstructorView();
	DECLARE_DYNCREATE(MaskImageReconstructorView)

public:
	enum{ IDD = IDD_MASKIMAGERECONSTRUCTOR_FORM };

// 특성입니다.
public:
	MaskImageReconstructorDoc* GetDocument() const;

public:
	HMODULE					m_hImageProcessAlgorithm;
	CImageProcessAlgorithm*	m_pImageProcessAlgorithm;

	HMODULE			m_hLogViewer;
	CLogViewer*		m_pLogViewer;

	HMODULE			m_hSharedMemory;
	CSharedMemory*	m_pSharedMemory;

	HMODULE			m_hTcpClient;
	CTcpClient*		m_pTcpClient;

	HMODULE			m_hTcpComClient;
	CTcpComClient*	m_pTcpComClient;

public:
	void* m_pView;
	void SetViewInstance(void* pView) { m_pView = pView; }
	void* GetViewInstance() { return m_pView; }

// 작업입니다.
public:
	int	m_nDialogWidth;
	int	m_nDialogHeight;

	BYTE* m_byRing_Run;
	BYTE* m_byRing_APacket;
	BYTE* m_byRing_MPacket;

	HANDLE			m_hScanPositionXMapping;
	BYTE*			m_pScanPositionXAddress;

	HANDLE			m_hScanPositionYMapping;
	BYTE*			m_pScanPositionYAddress;

	HANDLE			m_hTransmittanceMapping;
	BYTE*			m_pTransmittanceAddress;

	HANDLE			m_hReflectanceMapping;
	BYTE*			m_pReflectanceAddress;

	HANDLE			m_hRawMapping;
	BYTE*			m_pRawAddress;

	HANDLE			m_hRawInterpolationMapping;
	BYTE*			m_pRawInterpolationAddress;

	HANDLE			m_hInterpolationMapping;
	BYTE*			m_pInterpolationAddress;

	HANDLE			m_hAverageMapping;
	BYTE*			m_pAverageAddress;

	CQueueThread* m_pQueueThread[MULTI_CORE_QUEUE_COUNT];

	double** m_dScanPositionSum_X;
	double** m_dScanPositionSum_Y;
	double** m_dDetectorSum_1;
	double** m_dDetectorSum_2;
	double** m_dDetectorSum_3;

	unsigned int** m_nMatrixCount;

	BOOL** m_bMatrixCheck;

	Mat*	m_dMat_D1;
	Mat*	m_dMat_D2;
	Mat*	m_dMat_D3;
	Mat*	m_dMatBuffer_D1;
	Mat*	m_dMatBuffer_D2;
	Mat*	m_dMatBuffer_D3;

	Mat*	m_dMat_T;
	Mat*	m_dMatBuffer_T;
	Mat*	m_dMatInterpolation_T;
	Mat*	m_dMatAverage_T;

	Mat*	m_dMat_R;
	Mat*	m_dMatBuffer_R;
	Mat*	m_dMatInterpolation_R;
	Mat*	m_dMatAverage_R;

	Mat*	m_MatRawSRC;
	Mat*	m_MatRawBufferSRC;
	Mat*	m_MatInterpolationSRC;
	Mat*	m_MatAverageSRC;

	Mat*	m_dMatActualScanPos_X;
	Mat*	m_dMatActualScanPos_Y;

	Mat*	m_dMatStaticScanPos_X;
	Mat*	m_dMatStaticScanPos_Y;

	Mat*	m_dMatBufferActualScanPos_X;
	Mat*	m_dMatBufferActualScanPos_Y;

	Mat*	m_dMatBufferStaticScanPos_X;
	Mat*	m_dMatBufferStaticScanPos_Y;

	BYTE*	m_bySharedMemoryRaw;
	BYTE*	m_bySharedMemoryRawInterpolation;
	BYTE*	m_bySharedMemoryInterpolation;
	BYTE*	m_bySharedMemoryAverage;

	BYTE*	m_bySharedMemoryT;
	BYTE*	m_bySharedMemoryR;
	BYTE*	m_bySharedMemoryPosX;
	BYTE*	m_bySharedMemoryPosY;

	Mat*	m_MatRawInterpolationSRC;
	double* m_RawInterpolationPos_X;
	double* m_RawInterpolationPos_Y;
	double* m_RawInterpolationOutput;

	int m_nMatRawInterpolationControlX;
	int m_nMatRawInterpolationControlY;
	int m_nMatRawInterpolationResizeX;
	int m_nMatRawInterpolationResizeY;
	BITMAPINFO* m_bitmapInfoMatRawInterpolation;

	int m_nMatRawControlX;
	int m_nMatRawControlY;
	int m_nMatRawResizeX;
	int m_nMatRawResizeY;
	BITMAPINFO* m_bitmapInfoMatRaw;

	int m_nMatInterpolationControlX;
	int m_nMatInterpolationControlY;
	int m_nMatInterpolationResizeX;
	int m_nMatInterpolationResizeY;
	BITMAPINFO* m_bitmapInfoMatInterpolation;

	int m_nMatAverageControlX;
	int m_nMatAverageControlY;
	int m_nMatAverageResizeX;
	int m_nMatAverageResizeY;
	BITMAPINFO* m_bitmapInfoMatAverage;

	int m_nImageFOV_X;
	int m_nImageFOV_Y;
	int m_nScanGrid_X;
	int m_nScanGrid_Y;
	int m_nInterpolationCount;
	int m_nAverageCount;
	int m_nSetCount;
	int m_nScanDirection;
	int m_nPacketCount;

	int m_nBeforeImageWidth;
	int m_nBeforeImageHeight;

	int m_nPacketMode;

	int m_nMPacketCount;
	int m_nAPacketCount;

	int m_nPacketCount_BG;
	int m_nPacketCount_WO_P;
	int m_nPacketCount_BS;
	int m_nPacketCount_P_AS_POINT;

	int m_nPixelCountCheck;
	int m_nRawCountCheck;
	int m_nRawInterpolationCountCheck;
	int m_nInterpolationCountCheck;
	int m_nAverageCountCheck;
	int m_nScanSetCountCheck;
	BOOL m_bLastLineCheck;
	BOOL m_bLastScanCheck;

	BOOL m_bTurningPoint;
	int m_nTurningIndex;

	BOOL m_bLastScanSyncCheck;

	int m_nQueueRingCount;

	__int64 m_nPacketNumberCheck;

	int m_nPixelPosX;
	int m_nPixelPosY;

	unsigned int m_nDetectorSize;
	unsigned int m_nDetectorIndex;

	double* m_dScanPositionX;
	double* m_dScanPositionY;

	double* m_dDetector1;
	double* m_dDetector2;
	double* m_dDetector3;

	queue<PACKET_M_IMAGE_DATA_CONTENT*> m_qPacket_M;
	queue<PACKET_A_IMAGE_DATA_CONTENT*> m_qPacket_A;
	queue<PACKET_M_IMAGE_DATA_CONTENT*> m_qPacket_BS;
	queue<PACKET_M_IMAGE_DATA_CONTENT*> m_qPacket_P_As_Point;

	double m_dScanAreaThresholdX;
	double m_dScanAreaThresholdY;

	int m_nSavedItem, m_nSavedSubitem;

	HBRUSH m_brushsMenubar;
	HBRUSH m_brushsStatic;
	HBRUSH m_brushsEdit;
	HBRUSH m_brushsEqpStatusOn;
	HBRUSH m_brushsEqpStatusOff;
	HBRUSH m_brushsSocketConnectOn;
	HBRUSH m_brushsSocketConnectOff;

	CListCtrl m_ctlImageRecipe;

	CEdit m_ctlEditImageRecipe;

	CRect	m_rcSocketConnect;
	CRect	m_rcEqpStatus;
	CRect	m_rcManualMode;
	CRect	m_rcDisplayMode;

	BOOL m_bTimeUpdate;

	BOOL m_bEqpStatus;
	BOOL m_bManualModeStatus;
	BOOL m_bDisplayModeStatus;

	CFont m_fStaticBold;
	CFont m_fManualBold;

	CStatic m_ctlProjectName;

	BOOL m_bEquipmentStatus;

	HANDLE	m_hAliveMapping;
	BYTE*	m_pAliveAddress;

	BOOL	m_bAliveFlag;
	BOOL	m_bAliveThread;

	BOOL m_bAutoSocketConnectThread;
	BOOL m_bAutoComSocketConnectThread;

	LARGE_INTEGER m_lFrequency_Raw;
	LARGE_INTEGER m_lBeginTime_Raw;
	LARGE_INTEGER m_lEndtime_Raw;

	LARGE_INTEGER m_lFrequency_Interpolation;
	LARGE_INTEGER m_lBeginTime_Interpolation;
	LARGE_INTEGER m_lEndtime_Interpolation;

	LARGE_INTEGER m_lFrequency_Average;
	LARGE_INTEGER m_lBeginTime_Average;
	LARGE_INTEGER m_lEndtime_Average;

	LARGE_INTEGER			m_lFrequency;
	LARGE_INTEGER			m_lBeginTime;
	LARGE_INTEGER			m_lEndtime;
	BOOL					m_bRunPacketTimeOutCheck;
	BOOL					m_bRunPacketTimeOutCheckToggle;

	CWinThread*				m_pThread;
	HANDLE					m_hEvent;
	BOOL					m_bThreadStatus;
	CRITICAL_SECTION		m_pThreadCritical;
	void ReCreateThread();
	BOOL DeleteThread();
	BOOL InitDynamicVariable();
	BOOL Run(int nSequence, BOOL bMessage);
	static UINT Thread(LPVOID lpParam);
	BOOL StartThread();
	BOOL StopThread();
	void SetThreadStatus(BOOL bStatus) { m_bThreadStatus = bStatus; }
	BOOL GetThreadStatus()			   { return m_bThreadStatus;	}

	CWinThread*				m_pImageThread;
	HANDLE					m_hImageEvent;
	BOOL					m_bImageThreadStatus;
	CRITICAL_SECTION		m_pImageThreadCritical;
	void ReCreateImageThread();
	BOOL DeleteImageThread();
	BOOL InitDynamicImageVariable();
	BOOL RunImage(int nSequence, BOOL bMessage);
	static UINT ImageThread(LPVOID lpParam);
	BOOL StartImageThread();
	BOOL StopImageThread();
	void SetImageThreadStatus(BOOL bStatus) { m_bImageThreadStatus = bStatus; }
	BOOL GetImageThreadStatus()				{ return m_bImageThreadStatus;	  }

	static UINT AlgorithmThread(LPVOID lpParam);
	static UINT LineInterpolationThread(LPVOID lpParam);

public:
	BOOL OnComAdamRecipe(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComAdamRun(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComAdamStop(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComAdamAverageCount(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComAdamAPacketRun(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComAdamMPacketRun(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComAdamReadDetector(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComAdamEm1024Reset(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComMeasureBG(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComMeasureWO_P(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComMeasureBS(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComMeasureP_As_Point(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComMeasureP_As_Scan(ESOL_PACKET_DATA* pPacket, void* pData);
	BOOL OnComGetPacketReadyCheck(ESOL_PACKET_DATA* pPacket, void* pData);

public:
	BOOL OnComMemoryInitUpdate();
	BOOL OnComMemoryRawUpdate();
	BOOL OnComMemoryRawInterpolationUpdate();
	BOOL OnComMemoryInterpolationUpdate();
	BOOL OnComMemoryAverageUpdate();
	BOOL OnComMemoryTransmittanceUpdate(BOOL* bFirst = nullptr);
	BOOL OnComMemoryRunPacketTimeOut();
	BOOL OnComMeasureP_As_Scan_Stop();

public:
	void SetAutoSocketConnectThread(BOOL bFlag) { m_bAutoSocketConnectThread = bFlag; }
	BOOL GetAutoSocketConnectThread() { return m_bAutoSocketConnectThread; }

	void SetAutoComSocketConnectThread(BOOL bFlag) { m_bAutoComSocketConnectThread = bFlag; }
	BOOL GetAutoComSocketConnectThread() { return m_bAutoComSocketConnectThread; }

	void SetAliveThread(BOOL bFlag) { m_bAliveThread = bFlag; }
	BOOL GetAliveThread() { return m_bAliveThread; }

	void SetEquipmentStatus(BOOL bFlag) { m_bEqpStatus = bFlag; }
	BOOL GetEquipmentStatus() { return m_bEqpStatus; }

public:
	BOOL Protocol(ESOL_PACKET_DATA* pPacket);
	BOOL ProtocolCheck(unsigned char cStx, unsigned char cSender, unsigned char cReceiver, unsigned char cType, char* cSequenceCode, char* cFunctionCode, unsigned char cEtx, ESOL_PACKET_DATA* pPacket, unsigned int nLength, BOOL(MaskImageReconstructorView::*fp)(ESOL_PACKET_DATA*, void*));
	void ThreadSendPacketData(unsigned int nClient, ESOL_PACKET_DATA* pPacketData);
	void SendProtocolLog(ESOL_PACKET_DATA* pData);
	void ReceiveProtocolLog(ESOL_PACKET_DATA* pData, BOOL bFlag);

public:
	afx_msg LRESULT OnMessageComServerClientAccept(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageComServerClientReceive(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageComServerClientSocketClose(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageComServerClientProtocol(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageComServerClientThreadSend(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageServerClientAccept(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageServerClientReceive(WPARAM wParam, LPARAM lParam);
	afx_msg LRESULT OnMessageServerClientSocketClose(WPARAM wParam, LPARAM lParam);

public:
	void m_fnOnMessageDisplayInitUpdate();
	void m_fnOnMessageRawDisplayInitUpdate(ThreadParam* pData);
	void m_fnOnMessageRawDisplayUpdate(ThreadParam* pData);
	void m_fnOnMessageRawInterpolationDisplayUpdate(ThreadParam* pData);
	void m_fnOnMessageInterpolationDisplayUpdate(ThreadParam* pData);
	void m_fnOnMessageAverageDisplayUpdate(ThreadParam* pData);
	void m_fnOnMessageSetDisplayUpdate(ThreadParam* pData);
	void m_fnOnMessageRawInterpolationDisplayInitUpdate();
	void m_fnOnMessageStartAdamRunData();
	void m_fnOnMessageStartAPacketRunData();
	void m_fnOnMessageStartMPacketRunData();
	void m_fnOnMessageStartMaskImageReconstructoring();
	void m_fnOnMessageMeasureSequenceA();
	void m_fnOnMessageMeasureSequenceM();
	void m_fnOnMessageMeasureSequenceReadDetector();
	void m_fnOnMessageMeasureSequenceBG();
	void m_fnOnMessageMeasureSequenceWO_P();
	void m_fnOnMessageMeasureSequenceBS();
	void m_fnOnMessageMeasureSequenceP_As_Point();
	void m_fnOnMessageStartCreateQueueThread();
	void m_fnOnMessageStartCreateLineThread();

public:
	void m_fnServerConnect();
	void m_fnComServerConnect();
	CString m_fnGetCurrentTimeFunction();
	void m_fnImageData_RUN(PACKET_M_IMAGE_DATA* pQueueBufferCopy);
	void m_fnImageData_M(PACKET_M_IMAGE_DATA* pQueueBufferCopy);
	void m_fnImageData_A(PACKET_A_IMAGE_DATA* pQueueBufferCopy);
	void m_fnImageData_BS(PACKET_M_IMAGE_DATA* pQueueBufferCopy);
	void m_fnImageData_P_As_Point(PACKET_M_IMAGE_DATA* pQueueBufferCopy);
	void m_fnImageInfoList();
	void m_fnRefreshSocketRect();
	void m_fnRefreshEqpStatusRect();
	void m_fnRefreshManualRect();
	void m_fnRefreshDisplayModeRect();
	void m_fnImageCountDataUpdate();
	void m_fnOnCreateQueueThread();
	void m_fnDisplayImage(Mat* pMatImage, unsigned int nControlView, int* nPictureControlX, int* nPictureControlY, int* nResizeX, int* nResizeY, BITMAPINFO*& bitmapInfo);
	void m_fnDisplayImageInfoUpdate(Mat* pMatImage, unsigned int nControlView, int* nPictureControlX, int* nPictureControlY, int* nResizeX, int* nResizeY, BITMAPINFO*& bitmapInfo);
	void m_fnRawTackTimeUpdate();
	void m_fnInterpolationTackTimeUpdate();
	void m_fnAverageTackTimeUpdate();
	void m_fnImageSizeChangeAndInit();
	void m_fnCreateImageInit();
	void m_fnBufferSizeChange();
	int  m_fnPixelWidth();
	int  m_fnPixelHeight();
	BOOL m_fnPacketThreadFInishCheck();
	BOOL m_fnImageThreadFInishCheck(ThreadParam* stData);
	BOOL m_fnImageDisplayRawInit(int* nRawCount, int* nScanCount);
	BOOL m_fnImageDisplayRawInterpolationInit(int* nRawCount, int* nScanCount);
	BOOL m_fnImageDisplayRawUpdate();
	BOOL m_fnImageDisplayRawInterpolationUpdate();
	BOOL m_fnImageDisplayInterpolationUpdate(int* nInterpolationCount);
	BOOL m_fnImageDisplayAverageUpdate(int* nAverageCount);
	BOOL m_fnImageDisplaySetUpdate(int* nScanSetCount);
	void m_fnScanRecipeUpdate();
	void m_fnFixPosPixelAverageAdd(double* dScanPositionX, double* dScanPositionY, double* dDetector1, double* dDetector2, double* dDetector3);
	void m_fnFixPosPixelAverageCal();
	int m_fnReceivePacketCountCheck();
	void m_fnReceivePacketTypeCheck();
	BOOL m_fnStartMaskImageReconstructoringCheck(CTcpClient* pClient);
	void m_fnMeasureSeqDataInit();
	int m_fnGetPacketTotalByteLengh();
	int m_fnGetPacketModeCountHi();
	int m_fnGetPacketModeCountLow();
	unsigned int m_fnPixelPosX();
	BOOL SetRunPacketTimeOutStatus(BOOL bFlag);
	BOOL GetRunPacketTimeOutStatus();
	BOOL SetRunPacketTimeOutBeginTime();
	double SetRunPacketTimeOutEndTime();
	void m_StartForwardScan(PACKET_M_IMAGE_DATA_CONTENT* pTemp, double* dScanGridX, double* dScanGridY, double* dOffset, BOOL* bManualContinue);
	void m_StartBackwardScan(PACKET_M_IMAGE_DATA_CONTENT* pTemp, double* dScanGridX, double* dScanGridY, double* dOffset, BOOL* bManualContinue);
	void m_fnWriteLog(CString strName, CString strTemp);
	CString m_fnGetCurruntDateFunction();
	void m_fnFolderCreate();
	BOOL m_fnScanLastMatrixPositionStatusCheck();
	BOOL m_fnScanLastThresholdPositionStatusCheck();
	void m_fnMenuBarInit();
	void m_fnDialogInit();
	void m_fnControlInit();
	CString m_fnGetCurrentTime();
	CString m_fnFormattedString(LPCTSTR lpszFormat, ...);
	CString m_fnFormattedStringTimeLog(LPCTSTR lpszFormat, ...);
	void m_fnWriteLogMerge(CString strFileName, CString strType, MeasureDataReturn* pData);
	void m_fnSharedMemoryAllInit();
	void m_fnSharedMemoryInit(HANDLE* pHandle, unsigned int nSize, CString strSpace);
	void m_fnMatrixCreate(int nImageWidth, int nImageHeight);
	void m_fnMatrixInit(int nImageWidth, int nImageHeight);
	void m_fnMatrixDelete(int nImageWidth, int nImageHeight);
	void m_fnMatrix_Position_Machting_Algorithm(PACKET_M_IMAGE_DATA_CONTENT* pTemp, double* dOffset);
	void m_fnPosition_Threshold_Algorithm(PACKET_M_IMAGE_DATA_CONTENT* pTemp, double* dOffset, BOOL* bManualContinue);
	void m_fnSetControllEnable(BOOL bFlag);
	void m_fnSequenceDataInit();
	void m_fnCreateLogViewerInit();
	void m_fnDrawDesignButtonStay(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF bk_1, COLORREF edge_1, COLORREF font_1, COLORREF bk_2, COLORREF edge_2, COLORREF font_2, unsigned int nSize, BOOL bFlag);
	void m_fnDrawDesignButtonSelect(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF bk_1, COLORREF edge_1, COLORREF font_1, COLORREF bk_2, COLORREF edge_2, COLORREF font_2, unsigned int nSize);
	void m_fnCreateBrushInit();
	BOOL m_fnControlIsWindowEnableCheck(int nIDCtl, int nIDCheckCtl, BOOL bFlag);
	void m_fnAutoSocketConnect();
	void m_fnAutoComSocketConnect();
	void m_fnAutoProgramAliveCheck();
	void m_fnAutoProgramAliveUpdate();
	void m_fnVersionInit();
	void m_fnStandbyModeMemoryOptimizationCheck();
	BOOL m_fnPacketReadyCheck();
	void m_fnStartConvertFile();

// 재정의입니다.
public:
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 지원입니다.
	virtual void OnInitialUpdate(); // 생성 후 처음 호출되었습니다.

// 구현입니다.
public:
	virtual ~MaskImageReconstructorView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// 생성된 메시지 맵 함수
protected:
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnNMDblclkListReciepe(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnNMClickListReciepe(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	afx_msg void OnNMCustomdrawListReciepe(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnClickedButtonProtocolAPacketRun();
	afx_msg void OnBnClickedButtonProtocolEm1024Reset();
	afx_msg void OnBnClickedButtonProtocolMPacketRun();
	afx_msg void OnBnClickedButtonProtocolAdamRun();
	afx_msg void OnBnClickedButtonProtocolAdamStop();
	afx_msg void OnBnClickedButtonProtocolAdamRecipe();
	afx_msg void OnBnClickedButtonPtrTest();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnBnClickedButtonTestManualMode();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnBnClickedButtonDisplayMode();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg BOOL OnEraseBkgnd(CDC* pDC);
	afx_msg void OnBnClickedButtonProgramVersion();
	afx_msg void OnBnClickedButtonTestReadWrite();
};

#ifndef _DEBUG  // SeventhExProject_BView.cpp의 디버그 버전
inline MaskImageReconstructorDoc* MaskImageReconstructorView::GetDocument() const
   { return reinterpret_cast<MaskImageReconstructorDoc*>(m_pDocument); }
#endif

