
// MainFrm.cpp : CMainFrame 클래스의 구현
//

#include "stdafx.h"
#include "MaskImageReconstructor.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// CMainFrame

IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	ON_WM_CREATE()
	ON_WM_COPYDATA()
	ON_WM_ACTIVATE()
	ON_MESSAGE(WM_TRAY_NOTIFICATION, OnTrayNotification)
	ON_REGISTERED_MESSAGE(WM_TASKBARCREATED, OnTrayRegisterShow)
	ON_COMMAND(ID_TRAY_END, &CMainFrame::OnTrayEnd)
	ON_COMMAND(ID_TRAY_HIDE, &CMainFrame::OnTrayHide)
	ON_COMMAND(ID_TRAY_SHOW, &CMainFrame::OnTrayShow)
	ON_WM_DESTROY()
	ON_WM_SYSCOMMAND()
END_MESSAGE_MAP()

static UINT indicators[] =
{
	ID_SEPARATOR,           // 상태 줄 표시기
	ID_INDICATOR_CAPS,
	ID_INDICATOR_NUM,
	ID_INDICATOR_SCRL,
};

// CMainFrame 생성/소멸

CMainFrame::CMainFrame()
{
	m_bOnlyOnce = FALSE;
	m_bTrayIcon = FALSE;

	m_dDlgVersion = NULL;
}

CMainFrame::~CMainFrame()
{
	if (m_dDlgVersion != nullptr) { delete m_dDlgVersion; m_dDlgVersion = nullptr; }
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

	//if (!m_wndStatusBar.Create(this))
	//{
	//	TRACE0("상태 표시줄을 만들지 못했습니다.\n");
	//	return -1;      // 만들지 못했습니다.
	//}
	//m_wndStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT));

	m_bOnlyOnce = TRUE;

	return 0;
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
	if( !CFrameWnd::PreCreateWindow(cs) )
		return FALSE;
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	//int nWindowWidth	= 1200;
	//int nWindowHeight	= 900;

	//CPoint pos;

	//pos.x = GetSystemMetrics(SM_CXSCREEN) / 2.0f - nWindowWidth / 2.0f;
	//pos.y = GetSystemMetrics(SM_CYSCREEN) / 2.0f - nWindowHeight / 2.0f;

	//// -------- Set Frame size & Remove Menu -------- //
	cs.x		= 0;												// 메인프레임 첫 시작 x축 위치
	cs.y		= 0;												// 메인프레임 첫 시작 y축 위치
	//cs.cx		= nWindowWidth;										// 자신이 원하는 메인 프레임의 x축 크기
	//cs.cy		= nWindowHeight;									// 자신이 원하는 메인 프레임의 y축 크기
	cs.hMenu	= NULL;												// 메뉴 제거
	cs.lpszName = _T("MaskImageReconstructor");						// 메인프레임 제목명

	cs.style = WS_OVERLAPPED | WS_CAPTION | /*FWS_ADDTOTITLE |*/ WS_THICKFRAME | WS_MINIMIZEBOX | WS_SYSMENU;

	cs.style &= ~WS_THICKFRAME;

	return TRUE;
}

// CMainFrame 진단

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
	CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
	CFrameWnd::Dump(dc);
}
#endif //_DEBUG


// CMainFrame 메시지 처리기


BOOL CMainFrame::OnCopyData(CWnd* pWnd, COPYDATASTRUCT* pCopyDataStruct)
{
	switch (pCopyDataStruct->dwData) 
	{
	}

	return CFrameWnd::OnCopyData(pWnd, pCopyDataStruct);
}

void CMainFrame::m_fnAddTaskbarIcon()
{
	NOTIFYICONDATA nid;
	ZeroMemory(&nid, sizeof(nid));
	nid.cbSize = sizeof(nid);
	nid.uID = IDR_MAINFRAME;
	nid.hWnd = GetSafeHwnd();
	nid.uFlags = NIF_ICON | NIF_TIP | NIF_MESSAGE;
	nid.hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	nid.uCallbackMessage = WM_TRAY_NOTIFICATION;

	char strTitle[256];
	GetWindowText(strTitle, sizeof(strTitle));
	lstrcpy(nid.szTip, strTitle);
	Shell_NotifyIcon(NIM_ADD, &nid);
	SendMessage(WM_SETICON, (WPARAM)TRUE, (LPARAM)nid.hIcon);
	m_bTrayIcon = TRUE;
}

LRESULT CMainFrame::OnTrayNotification(WPARAM wParam, LPARAM lParam)
{
	switch(lParam)
	{
		case WM_RBUTTONDOWN:
		{
			CPoint ptMouse;::GetCursorPos(&ptMouse);
			CMenu menu;
			menu.LoadMenu(IDR_TRAY_MENU);
			CMenu* pMenu = menu.GetSubMenu(0);
			SetForegroundWindow();
			pMenu->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON,ptMouse.x, ptMouse.y, AfxGetMainWnd());
			SetForegroundWindow();
			break;
		}
		case WM_LBUTTONDBLCLK:
		{
			if (IsIconic()) { ShowWindow(SW_RESTORE);  }
			else			{ ShowWindow(SW_MINIMIZE); }
			break;
		} 
	}
	return TRUE;
}

LRESULT CMainFrame::OnTrayRegisterShow(WPARAM wParam, LPARAM lParam)
{
	m_fnAddTaskbarIcon();
	return TRUE;
}

void CMainFrame::OnTrayShow()
{
	ShowWindow(SW_SHOW);
}

void CMainFrame::OnTrayHide()
{
	ShowWindow(SW_HIDE);
}

void CMainFrame::OnTrayEnd()
{
	NOTIFYICONDATA nid;
	ZeroMemory(&nid, sizeof(nid));
	nid.cbSize = sizeof(nid);
	nid.uID = IDR_MAINFRAME;
	nid.hWnd = GetSafeHwnd();
	Shell_NotifyIcon(NIM_DELETE, &nid);
	exit(0);
}

void CMainFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);

	if (m_bOnlyOnce)
	{
		m_fnAddTaskbarIcon();

		m_bOnlyOnce = FALSE;
	}
}


void CMainFrame::OnDestroy()
{
	CFrameWnd::OnDestroy();

	if (m_bTrayIcon)
	{
		NOTIFYICONDATA nid;
		ZeroMemory(&nid, sizeof(nid));
		nid.cbSize = sizeof(nid);
		nid.uID = IDR_MAINFRAME;
		nid.hWnd = GetSafeHwnd();
		Shell_NotifyIcon(NIM_DELETE, &nid);
	}
}


void CMainFrame::OnSysCommand(UINT nID, LPARAM lParam)
{
	switch (nID)
	{
		case SC_CLOSE		: 
		{ 
			if (MessageBox(
				_T("Do you really want to exit?"),
				_T("MessageBox"),
				MB_YESNO | MB_ICONWARNING) == IDNO)
			{ return; }
			break; 
		}
		case SC_MAXIMIZE	:	{ break; }
		case SC_MINIMIZE	:	{ break; }
		case SC_RESTORE		:	{ break; }
	}

	CFrameWnd::OnSysCommand(nID, lParam);
}
