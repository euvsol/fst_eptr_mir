// ImageTab1.cpp : 구현 파일입니다.
//

#include "stdafx.h"
#include "MaskImageReconstructor.h"
#include "DlgVersion.h"

#include "MainFrm.h"
#include "MaskImageReconstructorView.h"

// CImageTab1 대화 상자입니다.

IMPLEMENT_DYNAMIC(CDlgVersion, CDialogEx)

CDlgVersion::CDlgVersion(void* pFramework, void* pParents, CWnd* pParent /*=NULL*/)
	: CDialogEx(CDlgVersion::IDD, pParent)
{
	m_pFramework	= pFramework;
	m_pParents		= pParents;

	m_brushsPaint = CreateSolidBrush(RGB(53, 53, 53));

	TCHAR chFilePath[256] = { 0, };
	GetModuleFileName(NULL, chFilePath, 256);

	CString strFolderPath(chFilePath);
	CString strFolderTemp;

	strFolderPath = strFolderPath.Left(strFolderPath.ReverseFind('\\'));
	strFolderPath = strFolderPath.Left(strFolderPath.ReverseFind('\\'));
	strFolderTemp = strFolderPath.Left(strFolderPath.ReverseFind('\\'));
	strFolderTemp = strFolderTemp + _T("\\Project\\Reference\\Config\\");

	m_strVersionPath		= strFolderTemp;
	m_strVersionTextPath	= m_strVersionPath + _T("Version.txt");

	if (GetFileAttributes((LPCTSTR)strFolderTemp) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)strFolderTemp, NULL); }
}

CDlgVersion::~CDlgVersion()
{
	if (m_brushsPaint != NULL) { DeleteObject(m_brushsPaint); m_brushsPaint = NULL; }
}

void CDlgVersion::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EDIT_TEXT_BOX, m_ctlTextBox);
}


BEGIN_MESSAGE_MAP(CDlgVersion, CDialogEx)
	ON_WM_MEASUREITEM()
	ON_WM_ERASEBKGND()
	ON_WM_DRAWITEM()
	ON_WM_CTLCOLOR()
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_MODIFY, &CDlgVersion::OnBnClickedButtonModify)
	ON_BN_CLICKED(IDC_BUTTON_SAVE, &CDlgVersion::OnBnClickedButtonSave)
	ON_WM_DRAWITEM()
	ON_WM_SHOWWINDOW()
	ON_BN_CLICKED(IDC_BUTTON_CLOSE, &CDlgVersion::OnBnClickedButtonClose)
END_MESSAGE_MAP()


// CImageTab1 메시지 처리기입니다.


BOOL CDlgVersion::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	m_fnControlInit();

	return TRUE;  // return TRUE unless you set the focus to a control
	// 예외: OCX 속성 페이지는 FALSE를 반환해야 합니다.
}

BOOL CDlgVersion::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);
	pDC->FillSolidRect(rect, RGB(38, 38, 38));

	return TRUE;
	//return CDialogEx::OnEraseBkgnd(pDC);
}

HBRUSH CDlgVersion::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	UINT nID = pWnd->GetDlgCtrlID();

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (nID == IDC_STATIC)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(38, 38, 38));
			hbr = m_brushsPaint;
		}
	}
	else if (nCtlColor == CTLCOLOR_EDIT)
	{
		/*
		if (nID == IDC_EDIT_TEXT_BOX)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(38, 38, 38));
			hbr = m_brushsPaint;
		}
		*/
	}

	return hbr;
}


void CDlgVersion::OnBnClickedButtonModify()
{
	m_ctlTextBox.EnableWindow(TRUE);
}

void CDlgVersion::m_fnControlInit()
{
	m_ctlTextBox.EnableWindow(FALSE);
}

void CDlgVersion::OnBnClickedButtonSave()
{
	MaskImageReconstructorView*	pImageView = (MaskImageReconstructorView *)m_pParents;

	CString strVersion;
	CString strTemp;
	char sData[1024] = { 0, };
	CFile File(m_strVersionTextPath, CFile::modeWrite | CFile::modeCreate );
	for( int i=0; i < m_ctlTextBox.GetLineCount(); ++i)
	{
		m_ctlTextBox.GetLine(i, sData, 1024);
		if (i == 0) 
		{ 
			strVersion = (LPSTR)sData; 
			strVersion = strVersion.Right(strVersion.ReverseFind(':') + 3);
		}
		strTemp = (LPSTR)sData;
		strTemp += _T("\r\n");
		File.Write(strTemp, sizeof(TCHAR) * strTemp.GetLength());
		memset(sData, 0x00, sizeof(char) * 1024);
		strTemp.Empty();
	}
	File.Flush();
	File.Close();

	m_ctlTextBox.EnableWindow(FALSE);

	pImageView->GetDlgItem(IDC_BUTTON_PROGRAM_VERSION)->SetWindowText(strVersion);
}


void CDlgVersion::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if ((nIDCtl == IDC_BUTTON_MODIFY)	||
		(nIDCtl == IDC_BUTTON_SAVE)		||
		(nIDCtl == IDC_BUTTON_CLOSE))
	{
		m_fnDrawDesignButtonSelect(lpDrawItemStruct, RGB(65, 65, 65), RGB(38, 38, 38), RGB(255, 255, 255));
	}

	CDialogEx::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CDlgVersion::m_fnDrawDesignButtonSelect(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF color_bk, COLORREF color_edge, COLORREF color_font)
{
	CDC dc;
	RECT rect;
	dc.Attach(lpDrawItemStruct->hDC);
	rect = lpDrawItemStruct->rcItem;
	dc.SetBkMode(TRANSPARENT);
	dc.Draw3dRect(&rect, color_edge, color_edge);
	dc.FillSolidRect(&rect, color_bk);

	UINT state = lpDrawItemStruct->itemState;

//    if((state &ODS_SELECTED))	{ dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT); }
//    else						{ dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT); }

	if ((state & ODS_SELECTED))
	{
		dc.Draw3dRect(&rect, RGB(53, 53, 53), RGB(53, 53, 53));
		dc.FillSolidRect(&rect, RGB(103, 103, 103));
		dc.SetBkColor(RGB(103, 103, 103));
		dc.SetTextColor(color_font);
	}

	dc.SetBkColor(color_bk);
	dc.SetTextColor(color_font);
	TCHAR buffer[MAX_PATH];
	ZeroMemory(buffer, MAX_PATH);
	::GetWindowTextA(lpDrawItemStruct->hwndItem, buffer, MAX_PATH);
	dc.DrawText(buffer, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	dc.Detach();
}

void CDlgVersion::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	if (this->IsWindowVisible() != TRUE)
	{
		m_ctlTextBox.SetSel(0, -1, TRUE);
		m_ctlTextBox.Clear();

		CStdioFile f;
		if (f.Open(m_strVersionTextPath, CFile::modeRead | CFile::typeText))
		{
			CString strLine;

			while (f.ReadString(strLine))
			{
				strLine += _T("\r\n");
				m_ctlTextBox.SetSel(-2, -1);
				int len = m_ctlTextBox.GetWindowTextLength();
				m_ctlTextBox.SetSel(len, len);
				m_ctlTextBox.ReplaceSel(strLine);
			}
		}
		f.Close();
	}
}


BOOL CDlgVersion::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN && pMsg->wParam == VK_RETURN)
	{
		if (GetDlgItem(IDC_EDIT_TEXT_BOX) == GetFocus())
		{
			CEdit* edit = (CEdit*)GetDlgItem(IDC_EDIT_TEXT_BOX);
			edit->ReplaceSel(_T("\r\n"));
		}
	}
	return CDialogEx::PreTranslateMessage(pMsg);
}


void CDlgVersion::OnBnClickedButtonClose()
{
	m_ctlTextBox.EnableWindow(FALSE);
	this->ShowWindow(SW_HIDE);
}
