#pragma once

#include "afxwin.h"
#include "QueueMainThread.h"
#include "../../../../CommonModuleMIR/PTR/Reference/include/PTR_SocketProtocolStructData.h"

#include <queue>
#include <opencv2/opencv.hpp>

using namespace std;
using namespace cv;


class CQueueThread
{
public:

	CQueueThread(void* pView, void* pClientSocket, int nQueueIndex);
	~CQueueThread();

public:
	void* m_pView;
	void* m_pClientSocket;

	int m_nQueueIndex;
	int m_nQueueMainIndex;

	int m_nImageWidth;
	int m_nImageHeight;
	int m_nImageFOV_X;
	int m_nImageFOV_Y;
	int m_nScanGridX;
	int m_nScanGridY;
	int m_nInterpolationCount;

	CWinThread*				m_pThread;
	HANDLE					m_hEvent;
	BOOL					m_bThreadStatus;
	CRITICAL_SECTION		m_pThreadCritical;
	BOOL ReCreateThread(int nQueueIndex);
	BOOL DeleteThread();
	BOOL InitDynamicVariable();
	BOOL Run(int nSequence, BOOL bMessage);
	static UINT Thread(LPVOID lpParam);
	BOOL StartThread();
	BOOL StopThread();
	void SetThreadStatus(BOOL bStatus) { m_bThreadStatus = bStatus; }
	BOOL GetThreadStatus()			   { return m_bThreadStatus;	}

	vector<BYTE*> m_pVecRawByteD1;
	vector<BYTE*> m_pVecRawByteD2;
	vector<BYTE*> m_pVecRawByteD3;
	vector<BYTE*> m_pVecRawByteT;
	vector<BYTE*> m_pVecRawByteR;
	vector<BYTE*> m_pVecRawActualScanBytePosX;
	vector<BYTE*> m_pVecRawActualScanBytePosY;
	vector<BYTE*> m_pVecRawStaticScanBytePosX;
	vector<BYTE*> m_pVecRawStaticScanBytePosY;
	vector<BYTE*> m_pVecInterpolationByte;
	vector<BYTE*> m_pVecInterpolationScanBytePosX;
	vector<BYTE*> m_pVecInterpolationScanBytePosY;
	vector<BYTE*> m_pVecAverageByte;
	vector<BYTE*> m_pVecAverageScanBytePosX;
	vector<BYTE*> m_pVecAverageScanBytePosY;
	vector<CQueueMainThread*> m_pVecMainThread;

public:
	void CreateQueueMainThread(BYTE* pRawByteD1, BYTE* pRawByteD2, BYTE* pRawByteD3, BYTE* pRawByteT, BYTE* pRawByteR, BYTE* pRawActualScanBytenPosX, BYTE* pRawActualScanBytenPosY, BYTE* pRawStaticScanBytenPosX, BYTE* pRawStaticScanBytenPosY);
	void SetQueueMainThread(int nIndex);
	void SetImageWH(int nWidth, int nHeight);
	void SetImageScanFOV(int nImageFOV_X, int nImageFOV_Y);
	void SetImageScanGrid(int nScanGridX, int nScanGridY);
	void SetImageInterpolationCount(int nInterpolationCount);
	void PushRawImageCopy(BYTE* pRawByteD1, BYTE* pRawByteD2, BYTE* pRawByteD3, BYTE* pRawByteT, BYTE* pRawByteR, BYTE* pRawActualScanBytenPosX, BYTE* pRawActualScanBytenPosY, BYTE* pRawStaticScanBytenPosX, BYTE* pRawStaticScanBytenPosY);
	void PushInterpolationImageCopy(BYTE* pInterpolationImageByte);
	void PushAverageImageCopy(BYTE* pAverageImageByte);
	void DeleteRawVector();
	void DeleteInterpolationVector();
	void DeleteAverageVector();
	void DeleteMainThreadVector();
	void QueueMainThreadMonitoring();
};

