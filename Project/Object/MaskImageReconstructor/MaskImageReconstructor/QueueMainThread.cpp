#include "stdafx.h"
#include "QueueMainThread.h"
#include "QueueThread.h"

#include "MainFrm.h"
#include "MaskImageReconstructorDoc.h"
#include "MaskImageReconstructor.h"

CQueueMainThread::CQueueMainThread(void* pView, void* pClientSocket, void* pQueueThread, int nQueueIndex, int nQueueMainIndex)
{
	m_pView				= pView;
	m_pClientSocket		= pClientSocket;
	m_pQueueThread		= pQueueThread;
	m_nQueueIndex		= nQueueIndex;
	m_nQueueMainIndex	= nQueueMainIndex;

	m_nImageWidth = 0;
	m_nImageHeight = 0;
	m_nImageFOV_X = 0;
	m_nImageFOV_Y = 0;
	m_nScanGridX = 0;
	m_nScanGridY = 0;

	m_bComplete = FALSE;

	m_pThread		= nullptr;
	m_hEvent		= NULL;
	m_bThreadStatus = FALSE;
	ReCreateThread();
}

CQueueMainThread::~CQueueMainThread()
{
	DeleteThread();
}

BOOL CQueueMainThread::ReCreateThread()
{
	DeleteThread();

	if (m_pThread == nullptr)
	{
		m_hEvent = CreateEventA(NULL, FALSE, FALSE, NULL);
		ResetEvent(m_hEvent);
		m_pThread = AfxBeginThread(Thread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
		m_pThread->ResumeThread();
	}

	return TRUE;
}

BOOL CQueueMainThread::DeleteThread()
{
	StopThread();
	Sleep(100);
	if (m_pThread != nullptr)
	{
		InitDynamicVariable();
		DWORD dw;
		::GetExitCodeThread(m_pThread->m_hThread, &dw);
		if (dw == STILL_ACTIVE)
		{
			m_pThread->SuspendThread();
			TerminateThread(m_pThread->m_hThread, 0);
			CloseHandle(m_pThread->m_hThread);
		}
		m_pThread->m_hThread = NULL;
		delete m_pThread;
		m_pThread = nullptr;
	}

	if (m_hEvent != NULL)
	{
		CloseHandle(m_hEvent);
		m_hEvent = NULL;
	}

	return TRUE;
}

BOOL CQueueMainThread::InitDynamicVariable()
{
	return TRUE;
}

UINT CQueueMainThread::Thread(LPVOID lpParam)
{
	CQueueMainThread* pThread = (CQueueMainThread*)lpParam;

	while (1)
	{
		WaitForSingleObject(pThread->m_hEvent, INFINITE);
		pThread->SetThreadStatus(TRUE);
		pThread->Run(NULL, FALSE);
		pThread->SetThreadStatus(FALSE);
		pThread->InitDynamicVariable();
		ResetEvent(pThread->m_hEvent);
	}

	return FALSE;
}

BOOL CQueueMainThread::Run(int nSequence, BOOL bMessage)
{
	CQueueThread*				pQueueThread	= (CQueueThread*)m_pQueueThread;
	MaskImageReconstructorView*	pView			= (MaskImageReconstructorView*)m_pView;

	CString strTemp;

	int nImageWidth		= m_nImageWidth;
	int nImageHeight	= m_nImageHeight;
	int nImageFOV_X		= m_nImageFOV_X;
	int nImageFOV_Y		= m_nImageFOV_Y;
	int nScanGridX		= m_nScanGridX;
	int nScanGridY		= m_nScanGridY;
	int nMagnification	= IMAGE_MAGNIFICATION;

	if (pView->m_bTimeUpdate)
	{
		QueryPerformanceFrequency(&pView->m_lFrequency_Interpolation);
		QueryPerformanceCounter(&pView->m_lBeginTime_Interpolation);
	}

	BYTE* raw_AverageFIlter = (BYTE*)malloc(sizeof(double) * (nImageHeight) * (nImageWidth));
	BYTE* Image_regrid		= (BYTE*)malloc(sizeof(double) * (nImageHeight * nMagnification) * (nImageWidth * nMagnification));
	BYTE* x_regrid			= (BYTE*)malloc(sizeof(double) * (nImageHeight * nMagnification) * (nImageWidth * nMagnification));
	BYTE* y_regrid			= (BYTE*)malloc(sizeof(double) * (nImageHeight * nMagnification) * (nImageWidth * nMagnification));

	int num_scans = 1;

	double x_start = 0;
	double y_start = 0;

	double FOV = nImageWidth * m_nScanGridX;

	int tempExtraPixel = 0;

	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"%4d%02d%02d_%02d_%02d_%02d_%03d",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay,
		SystemTime.wHour,
		SystemTime.wMinute,
		SystemTime.wSecond,
		SystemTime.wMilliseconds);

	pView->m_pImageProcessAlgorithm->WriteRawScanPositionFile(
		(double*)m_pRawStaticScanBytePosX,
		(double*)m_pRawStaticScanBytePosY,
		(double*)m_pRawByteD1,
		(double*)m_pRawByteD2,
		(double*)m_pRawByteD3,
		(double*)m_pRawByteT,
		(double*)m_pRawByteR,
		nImageWidth,
		nImageHeight,
		nImageFOV_X,
		nImageFOV_Y,
		nScanGridX,
		nScanGridY,
		strTime);

	pView->m_pImageProcessAlgorithm->WriteRawScanPositionFile2(
		(double*)m_pRawStaticScanBytePosX,
		(double*)m_pRawStaticScanBytePosY,
		(double*)m_pRawByteD1,
		(double*)m_pRawByteD2,
		(double*)m_pRawByteD3,
		(double*)m_pRawByteT,
		(double*)m_pRawByteR,
		nImageWidth,
		nImageHeight,
		nImageFOV_X,
		nImageFOV_Y,
		nScanGridX,
		nScanGridY,
		strTime);

	pView->m_pImageProcessAlgorithm->WriteRawScanDetectorFile(
		(double*)m_pRawActualScanBytePosX,
		(double*)m_pRawActualScanBytePosY,
		(double*)m_pRawByteT,
		nImageWidth,
		nImageHeight,
		nImageFOV_X,
		nImageFOV_Y,
		nScanGridX,
		nScanGridY,
		strTime);

	if (pView->m_bDisplayModeStatus)
	{
		pView->m_pImageProcessAlgorithm->MovingAverageFilter(
			(double*)m_pRawByteT,
			(double*)raw_AverageFIlter,
			(nImageHeight) * (nImageWidth),
			5);

		PythonAlgorithm::ReconstructImage(
			(double*)m_pRawByteT,
			(double*)m_pRawActualScanBytePosX, (double*)m_pRawActualScanBytePosY,
			(double*)Image_regrid,
			(double*)x_regrid, (double*)y_regrid,
			(nImageWidth), (nImageHeight),
			(nImageWidth * nMagnification), (nImageHeight * nMagnification),
			num_scans,
			x_start, y_start,
			FOV,
			tempExtraPixel);
	}

	double dDetector = 0;

	unsigned int nValue = 0;

	CImageProcessAlgorithm* pPA = pView->m_pImageProcessAlgorithm;

	uchar* cInterpolationData = (uchar*)malloc(sizeof(uchar) * ((nImageHeight * nMagnification) * (nImageWidth * nMagnification)) * 3);

	for (int row = 0; row < (nImageHeight * nMagnification); row++)
	{
		for (int col = 0; col < (nImageWidth * nMagnification); col++)
		{
			double* dValue = (double*)&Image_regrid[(row * (nImageWidth * nMagnification * sizeof(double))) + (col * sizeof(double))];

			if (isnan(*dValue)) { dDetector = 0; memcpy(dValue, &dDetector, sizeof(double)); }
			else				{ memcpy(&dDetector, dValue, sizeof(double)); }

			nValue = (unsigned int)pPA->ReMap((dDetector * 100), 256, 60, 100);

			cInterpolationData[(row * (nImageWidth * nMagnification)) * 3 + col * 3 + 0] = pPA->ReMap(pPA->GetColorMapParula(COLOR_MAP_PARULA_B, nValue), 256, 0.0, 1.0);
			cInterpolationData[(row * (nImageWidth * nMagnification)) * 3 + col * 3 + 1] = pPA->ReMap(pPA->GetColorMapParula(COLOR_MAP_PARULA_G, nValue), 256, 0.0, 1.0);
			cInterpolationData[(row * (nImageWidth * nMagnification)) * 3 + col * 3 + 2] = pPA->ReMap(pPA->GetColorMapParula(COLOR_MAP_PARULA_R, nValue), 256, 0.0, 1.0);
		}
	}

	pQueueThread->PushInterpolationImageCopy(Image_regrid);

	memcpy(pView->m_MatInterpolationSRC->data, cInterpolationData, ((nImageHeight * nMagnification) * (nImageWidth * nMagnification)) * 3);

	// Test Code
	CString strWritePath = _T("D:\\SaveScanDataBinary");
	if (GetFileAttributes((LPCTSTR)strWritePath) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)strWritePath, NULL); }
	strWritePath += _T("\\") + m_fnGetCurrentTime();
	pView->m_pImageProcessAlgorithm->WriteScanDataBinary(
		(double*)m_pRawByteD1, 
		(double*)m_pRawByteD2,
		(double*)m_pRawByteD3,
		(double*)m_pRawByteT,
		(double*)m_pRawByteR,
		(double*)m_pRawActualScanBytePosX,
		(double*)m_pRawActualScanBytePosY,
		(double*)m_pRawStaticScanBytePosX,
		(double*)m_pRawStaticScanBytePosY,
		(nImageHeight) * (nImageWidth),
		strWritePath);
	// Test Code

	free(raw_AverageFIlter); raw_AverageFIlter = nullptr;
	free(Image_regrid); Image_regrid = nullptr;
	free(x_regrid); x_regrid = nullptr;
	free(y_regrid); y_regrid = nullptr;
	free(cInterpolationData); cInterpolationData = nullptr;

	pView->m_nInterpolationCountCheck++;

	m_bComplete = TRUE;

	if (pView->m_bTimeUpdate)
	{
		QueryPerformanceCounter(&pView->m_lEndtime_Interpolation);
		pView->m_fnInterpolationTackTimeUpdate();
	}

	return TRUE;
}

BOOL CQueueMainThread::StartThread()
{
	SetEvent(m_hEvent);
	return TRUE;
}

BOOL CQueueMainThread::StopThread()
{
	SetThreadStatus(FALSE);
	return TRUE;
}

void CQueueMainThread::SetRawImageByte(BYTE* pRawByteD1, BYTE* pRawByteD2, BYTE* pRawByteD3, BYTE* pRawByteT, BYTE* pRawByteR, BYTE* pRawActualScanBytenPosX, BYTE* pRawActualScanBytenPosY, BYTE* pRawStaticScanBytenPosX, BYTE* pRawStaticScanBytenPosY, int nImageWidth, int nImageHeight, int nImageFOV_X, int nImageFOV_Y, int nScanGridX, int nScanGridY)
{
	m_pRawByteD1				= pRawByteD1;
	m_pRawByteD2				= pRawByteD2;
	m_pRawByteD3				= pRawByteD3;
	m_pRawByteT					= pRawByteT;
	m_pRawByteR					= pRawByteR;
	m_pRawActualScanBytePosX	= pRawActualScanBytenPosX;
	m_pRawActualScanBytePosY	= pRawActualScanBytenPosY;
	m_pRawStaticScanBytePosX	= pRawStaticScanBytenPosX;
	m_pRawStaticScanBytePosY	= pRawStaticScanBytenPosY;
	m_nImageWidth				= nImageWidth;
	m_nImageHeight				= nImageHeight;
	m_nImageFOV_X				= nImageFOV_X;
	m_nImageFOV_Y				= nImageFOV_Y;
	m_nScanGridX				= nScanGridX;
	m_nScanGridY				= nScanGridY;
}

CString CQueueMainThread::m_fnGetCurrentTime()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"%4d%02d%02d_%02d_%02d_%02d",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay,
		SystemTime.wHour,
		SystemTime.wMinute,
		SystemTime.wSecond);

	return strTime;
}