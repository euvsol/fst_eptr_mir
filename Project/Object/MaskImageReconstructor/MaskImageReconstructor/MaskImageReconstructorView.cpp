
// SeventhExProject_BView.cpp : MaskImageReconstructorView 클래스의 구현
//

#include "stdafx.h"
// SHARED_HANDLERS는 미리 보기, 축소판 그림 및 검색 필터 처리기를 구현하는 ATL 프로젝트에서 정의할 수 있으며
// 해당 프로젝트와 문서 코드를 공유하도록 해 줍니다.
#ifndef SHARED_HANDLERS
#include "MaskImageReconstructor.h"
#endif

#include "MaskImageReconstructorDoc.h"
#include "MaskImageReconstructorView.h"

#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// MaskImageReconstructorView

IMPLEMENT_DYNCREATE(MaskImageReconstructorView, CFormView)

BEGIN_MESSAGE_MAP(MaskImageReconstructorView, CFormView)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST_RECIEPE, &MaskImageReconstructorView::OnNMDblclkListReciepe)
	ON_NOTIFY(NM_CLICK, IDC_LIST_RECIEPE, &MaskImageReconstructorView::OnNMClickListReciepe)
	ON_WM_MEASUREITEM()
	ON_NOTIFY(NM_CUSTOMDRAW, IDC_LIST_RECIEPE, &MaskImageReconstructorView::OnNMCustomdrawListReciepe)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_A_PACKET_RUN, &MaskImageReconstructorView::OnBnClickedButtonProtocolAPacketRun)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_EM1024_RESET, &MaskImageReconstructorView::OnBnClickedButtonProtocolEm1024Reset)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_M_PACKET_RUN, &MaskImageReconstructorView::OnBnClickedButtonProtocolMPacketRun)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_RUN, &MaskImageReconstructorView::OnBnClickedButtonProtocolAdamRun)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_STOP, &MaskImageReconstructorView::OnBnClickedButtonProtocolAdamStop)
	ON_BN_CLICKED(IDC_BUTTON_PROTOCOL_ADAM_RECIPE, &MaskImageReconstructorView::OnBnClickedButtonProtocolAdamRecipe)
	ON_BN_CLICKED(IDC_BUTTON_PTR_TEST, &MaskImageReconstructorView::OnBnClickedButtonPtrTest)
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_BUTTON_TEST_MANUAL_MODE, &MaskImageReconstructorView::OnBnClickedButtonTestManualMode)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON_DISPLAY_MODE, &MaskImageReconstructorView::OnBnClickedButtonDisplayMode)
	ON_WM_SIZE()
	ON_WM_ERASEBKGND()
	ON_BN_CLICKED(IDC_BUTTON_PROGRAM_VERSION, &MaskImageReconstructorView::OnBnClickedButtonProgramVersion)
	ON_BN_CLICKED(IDC_BUTTON_TEST_READ_WRITE, &MaskImageReconstructorView::OnBnClickedButtonTestReadWrite)

	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_COM_SERVER_CLIENT_ACCEPT, &MaskImageReconstructorView::OnMessageComServerClientAccept)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_COM_SERVER_CLIENT_RECEIVE, &MaskImageReconstructorView::OnMessageComServerClientReceive)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_COM_SERVER_CLIENT_CLOSE, &MaskImageReconstructorView::OnMessageComServerClientSocketClose)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_COM_SERVER_CLIENT_PROTOCOL, &MaskImageReconstructorView::OnMessageComServerClientProtocol)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_COM_SERVER_CLIENT_THREAD_SEND, &MaskImageReconstructorView::OnMessageComServerClientThreadSend)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_ACCEPT, &MaskImageReconstructorView::OnMessageServerClientAccept)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_RECEIVE, &MaskImageReconstructorView::OnMessageServerClientReceive)
	ON_MESSAGE(WM_USER_MESSAGE_PROTOCOL_SOCKET_CLIENT_CLOSE, &MaskImageReconstructorView::OnMessageServerClientSocketClose)
END_MESSAGE_MAP()

// MaskImageReconstructorView 생성/소멸

void TraceRelease(LPCTSTR lpszFormat, ...)
{
	CString sResult;

	va_list vlMarker = NULL;
	va_start(vlMarker, lpszFormat);

	sResult.FormatV(lpszFormat, vlMarker);

	va_end(vlMarker);
	OutputDebugString(sResult);
}

template <class Function>
__int64 time_call(Function&& f)
{
	__int64 begin = GetTickCount();
	f();
	return GetTickCount() - begin;
}

MaskImageReconstructorView::MaskImageReconstructorView()
	: CFormView(MaskImageReconstructorView::IDD)
{
	SetViewInstance(this);

	m_nDialogWidth = 0;
	m_nDialogHeight = 0;

	m_pThread = nullptr;
	m_pImageThread = nullptr;

	m_nSavedItem = -1;
	m_nSavedSubitem = -1;

	m_nQueueRingCount = 0;

	m_nPixelCountCheck = 0;
	m_nRawCountCheck = 0;
	m_nRawInterpolationCountCheck = 0;
	m_nInterpolationCountCheck = 0;
	m_nAverageCountCheck = 0;
	m_nScanSetCountCheck = 0;

	m_nTurningIndex = 0;

	m_bTurningPoint = FALSE;

	m_nMPacketCount = 1000;
	m_nAPacketCount = 1000;

	m_nPacketCount_BG = 0;
	m_nPacketCount_WO_P = 0;
	m_nPacketCount_BS = 0;
	m_nPacketCount_P_AS_POINT = 0;

	m_nPacketNumberCheck = 0;

	m_bTimeUpdate = FALSE;

	m_bLastScanCheck = FALSE;

	m_bLastScanSyncCheck = FALSE;

	m_nPacketMode = MODE_RUN_PACKET;

	m_bEquipmentStatus = FALSE;

	m_bLastLineCheck = FALSE;

	m_bAliveFlag = FALSE;

	SetAliveThread(TRUE);
	SetAutoSocketConnectThread(TRUE);
	SetAutoComSocketConnectThread(TRUE);

	m_nPixelPosX = 0;
	m_nPixelPosY = 0;

	m_nBeforeImageWidth = 0;
	m_nBeforeImageHeight = 0;

	m_dMatActualScanPos_X = nullptr;
	m_dMatActualScanPos_Y = nullptr;

	m_dMatStaticScanPos_X = nullptr;
	m_dMatStaticScanPos_Y = nullptr;

	m_dMatBufferActualScanPos_X = nullptr;
	m_dMatBufferActualScanPos_Y = nullptr;

	m_dMatBufferStaticScanPos_X = nullptr;
	m_dMatBufferStaticScanPos_Y = nullptr;

	m_dMat_D1 = nullptr;
	m_dMat_D2 = nullptr;
	m_dMat_D3 = nullptr;
	m_dMatBuffer_D1 = nullptr;
	m_dMatBuffer_D2 = nullptr;
	m_dMatBuffer_D3 = nullptr;

	m_dMat_T = nullptr;
	m_dMatBuffer_T = nullptr;
	m_dMatInterpolation_T = nullptr;
	m_dMatAverage_T = nullptr;

	m_dMat_R = nullptr;
	m_dMatBuffer_R = nullptr;
	m_dMatInterpolation_R = nullptr;
	m_dMatAverage_R = nullptr;

	m_MatRawSRC = nullptr;
	m_MatRawBufferSRC = nullptr;
	m_MatInterpolationSRC = nullptr;
	m_MatAverageSRC = nullptr;

	m_MatRawInterpolationSRC = nullptr;
	m_RawInterpolationPos_X = nullptr;
	m_RawInterpolationPos_Y = nullptr;
	m_RawInterpolationOutput = nullptr;

	m_bySharedMemoryRaw = nullptr;
	m_bySharedMemoryRawInterpolation = nullptr;
	m_bySharedMemoryInterpolation = nullptr;
	m_bySharedMemoryAverage = nullptr;

	m_bySharedMemoryT = nullptr;
	m_bySharedMemoryR = nullptr;
	m_bySharedMemoryPosX = nullptr;
	m_bySharedMemoryPosY = nullptr;

	m_nDetectorSize = 0;
	m_nDetectorIndex = 0;

	m_dScanAreaThresholdX = 0;
	m_dScanAreaThresholdY = 0;

	SetThreadStatus(FALSE);
	SetImageThreadStatus(FALSE);

	SetEquipmentStatus(FALSE);
	m_bManualModeStatus = FALSE;
	m_bDisplayModeStatus = FALSE;

	m_bRunPacketTimeOutCheck = FALSE;
	m_bRunPacketTimeOutCheckToggle = FALSE;

	m_dScanPositionSum_X = nullptr;
	m_dScanPositionSum_Y = nullptr;
	m_dDetectorSum_1 = nullptr;
	m_dDetectorSum_2 = nullptr;
	m_dDetectorSum_3 = nullptr;
	m_nMatrixCount = nullptr;
	m_bMatrixCheck = nullptr;

	m_nMatRawInterpolationControlX = 0;
	m_nMatRawInterpolationControlY = 0;
	m_nMatRawInterpolationResizeX = 0;
	m_nMatRawInterpolationResizeY = 0;
	m_bitmapInfoMatRawInterpolation = nullptr;

	m_nMatRawControlX = 0;
	m_nMatRawControlY = 0;
	m_nMatRawResizeX = 0;
	m_nMatRawResizeY = 0;
	m_bitmapInfoMatRaw = nullptr;

	m_nMatInterpolationControlX = 0;
	m_nMatInterpolationControlY = 0;
	m_nMatInterpolationResizeX = 0;
	m_nMatInterpolationResizeY = 0;
	m_bitmapInfoMatInterpolation = nullptr;

	m_nMatAverageControlX = 0;
	m_nMatAverageControlY = 0;
	m_nMatAverageResizeX = 0;
	m_nMatAverageResizeY = 0;
	m_bitmapInfoMatAverage = nullptr;

	memset(&m_hScanPositionXMapping, 0x00, sizeof(m_hScanPositionXMapping));
	memset(&m_pScanPositionXAddress, 0x00, sizeof(m_pScanPositionXAddress));

	memset(&m_hScanPositionYMapping, 0x00, sizeof(m_hScanPositionYMapping));
	memset(&m_pScanPositionYAddress, 0x00, sizeof(m_pScanPositionYAddress));

	memset(&m_hTransmittanceMapping, 0x00, sizeof(m_hTransmittanceMapping));
	memset(&m_pTransmittanceAddress, 0x00, sizeof(m_pTransmittanceAddress));

	memset(&m_hReflectanceMapping, 0x00, sizeof(m_hReflectanceMapping));
	memset(&m_pReflectanceAddress, 0x00, sizeof(m_pReflectanceAddress));

	memset(&m_hRawMapping, 0x00, sizeof(m_hRawMapping));
	memset(&m_pRawAddress, 0x00, sizeof(m_pRawAddress));

	memset(&m_hRawInterpolationMapping, 0x00, sizeof(m_hRawInterpolationMapping));
	memset(&m_pRawInterpolationAddress, 0x00, sizeof(m_pRawInterpolationAddress));

	memset(&m_hInterpolationMapping, 0x00, sizeof(m_hInterpolationMapping));
	memset(&m_pInterpolationAddress, 0x00, sizeof(m_pInterpolationAddress));

	memset(&m_hAverageMapping, 0x00, sizeof(m_hAverageMapping));
	memset(&m_pAverageAddress, 0x00, sizeof(m_pAverageAddress));

	m_dScanPositionX = (double*)malloc(sizeof(double) * (FIX_POS_PIXEL_LIMIT_COUNT * FIX_POS_PIXEL_LIMIT_COUNT));
	m_dScanPositionY = (double*)malloc(sizeof(double) * (FIX_POS_PIXEL_LIMIT_COUNT * FIX_POS_PIXEL_LIMIT_COUNT));

	m_dDetector1 = (double*)malloc(sizeof(double) * (FIX_POS_PIXEL_LIMIT_COUNT * FIX_POS_PIXEL_LIMIT_COUNT));
	m_dDetector2 = (double*)malloc(sizeof(double) * (FIX_POS_PIXEL_LIMIT_COUNT * FIX_POS_PIXEL_LIMIT_COUNT));
	m_dDetector3 = (double*)malloc(sizeof(double) * (FIX_POS_PIXEL_LIMIT_COUNT * FIX_POS_PIXEL_LIMIT_COUNT));
	
	for (int i = 0; i < MULTI_CORE_QUEUE_COUNT; i++) { m_pQueueThread[i] = nullptr; }

	m_pThread		= nullptr;
	m_hEvent		= NULL;
	m_bThreadStatus = FALSE;
	ReCreateThread();

	m_pImageThread			= nullptr;
	m_hImageEvent			= NULL;
	m_bImageThreadStatus	= FALSE;
	ReCreateImageThread();

	m_byRing_Run		= (BYTE*)malloc(sizeof(BYTE) * SOCKET_PACKET_SIZE);
	m_byRing_APacket	= (BYTE*)malloc(sizeof(BYTE) * SOCKET_PACKET_SIZE);
	m_byRing_MPacket	= (BYTE*)malloc(sizeof(BYTE) * SOCKET_PACKET_SIZE);

	CMainFrame*	pFrame = (CMainFrame *)AfxGetMainWnd();

	m_hImageProcessAlgorithm = LoadLibrary(_T("ImageProcessAlgorithm"));
	m_pImageProcessAlgorithm = new CImageProcessAlgorithm(m_hImageProcessAlgorithm);

	m_hLogViewer = LoadLibrary(_T("LogViewer"));
	m_pLogViewer = new CLogViewer(m_hLogViewer);
	m_pLogViewer->FrameworkInstance(pFrame, this);

	m_hSharedMemory = LoadLibrary(_T("SharedMemory"));
	m_pSharedMemory = new CSharedMemory(m_hSharedMemory);

	m_hTcpClient = LoadLibrary(_T("TcpClient"));
	m_pTcpClient = new CTcpClient(m_hTcpClient);
	m_pTcpClient->FrameworkInstance(pFrame, this);

	m_hTcpComClient = LoadLibrary(_T("TcpComClient"));
	m_pTcpComClient = new CTcpComClient(m_hTcpComClient);
	m_pTcpComClient->FrameworkInstance(pFrame, this);

	TCHAR chFilePath[256] = { 0, };
	GetModuleFileName(NULL, chFilePath, 256);

	CString strFolderPath(chFilePath);
	CString strFolderTemp;

	strFolderPath = strFolderPath.Left(strFolderPath.ReverseFind('\\'));
	strFolderPath = strFolderPath.Left(strFolderPath.ReverseFind('\\'));
	strFolderTemp = strFolderPath.Left(strFolderPath.ReverseFind('\\'));
	strFolderTemp = strFolderTemp + _T("\\Project\\Reference\\Config\\");

	if (GetFileAttributes((LPCTSTR)strFolderTemp) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)strFolderTemp, NULL); }

	InitializeCriticalSection(&m_pImageThreadCritical);

	//_CrtSetBreakAlloc(6998);
}

MaskImageReconstructorView::~MaskImageReconstructorView()
{
	SetAutoSocketConnectThread(FALSE);
	SetAutoComSocketConnectThread(FALSE);
	SetAliveThread(FALSE);

	DeleteThread();
	DeleteImageThread();

	if (m_pImageProcessAlgorithm != nullptr) { delete m_pImageProcessAlgorithm; m_pImageProcessAlgorithm = nullptr; FreeLibrary(m_hImageProcessAlgorithm); m_hImageProcessAlgorithm = NULL; }
	if (m_pLogViewer != nullptr) { delete m_pLogViewer; m_pLogViewer = nullptr; FreeLibrary(m_hLogViewer); m_hLogViewer = NULL; }
	if (m_pSharedMemory != nullptr) { delete m_pSharedMemory; m_pSharedMemory = nullptr; FreeLibrary(m_hSharedMemory); m_hSharedMemory = NULL; }
	if (m_pTcpClient != nullptr) { delete m_pTcpClient; m_pTcpClient = nullptr; FreeLibrary(m_hTcpClient); m_hTcpClient = NULL; }
	if (m_pTcpComClient != nullptr) { delete m_pTcpComClient; m_pTcpComClient = nullptr; FreeLibrary(m_hTcpComClient); m_hTcpComClient = NULL; }

	if (m_brushsMenubar != NULL) { DeleteObject(m_brushsMenubar); m_brushsMenubar = NULL; }
	if (m_brushsStatic != NULL) { DeleteObject(m_brushsStatic); m_brushsStatic = NULL; }
	if (m_brushsEdit != NULL) { DeleteObject(m_brushsEdit); m_brushsEdit = NULL; }
	if (m_brushsEqpStatusOn != NULL) { DeleteObject(m_brushsEqpStatusOn); m_brushsEqpStatusOn = NULL; }
	if (m_brushsEqpStatusOff != NULL) { DeleteObject(m_brushsEqpStatusOff); m_brushsEqpStatusOff = NULL; }
	if (m_brushsSocketConnectOn != NULL) { DeleteObject(m_brushsSocketConnectOn); m_brushsSocketConnectOn = NULL; }
	if (m_brushsSocketConnectOff != NULL) { DeleteObject(m_brushsSocketConnectOff); m_brushsSocketConnectOff = NULL; }

	if (m_dMatActualScanPos_X != nullptr) { delete m_dMatActualScanPos_X; m_dMatActualScanPos_X = nullptr; }
	if (m_dMatActualScanPos_Y != nullptr) { delete m_dMatActualScanPos_Y; m_dMatActualScanPos_Y = nullptr; }

	if (m_dMatStaticScanPos_X != nullptr) { delete m_dMatStaticScanPos_X; m_dMatStaticScanPos_X = nullptr; }
	if (m_dMatStaticScanPos_Y != nullptr) { delete m_dMatStaticScanPos_Y; m_dMatStaticScanPos_Y = nullptr; }

	if (m_dMatBufferActualScanPos_X != nullptr) { delete m_dMatBufferActualScanPos_X; m_dMatBufferActualScanPos_X = nullptr; }
	if (m_dMatBufferActualScanPos_Y != nullptr) { delete m_dMatBufferActualScanPos_Y; m_dMatBufferActualScanPos_Y = nullptr; }

	if (m_dMatBufferStaticScanPos_X != nullptr) { delete m_dMatBufferStaticScanPos_X; m_dMatBufferStaticScanPos_X = nullptr; }
	if (m_dMatBufferStaticScanPos_Y != nullptr) { delete m_dMatBufferStaticScanPos_Y; m_dMatBufferStaticScanPos_Y = nullptr; }

	if (m_dMat_D1 != nullptr) { delete m_dMat_D1; m_dMat_D1 = nullptr; }
	if (m_dMat_D2 != nullptr) { delete m_dMat_D2; m_dMat_D2 = nullptr; }
	if (m_dMat_D3 != nullptr) { delete m_dMat_D3; m_dMat_D3 = nullptr; }
	if (m_dMatBuffer_D1 != nullptr) { delete m_dMatBuffer_D1; m_dMatBuffer_D1 = nullptr; }
	if (m_dMatBuffer_D2 != nullptr) { delete m_dMatBuffer_D2; m_dMatBuffer_D2 = nullptr; }
	if (m_dMatBuffer_D3 != nullptr) { delete m_dMatBuffer_D3; m_dMatBuffer_D3 = nullptr; }

	if (m_dMat_T != nullptr) { delete m_dMat_T; m_dMat_T = nullptr; }
	if (m_dMatBuffer_T != nullptr) { delete m_dMatBuffer_T; m_dMatBuffer_T = nullptr; }
	if (m_dMatInterpolation_T != nullptr) { delete m_dMatInterpolation_T; m_dMatInterpolation_T = nullptr; }
	if (m_dMatAverage_T != nullptr) { delete m_dMatAverage_T; m_dMatAverage_T = nullptr; }

	if (m_dMat_R != nullptr) { delete m_dMat_R; m_dMat_R = nullptr; }
	if (m_dMatBuffer_R != nullptr) { delete m_dMatBuffer_R; m_dMatBuffer_R = nullptr; }
	if (m_dMatInterpolation_R != nullptr) { delete m_dMatInterpolation_R; m_dMatInterpolation_R = nullptr; }
	if (m_dMatAverage_R != nullptr) { delete m_dMatAverage_R; m_dMatAverage_R = nullptr; }

	if (m_MatRawSRC != nullptr) { delete m_MatRawSRC; m_MatRawSRC = nullptr; }
	if (m_MatRawBufferSRC != nullptr) { delete m_MatRawBufferSRC; m_MatRawBufferSRC = nullptr; }
	if (m_MatInterpolationSRC != nullptr) { delete m_MatInterpolationSRC; m_MatInterpolationSRC = nullptr; }
	if (m_MatAverageSRC != nullptr) { delete m_MatAverageSRC; m_MatAverageSRC = nullptr; }

	if (m_MatRawInterpolationSRC != nullptr) { delete m_MatRawInterpolationSRC; m_MatRawInterpolationSRC = nullptr; }
	if (m_RawInterpolationPos_X != nullptr) { free(m_RawInterpolationPos_X); m_RawInterpolationPos_X = nullptr; }
	if (m_RawInterpolationPos_Y != nullptr) { free(m_RawInterpolationPos_Y); m_RawInterpolationPos_Y = nullptr; }
	if (m_RawInterpolationOutput != nullptr) { free(m_RawInterpolationOutput); m_RawInterpolationOutput = nullptr; }

	while (!m_qPacket_M.empty()) { PACKET_M_IMAGE_DATA_CONTENT* p = m_qPacket_M.front(); free(p); p = nullptr; m_qPacket_M.pop(); }
	while (!m_qPacket_A.empty()) { PACKET_A_IMAGE_DATA_CONTENT* p = m_qPacket_A.front(); free(p); p = nullptr; m_qPacket_A.pop(); }
	while (!m_qPacket_BS.empty()) { PACKET_M_IMAGE_DATA_CONTENT* p = m_qPacket_BS.front(); free(p); p = nullptr; m_qPacket_BS.pop(); }
	while (!m_qPacket_P_As_Point.empty()) { PACKET_M_IMAGE_DATA_CONTENT* p = m_qPacket_P_As_Point.front(); free(p); p = nullptr; m_qPacket_P_As_Point.pop(); }

	for (int i = 0; i < MULTI_CORE_QUEUE_COUNT; i++) { if (m_pQueueThread != nullptr) { delete m_pQueueThread[i]; m_pQueueThread[i] = nullptr; } }

	if (m_bitmapInfoMatRawInterpolation != nullptr) { free(m_bitmapInfoMatRawInterpolation); m_bitmapInfoMatRawInterpolation = nullptr; }
	if (m_bitmapInfoMatRaw != nullptr) { free(m_bitmapInfoMatRaw); m_bitmapInfoMatRaw = nullptr; }
	if (m_bitmapInfoMatInterpolation != nullptr) { free(m_bitmapInfoMatInterpolation); m_bitmapInfoMatInterpolation = nullptr; }
	if (m_bitmapInfoMatAverage != nullptr) { free(m_bitmapInfoMatAverage); m_bitmapInfoMatAverage = nullptr; }

	if (m_byRing_Run != nullptr) { free(m_byRing_Run); m_byRing_Run = nullptr; }
	if (m_byRing_APacket != nullptr) { free(m_byRing_APacket); m_byRing_APacket = nullptr; }
	if (m_byRing_MPacket != nullptr) { free(m_byRing_MPacket); m_byRing_MPacket = nullptr; }

	if (m_dScanPositionX != nullptr) { free(m_dScanPositionX); m_dScanPositionX = nullptr; }
	if (m_dScanPositionY != nullptr) { free(m_dScanPositionY); m_dScanPositionY = nullptr; }

	if (m_dDetector1 != nullptr) { free(m_dDetector1); m_dDetector1 = nullptr; }
	if (m_dDetector2 != nullptr) { free(m_dDetector2); m_dDetector2 = nullptr; }
	if (m_dDetector3 != nullptr) { free(m_dDetector3); m_dDetector3 = nullptr; }

	if (m_bitmapInfoMatRaw != nullptr) { free(m_bitmapInfoMatRaw); }
	if (m_bitmapInfoMatInterpolation != nullptr) { free(m_bitmapInfoMatInterpolation); }
	if (m_bitmapInfoMatAverage != nullptr) { free(m_bitmapInfoMatAverage); }

	if (m_bMatrixCheck != nullptr) { m_fnMatrixDelete(m_fnPixelWidth(), m_fnPixelHeight()); }

	if (m_bySharedMemoryRaw != nullptr) { free(m_bySharedMemoryRaw); m_bySharedMemoryRaw = nullptr; }
	if (m_bySharedMemoryRawInterpolation != nullptr) { free(m_bySharedMemoryRawInterpolation); m_bySharedMemoryRawInterpolation = nullptr; }
	if (m_bySharedMemoryInterpolation != nullptr) { free(m_bySharedMemoryInterpolation); m_bySharedMemoryInterpolation = nullptr; }
	if (m_bySharedMemoryAverage != nullptr) { free(m_bySharedMemoryAverage); m_bySharedMemoryAverage = nullptr; }

	if (m_bySharedMemoryT != nullptr) { free(m_bySharedMemoryT); m_bySharedMemoryT = nullptr; }
	if (m_bySharedMemoryR != nullptr) { free(m_bySharedMemoryR); m_bySharedMemoryR = nullptr; }
	if (m_bySharedMemoryPosX != nullptr) { free(m_bySharedMemoryPosX); m_bySharedMemoryPosX = nullptr; }
	if (m_bySharedMemoryPosY != nullptr) { free(m_bySharedMemoryPosY); m_bySharedMemoryPosY = nullptr; }

	if (m_pScanPositionXAddress != NULL) ::UnmapViewOfFile(m_pScanPositionXAddress); m_pScanPositionXAddress = NULL;
	if (m_hScanPositionXMapping != NULL) ::CloseHandle(m_hScanPositionXMapping); m_hScanPositionXMapping = NULL;

	if (m_pScanPositionYAddress != NULL) ::UnmapViewOfFile(m_pScanPositionYAddress); m_pScanPositionYAddress = NULL;
	if (m_hScanPositionYMapping != NULL) ::CloseHandle(m_hScanPositionYMapping); m_hScanPositionYMapping = NULL;

	if (m_pTransmittanceAddress != NULL) ::UnmapViewOfFile(m_pTransmittanceAddress); m_pTransmittanceAddress = NULL;
	if (m_hTransmittanceMapping != NULL) ::CloseHandle(m_hTransmittanceMapping); m_hTransmittanceMapping = NULL;

	if (m_pReflectanceAddress != NULL) ::UnmapViewOfFile(m_pReflectanceAddress); m_pReflectanceAddress = NULL;
	if (m_hReflectanceMapping != NULL) ::CloseHandle(m_hReflectanceMapping); m_hReflectanceMapping = NULL;

	if (m_pRawAddress != NULL) ::UnmapViewOfFile(m_pRawAddress); m_pRawAddress = NULL;
	if (m_hRawMapping != NULL) ::CloseHandle(m_hRawMapping); m_hRawMapping = NULL;

	if (m_pRawInterpolationAddress != NULL) ::UnmapViewOfFile(m_pRawInterpolationAddress); m_pRawInterpolationAddress = NULL;
	if (m_hRawInterpolationMapping != NULL) ::CloseHandle(m_hRawInterpolationMapping); m_hRawInterpolationMapping = NULL;

	if (m_pInterpolationAddress != NULL) ::UnmapViewOfFile(m_pInterpolationAddress); m_pInterpolationAddress = NULL;
	if (m_hInterpolationMapping != NULL) ::CloseHandle(m_hInterpolationMapping); m_hInterpolationMapping = NULL;

	if (m_pAverageAddress != NULL) ::UnmapViewOfFile(m_pAverageAddress); m_pAverageAddress = NULL;
	if (m_hAverageMapping != NULL) ::CloseHandle(m_hAverageMapping); m_hAverageMapping = NULL;

	DeleteCriticalSection(&m_pImageThreadCritical);

	//_CrtDumpMemoryLeaks();
}

void MaskImageReconstructorView::DoDataExchange(CDataExchange* pDX)
{
	CFormView::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST_RECIEPE, m_ctlImageRecipe);
	DDX_Control(pDX, IDC_EDIT_IMAGE_RECIPE, m_ctlEditImageRecipe);
	DDX_Control(pDX, IDC_STATIC_PROGRAM_NAME, m_ctlProjectName);
}

BOOL MaskImageReconstructorView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: CREATESTRUCT cs를 수정하여 여기에서
	//  Window 클래스 또는 스타일을 수정합니다.

	return CFormView::PreCreateWindow(cs);
}

void MaskImageReconstructorView::OnInitialUpdate()
{
	CFormView::OnInitialUpdate();
	GetParentFrame()->RecalcLayout();
	ResizeParentToFit();
	
	m_fnCreateLogViewerInit();
	m_fnStandbyModeMemoryOptimizationCheck();

	m_fnDialogInit();
	m_fnMenuBarInit();
	m_fnControlInit();
	m_fnCreateBrushInit();
	m_fnSetControllEnable(TRUE);
	m_fnImageInfoList();
	m_fnAutoSocketConnect();
	m_fnAutoComSocketConnect();
	m_fnOnCreateQueueThread();
	m_fnCreateImageInit();
	m_fnFolderCreate();
	m_fnVersionInit();
}


UINT MaskImageReconstructorView::Thread(LPVOID lpParam)
{
	MaskImageReconstructorView* pThread = (MaskImageReconstructorView*)lpParam;

	while (1)
	{
		WaitForSingleObject(pThread->m_hEvent, INFINITE);
		pThread->SetThreadStatus(TRUE);
		pThread->Run(NULL, FALSE);
		pThread->SetThreadStatus(FALSE);
		pThread->InitDynamicVariable();
		ResetEvent(pThread->m_hEvent);
	}

	return FALSE;
}

BOOL MaskImageReconstructorView::Run(int nSequence, BOOL bMessage)
{
	CTcpClient*	pClient	= (CTcpClient*)m_pTcpClient;

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("********** Start Receive Queue Sequence!! **********")));

	while (GetThreadStatus())
	{
		if (!m_fnPacketThreadFInishCheck()) { m_fnStartMaskImageReconstructoringCheck(pClient); }
		if (!GetThreadStatus()) { break; }
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("********** Finish Receive Queue Sequence!! **********")));

	return TRUE;
}

BOOL MaskImageReconstructorView::StartThread()
{
	SetEvent(m_hEvent);
	return TRUE;
}

BOOL MaskImageReconstructorView::StopThread()
{
	SetThreadStatus(FALSE);
	return TRUE;
}

UINT MaskImageReconstructorView::ImageThread(LPVOID lpParam)
{
	MaskImageReconstructorView* pThread = (MaskImageReconstructorView*)lpParam;

	while (1)
	{
		WaitForSingleObject(pThread->m_hImageEvent, INFINITE);
		pThread->SetImageThreadStatus(TRUE);
		pThread->RunImage(NULL, FALSE);
		pThread->SetImageThreadStatus(FALSE);
		pThread->InitDynamicImageVariable();
		ResetEvent(pThread->m_hImageEvent);
	}

	return FALSE;
}

BOOL MaskImageReconstructorView::RunImage(int nSequence, BOOL bMessage)
{
	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("********** Start Image Display Sequence!! **********")));

	ThreadParam stData;

	m_fnOnMessageDisplayInitUpdate();
	m_fnOnMessageStartCreateLineThread();

	while (GetImageThreadStatus())
	{
		m_fnImageThreadFInishCheck(&stData);
		if (!GetImageThreadStatus()) { break; }
		Sleep(1);
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("********** Finish Image Display Sequence!! **********")));

	if (m_bLastScanCheck) { Sleep(1000); OnComMeasureP_As_Scan_Stop(); }

	return TRUE;
}

BOOL MaskImageReconstructorView::StartImageThread()
{
	SetEvent(m_hImageEvent);
	return TRUE;
}

BOOL MaskImageReconstructorView::StopImageThread()
{
	SetImageThreadStatus(FALSE);
	return TRUE;
}

// MaskImageReconstructorView 진단

#ifdef _DEBUG
void MaskImageReconstructorView::AssertValid() const
{
	CFormView::AssertValid();
}

void MaskImageReconstructorView::Dump(CDumpContext& dc) const
{
	CFormView::Dump(dc);
}

MaskImageReconstructorDoc* MaskImageReconstructorView::GetDocument() const // 디버그되지 않은 버전은 인라인으로 지정됩니다.
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(MaskImageReconstructorDoc)));
	return (MaskImageReconstructorDoc*)m_pDocument;
}
#endif //_DEBUG


// MaskImageReconstructorView 메시지 처리기

void MaskImageReconstructorView::m_fnServerConnect()
{
	if (m_pTcpClient->CreateClient(static_cast<int>(CLIENT_SOCKET::CH_1), SERVER_ADAM, SERVER_ADAM_PORT))
	{
		m_pLogViewer->AddLog(
			static_cast<int>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("Client[%d] Server Connect : IP : %s / PORT : %d"), static_cast<int>(CLIENT_SOCKET::CH_1), SERVER_ADAM, SERVER_ADAM_PORT));

		SetAutoSocketConnectThread(FALSE);
	}
	else
	{
		if (m_pLogViewer != nullptr)
		{
			m_pLogViewer->AddLog(
				static_cast<int>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("Client[%d] Server Connect Fail."), static_cast<int>(CLIENT_SOCKET::CH_1)));
		}
	}
}

void MaskImageReconstructorView::m_fnComServerConnect()
{
	if (m_pTcpComClient->CreateClient(static_cast<int>(COM_CLIENT_SOCKET::CH_1), SERVER_LOCAL, SERVER_LOCAL_PORT))
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("Client[%d] Com Server Connect : IP : %s / PORT : %d"), static_cast<int>(CLIENT_SOCKET::CH_1), SERVER_LOCAL, SERVER_LOCAL_PORT));

		SetAutoComSocketConnectThread(FALSE);
	}
	else
	{
		if (m_pLogViewer != nullptr)
		{
			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("Client[%d] Com Server Connect Fail"), static_cast<int>(CLIENT_SOCKET::CH_1)));
		}
	}
}

void MaskImageReconstructorView::m_fnCreateBrushInit()
{
	m_brushsMenubar = CreateSolidBrush(RGB(91, 91, 91));
	m_brushsStatic = CreateSolidBrush(RGB(38, 38, 38));
	m_brushsEdit = CreateSolidBrush(RGB(53, 53, 53));
	m_brushsEqpStatusOn = CreateSolidBrush(RGB(0, 255, 0));
	m_brushsEqpStatusOff = CreateSolidBrush(RGB(255, 0, 0));
	m_brushsSocketConnectOn = CreateSolidBrush(RGB(0, 255, 0));
	m_brushsSocketConnectOff = CreateSolidBrush(RGB(255, 0, 0));
}

void MaskImageReconstructorView::m_fnImageData_RUN(PACKET_M_IMAGE_DATA* pQueueBufferCopy)
{
	PACKET_M_IMAGE_DATA* pPacketData = pQueueBufferCopy;

	int nRawImageCount	= (m_fnPixelWidth() * m_fnPixelHeight());
	int nCount			= m_nPacketCount;
	int nTotalSize		= PROTOCOL_M_PACKET_SIZE * nCount;
	int nRepeat			= nTotalSize / PROTOCOL_M_PACKET_SIZE;

	PACKET_M_IMAGE_DATA_CONTENT* pTemp = (PACKET_M_IMAGE_DATA_CONTENT*)malloc(sizeof(PACKET_M_IMAGE_DATA_CONTENT));

	double dOffset = 1;

	BOOL bManualContinue = FALSE;

	for (int i = 0; i < nRepeat; i++)
	{
		if (!GetThreadStatus()) { break; }

		PACKET_M_IMAGE_DATA_CONTENT* pDataContent = pPacketData->QueueData.front();

		// Param Data
		pTemp->PacketNumber		= pDataContent->PacketNumber;
		pTemp->EM1024X			= -(pDataContent->EM1024X * 1000 * 1000);
		pTemp->EM1024Y			= pDataContent->EM1024Y * 1000 * 1000;
		pTemp->Detector_1		= pDataContent->Detector_1;
		pTemp->Detector_2		= pDataContent->Detector_2;
		pTemp->Detector_3		= pDataContent->Detector_3;
		pTemp->Detector_4		= pDataContent->Detector_4;
		pTemp->Zr_IO			= pDataContent->Zr_IO;
		pTemp->BS_IO			= pDataContent->BS_IO;
		// Param Data

		if (m_nPacketNumberCheck == (pTemp->PacketNumber - 1)) { m_nPacketNumberCheck++; }
		else 
		{ 
			m_pLogViewer->AddLog(
				static_cast<int>(LOG_VIEWER::CH_1),
				LOG_SCAN,
				m_fnFormattedStringTimeLog(_T("* PacketNumber Different : Server : %d / Client : %d\n"), (pTemp->PacketNumber - 1), m_nPacketNumberCheck));
		}

		// ****************** Scan X, Y Position Pre-Processing Secnario ****************** //
		//
		// 1. Y Axis Scan Position이 Image Y FOV Size Limit 안에 들어올 시,
		//		1-1. Current Scan Positon Y Axis 정방향 일때
		//			1-1-1. X Axis Scan Position이 Image X FOV Size Limit 안에 들어올 시,
		//				1-1-1-1. X Axis Pixel Count가 X Aixs Last Count가 아니면,
		//					1-1-1-1-1. Y Axis Position이 Y Axis Scan Grid Range안에 들어올 시,
		//						1-1-1-1-1-1. X Axis Position이 X Axis Scan Grid Range안에 있을 경우
		//							1-1-1-1-1-1-1. Detector1,2,3 Data BUffer에 Push
		//						1-1-1-1-1-2. X Axis Position이 X Axis Scan Grid Range보다 클 경우
		//							1-1-1-1-1-2-1. Detector1,2,3 Data를 통해 T/R Value 계산
		//							1-1-1-1-1-2-2. Shared Memory Current Pixel Matrix Coord에 T/R Value Update
		//							1-1-1-1-1-2-3. X Axis Pixel Count 증가 / X Axis Scan Grid Limit 한단계 증가
		//				1-1-1-2. X Axis Pixel Count가 X Aixs Last Count이면
		//					1-1-1-2-1. Y Axis Pixel Count 증가 / Y Axis Scan Grid Limit 한단계 증가
		//		1-2. Current Scan Positon Y Axis 역방향 일때
		//			1-2-1. X Axis Scan Position이 Image X FOV Size Limit 안에 들어올 시,
		//				1-2-1-1. X Axis Pixel Count가 X Aixs Last Count가 아니면,
		//					1-2-1-1-1. Y Axis Position이 Y Axis Scan Grid Range안에 들어올 시,
		//						1-2-1-1-1-1. X Axis Position이 X Axis Scan Grid Range안에 있을 경우
		//							1-2-1-1-1-1-1. Detector1,2,3 Data BUffer에 Push
		//						1-2-1-1-1-2. X Axis Position이 X Axis Scan Grid Range보다 작을 경우
		//							1-2-1-1-1-2-1. Detector1,2,3 Data를 통해 T/R Value 계산
		//							1-2-1-1-1-2-2. Shared Memory Current Pixel Matrix Coord에 T/R Value Update
		//							1-2-1-1-1-2-3. X Axis Pixel Count 증가 / X Axis Scan Grid Limit 한단계 감소
		//				1-2-1-2. X Axis Pixel Count가 X Aixs Last Count이면
		//					1-2-1-2-1. Y Axis Pixel Count 증가 / Y Axis Scan Grid Limit 한단계 증가
		//
		// ****************** Scan X, Y Position Pre-Processing Secnario ****************** //

//		TRACE(_T("* Scan Data[%d][%d] : %lf, %lf\n"), m_nPixelPosY, m_nPixelPosX, pTemp->EM1024X, pTemp->EM1024Y);

		if (!m_bLastScanSyncCheck)
		{
			#if PRE_ALGORITHM_SELECT
				if (!m_bTurningPoint)
				{
					m_fnMatrix_Position_Machting_Algorithm(pTemp, &dOffset);
					if (m_fnScanLastMatrixPositionStatusCheck()) { break; }
				}
			#else 
				m_fnPosition_Threshold_Algorithm(pTemp, &dOffset, &bManualContinue);

				if (m_bManualModeStatus && bManualContinue)
				{
					i--;
					m_nPacketNumberCheck--;
					bManualContinue = FALSE;
					continue;
				}
				if (m_fnScanLastThresholdPositionStatusCheck()) { break; }
			#endif
		}

		free(pDataContent); pDataContent = nullptr;
		pPacketData->QueueData.pop();
	}

	std::free(pTemp);
}

void MaskImageReconstructorView::m_fnImageData_M(PACKET_M_IMAGE_DATA* pQueueBufferCopy)
{
	int nCount			= 0;
	int nTotalSize		= 0;
	int nRepeat			= 0;

	if ((m_pTcpClient->GetRingBufferReceiveIndex(static_cast<int>(CLIENT_SOCKET::CH_1)) + 1) == m_fnReceivePacketCountCheck() &&
		m_pTcpClient->GetScanPacketLast(static_cast<int>(CLIENT_SOCKET::CH_1)) == TRUE)
	{
		nCount			= m_fnGetPacketModeCountLow();
		nTotalSize		= PROTOCOL_M_PACKET_SIZE * nCount;
		nRepeat			= nTotalSize / PROTOCOL_M_PACKET_SIZE;
	}
	else
	{
		nCount			= PROTOCOLM_PACKET_COUNT_FIX;
		nTotalSize		= PROTOCOL_M_PACKET_SIZE * nCount;
		nRepeat			= nTotalSize / PROTOCOL_M_PACKET_SIZE;
	}

	PACKET_M_IMAGE_DATA* pPacketData = pQueueBufferCopy;

	for (int i = 0; i < nRepeat; i++)
	{
		if (!GetThreadStatus()) { break; }

		PACKET_M_IMAGE_DATA_CONTENT* pTemp			= (PACKET_M_IMAGE_DATA_CONTENT*)malloc(sizeof(PACKET_M_IMAGE_DATA_CONTENT));
		PACKET_M_IMAGE_DATA_CONTENT* pDataContent	= pPacketData->QueueData.front();

		// Param Data
		pTemp->PacketNumber		= pDataContent->PacketNumber;
		pTemp->EM1024X			= pDataContent->EM1024X;
		pTemp->EM1024Y			= pDataContent->EM1024Y;
		pTemp->Detector_1		= pDataContent->Detector_1;
		pTemp->Detector_2		= pDataContent->Detector_2;
		pTemp->Detector_3		= pDataContent->Detector_3;
		pTemp->Detector_4		= pDataContent->Detector_4;
		pTemp->Zr_IO			= pDataContent->Zr_IO;
		pTemp->BS_IO			= pDataContent->BS_IO;
		// Param Data

		m_qPacket_M.push(pTemp);

		delete pDataContent;
		pDataContent = nullptr;

		pPacketData->QueueData.pop();
	}
}

void MaskImageReconstructorView::m_fnImageData_BS(PACKET_M_IMAGE_DATA* pQueueBufferCopy)
{
	int nCount			= 0;
	int nTotalSize		= 0;
	int nRepeat			= 0;

	if ((m_pTcpClient->GetRingBufferReceiveIndex(static_cast<int>(CLIENT_SOCKET::CH_1)) + 1) == m_fnReceivePacketCountCheck() &&
		m_pTcpClient->GetScanPacketLast(static_cast<int>(CLIENT_SOCKET::CH_1)) == TRUE)
	{
		nCount			= m_fnGetPacketModeCountLow();
		nTotalSize		= PROTOCOL_M_PACKET_SIZE * nCount;
		nRepeat			= nTotalSize / PROTOCOL_M_PACKET_SIZE;
	}
	else
	{
		nCount			= PROTOCOLM_PACKET_COUNT_FIX;
		nTotalSize		= PROTOCOL_M_PACKET_SIZE * nCount;
		nRepeat			= nTotalSize / PROTOCOL_M_PACKET_SIZE;
	}

	PACKET_M_IMAGE_DATA* pPacketData = pQueueBufferCopy;

	for (int i = 0; i < nRepeat; i++)
	{
		if (!GetThreadStatus()) { break; }

		PACKET_M_IMAGE_DATA_CONTENT* pTemp			= (PACKET_M_IMAGE_DATA_CONTENT*)malloc(sizeof(PACKET_M_IMAGE_DATA_CONTENT));
		PACKET_M_IMAGE_DATA_CONTENT* pDataContent	= pPacketData->QueueData.front();

		// Param Data
		pTemp->PacketNumber		= pDataContent->PacketNumber;
		pTemp->EM1024X			= pDataContent->EM1024X;
		pTemp->EM1024Y			= pDataContent->EM1024Y;
		pTemp->Detector_1		= pDataContent->Detector_1;
		pTemp->Detector_2		= pDataContent->Detector_2;
		pTemp->Detector_3		= pDataContent->Detector_3;
		pTemp->Detector_4		= pDataContent->Detector_4;
		pTemp->Zr_IO			= pDataContent->Zr_IO;
		pTemp->BS_IO			= pDataContent->BS_IO;
		// Param Data

		m_qPacket_BS.push(pTemp);

		delete pDataContent;
		pDataContent = nullptr;

		pPacketData->QueueData.pop();
	}
}

void MaskImageReconstructorView::m_fnImageData_P_As_Point(PACKET_M_IMAGE_DATA* pQueueBufferCopy)
{
	int nCount			= 0;
	int nTotalSize		= 0;
	int nRepeat			= 0;

	if ((m_pTcpClient->GetRingBufferReceiveIndex(static_cast<int>(CLIENT_SOCKET::CH_1)) + 1) == m_fnReceivePacketCountCheck() &&
		m_pTcpClient->GetScanPacketLast(static_cast<int>(CLIENT_SOCKET::CH_1)) == TRUE)
	{
		nCount			= m_fnGetPacketModeCountLow();
		nTotalSize		= PROTOCOL_M_PACKET_SIZE * nCount;
		nRepeat			= nTotalSize / PROTOCOL_M_PACKET_SIZE;
	}
	else
	{
		nCount			= PROTOCOLM_PACKET_COUNT_FIX;
		nTotalSize		= PROTOCOL_M_PACKET_SIZE * nCount;
		nRepeat			= nTotalSize / PROTOCOL_M_PACKET_SIZE;
	}

	PACKET_M_IMAGE_DATA* pPacketData = pQueueBufferCopy;

	for (int i = 0; i < nRepeat; i++)
	{
		if (!GetThreadStatus()) { break; }

		PACKET_M_IMAGE_DATA_CONTENT* pTemp			= (PACKET_M_IMAGE_DATA_CONTENT*)malloc(sizeof(PACKET_M_IMAGE_DATA_CONTENT));
		PACKET_M_IMAGE_DATA_CONTENT* pDataContent	= pPacketData->QueueData.front();

		// Param Data
		pTemp->PacketNumber		= pDataContent->PacketNumber;
		pTemp->EM1024X			= pDataContent->EM1024X;
		pTemp->EM1024Y			= pDataContent->EM1024Y;
		pTemp->Detector_1		= pDataContent->Detector_1;
		pTemp->Detector_2		= pDataContent->Detector_2;
		pTemp->Detector_3		= pDataContent->Detector_3;
		pTemp->Detector_4		= pDataContent->Detector_4;
		pTemp->Zr_IO			= pDataContent->Zr_IO;
		pTemp->BS_IO			= pDataContent->BS_IO;
		// Param Data

		m_qPacket_P_As_Point.push(pTemp);

		delete pDataContent;
		pDataContent = nullptr;

		pPacketData->QueueData.pop();
	}
}

void MaskImageReconstructorView::m_fnImageData_A(PACKET_A_IMAGE_DATA* pQueueBufferCopy)
{
	int nRawImageCount	= (m_fnPixelWidth() * m_fnPixelHeight()) - 1;
	int nCount			= m_nPacketCount;
	int nTotalSize		= PROTOCOL_A_PACKET_SIZE * nCount;
	int nRepeat			= nTotalSize / PROTOCOL_A_PACKET_SIZE;
	
	PACKET_A_IMAGE_DATA*			pPacketData		= pQueueBufferCopy;
	PACKET_A_IMAGE_DATA_CONTENT*	pTemp			= (PACKET_A_IMAGE_DATA_CONTENT*)malloc(sizeof(PACKET_A_IMAGE_DATA_CONTENT));
	PACKET_A_IMAGE_DATA_CONTENT*	pDataContent	= pPacketData->QueueData.front();

	// Param Data
	pTemp->PacketNumber		= pDataContent->PacketNumber;
	pTemp->EM1024X			= pDataContent->EM1024X;
	pTemp->EM1024Y			= pDataContent->EM1024Y;
	pTemp->Detector_1		= pDataContent->Detector_1;
	pTemp->Detector_2		= pDataContent->Detector_2;
	pTemp->Detector_3		= pDataContent->Detector_3;
	pTemp->Detector_4		= pDataContent->Detector_4;
	pTemp->Zr_IO			= pDataContent->Zr_IO;
	pTemp->BS_IO			= pDataContent->BS_IO;
	pTemp->DetectorSub_1	= pDataContent->DetectorSub_1;
	pTemp->DetectorSub_2	= pDataContent->DetectorSub_2;
	pTemp->DetectorSub_3	= pDataContent->DetectorSub_3;
	pTemp->DetectorSub_4	= pDataContent->DetectorSub_4;
	// Param Data

	m_qPacket_A.push(pTemp);

	delete pDataContent;
	pDataContent = nullptr;

	pPacketData->QueueData.pop();
}

CString MaskImageReconstructorView::m_fnGetCurrentTimeFunction()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"[%4d-%02d-%02d %02d:%02d:%02d.%03d] : ",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay,
		SystemTime.wHour,
		SystemTime.wMinute,
		SystemTime.wSecond,
		SystemTime.wMilliseconds);

	return strTime;
}

CString MaskImageReconstructorView::m_fnGetCurruntDateFunction()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"%4d%02d%02d",
		SystemTime.wYear,
		SystemTime.wMonth,
		SystemTime.wDay);

	return strTime;
}

CString MaskImageReconstructorView::m_fnGetCurrentTime()
{
	SYSTEMTIME		SystemTime;
	CString			strTime;

	::GetLocalTime(&SystemTime);

	strTime.Format(
		"[ %02d:%02d:%02d.%03d ]\t",
		SystemTime.wHour,
		SystemTime.wMinute,
		SystemTime.wSecond,
		SystemTime.wMilliseconds);

	return strTime;
}

void MaskImageReconstructorView::OnNMDblclkListReciepe(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LPNMITEMACTIVATE pNMITEM = (LPNMITEMACTIVATE)pNMHDR;

	int m_nSavedItem = pNMITEM->iItem;
	int m_nSavedSubitem = pNMITEM->iSubItem;

	if (pNMITEM->iItem != -1)
	{
		if (m_nSavedSubitem == 1)
		{
			CRect rect;

			if (pNMITEM->iSubItem == 0)
			{
				m_ctlImageRecipe.GetItemRect(pNMITEM->iItem, rect, LVIR_BOUNDS);
				rect.right = rect.left + m_ctlImageRecipe.GetColumnWidth(0);
			}
			else
			{
				m_ctlImageRecipe.GetSubItemRect(pNMITEM->iItem, pNMITEM->iSubItem, LVIR_BOUNDS, rect);
			}

			m_ctlImageRecipe.ClientToScreen(rect);
			this->ScreenToClient(rect);

			m_ctlEditImageRecipe.ShowWindow(TRUE);
			m_ctlEditImageRecipe.SetWindowText(m_ctlImageRecipe.GetItemText(pNMITEM->iItem, pNMITEM->iSubItem));
			m_ctlEditImageRecipe.SetWindowPos(NULL, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_SHOWWINDOW);
			m_ctlEditImageRecipe.SetFocus();
		}
	}

	*pResult = 0;
}


void MaskImageReconstructorView::OnNMClickListReciepe(NMHDR* pNMHDR, LRESULT* pResult)
{
	//LPNMITEMACTIVATE pNMItemActivate = reinterpret_cast<LPNMITEMACTIVATE>(pNMHDR);

	LPNMITEMACTIVATE pNMITEM = (LPNMITEMACTIVATE)pNMHDR;

	m_nSavedItem = m_nSavedSubitem = -1;
	m_ctlEditImageRecipe.SetWindowPos(NULL, 0, 0, 0, 0, SWP_HIDEWINDOW);

	if (pNMITEM->iItem != -1)
	{
		m_nSavedItem = pNMITEM->iItem;
		m_nSavedSubitem = pNMITEM->iSubItem;
	}

	*pResult = 0;
}

void MaskImageReconstructorView::m_fnImageInfoList()
{
	m_ctlImageRecipe.DeleteAllItems();

	m_ctlImageRecipe.ModifyStyle(LVS_OWNERDRAWFIXED, 0, 0);
	m_ctlImageRecipe.SetExtendedStyle(LVS_EX_FULLROWSELECT | LVS_EX_GRIDLINES);

	m_ctlImageRecipe.InsertColumn(0, _T("                 Menu"),	LVCFMT_LEFT,	120, -1); 
	m_ctlImageRecipe.InsertColumn(1, _T("Value"),					LVCFMT_CENTER,	70 , -1); 
	m_ctlImageRecipe.InsertColumn(2, _T("Description"),				LVCFMT_CENTER,	144, -1); 

	CString strTemp;

	m_nImageFOV_X = 99900;
	strTemp.Format(_T("%d"), m_nImageFOV_X);

	m_ctlImageRecipe.InsertItem	(0, _T("Image X FOV")); 
	m_ctlImageRecipe.SetItem	(0, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(0, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nImageFOV_Y = 99900;
	strTemp.Format(_T("%d"), m_nImageFOV_Y);

	m_ctlImageRecipe.InsertItem	(1, _T("Image Y FOV")); 
	m_ctlImageRecipe.SetItem	(1, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(1, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nScanGrid_X = 100;
	strTemp.Format(_T("%d"), m_nScanGrid_X);

	m_ctlImageRecipe.InsertItem	(2, _T("Scan X Grid")); 
	m_ctlImageRecipe.SetItem	(2, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(2, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL );

	m_nScanGrid_Y = 100;
	strTemp.Format(_T("%d"), m_nScanGrid_Y);

	m_ctlImageRecipe.InsertItem	(3, _T("Scan Y Grid"));
	m_ctlImageRecipe.SetItem	(3, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL);
	m_ctlImageRecipe.SetItem	(3, 2, LVIF_TEXT, _T("um"), 0, 0, 0, NULL);

	m_nInterpolationCount = 1;
	strTemp.Format(_T("%d"), m_nInterpolationCount);

	m_ctlImageRecipe.InsertItem	(4, _T("Interpolation Count")); 
	m_ctlImageRecipe.SetItem	(4, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(4, 2, LVIF_TEXT, _T("Interplation Image Count"), 0, 0, 0, NULL );

	m_nAverageCount = 1;
	strTemp.Format(_T("%d"), m_nAverageCount);

	m_ctlImageRecipe.InsertItem	(5, _T("Average Count")); 
	m_ctlImageRecipe.SetItem	(5, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(5, 2, LVIF_TEXT, _T("Average Image Count"), 0, 0, 0, NULL );

	m_nSetCount = 1;
	strTemp.Format(_T("%d"), m_nSetCount);

	m_ctlImageRecipe.InsertItem	(6, _T("Set Count")); 
	m_ctlImageRecipe.SetItem	(6, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(6, 2, LVIF_TEXT, _T("Set Image Count"), 0, 0, 0, NULL );

	m_nScanDirection = 1;
	strTemp.Format(_T("%d"), m_nScanDirection);

	m_ctlImageRecipe.InsertItem	(7, _T("Scan Direction")); 
	m_ctlImageRecipe.SetItem	(7, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL );
	m_ctlImageRecipe.SetItem	(7, 2, LVIF_TEXT, _T("Ex) 1 : V / 0 : H"), 0, 0, 0, NULL );

	m_nPacketCount = 1000;
	strTemp.Format(_T("%d"), m_nPacketCount);

	m_ctlImageRecipe.InsertItem	(8, _T("Packet Count"));
	m_ctlImageRecipe.SetItem	(8, 1, LVIF_TEXT, strTemp, 0, 0, 0, NULL);
	m_ctlImageRecipe.SetItem	(8, 2, LVIF_TEXT, _T("Data Packet Count"), 0, 0, 0, NULL);

	SetDlgItemText(IDC_EDIT_M_PACKET_COUNT, _T("1000"));
	SetDlgItemText(IDC_EDIT_A_PACKET_COUNT, _T("1000"));
	SetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, _T("5000"));
}

void MaskImageReconstructorView::OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct)
{
	if (nIDCtl == IDC_LIST_RECIEPE)
	{
		lpMeasureItemStruct->itemHeight += 10;
	}
	CFormView::OnMeasureItem(nIDCtl, lpMeasureItemStruct);
}

BOOL MaskImageReconstructorView::PreTranslateMessage(MSG* pMsg)
{
	if (pMsg->message == WM_KEYDOWN) {
		if (pMsg->wParam == VK_RETURN) {

			if (pMsg->hwnd == GetDlgItem(IDC_EDIT_IMAGE_RECIPE)->GetSafeHwnd())
			{
				CString str;
				GetDlgItem(IDC_EDIT_IMAGE_RECIPE)->GetWindowText(str);
				m_ctlImageRecipe.SetItemText(m_nSavedItem, m_nSavedSubitem, str);

				switch (m_nSavedItem)
				{
					case 0: { m_nImageFOV_X			= _ttof(str);  break; }
					case 1: { m_nImageFOV_Y			= _ttof(str);  break; }
					case 2: { m_nScanGrid_X			= _ttoi(str);  break; }
					case 3: { m_nScanGrid_Y			= _ttoi(str);  break; }
					case 4: { m_nInterpolationCount	= _ttoi(str);  break; }
					case 5: { m_nAverageCount		= _ttoi(str);  break; }
					case 6: { m_nSetCount			= _ttoi(str);  break; }
					case 7: { m_nScanDirection		= _ttoi(str);  break; }
					case 8: { m_nPacketCount		= _ttoi(str);  break; }
				}

				GetDlgItem(IDC_EDIT_IMAGE_RECIPE)->ShowWindow(SW_HIDE);
			}

			return TRUE;
		}

		if (pMsg->wParam == VK_ESCAPE)
		{
			return TRUE;
		}
	}

	return CFormView::PreTranslateMessage(pMsg);
}

void MaskImageReconstructorView::m_fnSequenceDataInit()
{
	ReCreateThread();
	ReCreateImageThread();

	m_nQueueRingCount				= 0;
	m_nPixelCountCheck				= 0;
	m_nRawCountCheck				= 0;
	m_nRawInterpolationCountCheck	= 0;
	m_nInterpolationCountCheck		= 0;
	m_nAverageCountCheck			= 0;
	m_nScanSetCountCheck			= 0;
	m_nPacketNumberCheck			= 0;
	m_nPixelPosX					= 0;
	m_nPixelPosY					= 0;
	m_dScanAreaThresholdX			= 0;
	m_dScanAreaThresholdY			= 0;
	m_bLastScanCheck				= FALSE;
	m_bLastScanSyncCheck			= FALSE;

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), FALSE);
	SetRunPacketTimeOutStatus(FALSE);

	m_pTcpClient->BufferIndexInit(static_cast<int>(CLIENT_SOCKET::CH_1));
	
	m_fnBufferSizeChange();
//	m_fnImageCountDataUpdate();
	m_fnImageSizeChangeAndInit();
	m_fnMeasureSeqDataInit();
	m_fnSharedMemoryAllInit();

	for (int i = 0; i < MULTI_CORE_QUEUE_COUNT; i++)
	{
		m_pQueueThread[i]->DeleteMainThreadVector();
		m_pQueueThread[i]->DeleteRawVector();
		m_pQueueThread[i]->DeleteInterpolationVector();
		m_pQueueThread[i]->DeleteAverageVector();
		m_pQueueThread[i]->ReCreateThread(i);
	}

	InitializeCriticalSection(&m_pImageThreadCritical);
}

void MaskImageReconstructorView::m_fnImageCountDataUpdate()
{
	CString strTemp;

	strTemp.Format(_T("Raw Image Count : %d"), m_nRawCountCheck);
	SetDlgItemText(IDC_STATIC_RAW_COUNT, strTemp);

	strTemp.Format(_T("Raw Interpolation Image Count : %d"), m_nRawInterpolationCountCheck);
	SetDlgItemText(IDC_STATIC_RAW_INTERPOLATION_COUNT, strTemp);

	strTemp.Format(_T("Interpolation Image Count : %d"), m_nInterpolationCountCheck);
	SetDlgItemText(IDC_STATIC_INTERPOLATION_COUNT, strTemp);

	strTemp.Format(_T("Average Image Count : %d"), m_nAverageCountCheck);
	SetDlgItemText(IDC_STATIC_AVERAGE_COUNT, strTemp);
}

void MaskImageReconstructorView::m_fnOnCreateQueueThread()
{
	for (int i = 0; i < MULTI_CORE_QUEUE_COUNT; i++) { if (m_pQueueThread[i] == nullptr) { m_pQueueThread[i] = new CQueueThread(this, m_pTcpClient, i); } }
}

void MaskImageReconstructorView::OnBnClickedButtonProtocolAdamRecipe()
{
	COM_ADAM_RECIPE_DATA PacketData;

	PacketData.ImageFOV_X			= m_nImageFOV_X;
	PacketData.ImageFOV_Y			= m_nImageFOV_Y;
	PacketData.ScanGrid_X			= m_nScanGrid_X;
	PacketData.ScanGrid_Y			= m_nScanGrid_Y;
	PacketData.InterpolationCount	= m_nInterpolationCount;
	PacketData.AverageCount			= m_nAverageCount;
	PacketData.SetCount				= m_nSetCount;
	PacketData.PacketCount			= m_nPacketCount;

	OnComAdamRecipe(nullptr, &PacketData);
}

void MaskImageReconstructorView::ReCreateThread()
{
	DeleteThread();

	if (m_pThread == nullptr)
	{
		m_hEvent = CreateEventA(NULL, FALSE, FALSE, NULL);
		ResetEvent(m_hEvent);
		m_pThread = AfxBeginThread(Thread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
		m_pThread->ResumeThread();
	}
}

BOOL MaskImageReconstructorView::DeleteThread()
{
	StopThread();
	Sleep(100);
	if (m_pThread != nullptr)
	{
		InitDynamicVariable();
		DWORD dw;
		::GetExitCodeThread(m_pThread->m_hThread, &dw);
		if (dw == STILL_ACTIVE)
		{
			m_pThread->SuspendThread();
			TerminateThread(m_pThread->m_hThread, 0);
			CloseHandle(m_pThread->m_hThread);
		}
		m_pThread->m_hThread = NULL;
		delete m_pThread;
		m_pThread = nullptr;
	}

	if (m_hEvent != NULL)
	{
		CloseHandle(m_hEvent);
		m_hEvent = NULL;
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::InitDynamicVariable()
{
	return TRUE;
}

void MaskImageReconstructorView::ReCreateImageThread()
{
	DeleteImageThread();

	if (m_pImageThread == nullptr)
	{
		m_hImageEvent = CreateEventA(NULL, FALSE, FALSE, NULL);
		ResetEvent(m_hImageEvent);
		m_pImageThread = AfxBeginThread(ImageThread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
		m_pImageThread->ResumeThread();
	}
}

BOOL MaskImageReconstructorView::DeleteImageThread()
{
	StopImageThread();
	Sleep(100);
	if (m_pImageThread != nullptr)
	{
		InitDynamicImageVariable();
		DWORD dw;
		::GetExitCodeThread(m_pImageThread->m_hThread, &dw);
		if (dw == STILL_ACTIVE)
		{
			m_pImageThread->SuspendThread();
			TerminateThread(m_pImageThread->m_hThread, 0);
			CloseHandle(m_pImageThread->m_hThread);
		}
		m_pImageThread->m_hThread = NULL;
		delete m_pImageThread;
		m_pImageThread = nullptr;
	}

	if (m_hImageEvent != NULL)
	{
		CloseHandle(m_hImageEvent);
		m_hImageEvent = NULL;
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::InitDynamicImageVariable()
{

	return TRUE;
}

void MaskImageReconstructorView::m_fnRefreshSocketRect()
{
	InvalidateRect(m_rcSocketConnect, FALSE);
}

void MaskImageReconstructorView::m_fnRefreshEqpStatusRect()
{
	InvalidateRect(m_rcEqpStatus, FALSE);
}

void MaskImageReconstructorView::m_fnRefreshManualRect()
{
	InvalidateRect(m_rcManualMode, FALSE);
}

void MaskImageReconstructorView::m_fnRefreshDisplayModeRect()
{
	InvalidateRect(m_rcDisplayMode, FALSE);
}

void MaskImageReconstructorView::m_fnDisplayImage(Mat* pMatImage, unsigned int nControlView, int* nPictureControlX, int* nPictureControlY, int* nResizeX, int* nResizeY, BITMAPINFO*& bitmapInfo)
{
	Mat MatManualResize;

	resize(*pMatImage, MatManualResize, cv::Size(*nResizeX - 2, *nResizeY), INTER_LINEAR);

	CWnd* pWnd_ImageTraget;
	pWnd_ImageTraget = GetDlgItem(nControlView);

	CClientDC dcImageTraget(pWnd_ImageTraget);

	dcImageTraget.SetStretchBltMode(MAXSTRETCHBLTMODE);
	::StretchDIBits(
		dcImageTraget.GetSafeHdc(),
		0,
		0,
		*nPictureControlX,
		*nPictureControlY,
		0,
		0,
		MatManualResize.cols,
		MatManualResize.rows,
		MatManualResize.data,
		bitmapInfo,
		DIB_RGB_COLORS,
		SRCCOPY
	);
}

void MaskImageReconstructorView::m_fnDisplayImageInfoUpdate(Mat* pMatImage, unsigned int nControlView, int* nPictureControlX, int* nPictureControlY, int* nResizeX, int* nResizeY, BITMAPINFO*& bitmapInfo)
{
	RECT rcImageTraget;

	CWnd* pWnd_ImageTraget;
	pWnd_ImageTraget = GetDlgItem(nControlView);

	CClientDC dcImageTraget(pWnd_ImageTraget);
	pWnd_ImageTraget->GetClientRect(&rcImageTraget);

	*nPictureControlX = rcImageTraget.right;
	*nPictureControlY = rcImageTraget.bottom;

	double dMagnificationX = (double)pMatImage->cols / (double)*nPictureControlX;
	double dMagnificationY = (double)pMatImage->rows / (double)*nPictureControlY;

	double dMagnification = (dMagnificationX < dMagnificationY) ? dMagnificationY : dMagnificationX;

	*nResizeX = ((int)(floor((double)pMatImage->cols / dMagnification)));
	*nResizeY = ((int)(floor((double)pMatImage->rows / dMagnification)));

	if (*nResizeX % 2 == TRUE)  { *nResizeX -= 1; }
	if (*nResizeY % 2 == FALSE) { *nResizeY -= 1; }

	int nDummyX = (*nResizeX < *nPictureControlX) ? (*nPictureControlX - *nResizeX) / 2 : 0;
	int nDummyY = (*nResizeY < *nPictureControlY) ? (*nPictureControlY - *nResizeY) / 2 : 0;

	if (bitmapInfo != nullptr) { free(bitmapInfo); }

	bitmapInfo = (BITMAPINFO*)malloc(sizeof(BITMAPINFO));

	bitmapInfo->bmiHeader.biSize			= sizeof(BITMAPINFOHEADER);				 
	bitmapInfo->bmiHeader.biPlanes			= 1;									 
	bitmapInfo->bmiHeader.biCompression		= BI_RGB;								 
	bitmapInfo->bmiHeader.biSizeImage		= 0;									 
	bitmapInfo->bmiHeader.biXPelsPerMeter	= 0;									 
	bitmapInfo->bmiHeader.biYPelsPerMeter	= 0;									 
	bitmapInfo->bmiHeader.biClrUsed			= 0;									 
	bitmapInfo->bmiHeader.biClrImportant	= 0;									 
	bitmapInfo->bmiHeader.biWidth			= *nResizeX - 2;
	bitmapInfo->bmiHeader.biHeight			= -(*nResizeY);
	bitmapInfo->bmiHeader.biBitCount		= 3 * 8;		 
}

void MaskImageReconstructorView::OnNMCustomdrawListReciepe(NMHDR* pNMHDR, LRESULT* pResult)
{
	LPNMCUSTOMDRAW pNMCD;

	pNMCD = reinterpret_cast<LPNMCUSTOMDRAW>(pNMHDR);
	 
	LPNMLVCUSTOMDRAW  lplvcd = (LPNMLVCUSTOMDRAW)pNMHDR;

	int iRow = (int)lplvcd->nmcd.dwItemSpec;
	 
	switch(lplvcd->nmcd.dwDrawStage)
	{
		case CDDS_PREPAINT :
		{
			*pResult = CDRF_NOTIFYITEMDRAW;
			return;
		}
			 
		// Modify item text and or background
		case CDDS_ITEMPREPAINT:
		{
			lplvcd->clrText = RGB(0,0,0);

			// If you want the sub items the same as the item,
			// set *pResult to CDRF_NEWFONT
			*pResult = CDRF_NOTIFYSUBITEMDRAW;
			return;
		}
			 
		// Modify sub item text and/or background
		case CDDS_SUBITEM | CDDS_PREPAINT | CDDS_ITEM:
		{
			// 홀수열, 짝수열의 배경색 재설정
			if(iRow % 2)	{ lplvcd->clrTextBk = RGB(255, 255, 255); }
			else			{ lplvcd->clrTextBk = RGB(230, 230, 230); }
				
			*pResult = CDRF_DODEFAULT;
			return;
		}
	}

	*pResult = 0;
}

void MaskImageReconstructorView::m_fnRawTackTimeUpdate()
{
	int nImageWidth		= (m_nImageFOV_X * 100) / m_nScanGrid_X;
	int nImageHeight	= (m_nImageFOV_Y * 100) / m_nScanGrid_Y;

	__int64	elapsed		= m_lEndtime_Raw.QuadPart - m_lBeginTime_Raw.QuadPart;
	double	duringtime	= (double)elapsed / (double)m_lFrequency_Raw.QuadPart * 1000;

	CString strTimeTemp;

	strTimeTemp.Format(_T("1 Image Receive Time : %.3lf (Sec)"), (duringtime / 1000));
	SetDlgItemText(IDC_STATIC_PACKET_SEND_TIME, strTimeTemp);

	int		nHeaderSize		= PROTOCOL_HEADER_SIZE;
	int		nContentSize	= 0;

	if		(m_nPacketMode == MODE_M_PACKET)	{ nContentSize = sizeof(PACKET_M_IMAGE_BYTE_CONTENT); }
	else if (m_nPacketMode == MODE_A_PACKET)	{ nContentSize = sizeof(PACKET_A_IMAGE_BYTE_CONTENT); }
	else if (m_nPacketMode == MODE_RUN_PACKET)	{ nContentSize = sizeof(PACKET_M_IMAGE_BYTE_CONTENT); }
	
	int		nSendSize		= (nHeaderSize + (nContentSize * nImageWidth)) * nImageHeight;
	int		nTotalByte		= nImageWidth * nImageHeight;
	double	dByteBps		= (nTotalByte / (duringtime / 1000));
	double	dByteKbps		= dByteBps / 1024;
	double	dByteMbps		= dByteKbps / 1024;

	strTimeTemp.Format(_T("1 Sec Receive Byte Time : %.3lf (Kbps)"), dByteKbps);
	SetDlgItemText(IDC_STATIC_BYTE_BPS_TIME, strTimeTemp);
}

void MaskImageReconstructorView::m_fnInterpolationTackTimeUpdate()
{
	__int64	elapsed		= m_lEndtime_Interpolation.QuadPart - m_lBeginTime_Interpolation.QuadPart;
	double	duringtime	= (double)elapsed / (double)m_lFrequency_Interpolation.QuadPart * 1000;

	CString strTimeTemp;

	strTimeTemp.Format(_T("Interpolation Cal Time : %.3lf (ms)"), duringtime);
	SetDlgItemText(IDC_STATIC_CAL_INTERPOLATION_TIME, strTimeTemp);
}

void MaskImageReconstructorView::m_fnAverageTackTimeUpdate()
{
	__int64	elapsed		= m_lEndtime_Average.QuadPart - m_lBeginTime_Average.QuadPart;
	double	duringtime	= (double)elapsed / (double)m_lFrequency_Average.QuadPart * 1000;

	CString strTimeTemp;

	strTimeTemp.Format(_T("Average Cal Time : %.3lf (ms)"), duringtime);
	SetDlgItemText(IDC_STATIC_CAL_AVERAGE_TIME, strTimeTemp);
}

void MaskImageReconstructorView::m_fnImageSizeChangeAndInit()
{
	int nImageWidth		= m_fnPixelWidth();
	int nImageHeight	= m_fnPixelHeight();

	if (m_nBeforeImageHeight != nImageHeight || m_nBeforeImageWidth != nImageWidth)
	{
		delete m_dMatActualScanPos_X; m_dMatActualScanPos_X = nullptr;
		delete m_dMatActualScanPos_Y; m_dMatActualScanPos_Y = nullptr;

		delete m_dMatStaticScanPos_X; m_dMatStaticScanPos_X = nullptr;
		delete m_dMatStaticScanPos_Y; m_dMatStaticScanPos_Y = nullptr;

		delete m_dMatBufferActualScanPos_X; m_dMatBufferActualScanPos_X = nullptr;
		delete m_dMatBufferActualScanPos_Y; m_dMatBufferActualScanPos_Y = nullptr;

		delete m_dMatBufferStaticScanPos_X; m_dMatBufferStaticScanPos_X = nullptr;
		delete m_dMatBufferStaticScanPos_Y; m_dMatBufferStaticScanPos_Y = nullptr;

		delete m_dMat_D1; m_dMat_D1 = nullptr;
		delete m_dMat_D2; m_dMat_D2 = nullptr;
		delete m_dMat_D3; m_dMat_D3 = nullptr;
		delete m_dMatBuffer_D1; m_dMatBuffer_D1 = nullptr;
		delete m_dMatBuffer_D2; m_dMatBuffer_D2 = nullptr;
		delete m_dMatBuffer_D3; m_dMatBuffer_D3 = nullptr;

		delete m_dMat_T; m_dMat_T = nullptr;
		delete m_dMatBuffer_T; m_dMatBuffer_T = nullptr;
		delete m_dMatInterpolation_T; m_dMatInterpolation_T = nullptr;
		delete m_dMatAverage_T; m_dMatAverage_T = nullptr;

		delete m_dMat_R; m_dMat_R = nullptr;
		delete m_dMatBuffer_R; m_dMatBuffer_R = nullptr;
		delete m_dMatInterpolation_R; m_dMatInterpolation_R = nullptr;
		delete m_dMatAverage_R; m_dMatAverage_R = nullptr;

		delete m_MatRawSRC; m_MatRawSRC = nullptr;
		delete m_MatRawBufferSRC; m_MatRawBufferSRC = nullptr;
		delete m_MatInterpolationSRC; m_MatInterpolationSRC = nullptr;
		delete m_MatAverageSRC; m_MatAverageSRC = nullptr;

		delete m_MatRawInterpolationSRC; m_MatRawInterpolationSRC = nullptr;
		free(m_RawInterpolationPos_X); m_RawInterpolationPos_X = nullptr;
		free(m_RawInterpolationPos_Y); m_RawInterpolationPos_Y = nullptr;
		free(m_RawInterpolationOutput); m_RawInterpolationOutput = nullptr;

		free(m_bySharedMemoryRaw); m_bySharedMemoryRaw = nullptr;
		free(m_bySharedMemoryRawInterpolation); m_bySharedMemoryRawInterpolation = nullptr;
		free(m_bySharedMemoryInterpolation); m_bySharedMemoryInterpolation = nullptr;
		free(m_bySharedMemoryAverage); m_bySharedMemoryAverage = nullptr;

		free(m_bySharedMemoryT); m_bySharedMemoryT = nullptr;
		free(m_bySharedMemoryR); m_bySharedMemoryR = nullptr;
		free(m_bySharedMemoryPosX); m_bySharedMemoryPosX = nullptr;
		free(m_bySharedMemoryPosY); m_bySharedMemoryPosY = nullptr;

		m_dMatActualScanPos_X		= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatActualScanPos_Y		= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

		m_dMatStaticScanPos_X		= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatStaticScanPos_Y		= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

		m_dMatBufferActualScanPos_X = new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatBufferActualScanPos_Y = new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

		m_dMatBufferStaticScanPos_X = new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatBufferStaticScanPos_Y = new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

		m_dMat_D1					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMat_D2					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMat_D3					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatBuffer_D1				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatBuffer_D2				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatBuffer_D3				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

		m_dMat_T					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatBuffer_T				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatInterpolation_T		= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_64F));
		m_dMatAverage_T				= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_64F));

		m_dMat_R					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatBuffer_R				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
		m_dMatInterpolation_R		= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_64F));
		m_dMatAverage_R				= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_64F));

		m_MatRawInterpolationSRC	= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_8UC3));
		m_RawInterpolationPos_X		= (double*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * 3);
		m_RawInterpolationPos_Y		= (double*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * 3);
		m_RawInterpolationOutput	= (double*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * 3);

		m_MatRawSRC					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_8UC3));
		m_MatRawBufferSRC			= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_8UC3));
		m_MatInterpolationSRC		= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_8UC3));
		m_MatAverageSRC				= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_8UC3));
		
		m_bySharedMemoryRaw					= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * nImageHeight) * 3);
		m_bySharedMemoryRawInterpolation	= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		m_bySharedMemoryInterpolation		= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		m_bySharedMemoryAverage				= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);

		m_bySharedMemoryT					= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		m_bySharedMemoryR					= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		m_bySharedMemoryPosX				= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		m_bySharedMemoryPosY				= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));

		memset(m_bySharedMemoryRaw				, 0x00, sizeof(BYTE) * (nImageWidth * nImageHeight) * 3);
		memset(m_bySharedMemoryRawInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		memset(m_bySharedMemoryInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		memset(m_bySharedMemoryAverage			, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);

		memset(m_bySharedMemoryT				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		memset(m_bySharedMemoryR				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		memset(m_bySharedMemoryPosX				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		memset(m_bySharedMemoryPosY				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));

		m_fnMatrixDelete(m_nBeforeImageWidth, m_nBeforeImageHeight);
		m_fnMatrixCreate(nImageWidth, nImageHeight);
		m_fnMatrixInit(nImageWidth, nImageHeight);

		m_fnDisplayImageInfoUpdate(m_MatRawInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION, &m_nMatRawInterpolationControlX, &m_nMatRawInterpolationControlY, &m_nMatRawInterpolationResizeX, &m_nMatRawInterpolationResizeY, m_bitmapInfoMatRawInterpolation);
		m_fnDisplayImageInfoUpdate(m_MatRawSRC, IDC_IMAGE_DISPLAY_THREAD_RAW, &m_nMatRawControlX, &m_nMatRawControlY, &m_nMatRawResizeX, &m_nMatRawResizeY, m_bitmapInfoMatRaw);
		m_fnDisplayImageInfoUpdate(m_MatInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_INTERPOLATION, &m_nMatInterpolationControlX, &m_nMatInterpolationControlY, &m_nMatInterpolationResizeX, &m_nMatInterpolationResizeY, m_bitmapInfoMatInterpolation);
		m_fnDisplayImageInfoUpdate(m_MatAverageSRC, IDC_IMAGE_DISPLAY_THREAD_AVERAGE, &m_nMatAverageControlX, &m_nMatAverageControlY, &m_nMatAverageResizeX, &m_nMatAverageResizeY, m_bitmapInfoMatAverage);
	}
	else
	{
		*m_dMatActualScanPos_X			= Mat::zeros(m_dMatActualScanPos_X->rows, m_dMatActualScanPos_X->cols, CV_64F);
		*m_dMatActualScanPos_Y			= Mat::zeros(m_dMatActualScanPos_Y->rows, m_dMatActualScanPos_Y->cols, CV_64F);

		*m_dMatStaticScanPos_X			= Mat::zeros(m_dMatStaticScanPos_X->rows, m_dMatStaticScanPos_X->cols, CV_64F);
		*m_dMatStaticScanPos_Y			= Mat::zeros(m_dMatStaticScanPos_Y->rows, m_dMatStaticScanPos_Y->cols, CV_64F);

		*m_dMatBufferActualScanPos_X	= Mat::zeros(m_dMatBufferActualScanPos_X->rows, m_dMatBufferActualScanPos_X->cols, CV_64F);
		*m_dMatBufferActualScanPos_Y	= Mat::zeros(m_dMatBufferActualScanPos_Y->rows, m_dMatBufferActualScanPos_Y->cols, CV_64F);

		*m_dMatBufferStaticScanPos_X	= Mat::zeros(m_dMatBufferStaticScanPos_X->rows, m_dMatBufferStaticScanPos_X->cols, CV_64F);
		*m_dMatBufferStaticScanPos_Y	= Mat::zeros(m_dMatBufferStaticScanPos_Y->rows, m_dMatBufferStaticScanPos_Y->cols, CV_64F);

		*m_dMat_T						= Mat::zeros(m_dMat_T->rows, m_dMat_T->cols, CV_64F);
		*m_dMatBuffer_T					= Mat::zeros(m_dMatBuffer_T->rows, m_dMatBuffer_T->cols, CV_64F);
		*m_dMatInterpolation_T			= Mat::zeros(m_dMatInterpolation_T->rows, m_dMatInterpolation_T->cols, CV_64F);
		*m_dMatAverage_T				= Mat::zeros(m_dMatAverage_T->rows, m_dMatAverage_T->cols, CV_64F);

		*m_dMat_R						= Mat::zeros(m_dMat_R->rows, m_dMat_R->cols, CV_64F);
		*m_dMatBuffer_R					= Mat::zeros(m_dMatBuffer_R->rows, m_dMatBuffer_R->cols, CV_64F);
		*m_dMatInterpolation_R			= Mat::zeros(m_dMatInterpolation_R->rows, m_dMatInterpolation_R->cols, CV_64F);
		*m_dMatAverage_R				= Mat::zeros(m_dMatAverage_R->rows, m_dMatAverage_R->cols, CV_64F);

		*m_MatRawInterpolationSRC		= Mat::zeros(m_MatRawInterpolationSRC->rows, m_MatRawInterpolationSRC->cols, CV_8UC3);
		*m_MatRawSRC					= Mat::zeros(m_MatRawSRC->rows, m_MatRawSRC->cols, CV_8UC3);
		*m_MatRawBufferSRC				= Mat::zeros(m_MatRawBufferSRC->rows, m_MatRawBufferSRC->cols, CV_8UC3);
		*m_MatInterpolationSRC			= Mat::zeros(m_MatInterpolationSRC->rows, m_MatInterpolationSRC->cols, CV_8UC3);
		*m_MatAverageSRC				= Mat::zeros(m_MatAverageSRC->rows, m_MatAverageSRC->cols, CV_8UC3);

		memset(m_bySharedMemoryRaw				, 0x00, sizeof(BYTE) * (nImageWidth * nImageHeight) * 3);
		memset(m_bySharedMemoryRawInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		memset(m_bySharedMemoryInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
		memset(m_bySharedMemoryAverage			, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);

		memset(m_bySharedMemoryT				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		memset(m_bySharedMemoryR				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		memset(m_bySharedMemoryPosX				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
		memset(m_bySharedMemoryPosY				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));

		m_fnMatrixInit(nImageWidth, nImageHeight);
	}

	m_nBeforeImageWidth		= nImageWidth;
	m_nBeforeImageHeight	= nImageHeight;
}

void MaskImageReconstructorView::m_fnCreateImageInit()
{
	int nImageWidth		= m_fnPixelWidth();
	int nImageHeight	= m_fnPixelHeight();

	m_dMatActualScanPos_X		= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatActualScanPos_Y		= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

	m_dMatStaticScanPos_X		= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatStaticScanPos_Y		= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

	m_dMatBufferActualScanPos_X = new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatBufferActualScanPos_Y = new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

	m_dMatBufferStaticScanPos_X = new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatBufferStaticScanPos_Y = new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

	m_dMat_D1					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMat_D2					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMat_D3					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatBuffer_D1				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatBuffer_D2				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatBuffer_D3				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));

	m_dMat_T					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatBuffer_T				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatInterpolation_T		= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_64F));
	m_dMatAverage_T				= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_64F));

	m_dMat_R					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatBuffer_R				= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_64F));
	m_dMatInterpolation_R		= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_64F));
	m_dMatAverage_R				= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_64F));

	m_MatRawSRC					= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_8UC3));
	m_MatRawBufferSRC			= new Mat(Mat::zeros(nImageHeight, nImageWidth, CV_8UC3));
	m_MatInterpolationSRC		= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_8UC3));
	m_MatAverageSRC				= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_8UC3));

	m_MatRawInterpolationSRC	= new Mat(Mat::zeros(nImageHeight * IMAGE_MAGNIFICATION, nImageWidth * IMAGE_MAGNIFICATION, CV_8UC3));
	m_RawInterpolationPos_X		= (double*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * 3);
	m_RawInterpolationPos_Y		= (double*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * 3);
	m_RawInterpolationOutput	= (double*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * 3);

	m_bySharedMemoryRaw					= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * nImageHeight) * 3);
	m_bySharedMemoryRawInterpolation	= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
	m_bySharedMemoryInterpolation		= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
	m_bySharedMemoryAverage				= (BYTE*)malloc(sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);

	m_bySharedMemoryT					= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	m_bySharedMemoryR					= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	m_bySharedMemoryPosX				= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	m_bySharedMemoryPosY				= (BYTE*)malloc(sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));

	memset(m_bySharedMemoryRaw				, 0x00, sizeof(BYTE) * (nImageWidth * nImageHeight) * 3);
	memset(m_bySharedMemoryRawInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
	memset(m_bySharedMemoryInterpolation	, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);
	memset(m_bySharedMemoryAverage			, 0x00, sizeof(BYTE) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION) * 3);

	memset(m_bySharedMemoryT				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	memset(m_bySharedMemoryR				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	memset(m_bySharedMemoryPosX				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));
	memset(m_bySharedMemoryPosY				, 0x00, sizeof(double) * (nImageWidth * IMAGE_MAGNIFICATION) * (nImageHeight * IMAGE_MAGNIFICATION));

	m_fnMatrixCreate(nImageWidth, nImageHeight);
	m_fnMatrixInit(nImageWidth, nImageHeight);

	m_fnDisplayImageInfoUpdate(m_MatRawInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION, &m_nMatRawInterpolationControlX, &m_nMatRawInterpolationControlY, &m_nMatRawInterpolationResizeX, &m_nMatRawInterpolationResizeY, m_bitmapInfoMatRawInterpolation);
	m_fnDisplayImageInfoUpdate(m_MatRawSRC, IDC_IMAGE_DISPLAY_THREAD_RAW, &m_nMatRawControlX, &m_nMatRawControlY, &m_nMatRawResizeX, &m_nMatRawResizeY, m_bitmapInfoMatRaw);
	m_fnDisplayImageInfoUpdate(m_MatInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_INTERPOLATION, &m_nMatInterpolationControlX, &m_nMatInterpolationControlY, &m_nMatInterpolationResizeX, &m_nMatInterpolationResizeY, m_bitmapInfoMatInterpolation);
	m_fnDisplayImageInfoUpdate(m_MatAverageSRC, IDC_IMAGE_DISPLAY_THREAD_AVERAGE, &m_nMatAverageControlX, &m_nMatAverageControlY, &m_nMatAverageResizeX, &m_nMatAverageResizeY, m_bitmapInfoMatAverage);

	m_nBeforeImageWidth		= nImageWidth;
	m_nBeforeImageHeight	= nImageHeight;
}

void MaskImageReconstructorView::m_fnBufferSizeChange()
{
	if (m_pTcpClient->GetSocketStatus(static_cast<int>(CLIENT_SOCKET::CH_1)))
	{
		if		(m_nPacketCount <= 10  ) { m_pTcpClient->BufferSizeChange(static_cast<int>(CLIENT_SOCKET::CH_1), 2048);					}
		else if (m_nPacketCount <= 100 ) { m_pTcpClient->BufferSizeChange(static_cast<int>(CLIENT_SOCKET::CH_1), 20480);				}
		else if (m_nPacketCount <= 1000) { m_pTcpClient->BufferSizeChange(static_cast<int>(CLIENT_SOCKET::CH_1), SOCKET_BUFFER_SIZE);	}
	}
}

void MaskImageReconstructorView::m_fnOnMessageStartCreateQueueThread()
{
	CWinThread* pThread = AfxBeginThread(AlgorithmThread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
	pThread->m_bAutoDelete = TRUE;
	pThread->ResumeThread();
}

void MaskImageReconstructorView::m_fnOnMessageStartCreateLineThread()
{
	CWinThread* pThread = AfxBeginThread(LineInterpolationThread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
	pThread->m_bAutoDelete = TRUE;
	pThread->ResumeThread();
}

UINT MaskImageReconstructorView::LineInterpolationThread(LPVOID lpParam)
{
	MaskImageReconstructorView* pThread = (MaskImageReconstructorView*)lpParam;

	pThread->m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		pThread->m_fnFormattedStringTimeLog(_T("********** Start Line Image Sequence!! **********")));

	COM_ADAM_DETECTOR_DATA PacketData;
	PacketData.nRet = COM_RETURN_TYPE::PASS;

	ESOL_PACKET_DATA EsolPacketData;
	pThread->m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'C', 'S', 'A', "MEM", "DET", '@', &EsolPacketData, &PacketData, sizeof(COM_ADAM_DETECTOR_DATA));
	pThread->ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
	pThread->SendProtocolLog(&EsolPacketData);

	BOOL bFirst = FALSE;

	unsigned int nImageWidth	= pThread->m_fnPixelWidth();
	unsigned int nImageHeight	= pThread->m_fnPixelHeight();

	CImageProcessAlgorithm* pPA = pThread->m_pImageProcessAlgorithm;

	uchar* pImageRawData = pThread->m_MatRawInterpolationSRC->data;

	double* pDetector = (double*)pThread->m_dMat_T->data;

	Mat* pMatBufferSRC = new Mat(Mat::zeros(1, nImageWidth, CV_64F));

	double* pMatBuffer = (double*)pMatBufferSRC->data;

	unsigned int nPosY = 0;

	while (pThread->GetImageThreadStatus())
	{
		if (pThread->m_nRawInterpolationCountCheck < pThread->m_nPixelPosY)
		{
			nPosY = pThread->m_nRawInterpolationCountCheck;

			memcpy(pMatBuffer, &pDetector[nPosY * nImageWidth], sizeof(double) * nImageWidth);

			pPA->CalColorBuffer(pMatBufferSRC, pImageRawData, nPosY, nImageWidth, nImageHeight);

			pThread->m_nRawInterpolationCountCheck++;

			pThread->m_fnImageDisplayRawInterpolationUpdate();

			if (pThread->m_bManualModeStatus) { pThread->OnComMemoryRawInterpolationUpdate();		}
			else							  { pThread->OnComMemoryTransmittanceUpdate(&bFirst);	}
		}
		else if (pThread->m_bLastLineCheck)
		{
			nPosY = (nImageHeight - 1);

			memcpy(pMatBuffer, &pDetector[nPosY * nImageWidth], sizeof(double) * nImageWidth);

			pPA->CalColorBuffer(pMatBufferSRC, pImageRawData, nPosY, nImageWidth, nImageHeight);

			pThread->m_nRawInterpolationCountCheck++;

			pThread->m_fnImageDisplayRawInterpolationUpdate();

			if (pThread->m_bManualModeStatus) { pThread->OnComMemoryRawInterpolationUpdate();		}
			else							  { pThread->OnComMemoryTransmittanceUpdate(&bFirst);	}

			pThread->m_bLastLineCheck = FALSE;

			pThread->m_fnOnMessageRawInterpolationDisplayInitUpdate();
		}
		Sleep(1);
	}

	delete pMatBufferSRC; pMatBufferSRC = nullptr;

	pThread->m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		pThread->m_fnFormattedStringTimeLog(_T("********** Finish Line Image Sequence!! **********")));

	return TRUE;
}

UINT MaskImageReconstructorView::AlgorithmThread(LPVOID lpParam)
{
	MaskImageReconstructorView* pThread = (MaskImageReconstructorView*)lpParam;

	int nImageWidth		= pThread->m_fnPixelWidth();
	int nImageHeight	= pThread->m_fnPixelHeight();

	int nQueueThreadRingIndex = 0;

	while (pThread->GetThreadStatus())
	{
		nQueueThreadRingIndex = pThread->m_nQueueRingCount % MULTI_CORE_QUEUE_COUNT;

		CQueueThread* pQueueThread = pThread->m_pQueueThread[nQueueThreadRingIndex];

		if (pQueueThread->GetThreadStatus() == FALSE && pQueueThread->m_nQueueMainIndex == 0)
		{
			pThread->m_pLogViewer->AddLog(
				static_cast<int>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				pThread->m_fnFormattedStringTimeLog(_T("* Mask Image Main Copy Success : %d"), pQueueThread->m_nQueueMainIndex + 1));

			pQueueThread->SetImageWH(nImageWidth, nImageHeight);
			pQueueThread->SetImageScanFOV(pThread->m_nImageFOV_X, pThread->m_nImageFOV_Y);
			pQueueThread->SetImageScanGrid(pThread->m_nScanGrid_X, pThread->m_nScanGrid_Y);
			pQueueThread->SetImageInterpolationCount(pThread->m_nInterpolationCount);
			pQueueThread->CreateQueueMainThread(
				pThread->m_dMatBuffer_D1->data,
				pThread->m_dMatBuffer_D2->data,
				pThread->m_dMatBuffer_D3->data,
				pThread->m_dMatBuffer_T->data,
				pThread->m_dMatBuffer_R->data,
				pThread->m_dMatBufferActualScanPos_X->data,
				pThread->m_dMatBufferActualScanPos_Y->data,
				pThread->m_dMatBufferStaticScanPos_X->data,
				pThread->m_dMatBufferStaticScanPos_Y->data);
			pQueueThread->SetQueueMainThread(pQueueThread->m_nQueueMainIndex - 1);
			pQueueThread->StartThread();
			break;
		}
		else if (pQueueThread->GetThreadStatus() == TRUE && pQueueThread->m_nQueueMainIndex != pThread->m_nInterpolationCount)
		{
			if (pQueueThread->m_nQueueMainIndex == (pThread->m_nInterpolationCount - 1)) { pThread->m_nQueueRingCount++; }

			pThread->m_pLogViewer->AddLog(
				static_cast<int>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				pThread->m_fnFormattedStringTimeLog(_T("* Mask Image Main Copy Success : %d"), pQueueThread->m_nQueueMainIndex + 1));

			pQueueThread->CreateQueueMainThread(
				pThread->m_dMatBuffer_D1->data,
				pThread->m_dMatBuffer_D2->data,
				pThread->m_dMatBuffer_D3->data,
				pThread->m_dMatBuffer_T->data,
				pThread->m_dMatBuffer_R->data,
				pThread->m_dMatBufferActualScanPos_X->data,
				pThread->m_dMatBufferActualScanPos_Y->data,
				pThread->m_dMatBufferStaticScanPos_X->data,
				pThread->m_dMatBufferStaticScanPos_Y->data);
			pQueueThread->SetQueueMainThread(pQueueThread->m_nQueueMainIndex - 1);
			break;
		}
		else
		{
			pThread->m_pLogViewer->AddLog(
				static_cast<int>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				pThread->m_fnFormattedStringTimeLog(_T("QueueThread[%d] Full Running. Finish."), pThread->m_nQueueRingCount));
		}
	}

	return TRUE;
}

int MaskImageReconstructorView::m_fnPixelWidth()
{
	return (m_nImageFOV_X / m_nScanGrid_X) + 1;
}

int MaskImageReconstructorView::m_fnPixelHeight()
{
	return (m_nImageFOV_Y / m_nScanGrid_Y) + 1;
}

LRESULT MaskImageReconstructorView::OnMessageComServerClientAccept(WPARAM wParam, LPARAM lParam)
{
	CTcpComClientSocket* pClient = (CTcpComClientSocket*)lParam;

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_MESSAGE,
		m_fnFormattedStringTimeLog(_T("OnMessageComServerClientAccept")));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("Server[%d] Connect : IP %s / PORT %d"), pClient->m_nClient, pClient->m_strIP, pClient->m_nPort));

	return TRUE;
}

LRESULT MaskImageReconstructorView::OnMessageComServerClientReceive(WPARAM wParam, LPARAM lParam)
{
	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_MESSAGE,
		m_fnFormattedStringTimeLog(_T("OnMessageComServerClientReceive")));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("Server Receive Socket.")));

	return TRUE;
}

LRESULT MaskImageReconstructorView::OnMessageComServerClientSocketClose(WPARAM wParam, LPARAM lParam)
{
	CTcpComClientSocket* pClient = (CTcpComClientSocket*)lParam;

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_MESSAGE,
		m_fnFormattedStringTimeLog(_T("OnMessageComServerClientSocketClose")));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("Server[%d] Close : IP %s / PORT %d"), pClient->m_nClient, pClient->m_strIP, pClient->m_nPort));

	m_pTcpComClient->CloseClient(pClient->m_nClient);

	SetAutoComSocketConnectThread(TRUE);

	m_fnAutoComSocketConnect();

	return TRUE;
}

LRESULT MaskImageReconstructorView::OnMessageComServerClientProtocol(WPARAM wParam, LPARAM lParam)
{
	ESOL_PACKET_DATA* pData = (ESOL_PACKET_DATA*)lParam;
	if (!Protocol(pData)) { ReceiveProtocolLog(pData, FALSE); }
	return TRUE;
}

LRESULT MaskImageReconstructorView::OnMessageServerClientAccept(WPARAM wParam, LPARAM lParam)
{
	CTcpClientSocket* pClient = (CTcpClientSocket*)lParam;

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("Server[%d] Connect : IP %s / PORT %d"), pClient->m_nChannel, pClient->m_strIP, pClient->m_nPort));

	m_fnRefreshSocketRect();
	return TRUE;
}

LRESULT MaskImageReconstructorView::OnMessageServerClientReceive(WPARAM wParam, LPARAM lParam)
{
	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("Server Receive Socket.")));

	return TRUE;
}

LRESULT MaskImageReconstructorView::OnMessageServerClientSocketClose(WPARAM wParam, LPARAM lParam)
{
	CTcpClientSocket* pClient = (CTcpClientSocket*)lParam;

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("Server[%d] Close : IP %s / PORT %d"), pClient->m_nChannel, pClient->m_strIP, pClient->m_nPort));

	m_pTcpClient->CloseClient(pClient->m_nChannel, pClient->m_strIP, pClient->m_nPort);

	m_fnRefreshSocketRect();

	SetAutoSocketConnectThread(TRUE);

	m_fnAutoSocketConnect();

	return TRUE;
}

void MaskImageReconstructorView::OnBnClickedButtonProtocolEm1024Reset()
{
	COM_ADAM_EM1024_RESET_DATA PacketData;

	OnComAdamEm1024Reset(nullptr, &PacketData);
}

void MaskImageReconstructorView::OnBnClickedButtonProtocolAPacketRun()
{
	COM_ADAM_AVERAGE_COUNT_DATA PacketDataA;

	PacketDataA.nCount = m_nAPacketCount;

	OnComAdamAverageCount(nullptr, &PacketDataA);

	COM_ADAM_A_PACKET_RUN_DATA PacketDataB;

	OnComAdamAPacketRun(nullptr, &PacketDataB);
}

void MaskImageReconstructorView::OnBnClickedButtonProtocolMPacketRun()
{
	COM_ADAM_M_PACKET_RUN_DATA PacketData;

	PacketData.nCount = m_nMPacketCount;

	OnComAdamMPacketRun(nullptr, &PacketData);
}

void MaskImageReconstructorView::OnBnClickedButtonProtocolAdamRun()
{
	COM_ADAM_RUN_DATA PacketData;

	OnComAdamRun(nullptr, &PacketData);
}

void MaskImageReconstructorView::OnBnClickedButtonProtocolAdamStop()
{
	COM_ADAM_STOP_DATA PacketData;

	OnComAdamStop(nullptr, &PacketData);
}

BOOL MaskImageReconstructorView::m_fnPacketThreadFInishCheck()
{
	/*if (GetRunPacketTimeOutStatus())
	{
		if (!GetRunPacketTimeOutCheckToggle())
		{
			SetRunPacketTimeOutBeginTime();
			SetRunPacketTimeOutCheckToggle(TRUE);
		}
		else
		{
			double dTimeOut = SetRunPacketTimeOutEndTime();

			if (ADAM_RUN_PACKET_TIMEOUT_MS < dTimeOut)
			{
				SetThreadStatus(FALSE);
				SetImageThreadStatus(FALSE);
				SetRunPacketTimeOutStatus(FALSE);
				SetRunPacketTimeOutCheckToggle(FALSE);

				m_pLogViewer->AddLog(
					static_cast<int>(LOG_VIEWER::CH_1),
					LOG_SEQUENCE,
					m_fnFormattedStringTimeLog(_T("* ADAM Run Packet TimeOut! : %.3lf (Sec)"), dTimeOut / 1000));
				
				OnComMemoryRunPacketTimeOut();

				m_fnSetControllEnable(TRUE);

				return TRUE;
			}
			else { SetRunPacketTimeOutCheckToggle(FALSE); }
		}
	}*/
	

	switch (m_nPacketMode)
	{
		case MODE_M_PACKET: case MODE_BG_PACKET: case MODE_WO_P_PACKET: case MODE_BS_PACKET: case MODE_P_AS_POINT_PACKET:
		{ 
			if (m_pTcpClient->GetRingBufferReceiveIndex(static_cast<int>(CLIENT_SOCKET::CH_1)) == m_fnReceivePacketCountCheck())
			{ 
				SetThreadStatus(FALSE);
				SetRunPacketTimeOutStatus(FALSE);

				CString strTemp;

				int nPacketCount = 0;

				if (m_pTcpClient->GetScanPacketLast(static_cast<int>(CLIENT_SOCKET::CH_1))) { nPacketCount = (PROTOCOLM_PACKET_COUNT_FIX * m_fnGetPacketModeCountHi()) + m_fnGetPacketModeCountLow(); }
				else																		{ nPacketCount = (PROTOCOLM_PACKET_COUNT_FIX * m_fnGetPacketModeCountHi()); }

				switch (m_nPacketMode)
				{
					case MODE_M_PACKET				: { strTemp.Format(_T("* Receive M Packet[%d] Count : %d")				, nPacketCount, m_fnReceivePacketCountCheck()); break; }
					case MODE_BG_PACKET				: { strTemp.Format(_T("* Receive BG Packet[%d] Count : %d")				, nPacketCount, m_fnReceivePacketCountCheck()); break; }
					case MODE_WO_P_PACKET			: { strTemp.Format(_T("* Receive WO_P Packet[%d] Count : %d")			, nPacketCount, m_fnReceivePacketCountCheck()); break; }
					case MODE_BS_PACKET				: { strTemp.Format(_T("* Receive BS Packet[%d] Count : %d")				, nPacketCount, m_fnReceivePacketCountCheck()); break; }
					case MODE_P_AS_POINT_PACKET		: { strTemp.Format(_T("* Receive P_AS_POINT Packet[%d] Count : %d")		, nPacketCount, m_fnReceivePacketCountCheck()); break; }
				}

				m_pLogViewer->AddLog(
					static_cast<int>(LOG_VIEWER::CH_1),
					LOG_MEASURE,
					m_fnFormattedStringTimeLog(strTemp));

				m_fnReceivePacketTypeCheck();

				m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), FALSE);

				return TRUE;
			}
			break; 
		}
		case MODE_A_PACKET: 
		{ 
			if (m_pTcpClient->GetRingBufferReceiveIndex(static_cast<int>(CLIENT_SOCKET::CH_1)) == RECEIVE_A_PACKET_COUNT)
			{ 
				SetThreadStatus(FALSE);
				SetRunPacketTimeOutStatus(FALSE);

				m_pLogViewer->AddLog(
					static_cast<int>(LOG_VIEWER::CH_1),
					LOG_MEASURE,
					m_fnFormattedStringTimeLog(_T("* Receive A Packet[%d] Count : 1"), m_nAPacketCount));

				m_fnOnMessageMeasureSequenceA();

				return TRUE;
			} 
			break; 
		}
		case MODE_RUN_PACKET: 
		{ 
			if (m_nAverageCountCheck == (m_nAverageCount * m_nSetCount))
			{
				m_nScanSetCountCheck++;

				m_pLogViewer->AddLog(
					static_cast<int>(LOG_VIEWER::CH_1),
					LOG_SEQUENCE,
					m_fnFormattedStringTimeLog(_T("* Run Adam Mask Set Sucess : %d"), m_nScanSetCountCheck));

				SetThreadStatus(FALSE);
				SetRunPacketTimeOutStatus(FALSE);

				return TRUE;
			}
			break; 
		}
	}

	return FALSE;
}

BOOL MaskImageReconstructorView::m_fnImageThreadFInishCheck(ThreadParam* stData)
{
	CTcpClient* pClient = (CTcpClient*)m_pTcpClient;

	WPARAM wParam = 0;
	LPARAM lParam = 0;

	switch (m_nPacketMode)
	{
		case MODE_RUN_PACKET:
		{ 
			if (!m_bLastScanCheck)
			{
				if		(stData->nRawCount				<  m_nRawCountCheck)				{ m_fnOnMessageRawDisplayInitUpdate(stData); }
				else if (stData->nInterpolationCount	<  m_nInterpolationCountCheck)		{ m_fnOnMessageInterpolationDisplayUpdate(stData); }
				else if (stData->nAverageCount			<  m_nAverageCountCheck &&
						 m_nAverageCountCheck			!= m_nAverageCount * m_nSetCount)	{ m_fnOnMessageAverageDisplayUpdate(stData); }
				else if (stData->nScanSetCount			<  m_nScanSetCountCheck)			{ m_fnOnMessageSetDisplayUpdate(stData); }
				else																		
				{ 
					m_fnImageDisplayRawUpdate();
					if (m_bManualModeStatus) { OnComMemoryRawUpdate(); }
					else					 { OnComMemoryTransmittanceUpdate(); }
				}
			}
			else
			{
				m_fnOnMessageRawDisplayUpdate(stData);
				m_fnOnMessageRawInterpolationDisplayUpdate(stData);
				SetImageThreadStatus(FALSE);
				return TRUE;
			}
			break; 
		}
	}

	return FALSE;
}

BOOL MaskImageReconstructorView::m_fnImageDisplayRawInit(int* nRawCount, int* nScanCount)
{
	Mat* pMat			= m_MatRawSRC;
	unsigned int nCtlID = IDC_IMAGE_DISPLAY_THREAD_RAW;

	if (nRawCount != (nScanCount - 1))
	{
		if (m_bDisplayModeStatus)
		{
			*pMat = Mat::zeros(pMat->rows, pMat->cols, CV_8UC3);
		}

//		m_fnImageCountDataUpdate();
		*nRawCount += 1;
	}
	else
	{
//		m_fnImageCountDataUpdate();
		*nRawCount += 1;
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::m_fnImageDisplayRawInterpolationInit(int* nRawCount, int* nScanCount)
{
	Mat* pMat			= m_MatRawInterpolationSRC;
	unsigned int nCtlID = IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION;

	if (nRawCount != (nScanCount - 1))
	{
		if (m_bDisplayModeStatus)
		{
			*pMat = Mat::zeros(pMat->rows, pMat->cols, CV_8UC3);
		}
	}
	else
	{
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::m_fnImageDisplayRawUpdate()
{
	if (m_bDisplayModeStatus) { m_fnDisplayImage(m_MatRawSRC, IDC_IMAGE_DISPLAY_THREAD_RAW, &m_nMatRawControlX, &m_nMatRawControlY, &m_nMatRawResizeX, &m_nMatRawResizeY, m_bitmapInfoMatRaw); }
	return TRUE;
}

BOOL MaskImageReconstructorView::m_fnImageDisplayRawInterpolationUpdate()
{
	if (m_bDisplayModeStatus) { m_fnDisplayImage(m_MatRawInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION, &m_nMatRawInterpolationControlX, &m_nMatRawInterpolationControlY, &m_nMatRawInterpolationResizeX, &m_nMatRawInterpolationResizeY, m_bitmapInfoMatRawInterpolation); }
	return TRUE;
}

BOOL MaskImageReconstructorView::m_fnImageDisplayInterpolationUpdate(int* nInterpolationCount)
{
	if (m_bDisplayModeStatus) { m_fnDisplayImage(m_MatInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_INTERPOLATION, &m_nMatInterpolationControlX, &m_nMatInterpolationControlY, &m_nMatInterpolationResizeX, &m_nMatInterpolationResizeY, m_bitmapInfoMatInterpolation); }
	*nInterpolationCount += 1;
	return TRUE;
}

BOOL MaskImageReconstructorView::m_fnImageDisplayAverageUpdate(int* nAverageCount)
{
	if (m_bDisplayModeStatus) { m_fnDisplayImage(m_MatAverageSRC, IDC_IMAGE_DISPLAY_THREAD_AVERAGE, &m_nMatAverageControlX, &m_nMatAverageControlY, &m_nMatAverageResizeX, &m_nMatAverageResizeY, m_bitmapInfoMatAverage); }
	*nAverageCount += 1;
	return TRUE;
}

BOOL MaskImageReconstructorView::m_fnImageDisplaySetUpdate(int* nScanSetCount)
{
	if (m_bDisplayModeStatus) { m_fnDisplayImage(m_MatAverageSRC, IDC_IMAGE_DISPLAY_THREAD_AVERAGE, &m_nMatAverageControlX, &m_nMatAverageControlY, &m_nMatAverageResizeX, &m_nMatAverageResizeY, m_bitmapInfoMatAverage); }
	*nScanSetCount += 1;
	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMemoryInitUpdate()
{
	COM_DISPLAY_MEMORY_INIT_DATA PacketContents;
	PacketContents.nRet = COM_RETURN_TYPE::OK;

	ESOL_PACKET_DATA EsolPacketData;
	m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'C', 'S', 'A', "MEM", "INI", '@', &EsolPacketData, &PacketContents, sizeof(COM_DISPLAY_MEMORY_INIT_DATA));
	ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
	SendProtocolLog(&EsolPacketData);

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMemoryRawUpdate()
{
	COM_ADAM_RAW_DATA PacketData;

	strcpy(PacketData.cMemorySpace, SHARED_MEMORY_RAW_SPACE);
	PacketData.nBufferAddress	= 0;
	PacketData.nImageWidth		= m_MatRawSRC->cols;
	PacketData.nImageHeight		= m_MatRawSRC->rows;
	PacketData.nSize			= m_MatRawSRC->cols * m_MatRawSRC->rows * 3;
	PacketData.nRow				= 0;
	PacketData.nCol				= 0;
	PacketData.nCount			= m_nRawCountCheck;
	PacketData.nRet				= COM_RETURN_TYPE::OK;

	memcpy(m_bySharedMemoryRaw, m_MatRawSRC->data, sizeof(BYTE) * PacketData.nSize);

	m_pSharedMemory->SetMapCreateFileMapping(
		&m_hRawMapping,
		PacketData.nSize,
		SHARED_MEMORY_RAW_SPACE);

	m_pSharedMemory->SetMapViewOfFileAddress_Byte(
		&m_hRawMapping,
		&m_pRawAddress,
		&m_bySharedMemoryRaw,
		PacketData.nSize);

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMemoryRawInterpolationUpdate()
{
	COM_ADAM_RAW_INTERPOLATION_DATA PacketData;

	strcpy(PacketData.cMemorySpace, SHARED_MEMORY_RAW_INTERPOLATION_SPACE);
	PacketData.nBufferAddress	= 0;
	PacketData.nImageWidth		= m_MatRawInterpolationSRC->cols;
	PacketData.nImageHeight		= m_MatRawInterpolationSRC->rows;
	PacketData.nSize			= m_MatRawInterpolationSRC->cols * m_MatRawInterpolationSRC->rows * 3;
	PacketData.nRow				= 0;
	PacketData.nCol				= 0;
	PacketData.nCount			= m_nRawInterpolationCountCheck;
	PacketData.nRet				= COM_RETURN_TYPE::OK;

	memcpy(m_bySharedMemoryRawInterpolation, m_MatRawInterpolationSRC->data, sizeof(BYTE) * PacketData.nSize);

	m_pSharedMemory->SetMapCreateFileMapping(
		&m_hRawInterpolationMapping,
		PacketData.nSize,
		SHARED_MEMORY_RAW_INTERPOLATION_SPACE);

	m_pSharedMemory->SetMapViewOfFileAddress_Byte(
		&m_hRawInterpolationMapping,
		&m_pRawInterpolationAddress,
		&m_bySharedMemoryRawInterpolation,
		PacketData.nSize);

	ESOL_PACKET_DATA EsolPacketData;
	m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'C', 'S', 'A', "MEM", "RAW", '@', &EsolPacketData, &PacketData, sizeof(COM_ADAM_RAW_INTERPOLATION_DATA));
	ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
	SendProtocolLog(&EsolPacketData);

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMemoryInterpolationUpdate()
{
	COM_ADAM_INTERPOLATION_DATA PacketData;

	strcpy(PacketData.cMemorySpace, SHARED_MEMORY_INTERPOLATION_SPACE);
	PacketData.nBufferAddress	= 0;
	PacketData.nImageWidth		= m_MatInterpolationSRC->cols;
	PacketData.nImageHeight		= m_MatInterpolationSRC->rows;
	PacketData.nSize			= m_MatInterpolationSRC->cols * m_MatInterpolationSRC->rows * 3;
	PacketData.nRow				= 0;
	PacketData.nCol				= 0;
	PacketData.nCount			= m_nInterpolationCountCheck;
	PacketData.nRet				= COM_RETURN_TYPE::OK;

	memcpy(m_bySharedMemoryInterpolation, m_MatInterpolationSRC->data, sizeof(BYTE) * PacketData.nSize);

	m_pSharedMemory->SetMapCreateFileMapping(
		&m_hInterpolationMapping,
		PacketData.nSize,
		SHARED_MEMORY_INTERPOLATION_SPACE);

	m_pSharedMemory->SetMapViewOfFileAddress_Byte(
		&m_hInterpolationMapping,
		&m_pInterpolationAddress,
		&m_bySharedMemoryInterpolation,
		PacketData.nSize);

	ESOL_PACKET_DATA EsolPacketData;
	m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'C', 'S', 'A', "MEM", "INT", '@', &EsolPacketData, &PacketData, sizeof(COM_ADAM_INTERPOLATION_DATA));
	ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
	SendProtocolLog(&EsolPacketData);

	return TRUE;
}


BOOL MaskImageReconstructorView::OnComMemoryAverageUpdate()
{
	COM_ADAM_AVERAGE_DATA PacketData;

	strcpy(PacketData.cMemorySpace, SHARED_MEMORY_AVERAGE_SPACE);
	PacketData.nBufferAddress	= 0;
	PacketData.nImageWidth		= m_MatAverageSRC->cols;
	PacketData.nImageHeight		= m_MatAverageSRC->rows;
	PacketData.nSize			= m_MatAverageSRC->cols * m_MatAverageSRC->rows * 3;
	PacketData.nRow				= 0;
	PacketData.nCol				= 0;
	PacketData.nCount			= m_nAverageCountCheck;
	PacketData.nRet				= COM_RETURN_TYPE::OK;

	memcpy(m_bySharedMemoryAverage, m_MatAverageSRC->data, sizeof(BYTE) * PacketData.nSize);

	m_pSharedMemory->SetMapCreateFileMapping(
		&m_hAverageMapping,
		PacketData.nSize,
		SHARED_MEMORY_AVERAGE_SPACE);

	m_pSharedMemory->SetMapViewOfFileAddress_Byte(
		&m_hAverageMapping,
		&m_pAverageAddress,
		&m_bySharedMemoryAverage,
		PacketData.nSize);

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* Shared Memory H : 0x%p, P : 0x%p / Average"), &m_hAverageMapping, &m_pAverageAddress));

	ESOL_PACKET_DATA EsolPacketData;
	m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'C', 'S', 'A', "MEM", "AVG", '@', &EsolPacketData, &PacketData, sizeof(COM_ADAM_AVERAGE_DATA));
	ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
	SendProtocolLog(&EsolPacketData);

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMemoryTransmittanceUpdate(BOOL* bFirst)
{
	// Detector T and R Shared Memory Space //
	COM_ADAM_DETECTOR_DATA PacketData;

	strcpy(PacketData.cMemorySpace, SHARED_MEMORY_DETECTOR_T_SPACE);
	PacketData.nBufferAddress	= 0;
	PacketData.nImageWidth		= m_dMat_T->cols;
	PacketData.nImageHeight		= m_dMat_T->rows;
	PacketData.nSize			= (m_dMat_T->cols * m_dMat_T->rows) * sizeof(double);
	PacketData.nRow				= 0;
	PacketData.nCol				= 0;
	PacketData.nCount			= m_nRawCountCheck;
	PacketData.nRet				= COM_RETURN_TYPE::OK;

	memcpy(m_bySharedMemoryT, m_dMat_T->data, sizeof(BYTE) * PacketData.nSize);

	m_pSharedMemory->SetMapCreateFileMapping(
		&m_hTransmittanceMapping,
		PacketData.nSize,
		SHARED_MEMORY_DETECTOR_T_SPACE);

	m_pSharedMemory->SetMapViewOfFileAddress_Byte(
		&m_hTransmittanceMapping,
		&m_pTransmittanceAddress,
		&m_bySharedMemoryT,
		PacketData.nSize);

	memcpy(m_bySharedMemoryR, m_dMat_R->data, sizeof(BYTE) * PacketData.nSize);

	m_pSharedMemory->SetMapCreateFileMapping(
		&m_hReflectanceMapping,
		PacketData.nSize,
		SHARED_MEMORY_DETECTOR_R_SPACE);

	m_pSharedMemory->SetMapViewOfFileAddress_Byte(
		&m_hReflectanceMapping,
		&m_pReflectanceAddress,
		&m_bySharedMemoryR,
		PacketData.nSize);

	memcpy(m_bySharedMemoryPosX, m_dMatStaticScanPos_X->data, sizeof(BYTE) * PacketData.nSize);

	m_pSharedMemory->SetMapCreateFileMapping(
		&m_hScanPositionXMapping,
		PacketData.nSize,
		SHARED_MEMORY_SCAN_POSTION_X_SPACE);

	m_pSharedMemory->SetMapViewOfFileAddress_Byte(
		&m_hScanPositionXMapping,
		&m_pScanPositionXAddress,
		&m_bySharedMemoryPosX,
		PacketData.nSize);

	memcpy(m_bySharedMemoryPosY, m_dMatStaticScanPos_Y->data, sizeof(BYTE) * PacketData.nSize);

	m_pSharedMemory->SetMapCreateFileMapping(
		&m_hScanPositionYMapping,
		PacketData.nSize,
		SHARED_MEMORY_SCAN_POSTION_Y_SPACE);

	m_pSharedMemory->SetMapViewOfFileAddress_Byte(
		&m_hScanPositionYMapping,
		&m_pScanPositionYAddress,
		&m_bySharedMemoryPosY,
		PacketData.nSize);
	// Detector T and R Shared Memory Space //

	if (bFirst != nullptr)
	{
		if (!bFirst)
		{
			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("* Shared Memory H : 0x%p, P : 0x%p / RawInterpolation(T)"), &m_hTransmittanceMapping, &m_pTransmittanceAddress));

			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("* Shared Memory H : 0x%p, P : 0x%p / RawInterpolation(R)"), &m_hReflectanceMapping, &m_pReflectanceAddress));

			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("* Shared Memory H : 0x%p, P : 0x%p / RawInterpolation(X)"), &m_hScanPositionXMapping, &m_pScanPositionXAddress));

			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("* Shared Memory H : 0x%p, P : 0x%p / RawInterpolation(Y)"), &m_hScanPositionYMapping, &m_pScanPositionYAddress));
			(*bFirst) = TRUE;
		}
	}

	/*
	ESOL_PACKET_DATA EsolPacketData;
	m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'C', 'S', 'A', "MEM", "DET", '@', &EsolPacketData, &PacketData, sizeof(COM_ADAM_DETECTOR_DATA));
	ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
	SendProtocolLog(&EsolPacketData);
	*/

	return TRUE;
}

void MaskImageReconstructorView::m_fnOnMessageDisplayInitUpdate()
{
	m_fnDisplayImage(m_MatRawInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_RAW_INTERPOLATION, &m_nMatRawInterpolationControlX, &m_nMatRawInterpolationControlY, &m_nMatRawInterpolationResizeX, &m_nMatRawInterpolationResizeY, m_bitmapInfoMatRawInterpolation);
	m_fnDisplayImage(m_MatRawSRC, IDC_IMAGE_DISPLAY_THREAD_RAW, &m_nMatRawControlX, &m_nMatRawControlY, &m_nMatRawResizeX, &m_nMatRawResizeY, m_bitmapInfoMatRaw);
	m_fnDisplayImage(m_MatInterpolationSRC, IDC_IMAGE_DISPLAY_THREAD_INTERPOLATION, &m_nMatInterpolationControlX, &m_nMatInterpolationControlY, &m_nMatInterpolationResizeX, &m_nMatInterpolationResizeY, m_bitmapInfoMatInterpolation);
	m_fnDisplayImage(m_MatAverageSRC, IDC_IMAGE_DISPLAY_THREAD_AVERAGE, &m_nMatAverageControlX, &m_nMatAverageControlY, &m_nMatAverageResizeX, &m_nMatAverageResizeY, m_bitmapInfoMatAverage);
	
	if (m_bManualModeStatus) { OnComMemoryInitUpdate(); }
}

void MaskImageReconstructorView::m_fnOnMessageRawDisplayInitUpdate(ThreadParam* pData)
{
	m_fnImageDisplayRawInit(&pData->nRawCount, &pData->nScanCount);

	if (m_bManualModeStatus) { OnComMemoryRawUpdate(); }
}

void MaskImageReconstructorView::m_fnOnMessageRawDisplayUpdate(ThreadParam* pData)
{
	m_fnImageDisplayRawUpdate(); 

	if (m_bManualModeStatus) { OnComMemoryRawUpdate(); }
	else					 { OnComMemoryTransmittanceUpdate(); }
}

void MaskImageReconstructorView::m_fnOnMessageRawInterpolationDisplayUpdate(ThreadParam* pData)
{
	m_fnImageDisplayRawInterpolationUpdate();

	if (m_bManualModeStatus) { OnComMemoryRawInterpolationUpdate(); }
	else					 { OnComMemoryTransmittanceUpdate(); }
}

void MaskImageReconstructorView::m_fnOnMessageInterpolationDisplayUpdate(ThreadParam* pData)
{
	m_fnImageDisplayInterpolationUpdate(&pData->nInterpolationCount);

	if (m_bManualModeStatus) { OnComMemoryInterpolationUpdate(); }
}

void MaskImageReconstructorView::m_fnOnMessageAverageDisplayUpdate(ThreadParam* pData)
{
	m_fnImageDisplayAverageUpdate(&pData->nAverageCount);

	if (m_bManualModeStatus) { OnComMemoryAverageUpdate(); }
}

void MaskImageReconstructorView::m_fnOnMessageSetDisplayUpdate(ThreadParam* pData)
{
	m_fnImageDisplaySetUpdate(&pData->nScanSetCount);

	if (m_bManualModeStatus) { OnComMemoryAverageUpdate(); }
	m_bLastScanCheck = TRUE;
}

void MaskImageReconstructorView::m_fnOnMessageRawInterpolationDisplayInitUpdate()
{
//	m_fnImageDisplayRawInterpolationInit(&pData->nRawCount, &pData->nScanCount);

	if (m_bManualModeStatus) { OnComMemoryRawInterpolationUpdate(); }
}

void MaskImageReconstructorView::m_fnOnMessageStartMaskImageReconstructoring()
{
	switch (m_nPacketMode)
	{
		case MODE_RUN_PACKET			: { m_fnOnMessageStartAdamRunData();	break; }
		case MODE_A_PACKET				: { m_fnOnMessageStartAPacketRunData(); break; }
		case MODE_M_PACKET				: 
		case MODE_BG_PACKET				: 
		case MODE_WO_P_PACKET			: 
		case MODE_P_AS_POINT_PACKET		: 
		case MODE_BS_PACKET				: { m_fnOnMessageStartMPacketRunData(); break; }
	}
}

void MaskImageReconstructorView::m_fnOnMessageMeasureSequenceA()
{
	int nPacketSize = m_qPacket_A.size();

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_MEASURE,
		m_fnFormattedStringTimeLog(_T("* Push A Queue Count : %d"), nPacketSize));

	PACKET_A_IMAGE_DATA_CONTENT* pDataContent = m_qPacket_A.front();

	COM_ADAM_A_PACKET_RUN_DATA PacketData;

	MeasureDataReturn sRet;

	sRet.dExposureTime = nPacketSize;

	PacketData.nCount							= nPacketSize;
	PacketData.QueueData.PacketNumber			= pDataContent->PacketNumber;
	PacketData.QueueData.EM1024X				= pDataContent->EM1024X;
	PacketData.QueueData.EM1024Y				= pDataContent->EM1024Y;
	PacketData.QueueData.Detector_1				= pDataContent->Detector_1;
	PacketData.QueueData.Detector_2				= pDataContent->Detector_2;
	PacketData.QueueData.Detector_3				= pDataContent->Detector_3;
	PacketData.QueueData.Detector_4				= pDataContent->Detector_4;
	PacketData.QueueData.Zr_IO					= pDataContent->Zr_IO;
	PacketData.QueueData.BS_IO					= pDataContent->BS_IO;
	PacketData.QueueData.DetectorSub_1			= pDataContent->DetectorSub_1;
	PacketData.QueueData.DetectorSub_2			= pDataContent->DetectorSub_2;
	PacketData.QueueData.DetectorSub_3			= pDataContent->DetectorSub_3;
	PacketData.QueueData.DetectorSub_4			= pDataContent->DetectorSub_4;
	PacketData.nRet								= COM_RETURN_TYPE::OK;

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "ADA", "AAR", '@', &EsolPacketData, &PacketData, sizeof(COM_ADAM_A_PACKET_RUN_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	free(pDataContent); pDataContent = nullptr;

	m_qPacket_A.pop();

	SetEquipmentStatus(FALSE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(TRUE);
}

void MaskImageReconstructorView::m_fnOnMessageMeasureSequenceM()
{
	int nPacketSize = m_qPacket_M.size();

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_MEASURE,
		m_fnFormattedStringTimeLog(_T("* Push M Queue Count : %d"), nPacketSize));
	
	double* pEM1024X		= (double*)malloc(sizeof(double) * nPacketSize);
	double* pEM1024Y		= (double*)malloc(sizeof(double) * nPacketSize);
	double* pDetector_1		= (double*)malloc(sizeof(double) * nPacketSize);
	double* pDetector_2		= (double*)malloc(sizeof(double) * nPacketSize);
	double* pDetector_3		= (double*)malloc(sizeof(double) * nPacketSize);
	double* pDetector_4		= (double*)malloc(sizeof(double) * nPacketSize);
	double* pCap_1			= (double*)malloc(sizeof(double) * nPacketSize);
	double* pCap_2			= (double*)malloc(sizeof(double) * nPacketSize);
	double* pCap_3			= (double*)malloc(sizeof(double) * nPacketSize);
	double* pCap_4			= (double*)malloc(sizeof(double) * nPacketSize);
	double* pZr_IO			= (double*)malloc(sizeof(double) * nPacketSize);
	double* pBS_IO			= (double*)malloc(sizeof(double) * nPacketSize);

	for (int i = 0; i < nPacketSize; i++)
	{
		PACKET_M_IMAGE_DATA_CONTENT* pDataContent = m_qPacket_M.front();

		memcpy(&pEM1024X[i]		, &pDataContent->EM1024X		, sizeof(double));
		memcpy(&pEM1024Y[i]		, &pDataContent->EM1024Y		, sizeof(double));
		memcpy(&pDetector_1[i]	, &pDataContent->Detector_1		, sizeof(double));
		memcpy(&pDetector_2[i]	, &pDataContent->Detector_2		, sizeof(double));
		memcpy(&pDetector_3[i]	, &pDataContent->Detector_3		, sizeof(double));
		memcpy(&pDetector_4[i]	, &pDataContent->Detector_4		, sizeof(double));
		memcpy(&pZr_IO[i]		, &pDataContent->Zr_IO			, sizeof(double));
		memcpy(&pBS_IO[i]		, &pDataContent->BS_IO			, sizeof(double));

		free(pDataContent); pDataContent = nullptr;

		m_qPacket_M.pop();
	}

	COM_ADAM_M_PACKET_RUN_DATA PacketData;

	MeasureDataReturn sRet;

	PacketData.nCount = nPacketSize;

	sRet.dExposureTime = nPacketSize;

	PythonAlgorithm::MPacketStatistics(nPacketSize, pEM1024X	, &PacketData.EM1024X_Avg		, &PacketData.EM1024X_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pEM1024Y	, &PacketData.EM1024Y_Avg		, &PacketData.EM1024Y_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pDetector_1	, &PacketData.Detector_1_Avg	, &PacketData.Detector_1_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pDetector_2	, &PacketData.Detector_2_Avg	, &PacketData.Detector_2_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pDetector_3	, &PacketData.Detector_3_Avg	, &PacketData.Detector_3_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pDetector_4	, &PacketData.Detector_4_Avg	, &PacketData.Detector_4_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pCap_1		, &PacketData.Cap_1_Avg			, &PacketData.Cap_1_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pCap_2		, &PacketData.Cap_2_Avg			, &PacketData.Cap_2_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pCap_3		, &PacketData.Cap_3_Avg			, &PacketData.Cap_3_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pCap_4		, &PacketData.Cap_4_Avg			, &PacketData.Cap_4_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pZr_IO		, &PacketData.Zr_IO_Avg			, &PacketData.Zr_IO_Std);
	PythonAlgorithm::MPacketStatistics(nPacketSize, pBS_IO		, &PacketData.BS_IO_Avg			, &PacketData.BS_IO_Std);

	PacketData.nRet	= COM_RETURN_TYPE::OK;

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "ADA", "AMR", '@', &EsolPacketData, &PacketData, sizeof(COM_ADAM_M_PACKET_RUN_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	free(pEM1024X); pEM1024X = nullptr;
	free(pEM1024Y); pEM1024Y = nullptr;
	free(pDetector_1); pDetector_1 = nullptr;
	free(pDetector_2); pDetector_2 = nullptr;
	free(pDetector_3); pDetector_3 = nullptr;
	free(pDetector_4); pDetector_4 = nullptr;
	free(pCap_1); pCap_1 = nullptr;
	free(pCap_2); pCap_2 = nullptr;
	free(pCap_3); pCap_3 = nullptr;
	free(pCap_4); pCap_4 = nullptr;
	free(pZr_IO); pZr_IO = nullptr;
	free(pBS_IO); pBS_IO = nullptr;

	SetEquipmentStatus(FALSE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(TRUE);
}

void MaskImageReconstructorView::m_fnOnMessageMeasureSequenceBG()
{
	int nPacketSize = m_qPacket_BS.size();

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_MEASURE,
		m_fnFormattedStringTimeLog(_T("* Push BG Queue Count : %d"), nPacketSize));

	double* dDataA = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataB = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataC = (double*)malloc(sizeof(double) * nPacketSize);

	for (int i = 0; i < nPacketSize; i++)
	{
		PACKET_M_IMAGE_DATA_CONTENT* pPacket = m_qPacket_BS.front();

		memcpy(&dDataA[i], &pPacket->Detector_1, sizeof(double));
		memcpy(&dDataB[i], &pPacket->Detector_2, sizeof(double));
		memcpy(&dDataC[i], &pPacket->Detector_3, sizeof(double));

		free(pPacket); pPacket = nullptr;

		m_qPacket_BS.pop();
	}

	COM_MEASURE_BG_DATA PacketData;

	MeasureDataReturn sRet;

	sRet.dExposureTime = nPacketSize;

	PythonAlgorithm::MeasureBackGround(nPacketSize, dDataA, dDataB, dDataC, &sRet);

	sRet.dExposureTime = (nPacketSize / 5);

	m_fnWriteLogMerge(_T("PTR_Raw_Data.log"), _T("Background::"), &sRet);

	PacketData.nRet = COM_RETURN_TYPE::OK;

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "MBG", '@', &EsolPacketData, &PacketData, sizeof(COM_MEASURE_BG_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}
	
	free(dDataA); dDataA = nullptr;
	free(dDataB); dDataB = nullptr;
	free(dDataC); dDataC = nullptr;

	SetEquipmentStatus(FALSE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(TRUE);
}

void MaskImageReconstructorView::m_fnOnMessageMeasureSequenceWO_P()
{
	int nPacketSize = m_qPacket_BS.size();

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_MEASURE,
		m_fnFormattedStringTimeLog(_T("* Push WO_P Queue Count : %d"), nPacketSize));

	double* dDataA = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataB = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataC = (double*)malloc(sizeof(double) * nPacketSize);

	for (int i = 0; i < nPacketSize; i++)
	{
		PACKET_M_IMAGE_DATA_CONTENT* pPacket = m_qPacket_BS.front();

		memcpy(&dDataA[i], &pPacket->Detector_1, sizeof(double));
		memcpy(&dDataB[i], &pPacket->Detector_2, sizeof(double));
		memcpy(&dDataC[i], &pPacket->Detector_3, sizeof(double));

		free(pPacket); pPacket = nullptr;

		m_qPacket_BS.pop();
	}

	COM_MEASURE_WO_P_DATA PacketData;

	MeasureDataReturn sRet;

	PythonAlgorithm::MeasureWithoutPellicle(nPacketSize, dDataA, dDataB, dDataC, &sRet);

	sRet.dExposureTime = (nPacketSize / 5);

	m_fnWriteLogMerge(_T("PTR_Raw_Data.log"), _T("Without Pellicle::"), &sRet);

	PacketData.nRet = COM_RETURN_TYPE::OK;

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "WOP", '@', &EsolPacketData, &PacketData, sizeof(COM_MEASURE_WO_P_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}
	
	free(dDataA); dDataA = nullptr;
	free(dDataB); dDataB = nullptr;
	free(dDataC); dDataC = nullptr;

	SetEquipmentStatus(FALSE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(TRUE);
}

void MaskImageReconstructorView::m_fnOnMessageMeasureSequenceBS()
{
	PythonAlgorithm::LoadBsData();

	int nPacketSize = m_qPacket_BS.size();

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_MEASURE,
		m_fnFormattedStringTimeLog(_T("* Push BS Queue Count : %d"), nPacketSize));

	double* dDataA = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataB = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataC = (double*)malloc(sizeof(double) * nPacketSize);

	for (int i = 0; i < nPacketSize; i++)
	{
		PACKET_M_IMAGE_DATA_CONTENT* pPacket = m_qPacket_BS.front();

		memcpy(&dDataA[i], &pPacket->Detector_1, sizeof(double));
		memcpy(&dDataB[i], &pPacket->Detector_2, sizeof(double));
		memcpy(&dDataC[i], &pPacket->Detector_3, sizeof(double));

		free(pPacket); pPacket = nullptr;

		m_qPacket_BS.pop();
	}

	COM_MEASURE_BS_DATA PacketData;

	MeasureDataReturn sRet;

	sRet.dExposureTime = (nPacketSize / 5);

	PythonAlgorithm::MeasureBs(nPacketSize, dDataA, dDataB, dDataC, &sRet);

	PythonAlgorithm::GetBsData(
		&PacketData.WaveLengthBs, 
		&PacketData.TransmittanceBs, 
		&PacketData.ReflectanceBs, 
		&PacketData.D2toD1GainRatio, 
		&PacketData.D3toD1GainRatio);

	sRet.dTransmittance = PacketData.TransmittanceBs;
	sRet.dReflectance	= PacketData.ReflectanceBs;
	sRet.dWaveLength	= PacketData.WaveLengthBs;

	m_fnWriteLogMerge(_T("PTR_Raw_Data.log"), _T("BS Calibration::"), &sRet);

	PacketData.nRet = COM_RETURN_TYPE::OK;

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "MBS", '@', &EsolPacketData, &PacketData, sizeof(COM_MEASURE_BS_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	free(dDataA); dDataA = nullptr;
	free(dDataB); dDataB = nullptr;
	free(dDataC); dDataC = nullptr;

	SetEquipmentStatus(FALSE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(TRUE);
}

void MaskImageReconstructorView::m_fnOnMessageMeasureSequenceP_As_Point()
{
	int nPacketSize = m_qPacket_P_As_Point.size();

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_MEASURE,
		m_fnFormattedStringTimeLog(_T("* Push P As Point Queue Count : %d"), nPacketSize));

	double* dDataA = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataB = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataC = (double*)malloc(sizeof(double) * nPacketSize);

	for (int i = 0; i < nPacketSize; i++)
	{
		PACKET_M_IMAGE_DATA_CONTENT* pPacket = m_qPacket_P_As_Point.front();

		memcpy(&dDataA[i], &pPacket->Detector_1, sizeof(double));
		memcpy(&dDataB[i], &pPacket->Detector_2, sizeof(double));
		memcpy(&dDataC[i], &pPacket->Detector_3, sizeof(double));

		free(pPacket); pPacket = nullptr;

		m_qPacket_P_As_Point.pop();
	}

	COM_MEASURE_P_AS_POINT_DATA PacketData;

	MeasureDataReturn sRet;

	PythonAlgorithm::MeasurePellicle(nPacketSize, dDataA, dDataB, dDataC, &sRet);

	PythonAlgorithm::GetPellicleData(
		&PacketData.transmittance,
		&PacketData.reflectance);

	sRet.dTransmittance = PacketData.transmittance;
	sRet.dReflectance	= PacketData.reflectance;

	m_fnWriteLogMerge(_T("PTR_Raw_Data.log"), _T("Measure Point::"), &sRet);

	PacketData.nRet = COM_RETURN_TYPE::OK;

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "PAP", '@', &EsolPacketData, &PacketData, sizeof(COM_MEASURE_P_AS_POINT_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	free(dDataA); dDataA = nullptr;
	free(dDataB); dDataB = nullptr;
	free(dDataC); dDataC = nullptr;

	SetEquipmentStatus(FALSE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(TRUE);
}

void MaskImageReconstructorView::m_fnOnMessageMeasureSequenceReadDetector()
{
	int nPacketSize = m_qPacket_M.size();

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_MEASURE,
		m_fnFormattedStringTimeLog(_T("* Push M Queue Count : %d"), nPacketSize));

	double* dDataA = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataB = (double*)malloc(sizeof(double) * nPacketSize);
	double* dDataC = (double*)malloc(sizeof(double) * nPacketSize);

	for (int i = 0; i < nPacketSize; i++)
	{
		PACKET_M_IMAGE_DATA_CONTENT* pDataContent = m_qPacket_M.front();

		memcpy(&dDataA[i], &pDataContent->Detector_1, sizeof(double));
		memcpy(&dDataB[i], &pDataContent->Detector_2, sizeof(double));
		memcpy(&dDataC[i], &pDataContent->Detector_3, sizeof(double));

		free(pDataContent); pDataContent = nullptr;

		m_qPacket_M.pop();
	}

	COM_ADAM_READ_DETECTOR_RUN_DATA PacketData;

	MeasureDataReturn sRet;

	sRet.dExposureTime = nPacketSize;

	PythonAlgorithm::MPacketStatistics_Seq(nPacketSize, dDataA, dDataB, dDataC, &sRet);

	m_fnWriteLogMerge(_T("Detector_Raw_Data.log"), _T("ReadDetector::"), &sRet);

	PacketData.nRet						= COM_RETURN_TYPE::OK;
	PacketData.dD1IntensityMeasure		= sRet.dD1IntensityMeasure;
	PacketData.dD1StandardDeviation		= sRet.dD1StandardDeviation;
	PacketData.dD2IntensityMeasure		= sRet.dD2IntensityMeasure;
	PacketData.dD2StandardDeviation		= sRet.dD2StandardDeviation;
	PacketData.dD3IntensityMeasure		= sRet.dD3IntensityMeasure;
	PacketData.dD3StandardDeviation		= sRet.dD3StandardDeviation;

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "MRD", '@', &EsolPacketData, &PacketData, sizeof(COM_ADAM_READ_DETECTOR_RUN_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	free(dDataA); dDataA = nullptr;
	free(dDataB); dDataB = nullptr;
	free(dDataC); dDataC = nullptr;

	SetEquipmentStatus(FALSE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(TRUE);
}

void MaskImageReconstructorView::m_fnScanRecipeUpdate()
{
	CString strTemp;
	strTemp.Format(_T("%d"), m_nImageFOV_X);
	m_ctlImageRecipe.SetItemText(0, 1, strTemp);

	strTemp.Format(_T("%d"), m_nImageFOV_Y);
	m_ctlImageRecipe.SetItemText(1, 1, strTemp);

	strTemp.Format(_T("%d"), m_nScanGrid_X);
	m_ctlImageRecipe.SetItemText(2, 1, strTemp);

	strTemp.Format(_T("%d"), m_nScanGrid_Y);
	m_ctlImageRecipe.SetItemText(3, 1, strTemp);

	strTemp.Format(_T("%d"), m_nInterpolationCount);
	m_ctlImageRecipe.SetItemText(4, 1, strTemp);

	strTemp.Format(_T("%d"), m_nAverageCount);
	m_ctlImageRecipe.SetItemText(5, 1, strTemp);

	strTemp.Format(_T("%d"), m_nSetCount);
	m_ctlImageRecipe.SetItemText(6, 1, strTemp);

	strTemp.Format(_T("%d"), m_nScanDirection);
	m_ctlImageRecipe.SetItemText(7, 1, strTemp);

	strTemp.Format(_T("%d"), m_nPacketCount);
	m_ctlImageRecipe.SetItemText(8, 1, strTemp);
}

void MaskImageReconstructorView::m_fnFixPosPixelAverageAdd(double* dScanPositionX, double* dScanPositionY, double* dDetector1, double* dDetector2, double* dDetector3)
{
	if(m_nDetectorIndex == FIX_POS_PIXEL_LIMIT_COUNT) { m_nDetectorIndex = 0; }

	if(m_nDetectorSize == FIX_POS_PIXEL_LIMIT_COUNT)
	{
		memcpy(&m_dScanPositionX[m_nDetectorIndex], dScanPositionX, sizeof(double));
		memcpy(&m_dScanPositionY[m_nDetectorIndex], dScanPositionY, sizeof(double));

		memcpy(&m_dDetector1[m_nDetectorIndex], dDetector1, sizeof(double));
		memcpy(&m_dDetector2[m_nDetectorIndex], dDetector2, sizeof(double));
		memcpy(&m_dDetector3[m_nDetectorIndex], dDetector3, sizeof(double));

		m_nDetectorIndex += 1;
	}
	else
	{
		memcpy(&m_dScanPositionX[m_nDetectorSize], dScanPositionX, sizeof(double));
		memcpy(&m_dScanPositionY[m_nDetectorSize], dScanPositionY, sizeof(double));

		memcpy(&m_dDetector1[m_nDetectorSize], dDetector1, sizeof(double));
		memcpy(&m_dDetector2[m_nDetectorSize], dDetector2, sizeof(double));
		memcpy(&m_dDetector3[m_nDetectorSize], dDetector3, sizeof(double));

		m_nDetectorSize  += 1;
	}
}

void MaskImageReconstructorView::m_fnFixPosPixelAverageCal()
{
	uchar* pImageRawData		= m_MatRawSRC->data;
	double* pDetector_D1		= (double*)m_dMat_D1->data;
	double* pDetector_D2		= (double*)m_dMat_D2->data;
	double* pDetector_D3		= (double*)m_dMat_D3->data;
	double* pDetector_T			= (double*)m_dMat_T->data;
	double* pDetector_R			= (double*)m_dMat_R->data;
	double* pScanActualPos_X	= (double*)m_dMatActualScanPos_X->data;
	double* pScanActualPos_Y	= (double*)m_dMatActualScanPos_Y->data;
	double* pScanStaticPos_X	= (double*)m_dMatStaticScanPos_X->data;
	double* pScanStaticPos_Y	= (double*)m_dMatStaticScanPos_Y->data;

	double			dTransmittance	= 0;
	double			dReflectance	= 0;
	unsigned int	nImageWidth		= m_fnPixelWidth();
	unsigned int	nImageHeight	= m_fnPixelHeight();
	unsigned int	nPixelPos		= m_fnPixelPosX();

	if (m_bManualModeStatus)
	{
		MeasureDataReturn sRet;
		
		PythonAlgorithm::MeasurePellicle(m_nDetectorSize, m_dDetector1, m_dDetector2, m_dDetector3, &sRet);
		
		PythonAlgorithm::GetPellicleData(&dTransmittance, &dReflectance);
		
		if		(nPixelPos == 0)			   { pScanStaticPos_X[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)m_nScanGrid_X / 2; }
		else if (nPixelPos == nImageWidth - 1) { pScanStaticPos_X[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)(((double)nPixelPos - 1) * ((double)m_nScanGrid_X)) + ((double)m_nScanGrid_X / 2); }
		else
		{
			if (m_nPixelPosY % 2 == FALSE) { pScanStaticPos_X[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)((double)(m_nPixelPosX) * (double)m_nScanGrid_X); }
			else						   { pScanStaticPos_X[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)((double)(nPixelPos) * (double)m_nScanGrid_X); }
		}

		pScanStaticPos_Y[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)((double)(m_nPixelPosY) * (double)m_nScanGrid_Y);

		pScanActualPos_X[(m_nPixelPosY * nImageWidth  * 1) + nPixelPos] = m_pImageProcessAlgorithm->ArrayAverage(m_nDetectorSize, m_dScanPositionX);
		pScanActualPos_Y[(m_nPixelPosY * nImageHeight * 1) + nPixelPos] = m_pImageProcessAlgorithm->ArrayAverage(m_nDetectorSize, m_dScanPositionY);
		
		double dDetector = m_pImageProcessAlgorithm->ArrayAverage(m_nDetectorSize, m_dDetector2);

		m_pImageProcessAlgorithm->CalColorBufferScanMode(pDetector_T, pDetector_R, pImageRawData, dDetector, dDetector, nPixelPos, m_nPixelPosY, nImageWidth, nImageHeight);
	}
	else
	{
		MeasureDataReturn sRet;

		PythonAlgorithm::MeasurePellicle(m_nDetectorSize, m_dDetector1, m_dDetector2, m_dDetector3, &sRet);

		PythonAlgorithm::GetPellicleData(&dTransmittance, &dReflectance);

		if		(nPixelPos == 0)			   { pScanStaticPos_X[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)m_nScanGrid_X / 2; }
		else if (nPixelPos == nImageWidth - 1) { pScanStaticPos_X[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)(((double)nPixelPos - 1) * ((double)m_nScanGrid_X)) + ((double)m_nScanGrid_X / 2); }
		else
		{
			if (m_nPixelPosY % 2 == FALSE) { pScanStaticPos_X[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)((double)(m_nPixelPosX) * (double)m_nScanGrid_X); }
			else						   { pScanStaticPos_X[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)((double)(nPixelPos) * (double)m_nScanGrid_X); }
		}

		pScanStaticPos_Y[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = (double)((double)(m_nPixelPosY) * (double)m_nScanGrid_Y);

		pScanActualPos_X[(m_nPixelPosY * nImageWidth  * 1) + nPixelPos] = m_pImageProcessAlgorithm->ArrayAverage(m_nDetectorSize, m_dScanPositionX);
		pScanActualPos_Y[(m_nPixelPosY * nImageHeight * 1) + nPixelPos] = m_pImageProcessAlgorithm->ArrayAverage(m_nDetectorSize, m_dScanPositionY);

		pDetector_D1[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = m_pImageProcessAlgorithm->ArrayAverage(m_nDetectorSize, m_dDetector1);
		pDetector_D2[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = m_pImageProcessAlgorithm->ArrayAverage(m_nDetectorSize, m_dDetector2);
		pDetector_D3[(m_nPixelPosY * nImageWidth * 1) + nPixelPos] = m_pImageProcessAlgorithm->ArrayAverage(m_nDetectorSize, m_dDetector3);

		m_pImageProcessAlgorithm->CalColorBufferScanMode(pDetector_T, pDetector_R, pImageRawData, dTransmittance, dReflectance, nPixelPos, m_nPixelPosY, nImageWidth, nImageHeight);
	}

	m_nPixelPosX++;
	m_nPixelCountCheck++;

	m_nDetectorSize		= 0;
	m_nDetectorIndex	= 0;
}

void MaskImageReconstructorView::OnBnClickedButtonPtrTest()
{

}

void MaskImageReconstructorView::m_fnSharedMemoryInit(HANDLE* pHandle, unsigned int nSize, CString strSpace)
{
	m_pSharedMemory->SetMapCreateFileMapping(pHandle, nSize, strSpace);
}

void MaskImageReconstructorView::m_fnSharedMemoryAllInit()
{
	if (m_pScanPositionXAddress != NULL) ::UnmapViewOfFile(m_pScanPositionXAddress); m_pScanPositionXAddress = NULL;
	if (m_hScanPositionXMapping != NULL) ::CloseHandle(m_hScanPositionXMapping); m_hScanPositionXMapping = NULL;

	if (m_pScanPositionYAddress != NULL) ::UnmapViewOfFile(m_pScanPositionYAddress); m_pScanPositionYAddress = NULL;
	if (m_hScanPositionYMapping != NULL) ::CloseHandle(m_hScanPositionYMapping); m_hScanPositionYMapping = NULL;

	if (m_pTransmittanceAddress != NULL) ::UnmapViewOfFile(m_pTransmittanceAddress); m_pTransmittanceAddress = NULL;
	if (m_hTransmittanceMapping != NULL) ::CloseHandle(m_hTransmittanceMapping); m_hTransmittanceMapping = NULL;

	if (m_pReflectanceAddress != NULL) ::UnmapViewOfFile(m_pReflectanceAddress); m_pReflectanceAddress = NULL;
	if (m_hReflectanceMapping != NULL) ::CloseHandle(m_hReflectanceMapping); m_hReflectanceMapping = NULL;

	if (m_pRawAddress != NULL) ::UnmapViewOfFile(m_pRawAddress); m_pRawAddress = NULL;
	if (m_hRawMapping != NULL) ::CloseHandle(m_hRawMapping); m_hRawMapping = NULL;

	if (m_pRawInterpolationAddress != NULL) ::UnmapViewOfFile(m_pRawInterpolationAddress); m_pRawInterpolationAddress = NULL;
	if (m_hRawInterpolationMapping != NULL) ::CloseHandle(m_hRawInterpolationMapping); m_hRawInterpolationMapping = NULL;

	if (m_pInterpolationAddress != NULL) ::UnmapViewOfFile(m_pInterpolationAddress); m_pInterpolationAddress = NULL;
	if (m_hInterpolationMapping != NULL) ::CloseHandle(m_hInterpolationMapping); m_hInterpolationMapping = NULL;

	if (m_pAverageAddress != NULL) ::UnmapViewOfFile(m_pAverageAddress); m_pAverageAddress = NULL;
	if (m_hAverageMapping != NULL) ::CloseHandle(m_hAverageMapping); m_hAverageMapping = NULL;
}

int MaskImageReconstructorView::m_fnReceivePacketCountCheck()
{
	int nCount		= 0;
	int nCountHi	= m_fnGetPacketModeCountHi();
	int nCountLow	= m_fnGetPacketModeCountLow();

	if (nCountLow != 0) { nCountHi++; }

	nCount = nCountHi;

	return nCount;
}

void MaskImageReconstructorView::m_fnReceivePacketTypeCheck()
{
	switch (m_nPacketMode)
	{
		case MODE_M_PACKET				: { m_fnOnMessageMeasureSequenceM();			break; }
		case MODE_BG_PACKET				: { m_fnOnMessageMeasureSequenceBG();			break; }
		case MODE_WO_P_PACKET			: { m_fnOnMessageMeasureSequenceWO_P();			break; }
		case MODE_BS_PACKET				: { m_fnOnMessageMeasureSequenceBS();			break; }
		case MODE_P_AS_POINT_PACKET		: { m_fnOnMessageMeasureSequenceP_As_Point();	break; }
		case MODE_READ_DETECTOR			: { m_fnOnMessageMeasureSequenceReadDetector(); break; }
	}
}

BOOL MaskImageReconstructorView::m_fnStartMaskImageReconstructoringCheck(CTcpClient* pClient)
{
	if (pClient->GetRingBufferReceiveIndex(static_cast<int>(CLIENT_SOCKET::CH_1)) < pClient->GetRingBufferTotalIndex(static_cast<int>(CLIENT_SOCKET::CH_1))) { m_fnOnMessageStartMaskImageReconstructoring(); }
	return TRUE;
}

void MaskImageReconstructorView::m_fnMeasureSeqDataInit()
{
	m_nPacketCount_BG			= 0;
	m_nPacketCount_WO_P			= 0;
	m_nPacketCount_BS			= 0;
	m_nPacketCount_P_AS_POINT	= 0;
	m_nDetectorSize				= 0;
	m_nDetectorIndex			= 0;

	while (!m_qPacket_M.empty()) { PACKET_M_IMAGE_DATA_CONTENT* p = m_qPacket_M.front(); free(p); p = nullptr; m_qPacket_M.pop(); }
	while (!m_qPacket_A.empty()) { PACKET_A_IMAGE_DATA_CONTENT* p = m_qPacket_A.front(); free(p); p = nullptr; m_qPacket_A.pop(); }
	while (!m_qPacket_BS.empty()) { PACKET_M_IMAGE_DATA_CONTENT* p = m_qPacket_BS.front(); free(p); p = nullptr; m_qPacket_BS.pop(); }
	while (!m_qPacket_P_As_Point.empty()) { PACKET_M_IMAGE_DATA_CONTENT* p = m_qPacket_P_As_Point.front(); free(p); p = nullptr; m_qPacket_P_As_Point.pop(); }

	memset(m_dScanPositionX, 0x00, sizeof(double) * FIX_POS_PIXEL_LIMIT_COUNT);
	memset(m_dScanPositionY, 0x00, sizeof(double) * FIX_POS_PIXEL_LIMIT_COUNT);

	memset(m_dDetector1, 0x00, sizeof(double) * FIX_POS_PIXEL_LIMIT_COUNT);
	memset(m_dDetector2, 0x00, sizeof(double) * FIX_POS_PIXEL_LIMIT_COUNT);
	memset(m_dDetector3, 0x00, sizeof(double) * FIX_POS_PIXEL_LIMIT_COUNT);
}

int MaskImageReconstructorView::m_fnGetPacketTotalByteLengh()
{
	int nPacketTotalByteLengh = 0;

	int nCountHi	= m_fnGetPacketModeCountHi();
	int nCountLow	= m_fnGetPacketModeCountLow();

	if (nCountLow != 0) 
	{ 
		nPacketTotalByteLengh += (nCountHi * PROTOCOL_HEADER_SIZE) + ((nCountHi * 1000) * PROTOCOL_M_PACKET_SIZE);
		nPacketTotalByteLengh += PROTOCOL_HEADER_SIZE + (nCountLow * PROTOCOL_M_PACKET_SIZE);
	}
	else
	{
		nPacketTotalByteLengh += PROTOCOL_HEADER_SIZE + ((nCountHi * 1000) * PROTOCOL_M_PACKET_SIZE);
	}

	return nPacketTotalByteLengh;
}

int MaskImageReconstructorView::m_fnGetPacketModeCountHi()
{
	int nPacketModeCount = 0;

	switch (m_nPacketMode)
	{
		case MODE_RUN_PACKET			: { nPacketModeCount = m_nPacketCount;				break; }
		case MODE_A_PACKET				: { nPacketModeCount = m_nPacketCount;				break; }
		case MODE_M_PACKET				: { nPacketModeCount = m_nMPacketCount;				break; }
		case MODE_BG_PACKET				: { nPacketModeCount = m_nPacketCount_BG;			break; }
		case MODE_WO_P_PACKET			: { nPacketModeCount = m_nPacketCount_WO_P;			break; }
		case MODE_BS_PACKET				: { nPacketModeCount = m_nPacketCount_BS;			break; }
		case MODE_P_AS_POINT_PACKET		: { nPacketModeCount = m_nPacketCount_P_AS_POINT;	break; }
	}

	return nPacketModeCount / PROTOCOLM_PACKET_COUNT_FIX;
}

int MaskImageReconstructorView::m_fnGetPacketModeCountLow()
{
	int nPacketModeCount = 0;

	switch (m_nPacketMode)
	{
		case MODE_RUN_PACKET			: { nPacketModeCount = m_nPacketCount;				break; }
		case MODE_A_PACKET				: { nPacketModeCount = m_nPacketCount;				break; }
		case MODE_M_PACKET				: { nPacketModeCount = m_nMPacketCount;				break; }
		case MODE_BG_PACKET				: { nPacketModeCount = m_nPacketCount_BG;			break; }
		case MODE_WO_P_PACKET			: { nPacketModeCount = m_nPacketCount_WO_P;			break; }
		case MODE_BS_PACKET				: { nPacketModeCount = m_nPacketCount_BS;			break; }
		case MODE_P_AS_POINT_PACKET		: { nPacketModeCount = m_nPacketCount_P_AS_POINT;	break; }
	}

	return nPacketModeCount % PROTOCOLM_PACKET_COUNT_FIX;
}

void MaskImageReconstructorView::m_fnOnMessageStartAdamRunData()
{
	PACKET_M_IMAGE_DATA* pData_RUN = new PACKET_M_IMAGE_DATA;

	memcpy(m_byRing_Run, m_pTcpClient->GetReceiveByteRingBuffer(static_cast<int>(CLIENT_SOCKET::CH_1)), sizeof(BYTE) * SOCKET_PACKET_SIZE);

	if (m_byRing_Run[MSG_ID]	== ADAM_TO_MAIN_ID				&&
		m_byRing_Run[MSG_CMD]	== ADAM_TO_MAIN_CMD_ADAM_RUN)
	{
		m_pTcpClient->ByteToMImageData(static_cast<int>(CLIENT_SOCKET::CH_1), m_byRing_Run, pData_RUN);
		m_fnImageData_RUN(pData_RUN);
	}

	if (pData_RUN != nullptr) { delete pData_RUN; pData_RUN = nullptr; }
	m_pTcpClient->RingBufferReceiveIndexIncrement(static_cast<int>(CLIENT_SOCKET::CH_1));
}

void MaskImageReconstructorView::m_fnOnMessageStartAPacketRunData()
{
	PACKET_A_IMAGE_DATA* pData_A = new PACKET_A_IMAGE_DATA;

	memcpy(m_byRing_APacket, m_pTcpClient->GetReceiveByteRingBuffer(static_cast<int>(CLIENT_SOCKET::CH_1)), sizeof(BYTE) * SOCKET_PACKET_SIZE);

	if (m_byRing_APacket[MSG_ID]	== ADAM_TO_MAIN_ID				&& 
		m_byRing_APacket[MSG_CMD]	== ADAM_TO_MAIN_CMD_A_PACKET_RUN)
	{
		m_pTcpClient->ByteToAImageData(static_cast<int>(CLIENT_SOCKET::CH_1), m_byRing_APacket, pData_A);
		m_fnImageData_A(pData_A);
	}

	if (pData_A != nullptr) { delete pData_A; pData_A = nullptr; }
	m_pTcpClient->RingBufferReceiveIndexIncrement(static_cast<int>(CLIENT_SOCKET::CH_1));
}

void MaskImageReconstructorView::m_fnOnMessageStartMPacketRunData()
{
	PACKET_M_IMAGE_DATA* pData_M = new PACKET_M_IMAGE_DATA;

	memcpy(m_byRing_MPacket, m_pTcpClient->GetReceiveByteRingBuffer(static_cast<int>(CLIENT_SOCKET::CH_1)), sizeof(BYTE) * SOCKET_PACKET_SIZE);

	if (m_byRing_MPacket[MSG_ID]	== ADAM_TO_MAIN_ID				&& 
		m_byRing_MPacket[MSG_CMD]	== ADAM_TO_MAIN_CMD_M_PACKET_RUN)
	{
		m_pTcpClient->ByteToMImageData(static_cast<int>(CLIENT_SOCKET::CH_1), m_byRing_MPacket, pData_M);

		switch (m_nPacketMode)
		{
			case MODE_M_PACKET			: { m_fnImageData_M(pData_M);			break; }
			case MODE_BG_PACKET			: { m_fnImageData_BS(pData_M);			break; }
			case MODE_WO_P_PACKET		: { m_fnImageData_BS(pData_M);			break; }
			case MODE_BS_PACKET			: { m_fnImageData_BS(pData_M);			break; }
			case MODE_P_AS_POINT_PACKET	: { m_fnImageData_P_As_Point(pData_M);	break; }
		}
	}

	if (pData_M != nullptr) { delete pData_M; pData_M = nullptr; }
	m_pTcpClient->RingBufferReceiveIndexIncrement(static_cast<int>(CLIENT_SOCKET::CH_1));
}

unsigned int MaskImageReconstructorView::m_fnPixelPosX()
{
	unsigned int nPixelPos = 0;

	if (m_nPixelPosY % 2 == FALSE)  { nPixelPos = m_nPixelPosX;								}
	else							{ nPixelPos = (m_fnPixelWidth() - 1) - m_nPixelPosX;	}

	return nPixelPos;
}

void MaskImageReconstructorView::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct)
{
	if ((m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_ADAM_RUN, TRUE))		||
		(m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_ADAM_STOP, TRUE))		||
		(m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_ADAM_RECIPE, TRUE))		||
		(m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_M_PACKET_RUN, TRUE))	||
		(m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_A_PACKET_RUN, TRUE))	||
		(m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PTR_TEST, TRUE))					||
		(m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_TEST_READ_WRITE, TRUE))			||
		(m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_EM1024_RESET, TRUE)))
	{
		m_fnDrawDesignButtonSelect(
			lpDrawItemStruct,
			RGB(91, 91, 91), RGB(65, 65, 65), RGB(255, 255, 255),
			RGB(120, 120, 120), RGB(91, 91, 91), RGB(255, 255, 255), 2);
	}
	else if ((!m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_ADAM_RUN, FALSE))		||
			 (!m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_ADAM_STOP, FALSE))	||
			 (!m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_ADAM_RECIPE, FALSE))	||
			 (!m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_M_PACKET_RUN, FALSE))	||
			 (!m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_A_PACKET_RUN, FALSE))	||
			 (!m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PTR_TEST, FALSE))				||
			 (!m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_TEST_READ_WRITE, FALSE))		||
			 (!m_fnControlIsWindowEnableCheck(nIDCtl, IDC_BUTTON_PROTOCOL_EM1024_RESET, FALSE)))
	{
		m_fnDrawDesignButtonSelect(
			lpDrawItemStruct,
			RGB(255, 0, 0), RGB(200, 0, 0), RGB(255, 255, 255),
			RGB(255, 0, 0), RGB(200, 0, 0), RGB(255, 255, 255), 2);
	}

	if ((nIDCtl == IDC_BUTTON_TEST_MANUAL_MODE) && m_bManualModeStatus ||
		(nIDCtl == IDC_BUTTON_DISPLAY_MODE)		&& m_bDisplayModeStatus)
    {
		m_fnDrawDesignButtonStay(
			lpDrawItemStruct,
			RGB(255, 0, 0), RGB(200, 0, 0), RGB(255, 255, 255),
			RGB(0, 255, 0), RGB(0, 200, 0), RGB(0, 0, 0), 2, TRUE);
    }
	else if ((nIDCtl == IDC_BUTTON_TEST_MANUAL_MODE) && !m_bManualModeStatus ||
			 (nIDCtl == IDC_BUTTON_DISPLAY_MODE)	 && !m_bDisplayModeStatus)
	{
		m_fnDrawDesignButtonStay(
			lpDrawItemStruct,
			RGB(255, 0, 0), RGB(200, 0, 0), RGB(255, 255, 255),
			RGB(0, 255, 0), RGB(0, 200, 0), RGB(0, 0, 0), 2, FALSE);
	}

	if ((nIDCtl == IDC_BUTTON_PROGRAM_VERSION))
	{
		m_fnDrawDesignButtonSelect(
			lpDrawItemStruct,
			RGB(91, 91, 91), RGB(65, 65, 65), RGB(255, 255, 255),
			RGB(120, 120, 120), RGB(91, 91, 91), RGB(255, 255, 255), 2);
	}

	CFormView::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

BOOL MaskImageReconstructorView::m_fnControlIsWindowEnableCheck(int nIDCtl, int nIDCheckCtl, BOOL bFlag)
{
	BOOL bRet = FALSE;
	if (bFlag) { if ((nIDCtl == nIDCheckCtl) &&  GetDlgItem(nIDCheckCtl)->IsWindowEnabled()) { bRet = TRUE; } }
	else	   { if ((nIDCtl == nIDCheckCtl) && !GetDlgItem(nIDCheckCtl)->IsWindowEnabled()) { bRet = TRUE; } }
	return bRet;
}

void MaskImageReconstructorView::m_fnDrawDesignButtonStay(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF bk_1, COLORREF edge_1, COLORREF font_1, COLORREF bk_2, COLORREF edge_2, COLORREF font_2, unsigned int nSize, BOOL bFlag)
{
	CDC dc;
	RECT rect, rectEdge;
	dc.Attach(lpDrawItemStruct->hDC);
	rect = lpDrawItemStruct->rcItem;
	rectEdge = rect;
	rectEdge.left += nSize; rectEdge.top += nSize; rectEdge.right -= nSize; rectEdge.bottom -= nSize;

	UINT state = lpDrawItemStruct->itemState;
	UINT action = lpDrawItemStruct->itemAction;

//    if((state &ODS_SELECTED))	{ dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT); }
//    else						{ dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT); }

	if (bFlag)
    {
		dc.SetBkColor(bk_2);
		dc.Draw3dRect(&rect, edge_2, edge_2);
		dc.FillSolidRect(&rect, edge_2);
		dc.Draw3dRect(&rectEdge, bk_2, bk_2);
		dc.FillSolidRect(&rectEdge, bk_2);
		dc.SetTextColor(font_2);
    }
	else
	{
		dc.SetBkColor(bk_1);
		dc.Draw3dRect(&rect, edge_1, edge_1);
		dc.FillSolidRect(&rect, edge_1);
		dc.Draw3dRect(&rectEdge, bk_1, bk_1);
		dc.FillSolidRect(&rectEdge, bk_1);
		dc.SetTextColor(font_1);
	}

	TCHAR buffer[MAX_PATH];
	ZeroMemory(buffer, MAX_PATH);
	::GetWindowTextA(lpDrawItemStruct->hwndItem, buffer, MAX_PATH);
	dc.DrawText(buffer, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	dc.Detach();
}

void MaskImageReconstructorView::m_fnDrawDesignButtonSelect(LPDRAWITEMSTRUCT lpDrawItemStruct, COLORREF bk_1, COLORREF edge_1, COLORREF font_1, COLORREF bk_2, COLORREF edge_2, COLORREF font_2, unsigned int nSize)
{
	CDC dc;
	RECT rect, rectEdge;
	dc.Attach(lpDrawItemStruct->hDC);
	rect = lpDrawItemStruct->rcItem;
	rectEdge = rect;
	rectEdge.left += nSize; rectEdge.top += nSize; rectEdge.right -= nSize; rectEdge.bottom -= nSize;
	
	UINT state = lpDrawItemStruct->itemState;
	UINT action = lpDrawItemStruct->itemAction;

//    if((state &ODS_SELECTED))	{ dc.DrawEdge(&rect,EDGE_SUNKEN,BF_RECT); }
//    else						{ dc.DrawEdge(&rect,EDGE_RAISED,BF_RECT); }

	if ((state & ODS_SELECTED))
	{
		dc.SetBkColor(bk_2);
		dc.Draw3dRect(&rect, edge_2, edge_2);
		dc.FillSolidRect(&rect, edge_2);
		dc.Draw3dRect(&rectEdge, bk_2, bk_2);
		dc.FillSolidRect(&rectEdge, bk_2);
		dc.SetTextColor(font_2);
		
	}
	else
	{
		dc.SetBkColor(bk_1);
		dc.Draw3dRect(&rect, edge_1, edge_1);
		dc.FillSolidRect(&rect, edge_1);
		dc.Draw3dRect(&rectEdge, bk_1, bk_1);
		dc.FillSolidRect(&rectEdge, bk_1);
		dc.SetTextColor(font_1);
	}

	TCHAR buffer[MAX_PATH];
	ZeroMemory(buffer, MAX_PATH);
	::GetWindowTextA(lpDrawItemStruct->hwndItem, buffer, MAX_PATH);
	dc.DrawText(buffer, &rect, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	dc.Detach();
}

void MaskImageReconstructorView::OnBnClickedButtonTestManualMode()
{
	if (m_bManualModeStatus) { m_bManualModeStatus = FALSE; }
	else					 { m_bManualModeStatus = TRUE;  }

	m_fnRefreshManualRect();
}

BOOL MaskImageReconstructorView::SetRunPacketTimeOutStatus(BOOL bFlag)
{
	m_bRunPacketTimeOutCheck = bFlag;
	return TRUE;
}

BOOL MaskImageReconstructorView::GetRunPacketTimeOutStatus()
{
	return m_bRunPacketTimeOutCheck;
}

BOOL MaskImageReconstructorView::SetRunPacketTimeOutBeginTime()
{
	QueryPerformanceFrequency(&m_lFrequency);
	QueryPerformanceCounter(&m_lBeginTime);
	return TRUE;
}

double MaskImageReconstructorView::SetRunPacketTimeOutEndTime()
{
	QueryPerformanceCounter(&m_lEndtime);
	
	__int64	elapsed = m_lEndtime.QuadPart - m_lBeginTime.QuadPart;
	double	duringtime = (double)elapsed / (double)m_lFrequency.QuadPart;

	return duringtime * 1000;
}

BOOL MaskImageReconstructorView::OnComMemoryRunPacketTimeOut()
{
	COM_TIME_OUT_DATA PacketContents;
	PacketContents.nRet = COM_RETURN_TYPE::OK;

	ESOL_PACKET_DATA EsolPacketData;
	m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'C', 'S', 'A', "ADA", "TIM", '@', &EsolPacketData, &PacketContents, sizeof(COM_TIME_OUT_DATA));
	ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
	SendProtocolLog(&EsolPacketData);
	return TRUE;
}

void MaskImageReconstructorView::m_StartForwardScan(PACKET_M_IMAGE_DATA_CONTENT* pTemp, double* dScanGridX, double* dScanGridY, double* dOffset, BOOL* bManualContinue)
{
	double dActualPosX	= pTemp->EM1024X;
	double dActualPosY	= pTemp->EM1024Y;
	double dThPosX_A	= 0 - *dOffset;
	double dThPosY_A	= 0 - *dOffset;
	double dThPosX_B	= m_nImageFOV_X + *dOffset;
	double dThPosY_B	= m_nImageFOV_Y + *dOffset;

	double dAreaThPosX_A	= m_dScanAreaThresholdX - *dOffset;
	double dAreaThPosY_A	= m_dScanAreaThresholdY - *dOffset;
	double dAreaThPosX_B	= *dScanGridX;
	double dAreaThPosY_B	= *dScanGridY;

	if (MACRO_POSITION_LIMIT_PLUS(dActualPosX, dThPosX_A, dThPosX_B))
	{
		if(MACRO_POSITION_THRESHOLD_X(m_nPixelPosX, m_fnPixelWidth()))
		{
			if (MACRO_POSITION_CHECK_PLUS(dActualPosY, dAreaThPosY_A, dAreaThPosY_B))
			{
				if (MACRO_POSITION_CHECK_PLUS(dActualPosX, dAreaThPosX_A, dAreaThPosX_B))
				{
					m_fnFixPosPixelAverageAdd(&dActualPosX, &dActualPosY, &pTemp->Detector_1, &pTemp->Detector_2, &pTemp->Detector_3);

					if (m_bManualModeStatus)
					{
						if (m_nPixelPosX == m_fnPixelWidth() - 1)
						{
							m_fnFixPosPixelAverageCal();

							m_pLogViewer->AddLog(
								static_cast<int>(LOG_VIEWER::CH_1),
								LOG_SCAN,
								m_fnFormattedStringTimeLog(
									_T("* [->][↓] Scan Data[%d][%d] : PosX : %lf / PosY : %lf"),
									m_nPixelPosY + 1,
									m_nPixelPosX - 1,
									pTemp->EM1024X,
									pTemp->EM1024Y));

							m_dScanAreaThresholdY += dAreaThPosY_B;

							m_nPixelPosX = 0;
							m_nPixelPosY++;
						}
					}
				}
				else if (MACRO_POSITION_NEXT_CHECK_PLUS(dActualPosX, dAreaThPosX_A, dAreaThPosX_B))
				{
					m_fnFixPosPixelAverageCal();

					m_dScanAreaThresholdX += dAreaThPosX_B;

					if (m_bManualModeStatus) { *bManualContinue = TRUE; }
				}
			}
		}
		else
		{
			if (MACRO_POSITION_NEXT_CHECK_PLUS(dActualPosY, dAreaThPosY_A, dAreaThPosY_B))
			{
				m_pLogViewer->AddLog(
					static_cast<int>(LOG_VIEWER::CH_1),
					LOG_SCAN,
					m_fnFormattedStringTimeLog(
						_T("* [->][↓] Scan Data[%d][%d] : PosX : %lf / PosY : %lf"),
						m_nPixelPosY + 1,
						m_nPixelPosX - 1,
						pTemp->EM1024X,
						pTemp->EM1024Y));

				m_dScanAreaThresholdY += dAreaThPosY_B;

				m_nPixelPosX = 0;
				m_nPixelPosY++;

				if (m_bManualModeStatus) { *bManualContinue = TRUE; }
			}
		}
	}
}

void MaskImageReconstructorView::m_StartBackwardScan(PACKET_M_IMAGE_DATA_CONTENT* pTemp, double* dScanGridX, double* dScanGridY, double* dOffset, BOOL* bManualContinue)
{
	double dActualPosX	=  pTemp->EM1024X;
	double dActualPosY	=  pTemp->EM1024Y;
	double dThPosX_A	= 0 - *dOffset;
	double dThPosY_A	= 0 - *dOffset;
	double dThPosX_B	= m_nImageFOV_X + *dOffset;
	double dThPosY_B	= m_nImageFOV_Y + *dOffset;

	double dAreaThPosX_A	= m_dScanAreaThresholdX + *dOffset;
	double dAreaThPosY_A	= m_dScanAreaThresholdY - *dOffset;
	double dAreaThPosX_B	= *dScanGridX;
	double dAreaThPosY_B	= *dScanGridY;

	if (MACRO_POSITION_LIMIT_MINUS(dActualPosX, dThPosX_A, dThPosX_B))
	{
		if (MACRO_POSITION_THRESHOLD_X(m_nPixelPosX, m_fnPixelWidth()))
		{
			if (MACRO_POSITION_CHECK_PLUS(dActualPosY, dAreaThPosY_A, dAreaThPosY_B))
			{
				if (MACRO_POSITION_CHECK_MINUS(dActualPosX, dAreaThPosX_A, dAreaThPosX_B))
				{
					m_fnFixPosPixelAverageAdd(&dActualPosX, &dActualPosY, &pTemp->Detector_1, &pTemp->Detector_2, &pTemp->Detector_3);

					if (m_bManualModeStatus)
					{
						if (m_nPixelPosX == m_fnPixelWidth() - 1)
						{
							m_fnFixPosPixelAverageCal();

							m_pLogViewer->AddLog(
								static_cast<int>(LOG_VIEWER::CH_1),
								LOG_SCAN,
								m_fnFormattedStringTimeLog(
									_T("* [<-][↓] Scan Data[%d][%d] : PosX : %lf / PosY : %lf"),
									m_nPixelPosY + 1,
									m_nPixelPosX - 1,
									pTemp->EM1024X,
									pTemp->EM1024Y));

							m_dScanAreaThresholdY += *dScanGridY;

							m_nPixelPosX = 0;
							m_nPixelPosY++;
						}
					}
				}
				else if (MACRO_POSITION_NEXT_CHECK_MINUS(dActualPosX, dAreaThPosX_A, dAreaThPosX_B))
				{
					m_fnFixPosPixelAverageCal();

					m_dScanAreaThresholdX -= dAreaThPosX_B;

					if (m_bManualModeStatus) { *bManualContinue = TRUE; }
				}
			}
		}
		else
		{
			if (MACRO_POSITION_NEXT_CHECK_PLUS(dActualPosY, dAreaThPosY_A, dAreaThPosY_B))
			{
				m_pLogViewer->AddLog(
					static_cast<int>(LOG_VIEWER::CH_1),
					LOG_SCAN,
					m_fnFormattedStringTimeLog(
						_T("* [<-][↓] Scan Data[%d][%d] : PosX : %lf / PosY : %lf"),
						m_nPixelPosY + 1,
						m_nPixelPosX - 1,
						pTemp->EM1024X,
						pTemp->EM1024Y));

				m_dScanAreaThresholdY += dAreaThPosY_B;

				m_nPixelPosX = 0;
				m_nPixelPosY++;

				if (m_bManualModeStatus) { *bManualContinue = TRUE; }
			}
		}
	}
}

void MaskImageReconstructorView::m_fnWriteLog(CString strName, CString strTemp)
{
	TCHAR chFilePath[256] = { 0, };
	GetModuleFileName(NULL, chFilePath, 256);
	
	CString strFolderTemp(chFilePath);
	CString strFolderPath;
	CString strFilePath;
	CString strFileName = _T("MaskImageReconstructor");
	CString strFileTemp = _T("\\");

	strFolderTemp	= strFolderTemp.Left(strFolderTemp.ReverseFind('\\'));
	strFolderTemp	= strFolderTemp.Left(strFolderTemp.ReverseFind('\\'));
	strFolderTemp	= strFolderTemp.Left(strFolderTemp.ReverseFind('\\'));
	strFolderPath	= strFolderTemp + _T("\\Log\\") + strFileName;
	strFilePath		= strFolderPath + strFileTemp + m_fnGetCurruntDateFunction() + strFileTemp + strName;

	if (GetFileAttributes((LPCTSTR)strFolderPath) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)strFolderPath, NULL); }

	CFileStatus fs;

	BOOL bFlag = FALSE;

	if (CFile::GetStatus(strFilePath, fs))	{ bFlag = FALSE; }
	else									{ bFlag = TRUE;  }

	FILE* fp = fopen(strFilePath, "a+");

	fseek(fp, 0, SEEK_END);

	if (fp != nullptr)
	{
		if (bFlag)
		{
			CString strFirstTemp;
			strFirstTemp.Empty();
			strFirstTemp += _T("TIME\t");
			strFirstTemp += _T("TYPE\t");
			strFirstTemp += _T("D1 Intensity Measure\t");
			strFirstTemp += _T("D2 Intensity Measure\t");
			strFirstTemp += _T("D3 Intensity Measure\t");
			strFirstTemp += _T("D1 Standard Deviation\t");
			strFirstTemp += _T("D2 Standard Deviation\t");
			strFirstTemp += _T("D3 Standard Deviation\t");
			strFirstTemp += _T("D1 Background Noise\t");
			strFirstTemp += _T("D2 Background Noise\t");
			strFirstTemp += _T("D3 Background Noise\t");
			strFirstTemp += _T("D1 Intensity\t");
			strFirstTemp += _T("D2 Intensity\t");
			strFirstTemp += _T("D3 Intensity\t");
			strFirstTemp += _T("Transmittance\t");
			strFirstTemp += _T("Reflectance\t");
			strFirstTemp += _T("Wave Length(nm)\t");
			strFirstTemp += _T("Wave ExposureTime(ms)\t");
			fprintf(fp, strFirstTemp + _T("\n"));
			fprintf(fp, strTemp + _T("\n"));
		}
		else { fprintf(fp, strTemp + _T("\n")); }
		fclose(fp);
	}
}

void MaskImageReconstructorView::m_fnFolderCreate()
{
	if (GetFileAttributes((LPCTSTR)RESULT_LOG_FOLDER_PATH) == 0xFFFFFFFF) { CreateDirectory((LPCTSTR)RESULT_LOG_FOLDER_PATH, NULL); }
}

BOOL MaskImageReconstructorView::m_fnScanLastMatrixPositionStatusCheck()
{
	if (m_bTurningPoint)
	{
		memcpy(m_dMatBuffer_T->data, m_dMat_T->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBuffer_R->data, m_dMat_R->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBufferActualScanPos_X->data, m_dMatActualScanPos_X->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBufferActualScanPos_Y->data, m_dMatActualScanPos_Y->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBufferStaticScanPos_X->data, m_dMatStaticScanPos_X->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBufferStaticScanPos_Y->data, m_dMatStaticScanPos_Y->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));

		m_nRawInterpolationCountCheck = 0;
		m_bLastLineCheck = TRUE;

		m_nPixelCountCheck = 0;
		m_nPixelPosX = 0;
		m_nPixelPosY = 0;
		m_dScanAreaThresholdY = 0;
		m_dScanAreaThresholdX = 0;
		m_nPacketNumberCheck = 0;

		m_bTurningPoint = FALSE;

		m_nRawCountCheck++;

		if (m_nRawCountCheck == ((m_nInterpolationCount * m_nAverageCount) * m_nSetCount)) { m_bLastScanSyncCheck = TRUE; }

		m_fnOnMessageStartCreateQueueThread();

		return TRUE;
	}

	return FALSE;
}

BOOL MaskImageReconstructorView::m_fnScanLastThresholdPositionStatusCheck()
{
	if (m_nPixelCountCheck == (m_fnPixelWidth() * m_fnPixelHeight()))
	{
		memcpy(m_dMatBuffer_D1->data, m_dMat_D1->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBuffer_D2->data, m_dMat_D2->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBuffer_D3->data, m_dMat_D3->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBuffer_T->data, m_dMat_T->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBuffer_R->data, m_dMat_R->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBufferActualScanPos_X->data, m_dMatActualScanPos_X->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBufferActualScanPos_Y->data, m_dMatActualScanPos_Y->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBufferStaticScanPos_X->data, m_dMatStaticScanPos_X->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));
		memcpy(m_dMatBufferStaticScanPos_Y->data, m_dMatStaticScanPos_Y->data, (m_fnPixelWidth() * m_fnPixelHeight()) * sizeof(double));

		m_nRawInterpolationCountCheck = 0;
		m_bLastLineCheck = TRUE;

		m_nPixelCountCheck = 0;
		m_nPixelPosX = 0;
		m_nPixelPosY = 0;
		m_dScanAreaThresholdY = 0;
		m_dScanAreaThresholdX = 0;
		m_nPacketNumberCheck = 0;

		m_bTurningPoint = FALSE;

		m_nRawCountCheck++;

		if (m_nRawCountCheck == ((m_nInterpolationCount * m_nAverageCount) * m_nSetCount)) { m_bLastScanSyncCheck = TRUE; }

		m_fnOnMessageStartCreateQueueThread();

		return TRUE;
	}

	return FALSE;
}


HBRUSH MaskImageReconstructorView::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CFormView::OnCtlColor(pDC, pWnd, nCtlColor);

	UINT nID = pWnd->GetDlgCtrlID();

	if (nCtlColor == CTLCOLOR_STATIC)
	{
		if (nID == IDC_STATIC_PROGRAM_PROJECT)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(91, 91, 91));
			hbr = m_brushsMenubar;
		}

		if (nID == IDC_STATIC							|| 
			nID == IDC_STATIC_AVERAGE_COUNT				|| 
			nID == IDC_STATIC_RAW_COUNT					|| 
			nID == IDC_STATIC_RAW_INTERPOLATION_COUNT	||
			nID == IDC_STATIC_INTERPOLATION_COUNT		||
			nID == IDC_STATIC_PACKET_SEND_TIME			|| 
			nID == IDC_STATIC_BYTE_BPS_TIME				|| 
			nID == IDC_STATIC_CAL_INTERPOLATION_TIME	|| 
			nID == IDC_STATIC_CAL_AVERAGE_TIME)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(38, 38, 38));
			hbr = m_brushsStatic;
		}

		if ((nID == IDC_STATIC_EQP_STATUS) && GetEquipmentStatus())
		{
			GetDlgItem(IDC_STATIC_EQP_STATUS)->SetWindowText(_T("SCAN RUNNING..."));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(0, 0, 0));
			pDC->SetBkColor(RGB(0, 255, 0));
			hbr = m_brushsEqpStatusOn;
		}
		else if ((nID == IDC_STATIC_EQP_STATUS) && !GetEquipmentStatus())
		{
			GetDlgItem(IDC_STATIC_EQP_STATUS)->SetWindowText(_T("SCAN STOP"));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(255, 0, 0));
			hbr = m_brushsEqpStatusOff;
		}

		if ((nID == IDC_STATIC_SOCKET_CONNECT) && m_pTcpClient->GetSocketStatus(static_cast<int>(CLIENT_SOCKET::CH_1)))
		{
			GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->SetWindowText(_T("ADAM"));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(0, 0, 0));
			pDC->SetBkColor(RGB(0, 255, 0));
			hbr = m_brushsSocketConnectOn;
		}
		else if ((nID == IDC_STATIC_SOCKET_CONNECT) && !m_pTcpClient->GetSocketStatus(static_cast<int>(CLIENT_SOCKET::CH_1)))
		{
			GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->SetWindowText(_T("ADAM"));
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(255, 0, 0));
			hbr = m_brushsSocketConnectOff;
		}
	}
	else if (nCtlColor == CTLCOLOR_EDIT)
	{
		if (nID == IDC_EDIT_M_PACKET_COUNT ||
			nID == IDC_EDIT_A_PACKET_COUNT ||
			nID == IDC_EDIT_BS_PACKET_COUNT)
		{
			pDC->SetBkMode(TRANSPARENT);
			pDC->SetTextColor(RGB(255, 255, 255));
			pDC->SetBkColor(RGB(38, 38, 38));
			hbr = m_brushsEdit;
		}
	}

	return hbr;
}


void MaskImageReconstructorView::m_fnMenuBarInit()
{
	BITMAP	size;
	HBITMAP hBmp;

	m_ctlProjectName.ModifyStyle(0xF, SS_BITMAP | SS_CENTERIMAGE);

	hBmp = ::LoadBitmap(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDB_BITMAP_PROJECT_NAME));

	m_ctlProjectName.SetBitmap(hBmp);

	::GetObject(hBmp, sizeof(BITMAP), &size);

	m_fStaticBold.CreateFont(20, 8, 0, 0,
		FW_HEAVY, FALSE, FALSE, FALSE,
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH, "굴림");

	GetDlgItem(IDC_STATIC_PROGRAM_PROJECT)->SetWindowText(_T("EPTR #1"));
	GetDlgItem(IDC_STATIC_PROGRAM_PROJECT)->SetFont(&m_fStaticBold);

	GetDlgItem(IDC_STATIC_EQP_STATUS)->SetWindowText(_T("SCAN STOP"));
	GetDlgItem(IDC_STATIC_EQP_STATUS)->SetFont(&m_fStaticBold);
	GetDlgItem(IDC_STATIC_EQP_STATUS)->GetWindowRect(m_rcEqpStatus);
	ScreenToClient(&m_rcEqpStatus);

	GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->SetWindowText(_T("ADAM"));
	GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->SetFont(&m_fStaticBold);
	GetDlgItem(IDC_STATIC_SOCKET_CONNECT)->GetWindowRect(m_rcSocketConnect);
	ScreenToClient(&m_rcSocketConnect);
}

void MaskImageReconstructorView::m_fnDialogInit()
{
	CRect rc;
	GetClientRect(rc);

	m_nDialogWidth	= rc.Width();
	m_nDialogHeight = rc.Height();
}

void MaskImageReconstructorView::m_fnControlInit()
{
	m_fManualBold.CreateFont(14, 6, 0, 0,
		FW_HEAVY, FALSE, FALSE, FALSE,
		DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, OUT_DEFAULT_PRECIS,
		DEFAULT_QUALITY, DEFAULT_PITCH, "굴림");

	GetDlgItem(IDC_BUTTON_TEST_MANUAL_MODE)->SetFont(&m_fManualBold);
	GetDlgItem(IDC_BUTTON_TEST_MANUAL_MODE)->GetWindowRect(m_rcManualMode);
	ScreenToClient(&m_rcManualMode);

	GetDlgItem(IDC_BUTTON_DISPLAY_MODE)->SetFont(&m_fManualBold);
	GetDlgItem(IDC_BUTTON_DISPLAY_MODE)->GetWindowRect(m_rcDisplayMode);
	ScreenToClient(&m_rcDisplayMode);
}

CString MaskImageReconstructorView::m_fnFormattedString(LPCTSTR lpszFormat, ...)
{
	CString sResult;
	va_list vlMarker = NULL;
	va_start(vlMarker, lpszFormat);
	sResult.FormatV(lpszFormat, vlMarker);
	va_end(vlMarker);
	return sResult;
}

CString MaskImageReconstructorView::m_fnFormattedStringTimeLog(LPCTSTR lpszFormat, ...)
{
	CString sResult;
	va_list vlMarker = NULL;
	va_start(vlMarker, lpszFormat);
	sResult.FormatV(lpszFormat, vlMarker);
	va_end(vlMarker);
	CString strTemp;
	strTemp.Format(_T("%s%s"), m_fnGetCurrentTimeFunction(), sResult);
	return strTemp;
}

void MaskImageReconstructorView::m_fnWriteLogMerge(CString strFileName, CString strType, MeasureDataReturn* pData)
{
	m_fnWriteLog(strFileName,
		m_fnFormattedString(_T("%s%s\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t%.6lf\t"),
			m_fnGetCurrentTime(),
			strType,
			pData->dD1IntensityMeasure,
			pData->dD2IntensityMeasure,
			pData->dD3IntensityMeasure,
			pData->dD1StandardDeviation,
			pData->dD2StandardDeviation,
			pData->dD3StandardDeviation,
			pData->dD1BackgroundNoise,
			pData->dD2BackgroundNoise,
			pData->dD3BackgroundNoise,
			pData->dD1Intensity,
			pData->dD2Intensity,
			pData->dD3Intensity,
			pData->dTransmittance,
			pData->dReflectance,
			pData->dWaveLength,
			pData->dExposureTime));
}

void MaskImageReconstructorView::m_fnMatrixCreate(int nImageWidth, int nImageHeight)
{
	m_dScanPositionSum_X = (double**)malloc(sizeof(double*) * nImageHeight);
	m_dScanPositionSum_Y = (double**)malloc(sizeof(double*) * nImageHeight);
	m_dDetectorSum_1 = (double**)malloc(sizeof(double*) * nImageHeight);
	m_dDetectorSum_2 = (double**)malloc(sizeof(double*) * nImageHeight);
	m_dDetectorSum_3 = (double**)malloc(sizeof(double*) * nImageHeight);
	m_nMatrixCount = (unsigned int**)malloc(sizeof(unsigned int*) * nImageHeight);
	m_bMatrixCheck = (BOOL**)malloc(sizeof(BOOL*) * nImageHeight);

	for (int i = 0; i < nImageHeight; i++)
	{
		m_dScanPositionSum_X[i] = (double*)malloc(sizeof(double) * nImageWidth);
		m_dScanPositionSum_Y[i] = (double*)malloc(sizeof(double) * nImageWidth);
		m_dDetectorSum_1[i] = (double*)malloc(sizeof(double) * nImageWidth);
		m_dDetectorSum_2[i] = (double*)malloc(sizeof(double) * nImageWidth);
		m_dDetectorSum_3[i] = (double*)malloc(sizeof(double) * nImageWidth);
		m_nMatrixCount[i] = (unsigned int*)malloc(sizeof(unsigned int) * nImageWidth);
		m_bMatrixCheck[i] = (BOOL*)malloc(sizeof(BOOL) * nImageWidth);
	}
}

void MaskImageReconstructorView::m_fnMatrixInit(int nImageWidth, int nImageHeight)
{
	for (int y = 0; y < nImageHeight; y++)
	{
		for (int x = 0; x < nImageWidth; x++)
		{
			m_dScanPositionSum_X[y][x] = 0;
			m_dScanPositionSum_Y[y][x] = 0;
			m_dDetectorSum_1[y][x] = 0;
			m_dDetectorSum_2[y][x] = 0;
			m_dDetectorSum_3[y][x] = 0;
			m_nMatrixCount[y][x] = 0;
			m_bMatrixCheck[y][x] = FALSE;
		}
	}
}

void MaskImageReconstructorView::m_fnMatrixDelete(int nImageWidth, int nImageHeight)
{
	for (int i = 0; i < nImageHeight; i++)
	{
		free(m_dScanPositionSum_X[i]);
		free(m_dScanPositionSum_Y[i]);
		free(m_dDetectorSum_1[i]);
		free(m_dDetectorSum_2[i]);
		free(m_dDetectorSum_3[i]);
		free(m_nMatrixCount[i]);
		free(m_bMatrixCheck[i]);
	}

	free(m_dScanPositionSum_X);
	free(m_dScanPositionSum_Y);
	free(m_dDetectorSum_1);
	free(m_dDetectorSum_2);
	free(m_dDetectorSum_3);
	free(m_nMatrixCount);
	free(m_bMatrixCheck);

	m_dScanPositionSum_X = nullptr;
	m_dScanPositionSum_Y = nullptr;
	m_dDetectorSum_1 = nullptr;
	m_dDetectorSum_2 = nullptr;
	m_dDetectorSum_3 = nullptr;
	m_nMatrixCount = nullptr;
	m_bMatrixCheck = nullptr;
}

void MaskImageReconstructorView::m_fnMatrix_Position_Machting_Algorithm(PACKET_M_IMAGE_DATA_CONTENT* pTemp, double* dOffset)
{
	uchar* pImageRawData		= m_MatRawSRC->data;
	double* pDetector_T			= (double*)m_dMat_T->data;
	double* pDetector_R			= (double*)m_dMat_R->data;
	double* pScanActualPos_X	= (double*)m_dMatActualScanPos_X->data;
	double* pScanActualPos_Y	= (double*)m_dMatActualScanPos_Y->data;
	double* pScanStaticPos_X	= (double*)m_dMatStaticScanPos_X->data;
	double* pScanStaticPos_Y	= (double*)m_dMatStaticScanPos_Y->data;

	double			dTransmittance	= 0;
	double			dReflectance	= 0;
	unsigned int	nImageWidth		= m_fnPixelWidth();
	unsigned int	nImageHeight	= m_fnPixelHeight();

	double dActualPosX	= pTemp->EM1024X;
	double dActualPosY	= pTemp->EM1024Y;
	double dThPosX_A	= 0 - *dOffset;
	double dThPosY_A	= 0 - *dOffset;
	double dThPosX_B	= m_nImageFOV_X + *dOffset;
	double dThPosY_B	= m_nImageFOV_Y + *dOffset;

	if (MACRO_POSITION_LIMIT_PLUS(dActualPosY, dThPosY_A, dThPosY_B))
	{
		if (MACRO_POSITION_LIMIT_PLUS(dActualPosX, dThPosX_A, dThPosX_B))
		{
			if (m_bManualModeStatus)
			{
				unsigned int nMotionX = floor(pTemp->EM1024X + 0.5);
				unsigned int nMotionY = floor(pTemp->EM1024Y + 0.5);

				unsigned int nMatrixX = (unsigned int)((double)nMotionX / (double)m_nScanGrid_X);
				unsigned int nMatrixY = (unsigned int)((double)nMotionY / (double)m_nScanGrid_Y);

				if (nMatrixX >= m_fnPixelWidth())	{ return; }
				if (nMatrixY >= m_fnPixelHeight())	{ return; }
			
				m_nMatrixCount[nMatrixY][nMatrixX] += 1;

				m_dScanPositionSum_X[nMatrixY][nMatrixX] = dActualPosX;
				m_dScanPositionSum_Y[nMatrixY][nMatrixX] = dActualPosY;

				pScanActualPos_X[(nMatrixY * nImageWidth  * 1) + nMatrixX] = dActualPosX;
				pScanActualPos_Y[(nMatrixY * nImageHeight * 1) + nMatrixX] = dActualPosY;

				pScanStaticPos_X[(nMatrixY * nImageWidth  * 1) + nMatrixX] = (double)((double)(nMatrixX) * (double)m_nScanGrid_X);
				pScanStaticPos_Y[(nMatrixY * nImageHeight * 1) + nMatrixX] = (double)((double)(nMatrixY) * (double)m_nScanGrid_Y);

				m_dDetectorSum_1[nMatrixY][nMatrixX] += pTemp->Detector_1;
				m_dDetectorSum_2[nMatrixY][nMatrixX] += pTemp->Detector_2;
				m_dDetectorSum_3[nMatrixY][nMatrixX] += pTemp->Detector_3;

				PythonAlgorithm::MeasurePellicleSum(
					&m_nMatrixCount[nMatrixY][nMatrixX],
					&m_dDetectorSum_1[nMatrixY][nMatrixX],
					&m_dDetectorSum_2[nMatrixY][nMatrixX],
					&m_dDetectorSum_3[nMatrixY][nMatrixX]);

				PythonAlgorithm::GetPellicleData(&dTransmittance, &dReflectance);

				double dDetector = (m_dDetectorSum_2[nMatrixY][nMatrixX] / (double)m_nMatrixCount[nMatrixY][nMatrixX]);

				m_pImageProcessAlgorithm->CalColorBufferScanMode(pDetector_T, pDetector_R, pImageRawData, dDetector, dDetector, nMatrixX, nMatrixY, nImageWidth, nImageHeight);

				if (m_bMatrixCheck[nMatrixY][nMatrixX] == FALSE) { m_bMatrixCheck[nMatrixY][nMatrixX] = TRUE; m_nPixelCountCheck++; }
			}
			else
			{
				unsigned int nMotionX = floor(pTemp->EM1024X + 0.5);
				unsigned int nMotionY = floor(pTemp->EM1024Y + 0.5);

				unsigned int nMatrixX = (unsigned int)((double)nMotionX / (double)m_nScanGrid_X);
				unsigned int nMatrixY = (unsigned int)((double)nMotionY / (double)m_nScanGrid_Y);

				if (nMatrixX >= m_fnPixelWidth())	{ return; }
				if (nMatrixY >= m_fnPixelHeight())	{ return; }
			
				m_nMatrixCount[nMatrixY][nMatrixX] += 1;
				
				m_dScanPositionSum_X[nMatrixY][nMatrixX] = dActualPosX;
				m_dScanPositionSum_Y[nMatrixY][nMatrixX] = dActualPosY;

				pScanActualPos_X[(nMatrixY * nImageWidth  * 1) + nMatrixX] = dActualPosX;
				pScanActualPos_Y[(nMatrixY * nImageHeight * 1) + nMatrixX] = dActualPosY;

				pScanStaticPos_X[(nMatrixY * nImageWidth  * 1) + nMatrixX] = (double)((double)(nMatrixX) * (double)m_nScanGrid_X);
				pScanStaticPos_Y[(nMatrixY * nImageHeight * 1) + nMatrixX] = (double)((double)(nMatrixY) * (double)m_nScanGrid_Y);

				m_dDetectorSum_1[nMatrixY][nMatrixX] += pTemp->Detector_1;
				m_dDetectorSum_2[nMatrixY][nMatrixX] += pTemp->Detector_2;
				m_dDetectorSum_3[nMatrixY][nMatrixX] += pTemp->Detector_3;

				PythonAlgorithm::MeasurePellicleSum(
					&m_nMatrixCount[nMatrixY][nMatrixX],
					&m_dDetectorSum_1[nMatrixY][nMatrixX],
					&m_dDetectorSum_2[nMatrixY][nMatrixX],
					&m_dDetectorSum_3[nMatrixY][nMatrixX]);

				PythonAlgorithm::GetPellicleData(&dTransmittance, &dReflectance);

				m_pImageProcessAlgorithm->CalColorBufferScanMode(pDetector_T, pDetector_R, pImageRawData, dTransmittance, dReflectance, nMatrixX, nMatrixY, nImageWidth, nImageHeight);

				if (m_bMatrixCheck[nMatrixY][nMatrixX] == FALSE) { m_bMatrixCheck[nMatrixY][nMatrixX] = TRUE; m_nPixelCountCheck++; }

				// 정방향 Turning Point
				if (m_nPixelPosY == (nImageHeight - 1) && nMatrixX >= ((nImageWidth - 1) - 1))
				{
					if (nMatrixX >= m_nTurningIndex) { m_nTurningIndex = nMatrixX;	}
					else							 { m_bTurningPoint = TRUE;		}
				}

				// 역방향 Turning Point
//				if (((nImageHeight - 1) - nMatrixY) == (nImageHeight - 1) && ((nImageWidth - 1) - nMatrixX) >= ((nImageWidth - 1) - 1))
//				{
//					if (((nImageWidth - 1) - nMatrixX) > m_nTurningIndex) { m_nTurningIndex = (((nImageWidth - 1)) - nMatrixX); }
//					else												  { m_bTurningPoint = TRUE;								}
//				}
			}
		}
	}
}

void MaskImageReconstructorView::m_fnPosition_Threshold_Algorithm(PACKET_M_IMAGE_DATA_CONTENT* pTemp, double* dOffset, BOOL* bManualContinue)
{
	double dActualPosX	= pTemp->EM1024X;
	double dActualPosY	= pTemp->EM1024Y;
	double dThPosX_A	= 0 - *dOffset;
	double dThPosY_A	= 0 - *dOffset;
	double dThPosX_B	= m_nImageFOV_X + *dOffset;
	double dThPosY_B	= m_nImageFOV_Y + *dOffset;

	if (MACRO_POSITION_LIMIT_PLUS(dActualPosY, dThPosY_A, dThPosY_B))
	{
		double dScanGridX = 0;
		double dScanGridY = 0;

		if (!m_bManualModeStatus)
		{
			if (m_nPixelPosX == 0 || m_nPixelPosX == m_fnPixelWidth() - 1) { dScanGridX = (double)m_nScanGrid_X / 2; }
			else														   { dScanGridX = (double)m_nScanGrid_X;	 }
		}
		else
		{
			dScanGridX = (double)m_nScanGrid_X;
			*dOffset = 0.1;
		}

		dScanGridY = (double)m_nScanGrid_Y;

		if (m_nPixelPosY % 2 == FALSE)  { m_StartForwardScan	(pTemp, &dScanGridX, &dScanGridY, dOffset, bManualContinue); }
		else							{ m_StartBackwardScan	(pTemp, &dScanGridX, &dScanGridY, dOffset, bManualContinue); }
	}
}

void MaskImageReconstructorView::m_fnSetControllEnable(BOOL bFlag)
{
	GetDlgItem(IDC_BUTTON_PROTOCOL_ADAM_RUN)->EnableWindow(bFlag);
	GetDlgItem(IDC_BUTTON_PROTOCOL_ADAM_STOP)->EnableWindow(TRUE);
	GetDlgItem(IDC_BUTTON_PROTOCOL_ADAM_RECIPE)->EnableWindow(bFlag);
	GetDlgItem(IDC_BUTTON_PROTOCOL_M_PACKET_RUN)->EnableWindow(bFlag);
	GetDlgItem(IDC_BUTTON_PROTOCOL_A_PACKET_RUN)->EnableWindow(bFlag);
	GetDlgItem(IDC_BUTTON_PTR_TEST)->EnableWindow(bFlag);
	GetDlgItem(IDC_BUTTON_PROTOCOL_EM1024_RESET)->EnableWindow(bFlag);
	GetDlgItem(IDC_BUTTON_TEST_MANUAL_MODE)->EnableWindow(bFlag);
	GetDlgItem(IDC_BUTTON_DISPLAY_MODE)->EnableWindow(bFlag);
	GetDlgItem(IDC_LIST_RECIEPE)->EnableWindow(bFlag);
	GetDlgItem(IDC_EDIT_M_PACKET_COUNT)->EnableWindow(bFlag);
	GetDlgItem(IDC_EDIT_A_PACKET_COUNT)->EnableWindow(bFlag);
	GetDlgItem(IDC_EDIT_BS_PACKET_COUNT)->EnableWindow(bFlag);
}

void MaskImageReconstructorView::OnBnClickedButtonDisplayMode()
{
	if (m_bDisplayModeStatus) { m_bDisplayModeStatus = FALSE; }
	else					  { m_bDisplayModeStatus = TRUE;  }

	m_fnRefreshDisplayModeRect();
}


void MaskImageReconstructorView::OnSize(UINT nType, int cx, int cy)
{
	CFormView::OnSize(nType, cx, cy);

	int nX = cx - m_nDialogWidth;
	int nY = cy - m_nDialogHeight;

	m_pLogViewer->OnSize(static_cast<int>(LOG_VIEWER::CH_1), nType, nX, nY);
}

void MaskImageReconstructorView::m_fnCreateLogViewerInit()
{
	TCHAR chFilePath[256] = { 0, };
	GetModuleFileName(NULL, chFilePath, 256);
	
	CString strFolderTemp(chFilePath);
	CString strFolderPath;
	CString strTotalPath;
	CString strFileName = _T("MaskImageReconstructor");

	strFolderTemp	= strFolderTemp.Left(strFolderTemp.ReverseFind('\\'));
	strFolderTemp	= strFolderTemp.Left(strFolderTemp.ReverseFind('\\'));
	strFolderTemp	= strFolderTemp.Left(strFolderTemp.ReverseFind('\\'));
	strFolderPath	= strFolderTemp + _T("\\Log\\");
	strTotalPath	= strFolderPath + strFileName;

	m_pLogViewer->CreateCheckFolderPath(strTotalPath);
	m_pLogViewer->OnInitialDialog(static_cast<UINT>(LOG_VIEWER::CH_1), 820, 460, 715, 505);
	m_pLogViewer->SetProgramName(static_cast<int>(LOG_VIEWER::CH_1), strFileName);
	m_pLogViewer->SetLogFolderPath(static_cast<int>(LOG_VIEWER::CH_1), strFolderPath);
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_SEQUENCE, _T("Sequence"));
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_SCAN, _T("Scan"));
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_MEASURE, _T("Measure"));
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_EVENT, _T("Event"));
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_MESSAGE, _T("Message"));
	m_pLogViewer->CreateTabDlg(static_cast<UINT>(LOG_VIEWER::CH_1), LOG_COMMUNICATION, _T("Communication"));
	m_pLogViewer->SetTabControlPos(static_cast<int>(LOG_VIEWER::CH_1), LOG_SEQUENCE);
}

BOOL MaskImageReconstructorView::OnEraseBkgnd(CDC* pDC)
{
	CRect rect;
	GetClientRect(rect);
	pDC->FillSolidRect(rect, RGB(38, 38, 38));

	return TRUE;
	//return CFormView::OnEraseBkgnd(pDC);
}

void MaskImageReconstructorView::m_fnAutoSocketConnect()
{
	auto t = std::thread([&]()
		{
			if (m_pLogViewer != nullptr)
			{
				m_pLogViewer->AddLog(
					static_cast<UINT>(LOG_VIEWER::CH_1),
					LOG_SEQUENCE,
					m_fnFormattedStringTimeLog(_T("* Start m_fnAutoSocketConnect()")));
			}

			while (GetAutoSocketConnectThread())
			{
				if (!m_pTcpClient->GetSocketStatus(static_cast<int>(CLIENT_SOCKET::CH_1)))
				{
					m_fnServerConnect();
					Sleep(1000);
					if (GetAutoSocketConnectThread()) { m_fnRefreshSocketRect(); }
				}
				Sleep(1);
			}

			if (m_pLogViewer != nullptr)
			{
				m_pLogViewer->AddLog(
					static_cast<UINT>(LOG_VIEWER::CH_1),
					LOG_SEQUENCE,
					m_fnFormattedStringTimeLog(_T("* End m_fnAutoSocketConnect()")));
			}

		}); t.detach();
}

void MaskImageReconstructorView::m_fnAutoComSocketConnect()
{
	auto t = std::thread([&]()
		{
			if (m_pLogViewer != nullptr)
			{
				m_pLogViewer->AddLog(
					static_cast<UINT>(LOG_VIEWER::CH_1),
					LOG_SEQUENCE,
					m_fnFormattedStringTimeLog(_T("* Start m_fnAutoComSocketConnect()")));
			}

			while (GetAutoComSocketConnectThread())
			{
				if (!m_pTcpComClient->GetSocketStatus(static_cast<int>(CLIENT_SOCKET::CH_1)))
				{
					m_fnComServerConnect();
					Sleep(1000);
				}
				Sleep(1);
			}

			if (m_pLogViewer != nullptr)
			{
				m_pLogViewer->AddLog(
					static_cast<UINT>(LOG_VIEWER::CH_1),
					LOG_SEQUENCE,
					m_fnFormattedStringTimeLog(_T("* End m_fnAutoComSocketConnect()")));
			}
		}); t.detach();
}

void MaskImageReconstructorView::m_fnAutoProgramAliveCheck()
{
	auto t = std::thread([&]()
		{
			if (m_pLogViewer != nullptr)
			{
				m_pLogViewer->AddLog(
					static_cast<UINT>(LOG_VIEWER::CH_1),
					LOG_SEQUENCE,
					m_fnFormattedStringTimeLog(_T("* Start m_fnAutoProgramAliveCheck()")));
			}

			while (GetAliveThread())
			{
				m_fnAutoProgramAliveUpdate();
				Sleep(500);
				if (m_bAliveFlag) { m_bAliveFlag = FALSE; }
				else { m_bAliveFlag = TRUE; }
			}

			if (m_pLogViewer != nullptr)
			{
				m_pLogViewer->AddLog(
					static_cast<UINT>(LOG_VIEWER::CH_1),
					LOG_SEQUENCE,
					m_fnFormattedStringTimeLog(_T("* End m_fnAutoProgramAliveCheck()")));
			}
		}); t.detach();
}

void MaskImageReconstructorView::m_fnAutoProgramAliveUpdate()
{
	BYTE* byAlive = (BYTE*)malloc(sizeof(BYTE) * 1);

	if (m_bAliveFlag) { *byAlive = 0x01; }
	else { *byAlive = 0x00; }

	m_pSharedMemory->SetMapCreateFileMapping(
		&m_hAliveMapping,
		19100,
		SHARED_MEMORY_PUBLIC_ESOL_SPACE);

	int nLastError = 0;

	if (m_pAliveAddress == NULL)
	{
		m_pAliveAddress = (BYTE*)MapViewOfFile(
			m_hAliveMapping,
			FILE_MAP_ALL_ACCESS,
			0,
			0,
			19100);

		if (m_pAliveAddress == NULL)
		{
			nLastError = GetLastError();
		}
	}

	BYTE* pAliveAddress = &m_pAliveAddress[19001];
	memcpy(pAliveAddress, byAlive, 1);

	free(byAlive);
}

void MaskImageReconstructorView::OnBnClickedButtonProgramVersion()
{
	CMainFrame*	pFrame = (CMainFrame *)AfxGetMainWnd();

	if (pFrame->m_dDlgVersion->GetSafeHwnd() == NULL)
	{
		pFrame->m_dDlgVersion = new CDlgVersion(pFrame, this);
		pFrame->m_dDlgVersion->Create(IDD_DLG_VERSION, pFrame);
		pFrame->m_dDlgVersion->ShowWindow(SW_HIDE);
	}

	if (pFrame->m_dDlgVersion->GetSafeHwnd() != NULL && pFrame->m_dDlgVersion->IsWindowVisible() != TRUE)
	{
		CRect rParents;
		GetDlgItem(IDC_BUTTON_PROGRAM_VERSION)->GetWindowRect(&rParents);

		CRect rChilld;
		pFrame->m_dDlgVersion->GetWindowRect(&rChilld);

		pFrame->m_dDlgVersion->MoveWindow(rParents.right + 5, rParents.top, rChilld.Width(), rChilld.Height());
		pFrame->m_dDlgVersion->ShowWindow(SW_SHOW);
	}
	else
	{
		pFrame->m_dDlgVersion->ShowWindow(SW_HIDE);
	}
}

void MaskImageReconstructorView::m_fnVersionInit()
{
	TCHAR chFilePath[256] = { 0, };
	GetModuleFileName(NULL, chFilePath, 256);

	CString strFolderPath(chFilePath);
	CString strFolderTemp;

	strFolderPath = strFolderPath.Left(strFolderPath.ReverseFind('\\'));
	strFolderPath = strFolderPath.Left(strFolderPath.ReverseFind('\\'));
	strFolderTemp = strFolderPath.Left(strFolderPath.ReverseFind('\\'));
	strFolderTemp = strFolderTemp + _T("\\Project\\Reference\\Config\\");

	CString strVersionFile = strFolderTemp + _T("Version.txt");

	CString strVersion;

	CStdioFile f;
	if (f.Open(strVersionFile, CFile::modeRead | CFile::typeText))
	{
		CString strLine;
		while (f.ReadString(strLine)) { break; }
		strVersion = strLine.Right(strLine.ReverseFind(':') + 3);
	}
	f.Close();

	GetDlgItem(IDC_BUTTON_PROGRAM_VERSION)->SetFont(&m_fStaticBold);
	GetDlgItem(IDC_BUTTON_PROGRAM_VERSION)->SetWindowText(strVersion);
}

void MaskImageReconstructorView::m_fnStandbyModeMemoryOptimizationCheck()
{
	int nRet = m_pImageProcessAlgorithm->StandbyModeMemoryOptimization();

	switch (nRet)
	{
		case 1:
		{
			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("Standby Mode Memory Optimization : Success")));
			break;
		}
		case -1:
		{
			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("Standby Mode Memory : (ntdll.dll / SeIncreaseQuotaPrivilege / SeProfileSingleProcessPrivilege) Fail")));
			break;
		}
		case -2:
		{
			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("Standby Mode Memory : (NtSetSystemInformation) Fail")));
			break;
		}
		case -3:
		{
			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_SEQUENCE,
				m_fnFormattedStringTimeLog(_T("Standby Mode Memory : (OpenProcessToken) Fail")));
			break;
		}
	}
}

BOOL MaskImageReconstructorView::OnComAdamRecipe(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_ADAM_RECIPE_DATA* pDataParam = (COM_ADAM_RECIPE_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComAdamRecipe **************")));

	m_nImageFOV_X			= pDataParam->ImageFOV_X;
	m_nImageFOV_Y			= pDataParam->ImageFOV_Y;
	m_nScanGrid_X			= pDataParam->ScanGrid_X;
	m_nScanGrid_Y			= pDataParam->ScanGrid_Y;
	m_nInterpolationCount	= pDataParam->InterpolationCount;

	if (m_bManualModeStatus)
	{
		m_nAverageCount		= pDataParam->AverageCount;
		m_nSetCount			= pDataParam->SetCount;
		m_nScanDirection	= pDataParam->ScanDirection;
		m_nPacketCount		= pDataParam->PacketCount;
	}

	PACKET_RECIPE_DATA PacketData;

	PacketData.Stx					= MAIN_TO_ADAM_ID;
	PacketData.Len					= 42;
	PacketData.Cmd					= 0x98;
	PacketData.ImageFOV_X			= m_nImageFOV_X;
	PacketData.ImageFOV_Y			= m_nImageFOV_Y;
	PacketData.ScanGrid_X			= m_nScanGrid_X;
	PacketData.ScanGrid_Y			= m_nScanGrid_Y;
	PacketData.InterpolationCount	= m_nInterpolationCount;

	if (m_bManualModeStatus)
	{
		PacketData.AverageCount		= m_nAverageCount;
		PacketData.SetCount			= m_nSetCount;
		PacketData.ScanDirection	= m_nScanDirection;
		PacketData.PacketCount		= m_nPacketCount;
	}

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* FOV_X : %d"),
			m_nImageFOV_X));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* FOV_Y : %d"),
			m_nImageFOV_Y));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* ScanGrid_X : %d"),
			m_nScanGrid_X));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* ScanGrid_Y : %d"),
			m_nScanGrid_Y));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* InterpolationCount : %d"),
			m_nInterpolationCount));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* AverageCount : %d"),
			m_nAverageCount));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* SetCount : %d"),
			m_nSetCount));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* ScanDirection : %d"),
			m_nScanDirection));

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("* PacketCount : %d"),
			m_nPacketCount));

	m_fnScanRecipeUpdate();
	m_fnSequenceDataInit();

	if (m_bManualModeStatus) { m_pTcpClient->SendScanRecipe(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData); }

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		COM_ADAM_RECIPE_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "REC", '@', &EsolPacketData, &PacketContents, sizeof(COM_ADAM_RECIPE_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComAdamRun(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_ADAM_RECIPE_DATA* pDataParam = (COM_ADAM_RECIPE_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComAdamRun **************")));

	m_nPacketMode = MODE_RUN_PACKET;

	PACKET_RUN_STOP_DATA PacketData;

	PacketData.Stx	= MAIN_TO_ADAM_ID;
	PacketData.Len	= MAIN_TO_ADAM_LENGTH_1;
	PacketData.Cmd	= MAIN_TO_ADAM_CMD_ADAM_RUN_STOP;
	PacketData.Data = MAIN_TO_ADAM_DATA_ADAM_RUN;

	m_fnSequenceDataInit();
	StartThread();
	StartImageThread();

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), TRUE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendRunStopPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	SetEquipmentStatus(TRUE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(FALSE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		COM_ADAM_RUN_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "ADA", "ASR", '@', &EsolPacketData, &PacketContents, sizeof(COM_ADAM_RUN_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComAdamStop(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_ADAM_STOP_DATA* pDataParam = (COM_ADAM_STOP_DATA*)pData;

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComAdamStop **************")));

	m_nPacketMode = MODE_RUN_PACKET;

	PACKET_RUN_STOP_DATA PacketData;

	PacketData.Stx	= MAIN_TO_ADAM_ID;
	PacketData.Len	= MAIN_TO_ADAM_LENGTH_1;
	PacketData.Cmd	= MAIN_TO_ADAM_CMD_ADAM_RUN_STOP;
	PacketData.Data = MAIN_TO_ADAM_DATA_ADAM_STOP;

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), FALSE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendRunStopPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	StopThread();
	StopImageThread();

	SetEquipmentStatus(FALSE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(TRUE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		COM_ADAM_STOP_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "ADA", "AST", '@', &EsolPacketData, &PacketContents, sizeof(COM_ADAM_STOP_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComAdamAverageCount(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_ADAM_AVERAGE_COUNT_DATA* pDataParam = (COM_ADAM_AVERAGE_COUNT_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComAdamAverageCount **************")));

	m_nPacketMode = MODE_A_PACKET;
	m_nAPacketCount = pDataParam->nCount;

	PACKET_AVERAGE_COUNT_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_4;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_AVERAGE_COUNT;
	PacketData.Data = m_nAPacketCount;

	CString strCount;
	strCount.Format(_T("%d"), m_nAPacketCount);
	SetDlgItemText(IDC_EDIT_A_PACKET_COUNT, strCount);

	m_pTcpClient->SendAverageCountPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		COM_ADAM_AVERAGE_COUNT_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "ADA", "AGC", '@', &EsolPacketData, &PacketContents, sizeof(COM_ADAM_AVERAGE_COUNT_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComAdamAPacketRun(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_ADAM_A_PACKET_RUN_DATA* pDataParam = (COM_ADAM_A_PACKET_RUN_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComAdamAPacketRun **************")));

	m_fnSequenceDataInit();
	StartThread();

	m_nPacketMode = MODE_A_PACKET;

	PACKET_A_PACKET_RUN_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_1;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_A_PACKET_RUN;
	PacketData.Data = 1;

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), FALSE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendAPacketRunPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	SetEquipmentStatus(TRUE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(FALSE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::NG))
	{
		COM_ADAM_A_PACKET_RUN_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "ADA", "AAR", '@', &EsolPacketData, &PacketContents, sizeof(COM_ADAM_A_PACKET_RUN_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComAdamMPacketRun(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_ADAM_M_PACKET_RUN_DATA* pDataParam = (COM_ADAM_M_PACKET_RUN_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComAdamMPacketRun **************")));

	m_fnSequenceDataInit();
	StartThread();

	m_nPacketMode = MODE_M_PACKET;
	m_nMPacketCount = pDataParam->nCount;

	PACKET_M_PACKET_RUN_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_4;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_M_PACKET_RUN;
	PacketData.Data = m_nMPacketCount;

	CString strCount;
	strCount.Format(_T("%d"), m_nMPacketCount);
	SetDlgItemText(IDC_EDIT_M_PACKET_COUNT, strCount);

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), TRUE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendMPacketRunPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	SetEquipmentStatus(TRUE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(FALSE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::NG))
	{
		COM_ADAM_M_PACKET_RUN_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "ADA", "AMR", '@', &EsolPacketData, &PacketContents, sizeof(COM_ADAM_M_PACKET_RUN_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComAdamReadDetector(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_ADAM_READ_DETECTOR_RUN_DATA* pDataParam = (COM_ADAM_READ_DETECTOR_RUN_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComAdamReadDetector **************")));

	m_fnSequenceDataInit();
	StartThread();

	m_nPacketMode = MODE_READ_DETECTOR;
	m_nMPacketCount = pDataParam->nCount;

	PACKET_M_PACKET_RUN_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_4;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_M_PACKET_RUN;
	PacketData.Data = m_nMPacketCount;

	CString strCount;
	strCount.Format(_T("%d"), m_nMPacketCount);
	SetDlgItemText(IDC_EDIT_M_PACKET_COUNT, strCount);

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), TRUE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendMPacketRunPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	SetEquipmentStatus(TRUE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(FALSE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::NG))
	{
		COM_ADAM_READ_DETECTOR_RUN_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "ADA", "ARR", '@', &EsolPacketData, &PacketContents, sizeof(COM_ADAM_READ_DETECTOR_RUN_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComAdamEm1024Reset(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_ADAM_EM1024_RESET_DATA* pDataParam = (COM_ADAM_EM1024_RESET_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComAdamEm1024Reset **************")));

	m_nPacketMode = MODE_RUN_PACKET;

	PACKET_EM1024_RESET_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_1;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_EM1024_RESET;
	PacketData.Data = 1;

	m_pTcpClient->SendEm1024ResetPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		COM_ADAM_EM1024_RESET_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "ADA", "EMR", '@', &EsolPacketData, &PacketContents, sizeof(COM_ADAM_EM1024_RESET_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMeasureBG(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_MEASURE_BG_DATA* pDataParam = (COM_MEASURE_BG_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComMeasureBG **************")));

	m_fnSequenceDataInit();
	StartThread();

	m_nPacketMode = MODE_BG_PACKET;
	m_nPacketCount_BG = pDataParam->nCount;

	PACKET_M_PACKET_RUN_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_4;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_M_PACKET_RUN;
	PacketData.Data = m_nPacketCount_BG;

	CString strCount;
	strCount.Format(_T("%d"), m_nPacketCount_BG);
	SetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, strCount);

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), TRUE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendMPacketRunPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	SetEquipmentStatus(TRUE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(FALSE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::NG))
	{
		COM_MEASURE_BG_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "MBG", '@', &EsolPacketData, &PacketContents, sizeof(COM_MEASURE_BG_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMeasureWO_P(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_MEASURE_WO_P_DATA* pDataParam = (COM_MEASURE_WO_P_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComMeasureWO_P **************")));

	m_fnSequenceDataInit();
	StartThread();

	m_nPacketMode = MODE_WO_P_PACKET;
	m_nPacketCount_WO_P = pDataParam->nCount;

	PACKET_M_PACKET_RUN_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_4;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_M_PACKET_RUN;
	PacketData.Data = m_nPacketCount_WO_P;

	CString strCount;
	strCount.Format(_T("%d"), m_nPacketCount_WO_P);
	SetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, strCount);

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), TRUE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendMPacketRunPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	SetEquipmentStatus(TRUE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(FALSE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::NG))
	{
		COM_MEASURE_BG_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "WOP", '@', &EsolPacketData, &PacketContents, sizeof(COM_MEASURE_BG_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMeasureBS(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_MEASURE_BS_DATA* pDataParam = (COM_MEASURE_BS_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComMeasureBS **************")));

	m_fnSequenceDataInit();
	StartThread();

	m_nPacketMode = MODE_BS_PACKET;
	m_nPacketCount_BS = pDataParam->nCount;

	PACKET_M_PACKET_RUN_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_4;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_M_PACKET_RUN;
	PacketData.Data = m_nPacketCount_BS;

	CString strCount;
	strCount.Format(_T("%d"), m_nPacketCount_BS);
	SetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, strCount);

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), TRUE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendMPacketRunPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	SetEquipmentStatus(TRUE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(FALSE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::NG))
	{
		COM_MEASURE_BS_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "MBS", '@', &EsolPacketData, &PacketContents, sizeof(COM_MEASURE_BS_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMeasureP_As_Point(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_MEASURE_P_AS_POINT_DATA* pDataParam = (COM_MEASURE_P_AS_POINT_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComMeasureP_As_Point **************")));

	m_fnSequenceDataInit();
	StartThread();

	m_nPacketMode = MODE_P_AS_POINT_PACKET;
	m_nPacketCount_P_AS_POINT = pDataParam->nCount;

	PACKET_M_PACKET_RUN_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_4;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_M_PACKET_RUN;
	PacketData.Data = m_nPacketCount_P_AS_POINT;

	CString strCount;
	strCount.Format(_T("%d"), m_nPacketCount_P_AS_POINT);
	SetDlgItemText(IDC_EDIT_BS_PACKET_COUNT, strCount);

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), TRUE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendMPacketRunPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	SetEquipmentStatus(TRUE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(FALSE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::NG))
	{
		COM_MEASURE_P_AS_POINT_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "PAP", '@', &EsolPacketData, &PacketContents, sizeof(COM_MEASURE_P_AS_POINT_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMeasureP_As_Scan(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_MEASURE_P_AS_SCAN_DATA* pDataParam = (COM_MEASURE_P_AS_SCAN_DATA*)pData;

	if (!m_fnPacketReadyCheck())
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_SEQUENCE,
			m_fnFormattedStringTimeLog(_T("* Sequence is still running..")));
		return TRUE;
	}

	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComMeasureP_As_Scan **************")));

	m_fnSequenceDataInit();
	StartThread();
	StartImageThread();

	m_nPacketMode = MODE_RUN_PACKET;

	PACKET_RUN_STOP_DATA PacketData;

	PacketData.Stx = MAIN_TO_ADAM_ID;
	PacketData.Len = MAIN_TO_ADAM_LENGTH_1;
	PacketData.Cmd = MAIN_TO_ADAM_CMD_ADAM_RUN_STOP;
	PacketData.Data = MAIN_TO_ADAM_DATA_ADAM_RUN;

	m_pTcpClient->SetScanPacketMode(static_cast<int>(CLIENT_SOCKET::CH_1), TRUE);
	m_pTcpClient->SetScanPacketLengh(static_cast<int>(CLIENT_SOCKET::CH_1), m_fnGetPacketTotalByteLengh());
	m_pTcpClient->SendRunStopPacket(static_cast<int>(CLIENT_SOCKET::CH_1), &PacketData);

	SetEquipmentStatus(TRUE);
	m_fnRefreshEqpStatusRect();
	m_fnSetControllEnable(FALSE);

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		COM_MEASURE_P_AS_SCAN_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "SEQ", "PAS", '@', &EsolPacketData, &PacketContents, sizeof(COM_MEASURE_P_AS_SCAN_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComMeasureP_As_Scan_Stop()
{
	m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComMeasureP_As_Scan_Stop **************")));

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		COM_MEASURE_P_AS_SCAN_STOP_DATA PacketContents;
		PacketContents.nRet = COM_RETURN_TYPE::OK;

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'C', 'S', 'A', "SEQ", "PST", '@', &EsolPacketData, &PacketContents, sizeof(COM_MEASURE_P_AS_SCAN_STOP_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::OnComGetPacketReadyCheck(ESOL_PACKET_DATA* pPacket, void* pData)
{
	COM_GET_PACKET_READY_DATA* pDataParam = (COM_GET_PACKET_READY_DATA*)pData;

	m_pLogViewer->AddLog(
		static_cast<UINT>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		m_fnFormattedStringTimeLog(_T("************** OnComGetPacketReadyCheck **************")));

	if (static_cast<UINT>(COM_ACKNOWLEDGE::OK))
	{
		COM_GET_PACKET_READY_DATA PacketContents;

		if (!m_fnPacketReadyCheck()) { PacketContents.nRet = COM_RETURN_TYPE::NG; }
		else						 { PacketContents.nRet = COM_RETURN_TYPE::OK; }

		ESOL_PACKET_DATA EsolPacketData;
		m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'S', 'C', 'A', "PAK", "REY", '@', &EsolPacketData, &PacketContents, sizeof(COM_GET_PACKET_READY_DATA));
		ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
		SendProtocolLog(&EsolPacketData);
	}

	return TRUE;
}

BOOL MaskImageReconstructorView::m_fnPacketReadyCheck()
{
	BOOL bRet = FALSE;
	if (!GetThreadStatus() && !GetImageThreadStatus()) { bRet = TRUE; }
	return bRet;
}

BOOL MaskImageReconstructorView::ProtocolCheck(unsigned char cStx, unsigned char cSender, unsigned char cReceiver, unsigned char cType, char* cSequenceCode, char* cFunctionCode, unsigned char cEtx, ESOL_PACKET_DATA* pPacket, unsigned int nLength, BOOL (MaskImageReconstructorView::*fp)(ESOL_PACKET_DATA*, void*))
{
	BYTE* v = (BYTE*)malloc(sizeof(BYTE) * nLength);
	if (cStx				!= pPacket->cStx)				{ free(v); v = nullptr; return FALSE; }
	if (cSender				!= pPacket->cSender)			{ free(v); v = nullptr; return FALSE; }
	if (cReceiver			!= pPacket->cReceiver)			{ free(v); v = nullptr; return FALSE; }
	if (cType				!= pPacket->cType)				{ free(v); v = nullptr; return FALSE; }
	if (cSequenceCode[0]	!= pPacket->cSequenceCode[0])	{ free(v); v = nullptr; return FALSE; }
	if (cSequenceCode[1]	!= pPacket->cSequenceCode[1])	{ free(v); v = nullptr; return FALSE; }
	if (cSequenceCode[2]	!= pPacket->cSequenceCode[2])	{ free(v); v = nullptr; return FALSE; }
	if (cFunctionCode[0]	!= pPacket->cFunctionCode[0])	{ free(v); v = nullptr; return FALSE; }
	if (cFunctionCode[1]	!= pPacket->cFunctionCode[1])	{ free(v); v = nullptr; return FALSE; }
	if (cFunctionCode[2]	!= pPacket->cFunctionCode[2])	{ free(v); v = nullptr; return FALSE; }
	if (cEtx				!= pPacket->cEtx)				{ free(v); v = nullptr; return FALSE; }
	ReceiveProtocolLog(pPacket, TRUE);
	memcpy(v, pPacket->byStruct, sizeof(BYTE) * pPacket->nLength);
	(this->*fp)(pPacket, v);
	free(v); v = nullptr;
	return TRUE;
}

void MaskImageReconstructorView::SendProtocolLog(ESOL_PACKET_DATA* pData)
{
	if (m_pTcpComClient->GetSocketStatus(static_cast<int>(CLIENT_SOCKET::CH_1)))
	{
		m_pLogViewer->AddLog(
			static_cast<UINT>(LOG_VIEWER::CH_1),
			LOG_COMMUNICATION,
			m_fnFormattedStringTimeLog(_T("Send[%d][%d] : [%c][%c][%c][%c][%c%c%c][%c%c%c][%d][Data][%c] : [%d]"),
				m_pTcpComClient->GetRingBufferReceiveIndex(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1)),
				m_pTcpComClient->GetRingBufferIndex(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1)),
				pData->cStx,
				pData->cSender,
				pData->cReceiver,
				pData->cType,
				pData->cSequenceCode[0],
				pData->cSequenceCode[1],
				pData->cSequenceCode[2],
				pData->cFunctionCode[0],
				pData->cFunctionCode[1],
				pData->cFunctionCode[2],
				pData->nLength,
				pData->cEtx,
				m_pTcpComClient->GetRingBufferTotalIndex(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1))));
	}
}

void MaskImageReconstructorView::ReceiveProtocolLog(ESOL_PACKET_DATA* pData, BOOL bFlag)
{
	if (m_pTcpComClient->GetSocketStatus(static_cast<int>(CLIENT_SOCKET::CH_1)))
	{
		if (bFlag)
		{
			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_COMMUNICATION,
				m_fnFormattedStringTimeLog(_T("Recv[%d][%d] : [%c][%c][%c][%c][%c%c%c][%c%c%c][%d][Data][%c] : [%d]"),
					m_pTcpComClient->GetRingBufferReceiveIndex(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1)),
					m_pTcpComClient->GetRingBufferIndex(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1)),
					pData->cStx,
					pData->cSender,
					pData->cReceiver,
					pData->cType,
					pData->cSequenceCode[0],
					pData->cSequenceCode[1],
					pData->cSequenceCode[2],
					pData->cFunctionCode[0],
					pData->cFunctionCode[1],
					pData->cFunctionCode[2],
					pData->nLength,
					pData->cEtx,
					m_pTcpComClient->GetRingBufferTotalIndex(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1))));
		}
		else
		{
			m_pLogViewer->AddLog(
				static_cast<UINT>(LOG_VIEWER::CH_1),
				LOG_COMMUNICATION,
				m_fnFormattedStringTimeLog(_T("Recv Fail[%d][%d] : [%c][%c][%c][%c][%c%c%c][%c%c%c][%d][Data][%c] : [%d]"),
					m_pTcpComClient->GetRingBufferReceiveIndex(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1)),
					m_pTcpComClient->GetRingBufferIndex(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1)),
					pData->cStx,
					pData->cSender,
					pData->cReceiver,
					pData->cType,
					pData->cSequenceCode[0],
					pData->cSequenceCode[1],
					pData->cSequenceCode[2],
					pData->cFunctionCode[0],
					pData->cFunctionCode[1],
					pData->cFunctionCode[2],
					pData->nLength,
					pData->cEtx,
					m_pTcpComClient->GetRingBufferTotalIndex(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1))));
		}
	}
}

void MaskImageReconstructorView::ThreadSendPacketData(unsigned int nClient, ESOL_PACKET_DATA* pPacketData)
{
	MaskImageReconstructorView* pView	= (MaskImageReconstructorView*)GetViewInstance();
	WPARAM						wParam	= 0;
	LPARAM						lParam	= (LPARAM)pPacketData;
	pView->SendMessage(WM_USER_MESSAGE_PROTOCOL_SOCKET_COM_SERVER_CLIENT_THREAD_SEND, wParam, lParam);
}

LRESULT MaskImageReconstructorView::OnMessageComServerClientThreadSend(WPARAM wParam, LPARAM lParam)
{
	ESOL_PACKET_DATA* pEsolPacketData = (ESOL_PACKET_DATA*)lParam;
	m_pTcpComClient->SendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), pEsolPacketData);
	return TRUE;
}

BOOL MaskImageReconstructorView::Protocol(ESOL_PACKET_DATA* pPacket)
{
	if (ProtocolCheck('$', 'S', 'C', 'A', "SEQ", "REC", '@', pPacket, sizeof(COM_ADAM_RECIPE_DATA)					, &MaskImageReconstructorView::OnComAdamRecipe))				{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "ADA", "ASR", '@', pPacket, sizeof(COM_ADAM_RUN_DATA)						, &MaskImageReconstructorView::OnComAdamRun))					{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "ADA", "AST", '@', pPacket, sizeof(COM_ADAM_STOP_DATA)					, &MaskImageReconstructorView::OnComAdamStop))					{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "ADA", "AGC", '@', pPacket, sizeof(COM_ADAM_AVERAGE_COUNT_DATA)			, &MaskImageReconstructorView::OnComAdamAverageCount))			{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "ADA", "AAR", '@', pPacket, sizeof(COM_ADAM_A_PACKET_RUN_DATA)			, &MaskImageReconstructorView::OnComAdamAPacketRun))			{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "ADA", "AMR", '@', pPacket, sizeof(COM_ADAM_M_PACKET_RUN_DATA)			, &MaskImageReconstructorView::OnComAdamMPacketRun))			{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "ADA", "ARR", '@', pPacket, sizeof(COM_ADAM_READ_DETECTOR_RUN_DATA)		, &MaskImageReconstructorView::OnComAdamReadDetector))			{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "ADA", "EMR", '@', pPacket, sizeof(COM_ADAM_EM1024_RESET_DATA)			, &MaskImageReconstructorView::OnComAdamEm1024Reset))			{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "SEQ", "MBG", '@', pPacket, sizeof(COM_MEASURE_BG_DATA)					, &MaskImageReconstructorView::OnComMeasureBG))					{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "SEQ", "WOP", '@', pPacket, sizeof(COM_MEASURE_WO_P_DATA)					, &MaskImageReconstructorView::OnComMeasureWO_P))				{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "SEQ", "MBS", '@', pPacket, sizeof(COM_MEASURE_BS_DATA)					, &MaskImageReconstructorView::OnComMeasureBS))					{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "SEQ", "PAP", '@', pPacket, sizeof(COM_MEASURE_P_AS_POINT_DATA)			, &MaskImageReconstructorView::OnComMeasureP_As_Point))			{ return TRUE; }
	if (ProtocolCheck('$', 'S', 'C', 'A', "SEQ", "PAS", '@', pPacket, sizeof(COM_MEASURE_P_AS_SCAN_DATA)			, &MaskImageReconstructorView::OnComMeasureP_As_Scan))			{ return TRUE; }
	return TRUE;
}

void MaskImageReconstructorView::OnBnClickedButtonTestReadWrite()
{
	auto t = std::thread([&]()
		{
			GetDlgItem(IDC_BUTTON_TEST_READ_WRITE)->EnableWindow(FALSE);
			m_fnStartConvertFile();
			GetDlgItem(IDC_BUTTON_TEST_READ_WRITE)->EnableWindow(TRUE);
		});
	t.detach();
}

void MaskImageReconstructorView::m_fnStartConvertFile()
{
	char szFilter[] = "BIN File (*.bin) | *.bin; | All Files(*.*) | *.* ||";

	CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY, szFilter);

	CStdioFile file;

	if (IDOK == dlg.DoModal())
	{
		CString strSignalName = dlg.GetFileName();
		CString strFolderName = dlg.GetPathName();

		CString strConvertName;
		strConvertName.Format(_T("%s_Convert.csv"), strFolderName);

		double* pD1;
		double* pD2;
		double* pD3;
		double* pT; 
		double* pR;
		double* pActualX;
		double* pActualY;
		double* pStaticX;
		double* pStaticY;

		int nWidth	= 0;
		int nHeight = 0;

		m_pImageProcessAlgorithm->ReadScanDataBinary(pD1, pD2, pD3, pT, pR, pActualX, pActualY, pStaticX, pStaticY, nWidth * nHeight, strFolderName);

		CString strTemp, strAdd;

		double* pWriteData = pD1;

		CFile cfile;

		cfile.Open(strConvertName, CFile::modeCreate | CFile::modeNoTruncate | CFile::modeReadWrite);

		if (m_pImageProcessAlgorithm->CreateFile_(&cfile, strConvertName, 0))
		{
			for (int y = 0; y < nHeight; y++)
			{
				strAdd.Empty();
				for (int x = 0; x < nWidth; x++)
				{
					strTemp.Format(_T("%lf, "), pWriteData[(y * nWidth) + x]);
					strAdd += strTemp;
				}
				m_pImageProcessAlgorithm->WriteFile_(&cfile, strAdd);
			}
			m_pImageProcessAlgorithm->CloseFile_(&cfile);
		}

		free(pD1); pD1 = nullptr;
		free(pD2); pD2 = nullptr;
		free(pD3); pD3 = nullptr;
		free(pT); pT = nullptr;
		free(pR); pR = nullptr;
	}
}