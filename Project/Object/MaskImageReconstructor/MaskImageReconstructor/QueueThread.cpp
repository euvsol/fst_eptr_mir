#include "stdafx.h"
#include "QueueThread.h"

#include "MainFrm.h"
#include "MaskImageReconstructorDoc.h"
#include "MaskImageReconstructor.h"

CQueueThread::CQueueThread(void* pView, void* pClientSocket, int nQueueIndex)
{
	m_pView				= pView;
	m_pClientSocket		= pClientSocket;
	m_nQueueIndex		= nQueueIndex;

	m_nImageWidth		= 0;
	m_nImageHeight		= 0;
	m_nImageFOV_X		= 0;
	m_nImageFOV_Y		= 0;
	m_nScanGridX		= 0;
	m_nScanGridY		= 0;
	m_nQueueMainIndex	= 0;

	m_pThread		= nullptr;
	m_hEvent		= NULL;
	m_bThreadStatus = FALSE;
	ReCreateThread(nQueueIndex);
}

CQueueThread::~CQueueThread()
{
	DeleteMainThreadVector();
	DeleteRawVector();
	DeleteInterpolationVector();
	DeleteAverageVector();
	DeleteThread();
}

BOOL CQueueThread::ReCreateThread(int nQueueIndex)
{
	DeleteThread();

	if (m_pThread == nullptr)
	{
		GROUP_AFFINITY pgThread;
		GROUP_AFFINITY pgPreThread;

		pgThread.Mask = 0xfffffffff;
		pgThread.Reserved[0] = 0;
		pgThread.Reserved[1] = 0;
		pgThread.Reserved[2] = 0;
		pgThread.Group = nQueueIndex % GetMaximumProcessorGroupCount();

		m_hEvent = CreateEventA(NULL, FALSE, FALSE, NULL);
		ResetEvent(m_hEvent);
		m_pThread = AfxBeginThread(Thread, this, THREAD_PRIORITY_HIGHEST, 0, CREATE_SUSPENDED, NULL);
		SetThreadGroupAffinity(m_pThread->m_hThread, &pgThread, &pgPreThread);
		m_pThread->ResumeThread();
	}

	return TRUE;
}

BOOL CQueueThread::DeleteThread()
{
	StopThread();
	Sleep(100);
	if (m_pThread != nullptr)
	{
		InitDynamicVariable();
		DWORD dw;
		::GetExitCodeThread(m_pThread->m_hThread, &dw);
		if (dw == STILL_ACTIVE)
		{
			m_pThread->SuspendThread();
			TerminateThread(m_pThread->m_hThread, 0);
			CloseHandle(m_pThread->m_hThread);
		}
		m_pThread->m_hThread = NULL;
		delete m_pThread;
		m_pThread = nullptr;
	}

	if (m_hEvent != NULL)
	{
		CloseHandle(m_hEvent);
		m_hEvent = NULL;
	}

	return TRUE;
}

BOOL CQueueThread::InitDynamicVariable()
{
	m_nQueueMainIndex = 0;

	return TRUE;
}

UINT CQueueThread::Thread(LPVOID lpParam)
{
	CQueueThread* pThread = (CQueueThread*)lpParam;

	while (1)
	{
		WaitForSingleObject(pThread->m_hEvent, INFINITE);
		pThread->SetThreadStatus(TRUE);
		pThread->Run(NULL, FALSE);
		pThread->SetThreadStatus(FALSE);
		pThread->InitDynamicVariable();
		ResetEvent(pThread->m_hEvent);
	}

	return FALSE;
}

BOOL CQueueThread::Run(int nSequence, BOOL bMessage)
{
	MaskImageReconstructorView* pView = (MaskImageReconstructorView*)m_pView;

	CString strTemp;

	int nImageWidth		= m_nImageWidth;
	int nImageHeight	= m_nImageHeight;
	int nMagnification	= IMAGE_MAGNIFICATION;

	pView->m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		pView->m_fnFormattedStringTimeLog(_T("QueueThread[%d] Start."), m_nQueueIndex));

	QueueMainThreadMonitoring();

	if (pView->m_bTimeUpdate)
	{
		QueryPerformanceFrequency(&pView->m_lFrequency_Average);
		QueryPerformanceCounter(&pView->m_lBeginTime_Average);
	}

	BYTE* dAverageImage_regrid = (BYTE*)malloc(sizeof(double) * (nImageHeight * nMagnification) * (nImageWidth * nMagnification));

	int nVectorSize = m_pVecInterpolationByte.size();

	double dInterpolationDetector = 0;
	double dValueResult = 0;

	if (pView->m_bDisplayModeStatus)
	{
		for (int row = 0; row < (nImageHeight * nMagnification); row++)
		{
			for (int col = 0; col < (nImageWidth * nMagnification); col++)
			{
				for (int i = 0; i < nVectorSize; i++)
				{
					BYTE*	pByte	= m_pVecInterpolationByte.at(i);
					double* dValue	= (double*)&pByte[(row * (nImageWidth * nMagnification * sizeof(double))) + (col * sizeof(double))];

					if (isnan(*dValue)) { dInterpolationDetector = 0; }
					else				{ memcpy(&dInterpolationDetector, dValue, sizeof(double)); }

					dValueResult += dInterpolationDetector;
				}

				dValueResult = (double)((double)dValueResult / (double)nVectorSize);

				memcpy(&dAverageImage_regrid[(row * (nImageWidth * nMagnification * sizeof(double))) + (col * sizeof(double))], &dValueResult, sizeof(double));

				dValueResult = 0;
			}
		}
	}

	PushAverageImageCopy(dAverageImage_regrid);

	double dDetector = 0;

	unsigned int nValue = 0;

	CImageProcessAlgorithm* pPA = pView->m_pImageProcessAlgorithm;

	uchar* cAverageData = (uchar*)malloc(sizeof(uchar) * ((nImageHeight * nMagnification) * (nImageWidth * nMagnification)) * 3);

	if (pView->m_bDisplayModeStatus)
	{
		for (int row = 0; row < (nImageHeight * nMagnification); row++)
		{
			for (int col = 0; col < (nImageWidth * nMagnification); col++)
			{
				memcpy(&dDetector, &dAverageImage_regrid[(row * (nImageWidth * nMagnification * sizeof(double))) + (col * sizeof(double))], sizeof(double));

				nValue = (unsigned int)pPA->ReMap((dDetector * 100), 256, 60, 100);

				cAverageData[(row * (nImageWidth * nMagnification)) * 3 + col * 3 + 0] = pPA->ReMap(pPA->GetColorMapParula(COLOR_MAP_PARULA_B, nValue), 256, 0.0, 1.0);
				cAverageData[(row * (nImageWidth * nMagnification)) * 3 + col * 3 + 1] = pPA->ReMap(pPA->GetColorMapParula(COLOR_MAP_PARULA_G, nValue), 256, 0.0, 1.0);
				cAverageData[(row * (nImageWidth * nMagnification)) * 3 + col * 3 + 2] = pPA->ReMap(pPA->GetColorMapParula(COLOR_MAP_PARULA_R, nValue), 256, 0.0, 1.0);
			}
		}
	}

	memcpy(pView->m_MatAverageSRC->data, cAverageData, ((nImageHeight * nMagnification) * (nImageWidth * nMagnification)) * 3);

	//

	ScanData_D3_Check stD3_Check;

	double dD3_Sum = 0;
	double dD3_Avg = 0;
	double dD3_Min = 10;
	double dD3_Max = 0;
	double dD3_Var = 0;
	double dD3_Std = 0;

	unsigned int nRawCount = (nImageHeight) * (nImageWidth);

	int nRawDataSizeThreeScanSum = 0;

	for (int i = 0; i < m_pVecRawByteD3.size(); i++)
	{
		for (int ii = 0; ii < nRawCount; ii++)
		{
			double* dValue = &((double*)m_pVecRawByteD3[i])[ii];
			if ((*dValue) > dD3_Max) { dD3_Max = (*dValue); }
			if ((*dValue) < dD3_Min) { dD3_Min = (*dValue); }
			dD3_Sum += (*dValue);
		}
		nRawDataSizeThreeScanSum += nRawCount;
	}

	dD3_Avg = dD3_Sum / nRawDataSizeThreeScanSum;

	dD3_Sum = 0;

	for (int i = 0; i < m_pVecRawByteD3.size(); i++)
	{
		for (int ii = 0; ii < nRawCount; ii++)
		{
			double* dValue = &((double*)m_pVecRawByteD3[i])[ii];
			dD3_Sum += pow((*dValue) - dD3_Avg, 2);
		}
	}

	dD3_Var = dD3_Sum / nRawDataSizeThreeScanSum;
	dD3_Std = sqrt(dD3_Var);

	stD3_Check.dD3_Avg = dD3_Avg;
	stD3_Check.dD3_Min = dD3_Min;
	stD3_Check.dD3_Max = dD3_Max;
	stD3_Check.dD3_Std = dD3_Std;

	COM_COMPLETE_SCAN_D3_DATA PacketData;
	PacketData.dD3_Avg = stD3_Check.dD3_Avg;
	PacketData.dD3_Min = stD3_Check.dD3_Min;
	PacketData.dD3_Max = stD3_Check.dD3_Max;
	PacketData.dD3_Std = stD3_Check.dD3_Std;
	PacketData.nRet = COM_RETURN_TYPE::PASS;

	ESOL_PACKET_DATA EsolPacketData;
	pView->m_pTcpComClient->MakePacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), '$', 'C', 'S', 'A', "AVG", "CD3", '@', &EsolPacketData, &PacketData, sizeof(COM_COMPLETE_SCAN_D3_DATA));
	pView->ThreadSendPacketData(static_cast<UINT>(COM_CLIENT_SOCKET::CH_1), &EsolPacketData);
	pView->SendProtocolLog(&EsolPacketData);

	pView->m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		pView->m_fnFormattedStringTimeLog(_T("D3 Check : Avg %lf, Min %lf, Max %lf, Std %lf"), 
			stD3_Check.dD3_Avg,
			stD3_Check.dD3_Min,
			stD3_Check.dD3_Max,
			stD3_Check.dD3_Std));

	//

	free(cAverageData); cAverageData = nullptr;
	free(dAverageImage_regrid); dAverageImage_regrid = nullptr;

	pView->m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		pView->m_fnFormattedStringTimeLog(_T("QueueThread[%d] Average Finish."), m_nQueueIndex));

	DeleteRawVector();
	DeleteInterpolationVector();
	DeleteAverageVector();
	DeleteMainThreadVector();

	pView->m_pLogViewer->AddLog(
		static_cast<int>(LOG_VIEWER::CH_1),
		LOG_SEQUENCE,
		pView->m_fnFormattedStringTimeLog(_T("QueueThread[%d] Finish."), m_nQueueIndex));

	if (pView->m_bTimeUpdate)
	{
		QueryPerformanceCounter(&pView->m_lEndtime_Average);
		pView->m_fnAverageTackTimeUpdate();
	}

	m_nQueueMainIndex = 0;

	pView->m_nAverageCountCheck++;

	return TRUE;
}

BOOL CQueueThread::StartThread()
{
	SetEvent(m_hEvent);
	return TRUE;
}

BOOL CQueueThread::StopThread()
{
	SetThreadStatus(FALSE);
	return TRUE;
}

void CQueueThread::CreateQueueMainThread(BYTE* pRawByteD1, BYTE* pRawByteD2, BYTE* pRawByteD3, BYTE* pRawByteT, BYTE* pRawByteR, BYTE* pRawActualScanBytenPosX, BYTE* pRawActualScanBytenPosY, BYTE* pRawStaticScanBytenPosX, BYTE* pRawStaticScanBytenPosY)
{
	MaskImageReconstructorView* pView = (MaskImageReconstructorView*)m_pView;

	CQueueMainThread* pMainThread = new CQueueMainThread(m_pView, m_pClientSocket, this, m_nQueueIndex, m_nQueueMainIndex);

	PushRawImageCopy(
		pRawByteD1,
		pRawByteD2,
		pRawByteD3,
		pRawByteT,
		pRawByteR,
		pRawActualScanBytenPosX,
		pRawActualScanBytenPosY,
		pRawStaticScanBytenPosX,
		pRawStaticScanBytenPosY);

	pMainThread->SetRawImageByte(
		m_pVecRawByteD1.at(m_nQueueMainIndex),
		m_pVecRawByteD2.at(m_nQueueMainIndex),
		m_pVecRawByteD3.at(m_nQueueMainIndex),
		m_pVecRawByteT.at(m_nQueueMainIndex),
		m_pVecRawByteR.at(m_nQueueMainIndex),
		m_pVecRawActualScanBytePosX.at(m_nQueueMainIndex),
		m_pVecRawActualScanBytePosY.at(m_nQueueMainIndex),
		m_pVecRawStaticScanBytePosX.at(m_nQueueMainIndex),
		m_pVecRawStaticScanBytePosY.at(m_nQueueMainIndex),
		m_nImageWidth,
		m_nImageHeight,
		m_nImageFOV_X,
		m_nImageFOV_Y,
		m_nScanGridX,
		m_nScanGridY);

	m_pVecMainThread.push_back(pMainThread);

	memset(pRawByteD1, 0x00, sizeof(double) * m_nImageWidth * m_nImageHeight);
	memset(pRawByteD2, 0x00, sizeof(double) * m_nImageWidth * m_nImageHeight);
	memset(pRawByteD3, 0x00, sizeof(double) * m_nImageWidth * m_nImageHeight);
	memset(pRawByteT, 0x00, sizeof(double) * m_nImageWidth * m_nImageHeight);
	memset(pRawByteR, 0x00, sizeof(double) * m_nImageWidth * m_nImageHeight);
	memset(pRawActualScanBytenPosX, 0x00, sizeof(double) * m_nImageWidth * m_nImageHeight);
	memset(pRawActualScanBytenPosY, 0x00, sizeof(double) * m_nImageWidth * m_nImageHeight);
	memset(pRawStaticScanBytenPosX, 0x00, sizeof(double) * m_nImageWidth * m_nImageHeight);
	memset(pRawStaticScanBytenPosY, 0x00, sizeof(double) * m_nImageWidth * m_nImageHeight);

	m_nQueueMainIndex++;
}

void CQueueThread::SetQueueMainThread(int nIndex)
{
	CQueueMainThread* pMainThread = m_pVecMainThread.at(nIndex);
	pMainThread->StartThread();
}

void CQueueThread::PushRawImageCopy(BYTE* pRawByteD1, BYTE* pRawByteD2, BYTE* pRawByteD3, BYTE* pRawByteT, BYTE* pRawByteR, BYTE* pRawActualScanBytenPosX, BYTE* pRawActualScanBytenPosY, BYTE* pRawStaticScanBytenPosX, BYTE* pRawStaticScanBytenPosY)
{
	BYTE* pByteD1 = (BYTE*)malloc(sizeof(double) * m_nImageWidth * m_nImageHeight);

	memcpy(pByteD1, pRawByteD1, m_nImageWidth * m_nImageHeight * sizeof(double));

	m_pVecRawByteD1.push_back(pByteD1);

	BYTE* pByteD2 = (BYTE*)malloc(sizeof(double) * m_nImageWidth * m_nImageHeight);

	memcpy(pByteD2, pRawByteD2, m_nImageWidth * m_nImageHeight * sizeof(double));

	m_pVecRawByteD2.push_back(pByteD2);

	BYTE* pByteD3 = (BYTE*)malloc(sizeof(double) * m_nImageWidth * m_nImageHeight);

	memcpy(pByteD3, pRawByteD3, m_nImageWidth * m_nImageHeight * sizeof(double));

	m_pVecRawByteD3.push_back(pByteD3);

	BYTE* pByteT = (BYTE*)malloc(sizeof(double) * m_nImageWidth * m_nImageHeight);

	memcpy(pByteT, pRawByteT, m_nImageWidth * m_nImageHeight * sizeof(double));

	m_pVecRawByteT.push_back(pByteT);

	BYTE* pByteR = (BYTE*)malloc(sizeof(double) * m_nImageWidth * m_nImageHeight);

	memcpy(pByteR, pRawByteR, m_nImageWidth * m_nImageHeight * sizeof(double));

	m_pVecRawByteR.push_back(pByteR);

	BYTE* pScanActualBytePosX = (BYTE*)malloc(sizeof(double) * m_nImageWidth * m_nImageHeight);

	memcpy(pScanActualBytePosX, pRawActualScanBytenPosX, m_nImageWidth * m_nImageHeight * sizeof(double));

	m_pVecRawActualScanBytePosX.push_back(pScanActualBytePosX);

	BYTE* pScanActualBytePosY = (BYTE*)malloc(sizeof(double) * m_nImageWidth * m_nImageHeight);

	memcpy(pScanActualBytePosY, pRawActualScanBytenPosY, m_nImageWidth * m_nImageHeight * sizeof(double));

	m_pVecRawActualScanBytePosY.push_back(pScanActualBytePosY);

	BYTE* pScanStaticBytePosX = (BYTE*)malloc(sizeof(double) * m_nImageWidth * m_nImageHeight);

	memcpy(pScanStaticBytePosX, pRawStaticScanBytenPosX, m_nImageWidth * m_nImageHeight * sizeof(double));

	m_pVecRawStaticScanBytePosX.push_back(pScanStaticBytePosX);

	BYTE* pScanStaticBytePosY = (BYTE*)malloc(sizeof(double) * m_nImageWidth * m_nImageHeight);

	memcpy(pScanStaticBytePosY, pRawStaticScanBytenPosY, m_nImageWidth * m_nImageHeight * sizeof(double));

	m_pVecRawStaticScanBytePosY.push_back(pScanStaticBytePosY);
}

void CQueueThread::PushInterpolationImageCopy(BYTE* pInterpolationImageByte)
{
	BYTE* pByte = (BYTE*)malloc(sizeof(double) * (m_nImageWidth * IMAGE_MAGNIFICATION) * (m_nImageHeight * IMAGE_MAGNIFICATION));

	memcpy(pByte, pInterpolationImageByte, (m_nImageWidth * IMAGE_MAGNIFICATION) * (m_nImageHeight * IMAGE_MAGNIFICATION) * sizeof(double));

	m_pVecInterpolationByte.push_back(pByte);
}

void CQueueThread::PushAverageImageCopy(BYTE* pAverageImageByte)
{
	BYTE* pByte = (BYTE*)malloc(sizeof(double) * (m_nImageWidth * IMAGE_MAGNIFICATION) * (m_nImageHeight * IMAGE_MAGNIFICATION));

	memcpy(pByte, pAverageImageByte, (m_nImageWidth * IMAGE_MAGNIFICATION) * (m_nImageHeight * IMAGE_MAGNIFICATION) * sizeof(double));

	m_pVecAverageByte.push_back(pByte);
}

void CQueueThread::DeleteRawVector()
{
	for (int i = 0; i < m_pVecRawByteD1.size(); i++) { BYTE* pByte = m_pVecRawByteD1.at(i); free(pByte); pByte = nullptr; }
	m_pVecRawByteD1.clear();
	vector<BYTE*>().swap(m_pVecRawByteD1);

	for (int i = 0; i < m_pVecRawByteD2.size(); i++) { BYTE* pByte = m_pVecRawByteD2.at(i); free(pByte); pByte = nullptr; }
	m_pVecRawByteD2.clear();
	vector<BYTE*>().swap(m_pVecRawByteD2);

	for (int i = 0; i < m_pVecRawByteD3.size(); i++) { BYTE* pByte = m_pVecRawByteD3.at(i); free(pByte); pByte = nullptr; }
	m_pVecRawByteD3.clear();
	vector<BYTE*>().swap(m_pVecRawByteD3);

	for (int i = 0; i < m_pVecRawByteT.size(); i++) { BYTE* pByte = m_pVecRawByteT.at(i); free(pByte); pByte = nullptr; }
	m_pVecRawByteT.clear();
	vector<BYTE*>().swap(m_pVecRawByteT);

	for (int i = 0; i < m_pVecRawByteR.size(); i++) { BYTE* pByte = m_pVecRawByteR.at(i); free(pByte); pByte = nullptr; }
	m_pVecRawByteR.clear();
	vector<BYTE*>().swap(m_pVecRawByteR);

	for (int i = 0; i < m_pVecRawActualScanBytePosX.size(); i++) { BYTE* pByte = m_pVecRawActualScanBytePosX.at(i); free(pByte); pByte = nullptr; }
	m_pVecRawActualScanBytePosX.clear();
	vector<BYTE*>().swap(m_pVecRawActualScanBytePosX);

	for (int i = 0; i < m_pVecRawActualScanBytePosY.size(); i++) { BYTE* pByte = m_pVecRawActualScanBytePosY.at(i); free(pByte); pByte = nullptr; }
	m_pVecRawActualScanBytePosY.clear();
	vector<BYTE*>().swap(m_pVecRawActualScanBytePosY);

	for (int i = 0; i < m_pVecRawStaticScanBytePosX.size(); i++) { BYTE* pByte = m_pVecRawStaticScanBytePosX.at(i); free(pByte); pByte = nullptr; }
	m_pVecRawStaticScanBytePosX.clear();
	vector<BYTE*>().swap(m_pVecRawStaticScanBytePosX);

	for (int i = 0; i < m_pVecRawStaticScanBytePosY.size(); i++) { BYTE* pByte = m_pVecRawStaticScanBytePosY.at(i); free(pByte); pByte = nullptr; }
	m_pVecRawStaticScanBytePosY.clear();
	vector<BYTE*>().swap(m_pVecRawStaticScanBytePosY);
}

void CQueueThread::DeleteInterpolationVector()
{
	for (int i = 0; i < m_pVecInterpolationByte.size(); i++) { BYTE* pByte = m_pVecInterpolationByte.at(i); free(pByte); pByte = nullptr; }
	m_pVecInterpolationByte.clear();
	vector<BYTE*>().swap(m_pVecInterpolationByte);
}

void CQueueThread::DeleteAverageVector()
{
	for (int i = 0; i < m_pVecAverageByte.size(); i++) { BYTE* pByte = m_pVecAverageByte.at(i); free(pByte); pByte = nullptr; }
	m_pVecAverageByte.clear();
	vector<BYTE*>().swap(m_pVecAverageByte);
}

void CQueueThread::DeleteMainThreadVector()
{
	for (int i = 0; i < m_pVecMainThread.size(); i++) { CQueueMainThread* pMainThread = m_pVecMainThread.at(i); delete pMainThread; pMainThread = nullptr; }
	m_pVecMainThread.clear();
	vector<CQueueMainThread*>().swap(m_pVecMainThread);
}

void CQueueThread::QueueMainThreadMonitoring()
{
	MaskImageReconstructorView* pView = (MaskImageReconstructorView*)m_pView;

	CString strTemp;
	int		nCompleteCount		= 0;
	int		nInterpolationCount	= m_nInterpolationCount;
	BOOL*	bComplete			= (BOOL*)malloc(sizeof(BOOL) * nInterpolationCount);

	for (int i = 0; i < nInterpolationCount; i++) { bComplete[i] = FALSE; }

	while (GetThreadStatus())
	{
		for (int i = 0; i < m_pVecMainThread.size(); i++)
		{
			if (bComplete[i] == TRUE) { continue; }

			CQueueMainThread* pMainThread = m_pVecMainThread.at(i);

			if (!pMainThread->GetThreadStatus() && pMainThread->m_bComplete)
			{
				pView->m_pLogViewer->AddLog(
					static_cast<int>(LOG_VIEWER::CH_1),
					LOG_SEQUENCE,
					pView->m_fnFormattedStringTimeLog(_T("QueueMainThread[%d][%d] Interpolation Finish."), m_nQueueIndex, i));

				bComplete[i] = TRUE;

				nCompleteCount++;
			}
			if (nCompleteCount == nInterpolationCount && m_pVecMainThread.size() == nInterpolationCount) { break; }
		}
		if (nCompleteCount == nInterpolationCount && m_pVecMainThread.size() == nInterpolationCount) { break; }
		Sleep(1);
	}

	free(bComplete);
}

void CQueueThread::SetImageWH(int nWidth, int nHeight) { m_nImageWidth = nWidth; m_nImageHeight = nHeight; }
void CQueueThread::SetImageScanFOV(int nImageFOV_X, int nImageFOV_Y) { m_nImageFOV_X = nImageFOV_X; m_nImageFOV_Y = nImageFOV_Y; }
void CQueueThread::SetImageScanGrid(int nScanGridX, int nScanGridY) { m_nScanGridX = nScanGridX; m_nScanGridY = nScanGridY; }
void CQueueThread::SetImageInterpolationCount(int nInterpolationCount) { m_nInterpolationCount = nInterpolationCount; }